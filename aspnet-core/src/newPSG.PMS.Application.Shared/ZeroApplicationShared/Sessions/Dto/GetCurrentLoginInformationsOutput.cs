﻿using newPSG.PMS.UiCustomization.Dto;

namespace newPSG.PMS.Sessions.Dto
{
    public class GetCurrentLoginInformationsOutput
    {
        public UserLoginInfoDto User { get; set; }

        public TenantLoginInfoDto Tenant { get; set; }

        public ApplicationInfoDto Application { get; set; }

        public UiCustomizationSettingsDto Theme { get; set; }

      
        public CanBoInfo CanBo { set; get; }
    }
    public class CanBoInfo
    {
        public long CanBoId { set; get; }
        public long ChucVuChinhId { set; get; }
        public long DonViId { set; get; }
        public string MacDinhTinhId { set; get; }
        public string MacDinhHuyenId { set; get; }
    }
}