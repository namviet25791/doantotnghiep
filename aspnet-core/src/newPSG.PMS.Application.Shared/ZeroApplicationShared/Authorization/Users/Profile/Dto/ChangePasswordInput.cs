using System.ComponentModel.DataAnnotations;
using Abp.Auditing;

namespace newPSG.PMS.Authorization.Users.Profile.Dto
{
    public class ChangePasswordInput
    {
        public long UserId { get; set; }

        [Required]
        [DisableAuditing]
        public string CurrentPassword { get; set; }

        [Required]
        [DisableAuditing]
        public string NewPassword { get; set; }
    }
}