namespace newPSG.PMS.Configuration.Tenants.Dto
{
    public class TenantOtherSettingsEditDto
    {
        public bool IsQuickThemeSelectEnabled { get; set; }
    }

    public class GeneralSettingsDto
    {
        public string ThoiGianKetThucDanhGia { get; set; }
        public string TinhMacDinh { get; set; }
        public string HuyenMacDinh { get; set; }
        public bool SendMailNotificationEnabled { get; set; }
        public bool NhacNhoGuiPhieuDanhGiaEnabled { get; set; }
        public int GuiNhacNhoTruocNgayBatDau { get; set; }
        public int GuiNhacNhoTruocNgayKetThuc { get; set; }
    }
}