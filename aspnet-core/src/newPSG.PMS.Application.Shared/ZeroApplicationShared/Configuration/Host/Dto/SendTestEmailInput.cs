﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;

namespace newPSG.PMS.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }       

        public List<KeyValuePair<string, string>> BodyTemplate { get; set; }
    }

    public class SendEmailInput
    {
        [Required]
        [MaxLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public List<KeyValuePair<string, string>> BodyTemplate { get; set; }
    }
}