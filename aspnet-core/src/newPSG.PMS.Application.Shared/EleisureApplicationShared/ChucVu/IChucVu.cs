﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IChucVu: IApplicationService
    {
        PagedResultDto<ChucVuDto> GetByFilter(ChucVuInput input); 
		Task<ErroMsg> Upsert(ChucVuDto input);
        void Delete(long id);
        List<DanhSachCanBoChucVu> GetChucVuTheoCanBo(long canBoId);
        List<CanBoResponse> GetThanhVienNhomCanBoTheoChucVuId(CanBoInput input);
        bool ThemThanhVienNhomChucVu(List<ObjectTemp> lstData, long chucVuId);
        bool XoaThanhVienNhomChucVu(long id);
        PagedResultDto<ChucVuDto> GetChucVuBaoCaoDoiTuongChiTiet(ChucVuBaoCaoDoiTuongChiTieInput input);
    }
}
