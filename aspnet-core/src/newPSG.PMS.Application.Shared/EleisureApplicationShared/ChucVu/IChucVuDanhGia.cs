﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IChucVuDanhGia: IApplicationService
    {
        PagedResultDto<ChucVuDanhGiaDto> GetByFilter(ChucVuDanhGiaInput input);
		List<ChucVuDanhGiaDto> GetByChucVuDuocDanhGia(long chucVuId);
		Task<ErroMsg> Upsert(List<ChucVuDanhGiaDto> input);
        void Delete(long id);
        List<CauHinhChucVuCanBoDanhGiaDto> GetChucVuDanhGiaTheoChucVuCanBoId(long canBoId, long chucVuId);
    }
}
