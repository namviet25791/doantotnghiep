﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IBaoCaoChucVu: IApplicationService
    {
        PagedResultDto<BaoCaoChucVuDto> GetByFilter(BaoCaoChucVuInput input);
        Task<long> Upsert(BaoCaoChucVuDto input);
        void Delete(long id);
    }
}
