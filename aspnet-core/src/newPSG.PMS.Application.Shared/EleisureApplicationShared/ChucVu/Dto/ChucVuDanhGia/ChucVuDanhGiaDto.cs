﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ChucVuDanhGiaDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public int SoThuTu { set; get; }
        public long ChucVuDuocDanhGiaId { set; get; }
        public bool NguoiCham { set; get; }
        public long ChucVuThucHienDanhGiaId { set; get; }
        public long LoaiChucVu { set; get; }
    }

    public class CauHinhChucVuCanBoDanhGiaDto : CanBoChucVuDanhGiaDto
    {
        public List<CanBoChucVuTemp> ListCanBoChucVu { get; set; }
    }

    public class CanBoChucVuTemp
    {
        public long Id { get; set; }
        public long CanBoId { get; set; }
        public string TenCanBo { get; set; }
    }
}
