﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class BaoCaoDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string TenBaoCao { set; get; }
        public string Mota { set; get; }
        public string DuongDanBieuMau { set; get; }
        public string MaBaoCao { set; get; }
        public string Source { set; get; }
        public string TenChucVu { set; get; }
        public long ChucVuId { set; get; }
    }
    public class BaoCaoChucVuCauHinhDto
    {
        public long Id { get; set; }
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long BaoCaoNhomDoiTuongId { set; get; }
        public string TenChucVu { set; get; }
        public string TenNhomDoiTuong { set; get; }
    }
    public class BaoCaoNhomDoiTuongDto : EntityDto<long>
    {
        public long Id { get; set; }
        public virtual int? TenantId { get; set; }
        public string TenNhomDoiTuong { set; get; }
        public string TenHienThiTrenBaoCao { set; get; }
        public int LoaiNhomDoiTuong { set; get; }
    }
   
    public class BaoCaoNhomDoiTuongChiTietDto
    {
        public long Id { get; set; }
        public virtual int? TenantId { get; set; }
        public long BaoCaoNhomDoiTuongId { set; get; }
        public long ChucVuId { set; get; }
        public string TenChucVu { get; set; }
    }
    public class BaoCaoNhomDoiTuongChiTietInsert
    {
        public List<long> LstChucVu { get; set; }
        public long BaoCaoNhomDoiTuongId { set; get; }
    }
    public class BaoCaoChucVuCauHinhInsert
    {
        public long BaoCaoNhomDoiTuongId { get; set; }
        public List<long> LstChucVuId { set; get; }
    }
}
