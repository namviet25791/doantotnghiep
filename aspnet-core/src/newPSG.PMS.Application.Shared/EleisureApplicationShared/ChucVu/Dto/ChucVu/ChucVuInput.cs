﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ChucVuInput : PagedAndSortedInputDto
    { 
      
        public string Filter { get; set; }
        public bool? LaLanhDao { set; get; }
        public int? Khoi { set; get; }
        public int? LoaiChucVu { set; get; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
    public class ChucVuBaoCaoDoiTuongChiTieInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public long BaoCaoNhomDoiTuongId { set; get; }       
    }
    

}
