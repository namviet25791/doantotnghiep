﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ChucVuDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public bool LaLanhDao { set; get; }
        public int Khoi { set; get; }
        public int LoaiChucVu { set; get; }
        public int CapLanhDao { set; get; }
        public string TenHienThiTrenBieuMau { set; get; }
    }

    public class ThanhVienNhomChucVuDto
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long CanBoId { set; get; }
        public long ChucVuCanBoId { set; get; }
    }

    public class ThanhVienNhomChucVuViewDto
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long CanBoId { set; get; }
        public long ChucVuCanBoId { set; get; }
        public string TenCanBo { set; get; }
        public string TenChucVu { set; get; }
        public string IsLoaiChucVu { set; get; }
        public string TenDonVi { set; get; }
    }
}
