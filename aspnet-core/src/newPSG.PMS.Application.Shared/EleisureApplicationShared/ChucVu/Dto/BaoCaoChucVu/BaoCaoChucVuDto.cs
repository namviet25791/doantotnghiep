﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class BaoCaoChucVuDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long BaoCaoId { set; get; }
        public long ChucVuId { set; get; }
    }

    public class BaoCaoChucVuInsert
    {
        public List<long> LstChucVuId { get; set; }
        public long BaoCaoId { set; get; }
    }
}
