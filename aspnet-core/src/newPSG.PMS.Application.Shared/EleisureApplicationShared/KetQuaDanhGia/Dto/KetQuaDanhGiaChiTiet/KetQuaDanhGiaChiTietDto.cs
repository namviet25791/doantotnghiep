﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaChiTietDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; } 
        public long PhieuDanhGiaId { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long TieuChiId { set; get; }
        public float Diem { set; get; }
        public string NhanXet { set; get; }  
        public long LichSuKetQuaDanhGiaChiTiet { set; get; }
    }
}
