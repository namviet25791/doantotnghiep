﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaResponse : PhieuDanhGiaCanBoDto
    {
        public List<LichSuKetQuaDanhGiaChiTietDetail> LichSuKetQuas { set; get; }
    }

    public class LichSuKetQuaDanhGiaChiTietDetail : LichSuKetQuaDanhGiaChiTietDto
    {
        public List<KetQuaDanhGiaChiTietDto> KetQuaChiTiets { set; get; }
        public float TongDiem { set; get; }
        public string XepLoai { set; get; }
        public string NhanXet { set; get; }

        public string TenCanBo { set; get; }
        public string TenChucVu { set; get; }
    }


    public class KetQuaTongHopDto
    {
        public long CanBoDanhGiaId { set; get; }
        public string TenCanBo { set; get; }
        public string TenChucVu { set; get; }
        public long? ChucVuCanBoDanhGiaID { set; get; }
        public string NhanXet { set; get; }
        public decimal  TongDiem { set; get; }
        public long  ThangDiemChiTietId { set; get; }
        public int CapDanhGia { get; set; }
        public string TenPhieuDanhGia { get; set; }
    }

    public class ChiTietLuongDanhGiaDto
    {
        public long PhieuDanhGiaID { set; get; }
        public long? CanBoDanhGiaId { set; get; }
        public long ChucVuDanhGiaId { set; get; }
        public string TenCanBo { set; get; }
        public string TenChucVu { set; get; }
        public float? TongDiem { set; get; }
        public string XepLoai { set; get; }
        public long ThuTu { set; get; }
        public long TrangThai { set; get; }
    }

    public class CanBoDanhGia
    {
        public long ChucVuDanhGiaId { set; get; }
        public long? CanBoDanhGiaId { set; get; }

       public string ChucVuDanhGia { set; get; }
        public string CanBoDanhgia { set; get; }
        public int ThuTu { set; get; }
        public bool NguoiCham { set; get; }
    }
}
