﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaChiTietUpdate
    {   
        public int TrangThai { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public List<TieuChiITem> TieuChis { set; get; }
    } 
}
