﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaUpdate
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long CanBoDanhGiaId { get; set; }
        public long PhieuDanhGiaId { set; get; }  
        public int TrangThai { set; get; }
        public long ThangDiemChiTietId { set; get; }
        public List<TieuChiITem> TieuChis { set; get; }
    }
    public class TieuChiITem
    {

        public long TieuChiId { set; get; }
        public float Diem { set; get; }
        public string NhanXet { set; get; }
    }
}
