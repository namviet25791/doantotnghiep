﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaTongHop: PhieuDanhGiaCanBoDto
    {
        // Sum từ bảng KetQuaDanhGiaChiTiet
        public float TongDiem { set; get; }
        // DaHoanThanh
        public int TrangThai { set; get; }

        public string XepLoai { set; get; }
        public string XepLoaiCuoiCung { set; get; } 
    }
    public class KetQuaDanhGiaTongHopInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }

        public long CanBoId { set; get; }

        public int? Nam { set; get; } 
         
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }
}
