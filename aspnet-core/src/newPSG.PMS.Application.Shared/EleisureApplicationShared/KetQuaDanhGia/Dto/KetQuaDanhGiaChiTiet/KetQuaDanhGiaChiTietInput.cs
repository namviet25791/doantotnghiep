﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class KetQuaDanhGiaChiTietInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
           
        public long? TieuChiId { set; get; }   

        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
}
