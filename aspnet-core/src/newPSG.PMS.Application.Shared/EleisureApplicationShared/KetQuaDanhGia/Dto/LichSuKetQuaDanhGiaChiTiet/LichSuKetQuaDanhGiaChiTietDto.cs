﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class LichSuKetQuaDanhGiaChiTietDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long CanBoDanhGiaId { get; set; }
        public long CanBoId { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long LicSuPhieuDanhGiaChiTietTruocDoId { set; get; }
        public int TrangThai { set; get; }
        public long ThangDiemChiTietId { set; get; }
        public int CapDanhGia { set; get; }

        public DateTime NgayDanhGia { set; get; }
    }
}
