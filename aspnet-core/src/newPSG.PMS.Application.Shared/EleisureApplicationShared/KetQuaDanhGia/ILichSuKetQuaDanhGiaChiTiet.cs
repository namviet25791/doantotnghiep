﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ILichSuKetQuaDanhGiaChiTiet: IApplicationService
    {
        PagedResultDto<LichSuKetQuaDanhGiaChiTietDto> GetByFilter(LichSuKetQuaDanhGiaChiTietInput input);
        Task<long> Upsert(LichSuKetQuaDanhGiaChiTietDto input);
        void Delete(long id);
    }
}
