﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IKetQuaDanhGiaChiTiet: IApplicationService
    {
        PagedResultDto<KetQuaDanhGiaChiTietDto> GetByFilter(KetQuaDanhGiaChiTietInput input);
        Task<long> Upsert(KetQuaDanhGiaChiTietDto input);
        void Delete(long id);

        Task ThemDanhGia(KetQuaDanhGiaUpdate input);
        void UpdateDanhGia(KetQuaDanhGiaChiTietUpdate input);
        List<KetQuaTongHopDto> GetKetQuaChiTietAsync(long phieuDanhGiaCanBoId);
        PagedResultDto<KetQuaDanhGiaTongHop> GetKetQuaTongHop(KetQuaDanhGiaTongHopInput input);

        long ThemMoiLichSuVaKetQuaPhieuDanhGia(long phieuDanhGiaCanBoId, long lichSuKetQuaDanhGiaChiTietId, int loaiChucVuDanhGia);
        long CapNhatLichSuVaKetQuaPhieuDanhGia(long phieuDanhGiaCanBoId, long lsHienTaiId,
            long lsKetQuaTongHopId, List<TieuChiItem> lstItem);

        List<ChiTietLuongDanhGiaDto> GetChiTietLuongDanhGia(long phieuDanhGiaId, long canBoId, long chucVuCanBoId);
    }
}
