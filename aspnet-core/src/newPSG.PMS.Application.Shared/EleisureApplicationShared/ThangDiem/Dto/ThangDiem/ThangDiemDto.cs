﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ThangDiemDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }
        public long PhieuDanhGiaId { set; get; }
    }

}
