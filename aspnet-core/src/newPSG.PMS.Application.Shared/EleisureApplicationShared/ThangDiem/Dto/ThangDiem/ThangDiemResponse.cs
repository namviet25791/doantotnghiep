﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ThangDiemResponse : ThangDiemDto
    {
        public int SoLuongThangDiemChiTiet { set; get; }

        public List<ThangDiemChiTietDto> ThangDiemChiTiet { set; get; }
    }
}
