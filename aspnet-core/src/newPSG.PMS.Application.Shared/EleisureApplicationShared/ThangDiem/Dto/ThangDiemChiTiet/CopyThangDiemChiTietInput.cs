﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CopyThangDiemChiTietInput
    {
        public long ThangDiemTruocDoId { set; get; }
        public long ThangDiemId { set; get; }
    }
}
