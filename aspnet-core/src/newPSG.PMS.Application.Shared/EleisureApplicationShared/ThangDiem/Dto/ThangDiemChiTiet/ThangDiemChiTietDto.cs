﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ThangDiemChiTietDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public long ThangDiemId { set; get; }
        public float DiemTran { set; get; }
        public float DiemSan { set; get; }
        public int LoaiThangDiemChiTiet { get; set; }
    }
}
