﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ThangDiemChiTietInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public long? ThangDiemId { set; get; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
}
