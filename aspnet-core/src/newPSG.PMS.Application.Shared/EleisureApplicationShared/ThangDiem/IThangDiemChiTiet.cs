﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IThangDiemChiTiet: IApplicationService
    {
        PagedResultDto<ThangDiemChiTietDto> GetByFilter(ThangDiemChiTietInput input);
        Task<bool> Upsert(List<ThangDiemChiTietDto> input);
        void Delete(long id);

        void CopyByThangDiem(CopyThangDiemChiTietInput input);
    }
}
