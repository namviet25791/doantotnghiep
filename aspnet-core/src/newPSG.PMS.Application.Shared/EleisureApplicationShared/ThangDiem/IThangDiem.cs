﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static newPSG.PMS.CommonENum;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IThangDiem: IApplicationService
    {
        PagedResultDto<ThangDiemResponse> GetByFilter(ThangDiemInput input);
        Task<long> Upsert(ThangDiemDto input);
        void Delete(long id);

        void CopyThangDiem(CopyThangDiemInput input);
        List<ThangDiemDto> GetAllThangDiemCombo();
        List<EnumObj> getLoaiThangDiemChiTiet();
    }
}
