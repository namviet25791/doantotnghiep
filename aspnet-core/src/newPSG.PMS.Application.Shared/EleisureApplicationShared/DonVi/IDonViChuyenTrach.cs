﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IDonViChuyenTrach: IApplicationService
    {
        PagedResultDto<DonViChuyenTrachResponse> GetByFilter(DonViChuyenTrachInput input);
        bool InsertDonViChuyenTrach(DonViChuyenTrachUpdate input);
        bool DeleteDonViChuyenTrach(DonViChuyenTrachUpdate input);
        void Delete(long id);
        List<long> LoadDonViChuyenTrachTheoCannBoChucVu(long canBoId, long chucVuId);
        List<DonViChuyenTrachResponse> LoadDonViChuyenTrach(long canBoId, long chucVuId);
    }
}
    