﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ICapDonVi: IApplicationService
    {
        PagedResultDto<CapDonViDto> GetByFilter(CapDonViInput input);
        Task<bool> Upsert(CapDonViDto input);
        void Delete(long id);
    }
}
    