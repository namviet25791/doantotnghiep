﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IKhoi: IApplicationService
    {
        PagedResultDto<KhoiDto> GetByFilter(KhoiInput input);
        Task<bool> Upsert(KhoiDto input);
        void Delete(long id);
    }
}
    