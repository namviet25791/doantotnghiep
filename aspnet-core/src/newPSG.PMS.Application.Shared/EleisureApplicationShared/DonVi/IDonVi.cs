﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IDonVi: IApplicationService
    {
        PagedResultDto<DonViResponse> GetByFilter(DonViInput input);
        PagedResultDto<DonViResponse> GetDonViChoCanBoChucVu(DonViInput input);
        PagedResultDto<DonViResponse> GetDonViBaoCaoDoiTuongChiTiet(DonViBaoCaoDoiTuongChiTieInput input);
        Task<long> Upsert(DonViDto input);
        void Delete(long id);
    }
}
    