﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class LoaiDonViDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
    }
}
