﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public long? DonViQuanLyId { set; get; }
        public long CapDonViId { set; get; }
        public long KhoiId { set; get; }
        public long KhoiTruongHocId { set; get; }

        public string TinhId { set; get; }
        public string HuyenId { set; get; }
        public string XaId { set; get; }
    }
}
