﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; } 
        public long? DonViQuanLyId { set; get; }
        public int? CapDonVi { set; get; }
        public int? Khoi { set; get; }   
        public string TinhId { set; get; }
        public string HuyenId { set; get; }
        public string XaId { set; get; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
        //
        public int CanBoId { set; get; }
        public int ChucVuId { set; get; }
    }
    public class DonViBaoCaoDoiTuongChiTieInput : PagedAndSortedInputDto
    {
        public int? CapDonVi { set; get; }
        public int? Khoi { set; get; }
        public string Filter { get; set; }
        public long BaoCaoNhomDoiTuongId { set; get; }
    }
}
