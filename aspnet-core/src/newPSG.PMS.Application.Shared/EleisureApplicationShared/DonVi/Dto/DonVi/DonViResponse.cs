﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViResponse : DonViDto
    {
        
        //public string TenLoaiDonVi { set; get; }
        public string TenDonViQuanLy { set; get; }
        public string TenCapDonVi { set; get; }
        public string TenKhoi { set; get; } 
    }
}
