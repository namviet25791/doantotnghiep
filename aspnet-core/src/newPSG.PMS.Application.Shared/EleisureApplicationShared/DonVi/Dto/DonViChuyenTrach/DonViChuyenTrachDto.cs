﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViChuyenTrachDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long DonViId { set; get; }
        public long ChucVuId { set; get; }
    }
    public class DonViChuyenTrachUpdate
    {
        public long Id { set; get; }
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public List<long> LstDonVi { set; get; }
       
    }
}
