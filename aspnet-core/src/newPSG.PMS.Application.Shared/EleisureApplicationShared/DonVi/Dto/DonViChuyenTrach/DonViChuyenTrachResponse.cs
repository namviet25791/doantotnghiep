﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViChuyenTrachResponse : DonViChuyenTrachDto
    {
        public string TenCanBo { set; get; }
        public string TenDonVi { set; get; }
    }
}
