﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DonViChuyenTrachInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public long? CanBoId { set; get; }
        public long? DonViId { set; get; }
        public long? ChucVuId { set; get; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
}
