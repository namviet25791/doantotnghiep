﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ILoaiDonVi: IApplicationService
    {
        PagedResultDto<LoaiDonViDto> GetByFilter(LoaiDonViInput input);
        Task<bool> Upsert(LoaiDonViDto input);
        void Delete(long id);
    }
}
    