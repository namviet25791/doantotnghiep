﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class XaDto : EntityDto<string>
    {
        [StringLength(256)]
        public string Ten { get; set; }
        [StringLength(256)]
        public string Cap { get; set; }
        [StringLength(20)]
        public string HuyenId { get; set; }
    }
    public class XaInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public string HuyenId { get; set; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }
}
