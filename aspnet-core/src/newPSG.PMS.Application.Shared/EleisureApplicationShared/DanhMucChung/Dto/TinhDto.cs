﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class TinhDto : EntityDto<string>
    {
        [StringLength(256)]
        public string Ten { get; set; }
        [StringLength(256)]
        public string Cap { get; set; }
    }

    public class TinhInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; } 
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }
}
