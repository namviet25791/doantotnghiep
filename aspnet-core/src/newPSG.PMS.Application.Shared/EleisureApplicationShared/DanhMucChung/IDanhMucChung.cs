﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IDanhMucChung: IApplicationService
    {
        PagedResultDto<TinhDto> GetTinhByFilter(TinhInput input);
        PagedResultDto<HuyenDto> GetHuyenByFilter(HuyenInput input);
        PagedResultDto<XaDto> GetXaByFilter(XaInput input);
    }
}
