﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ICanBo: IApplicationService
    {
        PagedResultDto<CanBoResponse> GetByFilter(CanBoInput input);
        Task<CanBoDto> Upsert(CanBoDto input);
        Task<int> Delete(long id, long userId);
        Task<CanBoDto> GetChiTietCanBo(long CanBoId);
        PagedResultDto<CanBoResponse> TimKiemThanhVienNhomChucVu(CanBoInput input);
        List<CanBoResponse> GetDsNhanVienTheoDonViId(long donViId);
    }
}
