﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ICanBoChucVuDanhGia: IApplicationService
    {
        PagedResultDto<CanBoChucVuDanhGiaDto> GetByFilter(CanBoChucVuDanhGiaInput input);
        Task<ErroMsg> Upsert(List<CanBoChucVuDanhGiaDto> input);
        void Delete(long id);
        List<CauHinhChucVuCanBoDanhGiaDto> GetCanBoChucVuDanhGiaByCanBoId(long canBoId, long chucVuId);
        List<CanBoChucVuTemp> GetCanBoTheoChucVu(long chucVuDanhGiaId, long donViId);
    }
}
