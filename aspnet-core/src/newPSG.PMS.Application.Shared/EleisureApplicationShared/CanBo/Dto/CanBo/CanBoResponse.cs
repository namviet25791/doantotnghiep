﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoResponse : CanBoDto
    {  
        public string TenDonVi { set; get; }

        public Guid? Avatar { set; get; }
        public long ChucVuId { set; get; }
        public string TenChucVu { set; get; }
        
        public long? PhieuDanhGiaId { set; get; }


        // Sum từ bảng KetQuaDanhGiaChiTiet
        public float TongDiem { set; get; }
        // DaHoanThanh
        public int TrangThai { set; get; }

        public string XepLoai { set; get; }
        public string XepLoaiCuoiCung { set; get; }
        public long? DonViQuanLyId { set; get; }
        public string TenDonViQuanLy { set; get; }
        public string TenLoaiDonVi { set; get; }
        public long LoaiDonViId { set; get; }
        public long ChucVuChinh { set; get; } 
    }
    public class GetNhanVienCanDanhGiaInput : PagedAndSortedInputDto
    { 
        public string Filter { get; set; }
        public long CanBoId { set; get; } 
        // Them input
        public DateTime Date { set; get; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }
}
