﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Authorization.Users.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string PhoneNumber { set; get; }
        public string EmailAddress { set; get; }
        public string Ten { set; get; }
        public string Ho { set; get; } 
        public long DonViId { set; get; }
        public long UserId { set; get; }
        public string Address { set; get; }
        public string TinhId { set; get; }
        public string HuyenId { set; get; }
        public string XaId { set; get; }
        public int LoaiHopDong { set; get; }
        public string[] AssignedRoleNames { get; set; }
        public Guid? ProfilePictureId { get; set; }
        public UserRoleDto[] Roles { get; set; }
        public string erroMsg { get; set; }

        //
        public bool SetRandomPassword { get; set; }
        public string Password { get; set; }
    }

    public class ErroMsg
    {
        public bool Result { get; set; }
        public string Msg { get; set; }
        public bool IsNguoiCham { get; set; }
        public long Value { get; set; }
    }
}
