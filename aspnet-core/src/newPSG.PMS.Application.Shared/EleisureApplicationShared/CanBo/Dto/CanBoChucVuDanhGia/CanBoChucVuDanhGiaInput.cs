﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoChucVuDanhGiaInput : PagedAndSortedInputDto
    {
       
        public long? CanBoId { set; get; }
        public long? ChucVuCanBoId { set; get; } 
        
    }
}
