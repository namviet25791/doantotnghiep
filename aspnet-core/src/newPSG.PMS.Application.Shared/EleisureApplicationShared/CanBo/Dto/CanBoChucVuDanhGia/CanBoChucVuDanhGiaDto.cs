﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoChucVuDanhGiaDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuCanBoId { set; get; }
        public long? CanBoDanhGiaId { set; get; }
        public long ChucVuDanhGiaId { set; get; }
        public int ThuTu { set; get; }
        public bool NguoiCham { set; get; }
        public int LoaiChucVu { set; get; }
    }
}
