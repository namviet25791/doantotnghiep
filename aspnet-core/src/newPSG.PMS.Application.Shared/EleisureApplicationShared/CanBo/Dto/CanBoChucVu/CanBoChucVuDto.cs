﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoChucVuDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public long ChucVuChinh { set; get; }
        public string TenChucVu { set; get; }
    }

    public class DanhSachCanBoChucVu : CanBoChucVuDto
    {
        public bool IsCauHinh { set; get; }
    }
}
