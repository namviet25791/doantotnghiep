﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CanBoChucVuInput : PagedAndSortedInputDto
    {
       
        public long? CanBoId { set; get; }
        public long? ChucVuId { set; get; }
        public long? ChucVuChinh { set; get; }
        
    }
}
