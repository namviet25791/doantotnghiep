﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ICanBoChucVu: IApplicationService
    {
        PagedResultDto<CanBoChucVuDto> GetByFilter(CanBoChucVuInput input);
        List<CanBoChucVuDto> GetByFilterWithoutPaging(CanBoChucVuInput input);
        bool Upsert(List<CanBoChucVuDto> input);
        void Delete(long id, long canBoId, long chucVuId);
    }
}
