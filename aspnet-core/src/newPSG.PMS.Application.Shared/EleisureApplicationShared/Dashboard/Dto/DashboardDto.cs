﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static newPSG.PMS.AppConsts;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class DashboardInput
    {
        public decimal ChucVuId { get; set; }
        public decimal CanBoId { get; set; }
        public string Date { get; set; }
        public int TrangThai { get; set; }
    }
    public class ThongKeCanBoInput : PagedAndSortedInputDto
    {
        public decimal ChucVuId { get; set; }
        public decimal CanBoId { get; set; }
        public string Date { get; set; }
        public int TrangThai { get; set; }
    }
    
    public class DiemXepLoaiTheoThangDto
    {
        public int Thang { get; set; }
        public float? Diem { get; set; }
        public string   DanhGia { get; set; }
        public long PhieuDanhGiaId { get; set; }
        public LoaiThangDiemChiTiet LoaiThangDiemChiTietEnum { get; set; }
        
        //public float DiemView
        //{
        //    set
        //    {
        //        float value = (Diem == null) ? 0 : Diem;
        //        return value;
        //    }
        //}
    }
    public class PhanBoDiemTheoNhomTieuChiDto
    {
        public string NhomTieuChi { get; set; }
        public float Diem { get; set; }
    }    
    public class TyLeHoanThanhNhomTieuChiDto
    {
        public string NhomTieuChi { set; get; }
        public float Diem { set; get; }
        public float DiemToiDa { set; get; }
        public float DiemPhanTram { set; get; }
        public float DiemToiDaPhanTram { set; get; }
    }

    public class TopDanhSachTieuChiDto
    {
        public long TieuChiId { set; get; }
        public string TenTieuChi { set; get; }
        public float DiemChenhLech { set; get; }
        public float SoLanMatDiem { set; get; }
    }


    public class KetQuaDanhGiaCacThangDto
    {
        public int Thang { set; get; }
        public int HoanThanhXuatSac { set; get; }
        public int HoanThanhTot { set; get; }
        public int HoanThanh { set; get; }
        public int KhongHoanThanh { set; get; }
    }

    public class TyLeThangDiemDto
    {
        public string ThangDiem { get; set; }
        public int SoLuong { get; set; }
    }
    public class TyLeHoanThanhPhieuDto
    {
        public string Ten { get; set; }
        public int TyLe { get; set; }
    }
    public class DsDiemTrungBinhDto
    {
        public string TenChucVu { get; set; }
        public string TenCanBo { get; set; }
        public float DiemTrungBinh { get; set; }
    }

    public class ObjectTemp
    {
        public long ChucVuDanhGiaId { get; set; }
        public long CanBoId { get; set; }
        public long ChucVuCanBoId { get; set; }
        public long UserId { get; set; }
        public long DonViId { get; set; }
        public int DaHoanThanh { get; set; }
    }
    public class ThongKeCanBoDto
    {
        public int Stt { get; set; }
        public string TenCanBo { get; set; }
        public string TenChucVu { get; set; }
        public string PhongBan { get; set; }
        public string SoDienThoai { get; set; }
        public int TrangThai { get; set; }
        public string TenTrangThai { get; set; }
    }
    

}