﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IDashboard : IApplicationService
    {
        List<DiemXepLoaiTheoThangDto> Dashboard_DiemXepLoaiTheoThang(DashboardInput input);
        List<PhanBoDiemTheoNhomTieuChiDto> Dashboard_PhanBoDiemTheoNhomTieuChi(DashboardInput input);
        List<TyLeHoanThanhNhomTieuChiDto> Dashboard_TyLeHoanThanhTheoNhomTieuChi(DashboardInput input);
        List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiTienBo(DashboardInput input);
        List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiDiXuong(DashboardInput input);
        List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiHayMatDiem(DashboardInput input);

        //

        List<KetQuaDanhGiaCacThangDto> Dashboard_KetQuaDanhGiaCacThang(DashboardInput input);
        List<TyLeThangDiemDto> Dashboard_TyLeThangDiem(DashboardInput input);
        List<DsDiemTrungBinhDto> Dashboard_DsCanBoDiemTrungBinhCaoNhatTrongNam(DashboardInput input);
        List<DsDiemTrungBinhDto> Dashboard_DsCanBoDiemTrungBinhThapNhatTrongNam(DashboardInput input);

        // 

        PagedResultDto<ThongKeCanBoDto> ThongKeTinhTrangPhieuDanhGiaCanBo(ThongKeCanBoInput input);
        Task<FileDto> ExportCanBoToExcel(ThongKeCanBoInput input);
    }
}
