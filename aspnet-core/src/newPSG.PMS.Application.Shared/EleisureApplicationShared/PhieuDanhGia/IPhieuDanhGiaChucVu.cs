﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IPhieuDanhGiaChucVu: IApplicationService
    {
        PagedResultDto<PhieuDanhGiaChucVuDto> GetByFilter(PhieuDanhGiaChucVuInput input);
        //System.Threading.Tasks.Task<List<PhieuDanhGiaChucVuResponse>> GetByCanBo(long canBoId);
        Task<bool> Upsert(PhieuDanhGiaChucVuDto input);
        void Delete(long id);
    }
}
