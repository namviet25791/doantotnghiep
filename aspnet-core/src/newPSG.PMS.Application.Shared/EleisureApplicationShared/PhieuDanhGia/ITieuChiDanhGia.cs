﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ITieuChiDanhGia : IApplicationService
    {
        PagedResultDto<TieuChiDanhGiaResponse> GetByFilter(TieuChiDanhGiaInput input);
        Task<bool> Upsert(TieuChiDanhGiaDto input);
        void Delete(long id);
        void CopyByPhieuDanhGia(CopyTieuChiDanhGiaInput input);
    }
}
