﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IPhieuDanhGiaCanBo : IApplicationService
    {
        #region Trang viet ham moi
        PhieuDanhGiaTongHop GetChiTietPhieuDanhGia(long? canBoId, long? chucVuId, long? phieuDanhGiaCanBoId);
        PhieuDanhGiaTongHop GetChiTietPhieuDanhGiaTheoPhieuDanhGiaCanBoId(long? phieuDanhGiaCanBoId, long? canBoId, long? chucVuId, bool isExportFile);
        PagedResultDto<DanhSachPhieuDanhGiaCanBoDto> GetAllPhieuDanhGiaCanBo(PhieuDanhGiaCanBoInput input);
        PagedResultDto<DanhSachPhieuDanhGiaCanBoDto> GetAllPhieuDanhGiaNhanVienTheoChucVuLanhDao(PhieuDanhGiaCanBoInput input);
        PagedResultDto<DanhSachCanBoChuaGuiDanhGiaDto> GetAllNhanVienChuaDanhGia(PhieuDanhGiaCanBoInput input);
        bool TaoMoiHoacCapNhatPhieuDanhGiaCanBo(PhieuDanhGiaCanBoUpdateDto input);    
        ThongTinDuyetPhieuDanhGiaCanBoDto GetThongTinDuyetPhieuTheoCanBoDanhGia(PhieuDuyetInput input);
      
        #endregion
        bool KiemTraThoiGianKetThucDanhGia(DateTime dThoiGian);

        FileDto XuatWord(long phieuDanhGiaCanBoId);
        FileDto XuatPDF(long phieuDanhGiaCanBoId);

        ErroMsg KiemTraThongTinPhieuTuUrl(long phieuDanhGiaCanBoId, long chucVuDanhGiaId);

        PhieuDanhGiaTongHop GetKetQuaNhomDanhGia(long phieuDanhGiaCanBoId, long phieuDanhGiaId, long lichSuDanhGiaId);
        ChucVuDto GetNhomChucVu(long canBoId, long chucVuCanBoId, int thuTu);
        List<DanhSachDuyetPhieu> GetDanhSachPhieuCanDuyetPCT(PhieuDanhGiaCanBoInput input);
        bool TaoMoiVaCapNhatKetQuaNguoiDuyet(List<PhieuDanhGiaCanBoUpdateDto> lstData);
        bool TaoMoiVaCapNhatKetQuaNguoiDuyetProcess(PhieuDanhGiaCanBoUpdateDto input);

        void NhacNhoGuiPhieuDanhGiaCaNhan();
    }
}
