﻿using System;

namespace newPSG.PMS.EleisureApplicationShared.PhieuDanhGia.Dto.PhieuDanhGiaCanBo.Export
{
    public class KetQuaNhanXetCuoiCung
    {
        public long ChucVuLanhDaoId { get; set; }
        public string TenChucVuLanhDao { get; set; }
        public string TenLanhDao { get; set; }
        public string NhanXet { get; set; }
        public long ThangDiemChiTietId { get; set; }
        public int LoaiThangDiemChiTiet { get; set; }
        public DateTime NgayDanhGia { get; set; }
    }
}
