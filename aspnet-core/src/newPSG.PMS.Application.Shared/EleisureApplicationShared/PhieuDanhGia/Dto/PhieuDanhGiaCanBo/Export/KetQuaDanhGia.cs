﻿namespace newPSG.PMS.EleisureApplicationShared.PhieuDanhGia.Dto.PhieuDanhGiaCanBo.Export
{
    public class KetQuaDanhGia
    {
        public long KetQuaDanhGiaChiTietId { get; set; }
        public long TieuChiId { get; set; }
        public long? TieuChiChaId { get; set; }
        public int NhomTieuChi { get; set; }
        public int TieuChiThuTu { get; set; }
        public string TieuChiKyHieu { get; set; }
        public string TieuChiTen { get; set; }
        public float Diem { get; set; }
        public float DiemToiDa { get; set; }
        public string NhanXet { get; set; }
        public int LoaiThangDiemChiTiet { get; set; }
    }
}
