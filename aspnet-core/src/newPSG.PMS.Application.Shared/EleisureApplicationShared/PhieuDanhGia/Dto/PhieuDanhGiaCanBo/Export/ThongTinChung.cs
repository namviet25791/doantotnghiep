﻿namespace newPSG.PMS.EleisureApplicationShared.PhieuDanhGia.Dto.PhieuDanhGiaCanBo.Export
{
    public class ThongTinChung
    {
        public long PhieuDanhGiaCanBoId { get; set; }
        public int Nam { get; set; }
        public int Thang { get; set; }
        public int Quy { get; set; }
        public int Ky { get; set; }
        public int DaHoanThanh { get; set; }
        public long PhieuDanhGiaId { get; set; }
        public string TenPhieuDanhGia { get; set; }
        public string TieuDe { get; set; }
        public string GhiChu { get; set; }
        public string DuongDanPhieuDanhGia { get; set; }

        // Thong tin can bo
        public long CanBoId { get; set; }
        public string TenCanBo { get; set; }

        // Thong tin chuc vu duoc danh gia
        public string TenChucVu { get; set; }
        public long ChucVuId { get; set; }

        // Thong tin don vi cua can bo
        public string TenQuanHuyen { get; set; }
        public string TenDonVi { get; set; }
        public long DonViId { get; set; }
    }
}
