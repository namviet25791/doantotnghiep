﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    #region trangqt

    public class PhieuDanhGiaTongHop
    {
        public CanBoGuiDanhGiaDto ThongTinCanBoDto { get; set; } // canbo gui danh gia
        public List<PhieuDanhGiaChiTietDto> PhieuDanhGiaChiTietDto { get; set; }
        public PhieuDanhGiaDto PhieuDanhGiaDto { get; set; }
        public string NhanXetChung { get; set; } // nhan xet nay la cua ng duyet
        public List<ThangDiemChiTietDto> ThangDiemChiTietDto { get; set; }
        //public bool IsEditKetQuaPhieuDanhGia { get; set; }
        public int TrangThai { get; set; }
        public long LichSuCanBoDangDanhGiaId { get; set; }

        // Điểm cá nhân tự chấm      
        public float TongDiemCaNhanCham { get; set; }
        public long ThangDiemCaNhanTuDanhGiaId { get; set; }
        //

        // Điểm ng đag chấm
        public string TieuDeCot { get; set; }
        public float TongDiemLanhDaoCham { get; set; }
        public string NhanXetLanhDaoDanhGia { get; set; }
        public long ThangDiemLanhDaoDanhGiaId { get; set; }
        //

        //Điểm của ng đã chấm rồi 
        public string TieuDeCot1 { get; set; }
        public float TongDiemCapDanhGia1 { get; set; }
        public string NhanXetCapDanhGia1 { get; set; }
        public long ThangDiemCapDanhGiaId1 { get; set; }
        public bool HienThiChiTietCot1 { get; set; }
        public long LichSuDanhGiaId1 { get; set; }

        public string TieuDeCot2 { get; set; }
        public float TongDiemCapDanhGia2 { get; set; }
        public string NhanXetCapDanhGia2 { get; set; }
        public long ThangDiemCapDanhGiaId2 { get; set; }
        public bool HienThiChiTietCot2 { get; set; }
        public long LichSuDanhGiaId2 { get; set; }

        public string TieuDeCot3 { get; set; }
        public float TongDiemCapDanhGia3 { get; set; }
        public string NhanXetCapDanhGia3 { get; set; }
        public long ThangDiemCapDanhGiaId3 { get; set; }
        public bool HienThiChiTietCot3 { get; set; }
        public long LichSuDanhGiaId3 { get; set; }

        public string TieuDeCot4 { get; set; }
        public float TongDiemCapDanhGia4 { get; set; }
        public string NhanXetCapDanhGia4 { get; set; }
        public long ThangDiemCapDanhGiaId4 { get; set; }
        public bool HienThiChiTietCot4 { get; set; }
        public long LichSuDanhGiaId4 { get; set; }
        //
        public bool HienThiChiTietChoNguoiDangDangNhap { get; set; }

        public string HoTenLanhDao { get; set; }
    }

    public class CanBoGuiDanhGiaDto
    {
        public string TenCanBo { get; set; }
        public string TenChucVu { get; set; }
        public string ThoiGian { get; set; } // thời gian tạo phiếu
    }

    public class LichSuKetQuaDanhGiaDto
    {
        public long? TieuChiId { get; set; }
        public long LichSuDanhGiaId { get; set; }
        public long ChucVuCanBoDanhGiaId { get; set; }
        public long ThangDiemId { get; set; }
        public float? Diem { get; set; }
        public string NhanXet { get; set; }
        public bool NguoiCham { get; set; }
    }

    public class PhieuDanhGiaChiTietDto
    {
        public long Id { get; set; }
        public long? TieuChiChaId { get; set; }
        public string KyHieu { get; set; }
        public int ThuTu { get; set; }
        public string Ten { get; set; }
        public float DiemToiThieu { get; set; }
        public float DiemToiDa { get; set; }
        public string TuNhanXet { set; get; }
        public float? DiemTuCham { set; get; }
        public string LanhDaoNhanXet { set; get; }
        public float? DiemLanhDaoCham { set; get; }
        public float? TongDiemCaNhanTuCham { set; get; }
        public float? TongDiemLanhDaoCham { set; get; }
        public bool IsCapCha { get; set; }

        public float? LichSuDiemCapDanhGia1 { set; get; }
        public float? LichSuTongDiemCapDanhGia1 { set; get; }
        public string LichSuNhanXetTuDanhgia { set; get; }

        public float? LichSuDiemCapDanhGia2 { set; get; }
        public float? LichSuTongDiemCapDanhGia2 { set; get; }

        public float? LichSuDiemCapDanhGia3 { set; get; }
        public float? LichSuTongDiemCapDanhGia3 { set; get; }

        public float? LichSuDiemCapDanhGia4 { set; get; }
        public float? LichSuTongDiemCapDanhGia4 { set; get; }
    }
    public class PhieuDanhGiaCanBoInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public int Nam { set; get; }
        public int? TrangThaiPhieu { set; get; }
        public long? CanBoId { set; get; }
        public long? CanBoDanhGiaId { set; get; }
        public long? PhieuDanhGiaId { get; set; }
        public long? ChucVuId { get; set; }
        public long? DonViId { get; set; }
        public DateTime? ThoiGian { get; set; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }

    public class PhieuDuyetInput
    {
        public long PhieuDanhGiaCanBoId { get; set; }
        public long CanBoId { get; set; }
        public long CanBoDanhGiaId { get; set; }
        public long ChucVuCanBoDanhGiaId { get; set; }
    }
    public class DanhSachPhieuDanhGiaCanBoDto
    {
        public long Id { set; get; }
        public int Thang { set; get; }
        public int Nam { set; get; }
        public long? PhieuDanhGiaId { set; get; }

        // ds danh gia nv
        public long? CanBoId { set; get; }
        public string TenNhanVien { set; get; }
        public string TenChucVu { set; get; }
        public string DonViCongTac { set; get; }
        public bool IsSuaKetQua { set; get; }
        public bool IsNguoiCham { set; get; }

        //Chung
        public string TongDiemCaNhan { set; get; }
        public string XepLoaiCaNhan { set; get; }
        public string XepLoaiCuoiCung { set; get; }
        public int TrangThai { set; get; }
        public long LichSuKetQuaDanhGiaChiTietId { get; set; }
        public string TenPhieuDanhGia { get; set; }
        public bool? HetHanDanhGia { get; set; }
        public int LoaiChucVuDanhGia { get; set; }
        public int LoaiHienThiChiTiet { get; set; }
        public bool IsChanDiemLan2 { get; set; }
    }
    public class PhieuDanhGiaCanBoUpdateDto
    {
        public long? PhieuDanhGiaCanBoId { get; set; }
        public long PhieuDanhGiaId { set; get; }
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public long CanBoDanhGiaId { set; get; }
        public int CapDanhGia { set; get; }
        public int TrangThai { set; get; }
        public long ThangDiemChiTietId { get; set; }
        public List<TieuChiItem> LstTieuChi { get; set; }
        public DateTime ThoiGianTaoPhieu { get; set; }
        public int LoaiChucVuDanhGia { get; set; }// AppConsts.LoaiHinhChucVu
        //danh cho ng duyet
        public long? KetQuaChiTietId { get; set; }
        public string NhanXet { get; set; }
        public long? LichSuKetQuaDanhGiaChiTietId { get; set; }

    }

    public class TieuChiItem
    {
        public long TieuChiId { set; get; }
        public float? Diem { set; get; }
        public string NhanXet { set; get; }

        // mo rong
        public long? PhieuDanhGiaId { set; get; }
        public long? PhieuDanhGiaCanBoId { set; get; }
        public long? CanBoDanhGiaId { set; get; }
    }

    public class ThongTinDuyetPhieuDanhGiaCanBoDto
    {
        public long? KetQuaChiTietId { set; get; }
        public long? LichSuKetQuaDanhGiaChiTietId { set; get; }
        public long CanBoDanhGiaId { set; get; }
        public string NhanXet { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long ThangDiemChiTietId { set; get; }
    }

    #endregion

    public class DanhSachCanBoChuaGuiDanhGiaDto{
        public string TenCanBo { get; set; }
        public string TenChucVu { get; set; }
        public long CanBoId { get; set; }
        public long ChucVuCanBoId { get; set; }
        public long PhieuDanhGiaCanBoId { get; set; }
    }


    public class PhieuDanhGiaCanBoDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public int Thang { set; get; }
        public int Nam { set; get; }
        public int Quy { set; get; }
        public int Ky { set; get; }
        public int DaHoanThanh { set; get; }
        public long LichSuKetQuaDanhGiaChiTietHienTaiId { set; get; }
    }

    public class DanhSachDuyetPhieu
    {
        public long PhieuDanhGiaCanBoId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public string TenChucVu { set; get; }
        public string TenCanBo { set; get; }
        public long LichSuKetQuaDanhGiaChiTietId { set; get; }
        public long KetQuaDanhGiaChiTietId { set; get; }
        public long TieuChiId { set; get; }
        public long ThangDiemChiTietId { set; get; }
        public int LoaiThangDiemChiTiet { set; get; }
        public string NhanXet { set; get; }
        public bool? HetHanDanhGia { get; set; }
        public bool IsSuaKetQua { set; get; }
        public int CapDoDanhGia { set; get; }
        public int LoaiChucVuDanhGia { get; set; }
    }
    public class DanhSachDuyetPhieuChuTich : DanhSachDuyetPhieu
    {
        public long ThangDiemDanhGiaCaNhanId { get; set; }
    }
}
