﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class TieuChiDanhGiaResponse : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long? TieuChiChaId { set; get; }
        public string Ten { set; get; }
        public long CanBoId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public int NhomTieuChi { set; get; }
        public int ThuTu { set; get; }
        public string KyHieu { set; get; }
        public float DiemToiDa { set; get; }
        public float DiemToiThieu { set; get; }

        //public List<TieuChiDanhGiaResponse> MangTieuChiCon { set; get; }
    }
}
