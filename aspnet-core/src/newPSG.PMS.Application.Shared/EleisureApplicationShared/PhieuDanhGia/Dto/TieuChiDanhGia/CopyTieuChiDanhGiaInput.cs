﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class CopyTieuChiDanhGiaInput
    {
        public long PhieuDanhGiaTruocDoId { set; get; }
        public long PhieuDanhGiaId { set; get; }
    }
}
