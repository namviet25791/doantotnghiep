﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class PhieuDanhGiaDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string TieuDe { set; get; }
        public string Ten { set; get; }
        public string GhiChu { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }
        public long?  ThangDiemId { set; get; }
    }
}
