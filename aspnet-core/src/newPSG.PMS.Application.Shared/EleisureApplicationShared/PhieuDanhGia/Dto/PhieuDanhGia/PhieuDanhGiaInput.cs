﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class PhieuDanhGiaInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
}
