﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class PhieuDanhGiaChucVuDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }
    }
}
