﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
   
    public class PhieuDanhGiaChucVuResponse : PhieuDanhGiaChucVuDto
    {
        public PhieuDanhGiaDetail PhieuDanhGia { set; get; }
    }

    public class PhieuDanhGiaDetail : PhieuDanhGiaDto
    { 
        public int TrangThai { set; get; }
        public float TongDiemCaNhanTuDanhGia { set; get; }
        public float TongDiemLanhDaoDanhGia { set; get; }
        public List<TieuChiDanhGiaDetails> TieuChiDanhGias { set; get; }
        public List<ThangDiemDetails> ThangDiems { set; get; }
    }
 

    public class ThangDiemDetails : ThangDiemDto
    { 
        public List<ThangDiemChiTietDto> ThangDiemChiTiets { set; get; }
    }
    public class TieuChiDanhGiaDetails: TieuChiDanhGiaResponse
    {
        public string TuNhanXet { set; get; }
        public float DiemTuCham { set; get; }
        public string LanhDaoNhanXet { set; get; }
        public float? DiemLanhDaoCham { set; get; }
        public float TongDiemCaNhanTuCham { set; get; }
        public float TongDiemLanhDaoCham { set; get; }
    }

}
