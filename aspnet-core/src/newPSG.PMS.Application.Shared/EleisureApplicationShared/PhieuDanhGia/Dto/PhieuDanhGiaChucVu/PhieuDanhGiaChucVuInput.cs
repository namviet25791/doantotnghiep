﻿using Abp.Application.Services.Dto;
using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class PhieuDanhGiaChucVuInput : PagedAndSortedInputDto
    {
        public string Filter { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public long? PhieuDanhGiaId { set; get; }
        public long? ChucVuId { set; get; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);  
        }
    }
}
