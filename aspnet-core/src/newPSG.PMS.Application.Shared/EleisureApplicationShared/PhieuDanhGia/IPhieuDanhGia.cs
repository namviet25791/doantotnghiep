﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IPhieuDanhGia: IApplicationService
    {
        PagedResultDto<PhieuDanhGiaDto> GetByFilter(PhieuDanhGiaInput input);
        Task<long> Upsert(PhieuDanhGiaDto input);
        void Delete(long id);

        bool CopyPhieuDanhGia(CopyPhieuDanhGiaInput input);
    }
}
    