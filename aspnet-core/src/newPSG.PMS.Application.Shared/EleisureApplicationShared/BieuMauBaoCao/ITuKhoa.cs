﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ITuKhoa: IApplicationService
    {
        PagedResultDto<TuKhoaDto> GetByFilter(TuKhoaInput input);
        Task<long> Upsert(TuKhoaDto input);
        void Delete(long id);
    }
}
