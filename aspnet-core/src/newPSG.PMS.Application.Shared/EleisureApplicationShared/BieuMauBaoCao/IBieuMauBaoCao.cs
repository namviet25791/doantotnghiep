﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IBieuMauBaoCao: IApplicationService
    {
        PagedResultDto<BieuMauBaoCaoDto> GetByFilter(BieuMauBaoCaoInput input);
        Task<long> Upsert(BieuMauBaoCaoDto input);
        void Delete(long id);
    }
}
