﻿using newPSG.PMS.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class BaoCaoInput : PagedAndSortedInputDto
    {
        public long ChucVuId { get; set; }
        public string Filter { get; set; }
        public long BaoCaoNhomDoiTuongId { get; set; }
        public int LoaiNhomDoiTuong { get; set; }
        public void Format()
        {
            this.Filter = UtilityStringExtensions.FullFomatKhongDau(this.Filter);
        }
    }

    public class FilterBaoCaoChucVu
    {
        public long ChucVuId { get; set; }
    }
    public class FilterBaoCaoInput
    {
        public int BaoCaoId { get; set; }
        public int LoaiThoiGian { get; set; }
        public string ThoiGianBatDau { get; set; }
        public List<long> LstNhomChucVu { get; set; }
        public List<long> LstDonVi { get; set; }
        public List<long> LstNhomDonVi { get; set; }
        public string Source { get; set; }
    }

    public class BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi
    {
        public long DonViId { get; set; }
        public string GhiChu { get; set; }
        public string TenDonVi { get; set; }
        public int SL_DanhGia { get; set; }
        public int SL_HTXSNV { get; set; }
        public string TiLe_HTXSNV { get; set; }
        public int SL_HTTNV { get; set; }
        public string TiLe_HTTNV { get; set; }
        public int SL_HTNV { get; set; }
        public string TiLe_HTNV { get; set; }
        public int SL_KHTNV { get; set; }
        public string TiLe_KHTNV { get; set; }
    }
    public class BaoCaoChiTietKeQuaDanhGiaTheoDonVi
    {
        public long LoaiDonViId { get; set; }
        public string TenLoaiDonVi { get; set; }
        public long DonViId { get; set; }
        public string TenDonVi { get; set; }
        public string HoVaTen { get; set; }
        public string ChucVu { get; set; }
        public string XepLoaiCuaCaNhan { get; set; }
        public string XepLoaiCapTren { get; set; }
        public string NhanXet { get; set; }
    }
    public class BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong
    {
        public string TenDoiTuong { get; set; }
        public int SL_DanhGia { get; set; }
        public int SL_HTXSNV { get; set; }
        public string TiLe_HTXSNV { get; set; }
        public int SL_HTTNV { get; set; }
        public string TiLe_HTTNV { get; set; }
        public int SL_HTNV { get; set; }
        public string TiLe_HTNV { get; set; }
        public int SL_KHTNV { get; set; }
        public string TiLe_KHTNV { get; set; }
        public int TongSo { get; set; }
    }

    public class BaoCaoThongBaoKetQuaDanhGia
    {
        public string TenDonVi { get; set; }
        public string HoVaTen { get; set; }
        public string ChucVu { get; set; }
        public float DiemCaNhanCham { get; set; }
        public string CaNhanTuXepLoai { get; set; }
        public string DiemLanhDaoCham { get; set; }
        public string LanhDaoXepLoai { get; set; }
        public string NhanXet { get; set; }
        public long PhieuDanhGiaCanBoId { get; set; }
    }
    public class BaoCaoChucVuCauHinhInput
    {
        public long ChucVuId { set; get; }
        public long BaoCaoId { set; get; }
        public long BaoCaoNhomDoiTuongId { get; set; }
    }
    public class BaoCaoChucVuCauHinhChiTietDto
    {
        public long Id { get; set; }
        public long BaoCaoChucVuCauHinhId { set; get; }
        public long ChucVuId { set; get; }
    }
    public class BaoCaoTongHopXepLoai
    {
        public string STT { get; set; }
        public string TenDoiTuong { get; set; }
        public int SL_DanhGia { get; set; }
        public int SL_HTXSNV { get; set; }
        public int SL_HTTNV { get; set; }
        public int SL_HTNV { get; set; }
        public int SL_KHTNV { get; set; }
        public string GhiChu { get; set; }
    }

    public class BaoCaoTongHopKetQuaDanhGiaHangThang
    {
        public long CanBoId { get; set; }
        public long ChucVuId { get; set; }
        public string TenCanBo { get; set; }
        public string TenChucVu { get; set; }
        public string TenDonvi { get; set; }
        public string T1 { get; set; }
        public string T2 { get; set; }
        public string T3 { get; set; }
        public string T4 { get; set; }
        public string T5 { get; set; }
        public string T6 { get; set; }
        public string T7 { get; set; }
        public string T8 { get; set; }
        public string T9 { get; set; }
        public string T10 { get; set; }
        public string T11 { get; set; }
        public string T12 { get; set; }

        public long LoaiA { get; set; }
        public long LoaiB { get; set; }
        public long LoaiC { get; set; }
        public long LoaiD { get; set; }

        public string STT { get; set; }

    }

    public class BaoCaoTongHopKetQuaDanhGiaXepLoaiThang
    {
        public string STT { get; set; }
        public string TenDoiTuong { get; set; }
        public long CanBoId { get; set; }
        public long ChucVuId { get; set; }
        public string TenCanBo { get; set; }
        public string TenChucVu { get; set; }
        public string LoaiThangDiemChiTiet { get; set; }
        public string TenLoaiThangDiemChiTiet { get; set; }
    }

    public class TongHopKhoiCacPhuong04Dto
    {
        public List<KetQuaThangDiemChiTietDto> ThangDiemChiTiet { get; set; }
        public List<NhomChucVuDto> LstNhomChucVu { get; set; }
        public List<KetQuaDonVi> LstKetQuaDonVi { get; set; }
    }

    public class KetQuaDonVi
    {
        public string TenDonVi { get; set; }
        public int TongSo { get; set; }
        public int SoDuocDanhGia { get; set; }
        public int SoKhongDanhGia { get; set; }
        public string LyDoKhongDanhGia { get; set; }
        public List<NhomChucVuDto> LstNhomChucVu { get; set; }
    }

    public class NhomChucVuDto
    {
        public bool IsNhomChucVu { get; set; }
        public int ThuTu { get; set; }
        public long NhomChucVuId { get; set; }
        public string TenNhom { get; set; }
        public List<ChucVuChiTietDto> LstChucVu { get; set; }
        public List<KetQuaThangDiemChiTietDto> LstThangDiem { get; set; }
        public NhomChucVuDto Clone()
        {
            return new NhomChucVuDto
            {
                IsNhomChucVu = this.IsNhomChucVu,
                ThuTu = this.ThuTu,
                NhomChucVuId = this.NhomChucVuId,
                TenNhom = this.TenNhom,
                LstChucVu = this.LstChucVu,
                LstThangDiem = this.LstThangDiem
            };
        }
    }

    public class ChucVuChiTietDto
    {
        public long ChucVuId { get; set; }
        public string TenChucVu { get; set; }
        public List<KetQuaThangDiemChiTietDto> LstThangDiem { get; set; }
        public ChucVuChiTietDto Clone()
        {
            return new ChucVuChiTietDto
            {
                ChucVuId = this.ChucVuId,
                TenChucVu = this.TenChucVu,
                LstThangDiem = this.LstThangDiem
            };
        }
    }
    public class KetQuaThangDiemChiTietDto
    {
        public long ThangDiemChiTietId { get; set; }
        public string TenThangDiem { get; set; }
        public int SoLuong { get; set; }
        public KetQuaThangDiemChiTietDto Clone()
        {
            return new KetQuaThangDiemChiTietDto
            {
                ThangDiemChiTietId = this.ThangDiemChiTietId,
                TenThangDiem = this.TenThangDiem,
                SoLuong = this.SoLuong
            };
        }
    }

    public class ObjHeader
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class TongHopKhoiDang06Dto
    {
        public List<KetQuaThangDiemChiTietDto> ThangDiemChiTiet { get; set; }
        public List<KetQuaDonVi06> LstKetQuaDonVi { get; set; }
    }
    public class KetQuaDonVi06
    {
        public int Lv1 { get; set; }
        public int Lv2 { get; set; }
        public int  SttLv1 { get; set; }
        public int  SttLv2 { get; set; }
        public string TenDonVi { get; set; }
        public int TongSo { get; set; }
        public int SoDuocDanhGia { get; set; }
        public int SoKhongDanhGia { get; set; }
        public string LyDoKhongDanhGia { get; set; }
        public List<NhomChucVu06Dto> LstNhomChucVu { get; set; }
    }
    public class NhomChucVu06Dto
    {
        public List<KetQuaThangDiemChiTietDto> LstThangDiem { get; set; }
       
    }

    public class TongHopKhoiTruongHoc05Dto
    {
        public List<KetQuaThangDiemChiTietDto> ThangDiemChiTiet { get; set; }
        public List<NhomChucVuDto> LstNhomChucVu { get; set; }
        public List<KetQuaDonVi> LstKetQuaDonViTHCS { get; set; }
        public List<KetQuaDonVi> LstKetQuaDonViTieuHoc { get; set; }
        public List<KetQuaDonVi> LstKetQuaDonViMamNon { get; set; }
        public int SoLuongThangDiemDanhGia { get; set; }
    }
}
