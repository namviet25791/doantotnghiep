﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class TuKhoaBieuMauBaoCaoDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long BieuMauBaoCaoId { set; get; }
        public long TuKhoaId { set; get; }
    } 
    public class TuKhoaBieuMauBaoCaoUpdate
    {
        public virtual int? TenantId { get; set; }
        public long BieuMauBaoCaoId { set; get; }
        public List<long> TuKhoaIds { set; get; }

    }
    public class TuKhoaBieuMauBaoCaoResponse : TuKhoaBieuMauBaoCaoDto
    { 
        public string MaTuKhoa { set; get; }
    }
}
