﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class ChucVuBieuMauBaoCaoDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long BieuMauBaoCaoId { set; get; }
    }
}
