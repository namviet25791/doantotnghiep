﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace newPSG.PMS.EleisureApplicationShared
{
    public class TuKhoaDto : EntityDto<long>
    {
        public virtual int? TenantId { get; set; }
        public string Ma { set; get; }
        public string MoTa { set; get; }
    }
}
