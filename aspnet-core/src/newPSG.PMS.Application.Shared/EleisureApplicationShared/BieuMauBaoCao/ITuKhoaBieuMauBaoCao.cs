﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface ITuKhoaBieuMauBaoCao: IApplicationService
    {
        PagedResultDto<TuKhoaBieuMauBaoCaoResponse> GetByFilter(TuKhoaBieuMauBaoCaoInput input);
        Task<List<long>> Upsert(TuKhoaBieuMauBaoCaoUpdate input);
        void Delete(long id);
    }
}
