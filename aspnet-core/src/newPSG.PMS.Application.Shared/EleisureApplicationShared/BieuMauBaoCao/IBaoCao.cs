﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IBaoCao : IApplicationService
    {
        string BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong(FilterBaoCaoInput input);
        string BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi(FilterBaoCaoInput input);
        string BaoCaoChiTietKeQuaDanhGiaTheoDonVi(FilterBaoCaoInput input);
        string BaoCaoThongBaoKetQuaDanhGia(FilterBaoCaoInput input);

        PagedResultDto<BaoCaoNhomDoiTuongDto> GetBaoCaoNhomDoiTuong(BaoCaoInput input);
        Task<long> InsertOrUpdateBaoCaoNhomDoiTuong(BaoCaoNhomDoiTuongDto input);
        void DeleteBaoCaoNhomDoiTuong(long id);
        //
        List<BaoCaoNhomDoiTuongChiTietDto> GetBaoCaoNhomDoiTuongChiTiet(BaoCaoInput input);
        Task<bool> InsertBaoCaoNhomDoiTuongChiTiet(BaoCaoNhomDoiTuongChiTietInsert input);
        Task<bool> InsertBaoCaoNhomDoiTuongChiTietDonVi(BaoCaoNhomDoiTuongChiTietInsert input);
        void DeleteBaoCaoNhomDoiTuongChiTiet(long id);
        //
        List<BaoCaoChucVuCauHinhDto> GetBaoCaoChucVuCauHinh(BaoCaoChucVuCauHinhInput input);
        Task<bool> InsertOrUpdateBaoCaoChucVuCauHinh(BaoCaoChucVuCauHinhInsert input);
        void DeleteBaoCaoChucVuCauHinh(long id);
        PagedResultDto<BaoCaoDto> GetAllBaoCao(BaoCaoInput input);
        Task<long> InsertOrUpdateBaoCao(BaoCaoDto input);
        void DeleteBaoCao(long id);
        //Task<bool> InsertBaoCaoChucVu(long baoCaoId);
        List<BaoCaoDto> GetChucVuTheoBaoCao(long baoCaoId);

        Task<bool> InsertBaoCaoChucVu(BaoCaoChucVuInsert input);
        void DeleteBaoCaoChucVu(long id);
        //

        List<BaoCaoDto> GetBaoCaoTheoChucVu(BaoCaoInput input);
        List<BaoCaoChucVuCauHinhDto> GetBaoCaoChucVuCauHinhTheoChucVu(BaoCaoChucVuCauHinhInput input);

        #region bc cho Quận Hoàn Kiếm
        string TongHopKetQuaDanhGiaXepLoaiMau01(FilterBaoCaoInput input);
        string TongHopKetQuaDanhGiaXepLoaiMau02(FilterBaoCaoInput input);
        string TongHopKetQuaDanhGiaXepLoaiMau03(FilterBaoCaoInput input);

        string TongHopKetQuaDanhGiaXepLoaiHangThangMau10(FilterBaoCaoInput input);
        string TongHopKetQuaDanhGiaXepLoaiTheoThangMau07(FilterBaoCaoInput input);
        string TongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11(FilterBaoCaoInput input);

        TongHopKhoiCacPhuong04Dto TongHopKhoiCacPhuong04(FilterBaoCaoInput input);
        TongHopKhoiDang06Dto TongHopKhoiCacDang06(FilterBaoCaoInput input);
        #endregion
    }
}
