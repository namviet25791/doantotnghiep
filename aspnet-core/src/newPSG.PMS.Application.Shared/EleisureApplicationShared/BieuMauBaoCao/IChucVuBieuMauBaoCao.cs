﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplicationShared
{
    public interface IChucVuBieuMauBaoCao: IApplicationService
    {
        PagedResultDto<ChucVuBieuMauBaoCaoDto> GetByFilter(ChucVuBieuMauBaoCaoInput input);
        Task<long> Upsert(ChucVuBieuMauBaoCaoDto input);
        void Delete(long id);
    }
}
