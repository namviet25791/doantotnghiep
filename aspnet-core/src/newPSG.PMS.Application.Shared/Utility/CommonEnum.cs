﻿using System;
using System.Reflection;

namespace newPSG.PMS
{
    public static class CommonEnum
    {
        public enum ROLE_LEVEL
        {
            [EnumDisplayString("Khách hàng")]
            KHACH_HANG = 1,
            [EnumDisplayString("Trung tâm")]
            TRUNG_TAM = 2
        }
        public enum TRANG_THAI_DUYET
        {
            [EnumDisplayString("Chờ duyệt")]
            CHO_DUYET = 1,
            [EnumDisplayString("Đã duyệt")]
            DA_DUYET = 2
        }
        public enum TYPE_CATEGORY
        {
            [EnumDisplayString("INSTRUMENT")]
            INSTRUMENT = 1,
            [EnumDisplayString("REAGENT SUPPLIER ")]
            REAGENT_SUPPLIER = 2,
            [EnumDisplayString("METHOD")]
            METHOD = 3,
            [EnumDisplayString("UNIT OF MEASUREMENT")]
            UNIT = 4,
            [EnumDisplayString("PARAMETER")]
            PARAMETER = 5,
        }
        public enum STATUS_CODE_ERROR
        {
            IsExistEmail = -1,
        }
        public enum TYPE_PARAMETER
        {
            [EnumDisplayString("Định lượng")]
            DINH_LUONG = 1,
            [EnumDisplayString("Định tính")]
            DINH_TINH = 2,
            [EnumDisplayString("Bán định lượng")]
            BAN_DINH_LUONG = 3,
        }
        public enum TRANG_THAI_DANG_KY_CHUONG_TRINH
        {
            [EnumDisplayString("Chờ duyệt")]
            CHO_DUYET = 1,
            [EnumDisplayString("Đã thanh toán")]
            DA_THANH_TOAN = 2,
            [EnumDisplayString("Từ chối đăng ký")]
            BI_TU_CHOI = -1,
        }
        public enum HINH_THUC_THANH_TOAN
        {
            [EnumDisplayString("Chuyển khoản")]
            CHUYEN_KHOAN = 1,
            [EnumDisplayString("Nộp trực tiếp")]
            NOP_TRUC_TIEP = 2,
        }
        public enum TRANG_THAI_NHAP_KET_QUA
        {
            CHUA_NHAP = 0,
            DA_NHAP = 1
        }

        public enum DeliveryStatus
        {
            None=0,
            InPlan=1,
            Delivering=2,
            Delivered=3,
            LostOrBroken=4
        }

    }
    public static class UtilityCommonEnum
    {
        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();
            try
            {
                MemberInfo[] memInfo = type.GetMember(en.ToString());

                if (memInfo != null && memInfo.Length > 0)
                {
                    object[] attrs = memInfo[0].GetCustomAttributes(typeof(EnumDisplayString), false);

                    if (attrs != null && attrs.Length > 0)
                        return ((EnumDisplayString)attrs[0]).DisplayString;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return en.ToString();
        }
    }
    public class EnumDisplayString : Attribute
    {
        public string DisplayString { get; set; }

        public EnumDisplayString(string text)
        {
            this.DisplayString = text;
        }
    }
}
