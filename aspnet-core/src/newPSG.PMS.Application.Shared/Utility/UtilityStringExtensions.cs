﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace newPSG.PMS
{
    public static class UtilityStringExtensions
    {
        public static string FullFomatKhongDau(string input)
        {
            input = FomatFilterText(input);
            input = RemoveDau(input);
            return input;
        }
        public static string FomatFilterText(string input)
        {
            input = UtilityStringExtensions.RemoveMultipleSpaces(input);
            if (string.IsNullOrEmpty(input)) return string.Empty;
            return input.Trim().ToLower();
        }
        
        public static string RemoveDau(string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public static string RemoveMultipleSpaces(string input)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;
            input = input.Trim();
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            input = regex.Replace(input, " ");
            return input;
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public static bool IsFullEmpty(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                return true;
            }
            input = input.Replace(" ", "");
            return String.IsNullOrEmpty(input);
        }
        public static bool IsValidateMa(string ma)
        {
            Regex rgx = new Regex("[^0-9a-zA-Z-.* ]");
            return rgx.IsMatch(ma);
        }
    }
}
