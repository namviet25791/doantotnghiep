﻿
using Abp.Dependency;
using newPSG.PMS.EleisureApplication;
using newPSG.PMS.EleisureApplicationShared;
using System;
using System.Collections.Generic;

namespace eLNG
{
    public class HangFileScheduler
    {             
        public static void HangFileSchedulerDayly()
        {
            TuDongNhacNhoGuiPhieuDanhGiaCaNhan();
        }

        public static void TuDongNhacNhoGuiPhieuDanhGiaCaNhan()
        {
            using (var scope = IocManager.Instance.CreateScope())
            {
                var _callService = scope.Resolve<IPhieuDanhGiaCanBo>();
                _callService.NhacNhoGuiPhieuDanhGiaCaNhan();
            }
        }

    }
}