﻿namespace newPSG.PMS.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";

        // quan tri
        public const string Pages_Admin_CapDonVi = "Pages.Admin.CapDonVi";
        public const string Pages_Admin_CapDonVi_Create = "Pages.Admin.CapDonVi.Create";
        public const string Pages_Admin_CapDonVi_Edit = "Pages.Admin.CapDonVi.Edit";
        public const string Pages_Admin_CapDonVi_Delete = "Pages.Admin.CapDonVi.Delete";


        // bieu mau
        public const string Pages_Admin_BieuMauBaoCao = "Pages.Admin.BieuMauBaoCao";
        public const string Pages_Admin_BieuMauBaoCao_Create = "Pages.Admin.BieuMauBaoCao.Create";
        public const string Pages_Admin_BieuMauBaoCao_Edit = "Pages.Admin.BieuMauBaoCao.Edit";
        public const string Pages_Admin_BieuMauBaoCao_Delete = "Pages.Admin.BieuMauBaoCao.Delete";

        public const string Pages_Admin_ChucVu = "Pages.Admin.ChucVu";
        public const string Pages_Admin_ChucVu_Create = "Pages.Admin.ChucVu.Create";
        public const string Pages_Admin_ChucVu_Edit = "Pages.Admin.ChucVu.Edit";
        public const string Pages_Admin_ChucVu_Delete = "Pages.Admin.ChucVu.Delete";

        public const string Pages_Admin_DonVi = "Pages.Admin.DonVi";
        public const string Pages_Admin_DonVi_Create = "Pages.Admin.DonVi.Create";
        public const string Pages_Admin_DonVi_Edit = "Pages.Admin.DonVi.Edit";
        public const string Pages_Admin_DonVi_Delete = "Pages.Admin.DonVi.Delete";

        public const string Pages_Admin_Khoi = "Pages.Admin.Khoi";
        public const string Pages_Admin_Khoi_Create = "Pages.Admin.Khoi.Create";
        public const string Pages_Admin_Khoi_Edit = "Pages.Admin.Khoi.Edit";
        public const string Pages_Admin_Khoi_Delete = "Pages.Admin.Khoi.Delete";

        public const string Pages_Admin_LoaiDonVi = "Pages.Admin.LoaiDonVi";
        public const string Pages_Admin_LoaiDonVi_Create = "Pages.Admin.LoaiDonVi.Create";
        public const string Pages_Admin_LoaiDonVi_Edit = "Pages.Admin.LoaiDonVi.Edit";
        public const string Pages_Admin_LoaiDonVi_Delete = "Pages.Admin.LoaiDonVi.Delete";

        public const string Pages_Admin_PhieuDanhGia = "Pages.Admin.PhieuDanhGia";
        public const string Pages_Admin_PhieuDanhGia_Create = "Pages.Admin.PhieuDanhGia.Create";
        public const string Pages_Admin_PhieuDanhGia_Edit = "Pages.Admin.PhieuDanhGia.Edit";
        public const string Pages_Admin_PhieuDanhGia_Delete = "Pages.Admin.PhieuDanhGia.Delete";
        public const string Pages_Admin_PhieuDanhGia_Clone = "Pages.Admin.PhieuDanhGia.Clone";

        public const string Pages_Admin_ThangDiem = "Pages.Admin.ThangDiem";
        public const string Pages_Admin_ThangDiem_Create = "Pages.Admin.ThangDiem.Create";
        public const string Pages_Admin_ThangDiem_Edit = "Pages.Admin.ThangDiem.Edit";
        public const string Pages_Admin_ThangDiem_Delete = "Pages.Admin.ThangDiem.Delete";
        public const string Pages_Admin_ThangDiem_Clone = "Pages.Admin.ThangDiem.Clone";

        public const string Pages_Admin_CanBo = "Pages.Admin.CanBo";
        public const string Pages_Admin_CanBo_Create = "Pages.Admin.CanBo.Create";
        public const string Pages_Admin_CanBo_Edit = "Pages.Admin.CanBo.Edit";
        public const string Pages_Admin_CanBo_Delete = "Pages.Admin.CanBo.Delete";
        public const string Pages_Admin_CanBo_XemChiTiet = "Pages.Admin.CanBo.XemChiTiet";
        public const string Pages_Admin_CanBo_Edit_CauHinhLuongDanhGia = "Pages.Admin.CanBo.Edit.CauHinhLuongDanhGia";
        public const string Pages_Admin_CanBo_Edit_XoaCauHinhLuongDanhGia = "Pages.Admin.CanBo.Edit.XoaCauHinhLuongDanhGia";

        // nguoi dung

        public const string Pages_Systems = "Pages.Systems";

        public const string Pages_Systems_Report = "Pages.Systems.Report";

        public const string Pages_Systems_Create = "Pages.Systems.Create";
        public const string Pages_Systems_Edit = "Pages.Systems.Edit";
        public const string Pages_Systems_Delete = "Pages.Systems.Delete"; 
        public const string Pages_Systems_MoveTenantsToAnotherEdition = "Pages.Systems.MoveTenantsToAnotherEdition";
        public const string Pages_Systems_Dashboard = "Pages.Systems.Dashboard";
        public const string Pages_Systems_Dashboard_DonVi = "Pages.Systems.DashboardDonVi";
                
        public const string Pages_User_DanhGiaCaNhan = "Pages.User.DanhGiaCaNhan";
        public const string Pages_User_DanhGiaCaNhan_Create = "Pages.User.DanhGiaCaNhan.Create";
        public const string Pages_User_DanhGiaCaNhan_Edit = "Pages.User.DanhGiaCaNhan.Edit";
        public const string Pages_User_DanhGiaCaNhan_Delete = "Pages.User.DanhGiaCaNhan.Delete";
       
        public const string Pages_User_DanhGiaNhanVien_ChuTich = "Pages.User.DanhGiaCaNhan.ChuTich";
        public const string Pages_User_DanhGiaNhanVien_PhoChuTich = "Pages.User.DanhGiaCaNhan.PhoChuTich";

        public const string Pages_User_DanhGiaNhanVien = "Pages.User.DanhGiaNhanVien";
        public const string Pages_User_DanhGiaNhanVien_Create = "Pages.User.DanhGiaNhanVien.Create";
        public const string Pages_User_DanhGiaNhanVien_Edit = "Pages.User.DanhGiaNhanVien.Edit";
        public const string Pages_User_DanhGiaNhanVien_Delete = "Pages.User.DanhGiaNhanVien.Delete";

        public const string Pages_ThongKe = "Pages.ThongKe";
        public const string Pages_ThongKe_TinhTrangPhieuDanhGia = "Pages.ThongKe.TinhTrangPhieuDanhGia";

    }
}