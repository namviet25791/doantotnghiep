﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace newPSG.PMS.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
            //pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            //var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            //administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));
            //var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            //organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            //organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            //organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            //administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            //TENANT-SPECIFIC PERMISSIONS

            //pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            //administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            //administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);


            var capDonVi = administration.CreateChildPermission(AppPermissions.Pages_Admin_CapDonVi, L("CapDonVi"));
            capDonVi.CreateChildPermission(AppPermissions.Pages_Admin_CapDonVi_Create, L("TaoMoi"));
            capDonVi.CreateChildPermission(AppPermissions.Pages_Admin_CapDonVi_Edit, L("Sua"));
            capDonVi.CreateChildPermission(AppPermissions.Pages_Admin_CapDonVi_Delete, L("Xoa"));

            var chucVu = administration.CreateChildPermission(AppPermissions.Pages_Admin_ChucVu, L("ChucVuVi"));
            chucVu.CreateChildPermission(AppPermissions.Pages_Admin_ChucVu_Create, L("TaoMoi"));
            chucVu.CreateChildPermission(AppPermissions.Pages_Admin_ChucVu_Edit, L("Sua"));
            chucVu.CreateChildPermission(AppPermissions.Pages_Admin_ChucVu_Delete, L("Xoa"));

            var donVi = administration.CreateChildPermission(AppPermissions.Pages_Admin_DonVi, L("DonViCongTac"));
            donVi.CreateChildPermission(AppPermissions.Pages_Admin_DonVi_Create, L("TaoMoi"));
            donVi.CreateChildPermission(AppPermissions.Pages_Admin_DonVi_Edit, L("Sua"));
            donVi.CreateChildPermission(AppPermissions.Pages_Admin_DonVi_Delete, L("Xoa"));

            var khoi = administration.CreateChildPermission(AppPermissions.Pages_Admin_Khoi, L("Khoi"));
            khoi.CreateChildPermission(AppPermissions.Pages_Admin_Khoi_Create, L("TaoMoi"));
            khoi.CreateChildPermission(AppPermissions.Pages_Admin_Khoi_Edit, L("Sua"));
            khoi.CreateChildPermission(AppPermissions.Pages_Admin_Khoi_Delete, L("Xoa"));

            var loaiDonVi = administration.CreateChildPermission(AppPermissions.Pages_Admin_LoaiDonVi, L("LoaiDonVi"));
            loaiDonVi.CreateChildPermission(AppPermissions.Pages_Admin_LoaiDonVi_Create, L("TaoMoi"));
            loaiDonVi.CreateChildPermission(AppPermissions.Pages_Admin_LoaiDonVi_Edit, L("Sua"));
            loaiDonVi.CreateChildPermission(AppPermissions.Pages_Admin_LoaiDonVi_Delete, L("Xoa"));

            var phieuDanhGia = administration.CreateChildPermission(AppPermissions.Pages_Admin_PhieuDanhGia, L("PhieuDanhGia"));
            phieuDanhGia.CreateChildPermission(AppPermissions.Pages_Admin_PhieuDanhGia_Create, L("TaoMoi"));
            phieuDanhGia.CreateChildPermission(AppPermissions.Pages_Admin_PhieuDanhGia_Edit, L("Sua"));
            phieuDanhGia.CreateChildPermission(AppPermissions.Pages_Admin_PhieuDanhGia_Delete, L("Xoa"));
            phieuDanhGia.CreateChildPermission(AppPermissions.Pages_Admin_PhieuDanhGia_Clone, L("SaoChep"));

            var thangDiem = administration.CreateChildPermission(AppPermissions.Pages_Admin_ThangDiem, L("ThangDiem"));
            thangDiem.CreateChildPermission(AppPermissions.Pages_Admin_ThangDiem_Create, L("TaoMoi"));
            thangDiem.CreateChildPermission(AppPermissions.Pages_Admin_ThangDiem_Edit, L("Sua"));
            thangDiem.CreateChildPermission(AppPermissions.Pages_Admin_ThangDiem_Delete, L("Xoa"));
            thangDiem.CreateChildPermission(AppPermissions.Pages_Admin_ThangDiem_Clone, L("SaoChep"));

            var canBo = administration.CreateChildPermission(AppPermissions.Pages_Admin_CanBo, L("CanBo"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_Create, L("TaoMoi"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_Edit, L("Sua"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_Delete, L("Xoa"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_XemChiTiet, L("XemChiTiet"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_Edit_CauHinhLuongDanhGia, L("CauHinhLuongDanhGia"));
            canBo.CreateChildPermission(AppPermissions.Pages_Admin_CanBo_Edit_XoaCauHinhLuongDanhGia, L("XoaCauHinhLuongDanhGia"));

            var bieuMauBaoCao = administration.CreateChildPermission(AppPermissions.Pages_Admin_BieuMauBaoCao, L("BieuMauBaoCao"));
            bieuMauBaoCao.CreateChildPermission(AppPermissions.Pages_Admin_BieuMauBaoCao_Create, L("TaoMoi"));
            bieuMauBaoCao.CreateChildPermission(AppPermissions.Pages_Admin_BieuMauBaoCao_Edit, L("Sua"));
            bieuMauBaoCao.CreateChildPermission(AppPermissions.Pages_Admin_BieuMauBaoCao_Delete, L("Xoa"));


            var system = pages.CreateChildPermission(AppPermissions.Pages_Systems, L("Systems"), multiTenancySides: MultiTenancySides.Tenant);
            //system.CreateChildPermission(AppPermissions.Pages_Systems_Create, L("CreatingNewSystem"), multiTenancySides: MultiTenancySides.Tenant);
            //system.CreateChildPermission(AppPermissions.Pages_Systems_Edit, L("EditingSystem"), multiTenancySides: MultiTenancySides.Tenant);
            //system.CreateChildPermission(AppPermissions.Pages_Systems_Delete, L("DeletingSystem"), multiTenancySides: MultiTenancySides.Tenant);
            //system.CreateChildPermission(AppPermissions.Pages_Systems_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherSystem"), multiTenancySides: MultiTenancySides.Tenant);
            system.CreateChildPermission(AppPermissions.Pages_Systems_Dashboard, L("DashboardCaNhan"), multiTenancySides: MultiTenancySides.Tenant);
            system.CreateChildPermission(AppPermissions.Pages_Systems_Dashboard_DonVi, L("DashboardDonVi"), multiTenancySides: MultiTenancySides.Tenant);
            system.CreateChildPermission(AppPermissions.Pages_Systems_Report, L("BaoCao"), multiTenancySides: MultiTenancySides.Tenant);

            var danhGiaCaNhan = system.CreateChildPermission(AppPermissions.Pages_User_DanhGiaCaNhan, L("DanhGiaCaNhan"));
            //danhGiaCaNhan.CreateChildPermission(AppPermissions.Pages_User_DanhGiaCaNhan_Create, L("TaoMoi"));
            //danhGiaCaNhan.CreateChildPermission(AppPermissions.Pages_User_DanhGiaCaNhan_Edit, L("Sua"));
            danhGiaCaNhan.CreateChildPermission(AppPermissions.Pages_User_DanhGiaCaNhan_Delete, L("Xoa"));

            var danhGiaNhanVienCT = system.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien_ChuTich, L("ChuTichDanhGiaNhanVien"));
            var danhGiaNhanVienPCT = system.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien_PhoChuTich, L("PhoChuTichDanhGiaNhanVien"));

            var danhGiaNhanVien = system.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien, L("DanhGiaNhanVien"));
            //danhGiaNhanVien.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien_Create, L("TaoMoi"));
            //danhGiaNhanVien.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien_Edit, L("Sua"));
            //danhGiaNhanVien.CreateChildPermission(AppPermissions.Pages_User_DanhGiaNhanVien_Delete, L("Xoa"));

            var thongKe = system.CreateChildPermission(AppPermissions.Pages_ThongKe, L("ThongKe"));
            thongKe.CreateChildPermission(AppPermissions.Pages_ThongKe_TinhTrangPhieuDanhGia, L("TinhTrangPhieuDanhGia"));
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, PMSConsts.LocalizationSourceName);
        }
    }
}
