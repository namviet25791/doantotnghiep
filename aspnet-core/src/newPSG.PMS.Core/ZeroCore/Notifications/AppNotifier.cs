﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Abp;
using Abp.Collections.Extensions;
using Abp.Localization;
using Abp.Notifications;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.MultiTenancy;

namespace newPSG.PMS.Notifications
{
    public class AppNotifier : PMSDomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;

        public AppNotifier(INotificationPublisher notificationPublisher)
        {
            _notificationPublisher = notificationPublisher;
        }

        public async Task WelcomeToTheApplicationAsync(User user)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.WelcomeToTheApplication,
                new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }

        public async Task NewUserRegisteredAsync(User user)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewUserRegisteredNotificationMessage",
                    PMSConsts.LocalizationSourceName
                    )
                );

            notificationData["userName"] = user.UserName;
            notificationData["emailAddress"] = user.EmailAddress;

            await _notificationPublisher.PublishAsync(AppNotificationNames.NewUserRegistered, notificationData, tenantIds: new[] { user.TenantId });
        }

        public async Task NewTenantRegisteredAsync(Tenant tenant)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    PMSConsts.LocalizationSourceName
                    )
                );

            notificationData["tenancyName"] = tenant.TenancyName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewTenantRegistered, notificationData);
        }

        public async Task GdprDataPrepared(UserIdentifier user, Guid binaryObjectId)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "GdprDataPreparedNotificationMessage",
                    PMSConsts.LocalizationSourceName
                )
            );

            notificationData["binaryObjectId"] = binaryObjectId;

            await _notificationPublisher.PublishAsync(AppNotificationNames.GdprDataPrepared, notificationData, userIds: new[] { user });
        }

        //This is for test purposes
        public async Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info)
        {
            await _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(message),
                severity: severity,
                userIds: new[] { user }
                );
        }

        public async Task TenantsMovedToEdition(UserIdentifier user, string sourceEditionName, string targetEditionName)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "TenantsMovedToEditionNotificationMessage",
                    PMSConsts.LocalizationSourceName
                )
            );

            notificationData["sourceEditionName"] = sourceEditionName;
            notificationData["targetEditionName"] = targetEditionName;

            await _notificationPublisher.PublishAsync(AppNotificationNames.TenantsMovedToEdition, notificationData, userIds: new[] { user });
        }

        public Task<TResult> TenantsMovedToEdition<TResult>(UserIdentifier argsUser, int sourceEditionId, int targetEditionId)
        {
            throw new NotImplementedException();
        }

        public async Task SomeUsersCouldntBeImported(UserIdentifier argsUser, string fileToken, string fileType, string fileName)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "ClickToSeeInvalidUsers",
                    PMSConsts.LocalizationSourceName
                )
            );

            notificationData["fileToken"] = fileToken;
            notificationData["fileType"] = fileType;
            notificationData["fileName"] = fileName;

            await _notificationPublisher.PublishAsync(AppNotificationNames.DownloadInvalidImportUsers, notificationData, userIds: new[] { argsUser });
        }

        public async Task DemoThongBao(User user, string tenNguoiGui, int giaTri1, int giaTri2)
        {

            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "VietGuiThongBaoNotificationMessage",
                    PMSConsts.LocalizationSourceName
                )
            );
            notificationData["tenNguoiGui"] = tenNguoiGui;
            notificationData["giaTri1"] = giaTri1;
            notificationData["giaTri2"] = giaTri2;
            await _notificationPublisher.PublishAsync(AppNotificationNames.DemoThongBao,
                notificationData, userIds: new[] { user.ToUserIdentifier() });
        }

        public async Task ThongBaoDanhGiaCaNhan(User ngNhan, string notificationMessage, long phieuDanhGiaCanBoId)
        {

            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    notificationMessage, ""
                    //PMSConsts.LocalizationSourceName
                )
            );
            //notificationData["tenNguoiGui"] = tenNguoiGui;
            notificationData["giaTri1"] = phieuDanhGiaCanBoId;

            await _notificationPublisher.PublishAsync(AppNotificationNames.LinkThongBaoDanhGiaCaNhan,
                notificationData, userIds: new[] { ngNhan.ToUserIdentifier() });
        }
        public async Task ThongBaoPhieuDanhGiaNhanVien(User ngNhan, string notificationMessage, long phieuDanhGiaCanBoId, long chucVuId)
        {

            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    notificationMessage, ""
                )
            );
            //notificationData["tenNguoiGui"] = tenNguoiGui;
            notificationData["giaTri1"] = phieuDanhGiaCanBoId;
            notificationData["giaTri2"] = chucVuId;

            await _notificationPublisher.PublishAsync(AppNotificationNames.LinkThongBaoDanhGiaCaNhan,
              notificationData, userIds: new[] { ngNhan.ToUserIdentifier() });
        }

    }
}