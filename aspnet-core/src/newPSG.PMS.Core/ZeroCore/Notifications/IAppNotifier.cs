﻿using System;
using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.MultiTenancy;

namespace newPSG.PMS.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task GdprDataPrepared(UserIdentifier user, Guid binaryObjectId);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);

        Task TenantsMovedToEdition(UserIdentifier argsUser, string sourceEditionName, string targetEditionName);

        Task SomeUsersCouldntBeImported(UserIdentifier argsUser, string fileToken, string fileType, string fileName);

        /// <summary>
        /// Test Gửi thông báo
        /// </summary>
        /// <param name="user">Người nhận</param>
        /// <param name="tenNguoiGui"></param>
        /// <param name="giaTri1"></param>
        /// <param name="giaTri2"></param>
        /// <returns></returns>
        Task DemoThongBao(User user, string tenNguoiGui, int giaTri1, int giaTri2);

        Task ThongBaoDanhGiaCaNhan(User ngNhan, string notificationMessage, long phieuDanhGiaCanBoId);
        Task ThongBaoPhieuDanhGiaNhanVien(User ngNhan, string notificationMessage, long phieuDanhGiaCanBoId, long chucVuId);
    }
}
