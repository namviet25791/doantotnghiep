﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("BaoCao")]
    public class BaoCao : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string TenBaoCao { set; get; }
        public string Mota { set; get; }
        public string DuongDanBieuMau { set; get; }
        public string MaBaoCao { set; get; }
        public string Source { set; get; }
    }
}
