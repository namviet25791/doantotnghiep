﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("BaoCaoChucVu")]
    public class BaoCaoChucVu : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long BaoCaoId { set; get; }
    }

    [Table("BaoCaoChucVuCauHinh")]
    public class BaoCaoChucVuCauHinh : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long BaoCaoNhomDoiTuongId { set; get; }
    }

    [Table("BaoCaoNhomDoiTuong")]
    public class BaoCaoNhomDoiTuong : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string TenNhomDoiTuong { set; get; }
        public string TenHienThiTrenBaoCao { set; get; }
        public int LoaiNhomDoiTuong { set; get; } // Chuc Vu hoac don vi
        public int ThuTu { set; get; } // Vi tri nhom
    }

    [Table("BaoCaoNhomDoiTuongChiTiet")]
    public class BaoCaoNhomDoiTuongChiTiet : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long BaoCaoNhomDoiTuongId { set; get; }
        public long ChucVuId { set; get; }
        public long DonViId { set; get; }
    }
}
