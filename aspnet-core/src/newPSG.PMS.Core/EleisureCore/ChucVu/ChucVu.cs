﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("ChucVu")]
    public class ChucVu : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public bool LaLanhDao { set; get; }
        public int Khoi { set; get; }
        public int LoaiChucVu { set; get; } // chuc vu hoac to chuyen mon
        public string TenHienThiTrenBieuMau { set; get; }
        public int CapLanhDao { get; set; }
    }

    [Table("ThanhVienNhomChucVu")]
    public class ThanhVienNhomChucVu : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long CanBoId { set; get; }
        public long ChucVuCanBoId { set; get; }
    }
}
