﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("ChucVuDanhGia")]
    public class ChucVuDanhGia : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public int SoThuTu { set; get; }
        public long ChucVuDuocDanhGiaId { set; get; }
        public bool NguoiCham { set; get; }
        public long ChucVuThucHienDanhGiaId { set; get; } 
    }
}
