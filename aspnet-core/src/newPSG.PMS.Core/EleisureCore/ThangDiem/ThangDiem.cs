﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("ThangDiem")]
    public class ThangDiem : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }
    }
}
