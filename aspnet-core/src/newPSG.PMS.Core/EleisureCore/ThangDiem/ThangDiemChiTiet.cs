﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("ThangDiemChiTiet")]
    public class ThangDiemChiTiet : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public long ThangDiemId { set; get; }
        public float DiemTran { set; get; }
        public float DiemSan { set; get; } 
        public int LoaiThangDiemChiTiet { set; get; } 
    }
}
