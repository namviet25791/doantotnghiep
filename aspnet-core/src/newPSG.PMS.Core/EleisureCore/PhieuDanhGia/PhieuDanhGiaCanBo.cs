﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("PhieuDanhGiaCanBo")]
    public class PhieuDanhGiaCanBo : FullAuditedEntity<long>, IMayHaveTenant
    {

        public virtual int? TenantId { get; set; } 
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public int Thang { set; get; }
        public int Nam { set; get; }
        public int Quy { set; get; }
        public int Ky { set; get; }
        public int DaHoanThanh { set; get; }
        public long LichSuKetQuaDanhGiaChiTietHienTaiId { set; get; }
    }

    [Table("ThongTinDanhGiaKeTiepTemp")]
    public class ThongTinDanhGiaKeTiepTemp : FullAuditedEntity<long>, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long CanBoDanhGiaKeTiepId { set; get; }
        public int TrangThai { set; get; }
        public long ChucVuId { set; get; }
        public bool NguoiCham { set; get; }
        public int LoaiChucVuDanhGia { set; get; } // AppConsts.LoaiHinhChucVu
        public int ThuTu { set; get; } 
    }
}
