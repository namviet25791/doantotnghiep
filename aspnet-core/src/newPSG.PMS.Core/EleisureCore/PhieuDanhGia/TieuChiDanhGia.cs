﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("TieuChiDanhGia")]
    public class TieuChiDanhGia : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long? TieuChiChaId { set; get; }
        public string Ten { set; get; }
        public long CanBoId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public int NhomTieuChi { set; get; }
        public int ThuTu { set; get; }
        public string KyHieu { set; get; }
        public float DiemToiDa { set; get; }
        public float DiemToiThieu { set; get; }

    }
}
