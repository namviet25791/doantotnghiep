﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("PhieuDanhGia")]
    public class PhieuDanhGia : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string TieuDe { set; get; }
        public string Ten { set; get; }
        public string GhiChu { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }

        [Column("ThangDiemId")]
        public long? ThangDiemId { set; get; }
        public string DuongDanMauBaoCao { set; get; }
    }
}
