﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("LichSuKetQuaDanhGiaChiTiet")]
    public class LichSuKetQuaDanhGiaChiTiet : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long CanBoDanhGiaId { get; set; }
        public long CanBoId { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long LicSuPhieuDanhGiaChiTietTruocDoId { set; get; }
        public int TrangThai { set; get; }
        public long ThangDiemChiTietId { set; get; }
        public int CapDanhGia { set; get; } 
        public DateTime NgayDanhGia { set; get; }
        public bool IsKetQuaCuoiCung { set; get; }
        public long ChucVuCanBoDanhGiaId { set; get; }
        public int LoaiChucVuDanhGia { set; get; } // AppConsts.LoaiHinhChucVu
        public bool KetQuaTongHop { set; get; }
        public int CapDoDanhGia { set; get; }
    }
}
