﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("KetQuaDanhGiaChiTiet")]
    public class KetQuaDanhGiaChiTiet : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }

        public long PhieuDanhGiaId { set; get; }
        public long PhieuDanhGiaCanBoId { set; get; }
        public long TieuChiId { set; get; }
        public float? Diem { set; get; }
        public string NhanXet { set; get; }

        public long LichSuKetQuaDanhGiaChiTietId { set; get; }
    }
}
