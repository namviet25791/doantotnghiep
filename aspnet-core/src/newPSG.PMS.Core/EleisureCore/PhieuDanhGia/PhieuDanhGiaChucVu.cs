﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("PhieuDanhGiaChucVu")]
    public class PhieuDanhGiaChucVu : FullAuditedEntity<long>, IMayHaveTenant
    { 
        public virtual int? TenantId { get; set; }  
        public long ChucVuId { set; get; }
        public long PhieuDanhGiaId { set; get; }
        public DateTime? HieuLucTuNgay { set; get; }
        public DateTime? HieuLucToiNgay { set; get; }
    }
}
