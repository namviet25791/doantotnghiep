﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("TuKhoaBieuMauBaoCao")]
    public class TuKhoaBieuMauBaoCao : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; } 
        public long BieuMauBaoCaoId { set; get; }
        public long TuKhoaId { set; get; } 
    }
}
