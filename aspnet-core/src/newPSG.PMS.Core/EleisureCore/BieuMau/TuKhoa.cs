﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("TuKhoa")]
    public class TuKhoa : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; } 
        public string Ma { set; get; }
        public string MoTa { set; get; } 
    }
}
