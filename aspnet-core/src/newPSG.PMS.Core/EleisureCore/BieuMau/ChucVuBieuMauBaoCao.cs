﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("ChucVuBieuMauBaoCao")]
    public class ChucVuBieuMauBaoCao : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long ChucVuId { set; get; }
        public long BIeuMauBaoCaoId { set; get; } 
    }
}
