﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("Tinh")]
    public class Tinh : Entity<string>
    {
        [StringLength(256)]
        public string Ten { get; set; }
        [StringLength(256)]
        public string Cap { get; set; }
    }
}
