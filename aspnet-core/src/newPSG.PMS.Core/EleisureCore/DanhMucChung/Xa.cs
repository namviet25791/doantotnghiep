﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("Xa")]
    public class Xa : Entity<string>
    {
        [StringLength(256)]
        public string Ten { get; set; }
        [StringLength(256)]
        public string Cap { get; set; }
        [StringLength(20)]
        public string HuyenId { get; set; }
    }
}
