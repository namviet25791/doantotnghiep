﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("DonVi")]
    public class DonVi : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public string Ten { set; get; }
        public long? DonViQuanLyId { set; get; }
        public int LoaiDonViId { set; get; }
        public int CapDonViId { set; get; }
        public int KhoiId { set; get; }
        public int KhoiTruongHocId { set; get; }

        public string TinhId { set; get; }
        public string HuyenId { set; get; }
        public string XaId { set; get; }
    }
}
