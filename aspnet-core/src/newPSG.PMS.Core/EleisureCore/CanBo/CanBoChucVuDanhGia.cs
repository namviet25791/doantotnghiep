﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("CanBoChucVuDanhGia")]
    public class CanBoChucVuDanhGia : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuCanBoId { set; get; }
        public long ChucVuDanhGiaId { set; get; } // chức vụ sẽ đánh giá cán bộ
        public int ThuTu { set; get; } 
        public bool NguoiCham {set;get;}
        public long? CanBoDanhGiaId { set;get;} // nếu tồn tại cán bọ này thì phiếu của nhân viên sẽ gửi tới cán bộ này

    }
}
