﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("CanBoChucVu")]
    public class CanBoChucVu : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }
        public long CanBoId { set; get; }
        public long ChucVuId { set; get; }
        public long ChucVuChinh { set; get; } 
    }
} 