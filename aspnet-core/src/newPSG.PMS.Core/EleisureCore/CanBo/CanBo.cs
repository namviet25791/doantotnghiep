﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace newPSG.PMS.EntityDB
{
    [Table("CanBo")]
    public class CanBo : FullAuditedEntity<long>, IMayHaveTenant
    {
        public virtual int? TenantId { get; set; }  
        public long DonViId { set; get; }
        public long UserId { set; get; } 

        public string Address { set; get; }
        public string TinhId { set; get; }
        public string HuyenId { set; get; }
        public string XaId { set; get; }
        public int LoaiHopDong { set; get; }
    }
}
