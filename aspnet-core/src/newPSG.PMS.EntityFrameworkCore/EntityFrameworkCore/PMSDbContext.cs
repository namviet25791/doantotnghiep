﻿using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Authorization.Roles;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Chat;
using newPSG.PMS.Editions;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Friendships;
using newPSG.PMS.MultiTenancy;
using newPSG.PMS.MultiTenancy.Accounting;
using newPSG.PMS.MultiTenancy.Payments;
using newPSG.PMS.Storage;

namespace newPSG.PMS.EntityFrameworkCore
{
    public class PMSDbContext : AbpZeroDbContext<Tenant, Role, User, PMSDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */

        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual DbSet<Friendship> Friendships { get; set; }

        public virtual DbSet<ChatMessage> ChatMessages { get; set; }

        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }

        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }

        public virtual DbSet<Invoice> Invoices { get; set; }

        public virtual DbSet<PersistedGrantEntity> PersistedGrants { get; set; }
        public virtual DbSet<Tinh> Tinhs { get; set; }
        public virtual DbSet<Xa> Xas { get; set; }
        public virtual DbSet<Huyen> Huyens { get; set; }


        public virtual DbSet<CanBo> CanBos { get; set; }
        public virtual DbSet<CanBoChucVu> CanBoChucVus { get; set; }
        public virtual DbSet<CanBoChucVuDanhGia> CanBoChucVuDanhGias { get; set; }


        public virtual DbSet<ChucVu> ChucVus { get; set; }
        public virtual DbSet<ChucVuDanhGia> ChucVuDanhGias { get; set; }
        public virtual DbSet<BaoCaoChucVu> BaoCaoChucVus { get; set; }
        public virtual DbSet<BaoCao> BaoCaos { get; set; }
        public virtual DbSet<BaoCaoNhomDoiTuong> BaoCaoNhomDoiTuongs { get; set; }
        public virtual DbSet<BaoCaoNhomDoiTuongChiTiet> BaoCaoNhomDoiTuongChiTiets { get; set; }


        public virtual DbSet<DonVi> DonVis { get; set; }
        public virtual DbSet<CapDonVi> CapDonVis { get; set; }
        public virtual DbSet<Khoi> Khois { get; set; }
        public virtual DbSet<LoaiDonVi> LoaiDonVis { get; set; }
        public virtual DbSet<DonViChuyenTrach> DonViChuyenTrachs { get; set; }

        public virtual DbSet<PhieuDanhGia> PhieuDanhGias { get; set; }
        public virtual DbSet<PhieuDanhGiaCanBo> PhieuDanhGiaCanBos { get; set; }
        public virtual DbSet<PhieuDanhGiaChucVu> PhieuDanhGiaChucVus { get; set; }
        public virtual DbSet<TieuChiDanhGia> TieuChiDanhGias { get; set; }
        public virtual DbSet<ThongTinDanhGiaKeTiepTemp> ThongTinDanhGiaKeTiepTemps { get; set; }


        public virtual DbSet<KetQuaDanhGiaChiTiet> KetQuaDanhGiaChiTiets { get; set; }

        public virtual DbSet<LichSuKetQuaDanhGiaChiTiet> LichSuKetQuaDanhGiaChiTiets { get; set; }

        public virtual DbSet<ThangDiem> ThangDiems { get; set; }
        public virtual DbSet<ThangDiemChiTiet> ThangDiemChiTiets { get; set; }

        public virtual DbSet<ChucVuBieuMauBaoCao> ChucVuBieuMauBaoCaos { set; get; }
        public virtual DbSet<BieuMauBaoCao> BieuMauBaoCaos { set; get; }
        public virtual DbSet<TuKhoa> TuKhoas { set; get; }
        public virtual DbSet<TuKhoaBieuMauBaoCao> TuKhoaBieuMauBaoCaos { set; get; }
        public virtual DbSet<BaoCaoChucVuCauHinh> BaoCaoChucVuCauHinhs { set; get; }
        //public virtual DbSet<BaoCaoChucVuCauHinhChiTiet> BaoCaoChucVuCauHinhChiTiets { set; get; }
        public virtual DbSet<ThanhVienNhomChucVu> ThanhVienNhomChucVus { set; get; }
        
        public PMSDbContext(DbContextOptions<PMSDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
         
    }
}
