﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Net.Mail;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using newPSG.PMS.Configuration.Host.Dto;

namespace newPSG.PMS.Configuration
{
    public abstract class SettingsAppServiceBase : PMSAppServiceBase
    {
        private readonly IEmailSender _emailSender;

        protected SettingsAppServiceBase(
            IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        #region Send Test Email
        public async Task SendTestEmail(SendTestEmailInput input)
        {
            input.BodyTemplate = new List<KeyValuePair<string, string>>();
            var key1 = new KeyValuePair<string, string>("<%FullName%>", "Nguyễn Văn A");
            var key2 = new KeyValuePair<string, string>("<%NoiDung%>", "Trịnh Văn B đã gửi phiếu đánh giá, click link dưới để đánh giá.");
            var key3 = new KeyValuePair<string, string>("<%LinkChiTiet%>", "https://ubndthanhtri.e-kpi.vn/");
            input.BodyTemplate.Add(key1);
            input.BodyTemplate.Add(key2); 
            input.BodyTemplate.Add(key3);

            string body = UtilityExtensions.GetContentWidthTemplate("./TemplateBaoCao/email-template/Notification.html",
                input.BodyTemplate);

            try
            {
                var host = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Host, AbpSession.GetTenantId());
                var port = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Port, AbpSession.GetTenantId());
                var sendfrom = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.UserName, AbpSession.GetTenantId());

                var smtpPassword = await SettingManager.GetSettingValueForTenantAsync(EmailSettingNames.Smtp.Password, AbpSession.GetTenantId());
                var emailpass = SimpleStringCipher.Instance.Decrypt(smtpPassword);

                var ssl = SettingManager.GetSettingValueForTenant<bool>(EmailSettingNames.Smtp.EnableSsl, AbpSession.GetTenantId());


                var client = new SmtpClient(host, int.Parse(port));
                client.EnableSsl = ssl;
                client.Credentials = new System.Net.NetworkCredential(sendfrom, emailpass);

                var message = new MailMessage();
                message.From = new MailAddress(sendfrom, "e-KPI");
                message.Subject = "Thông báo từ e-KPI";

                message.To.Add(new MailAddress(input.EmailAddress));
                message.Body = body;

                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;

                var mailThread = new Thread(new ThreadStart(() =>
                {
                    client.SendMailAsync(message);
                }));

                mailThread.Start();
            }
            catch (System.Exception ex)
            {

                throw;
            }
        }
        #endregion

     
    }
}
