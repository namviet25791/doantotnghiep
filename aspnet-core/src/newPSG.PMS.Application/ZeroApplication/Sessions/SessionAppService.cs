﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Configuration;
using newPSG.PMS.Editions;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Sessions.Dto;
using newPSG.PMS.UiCustomization;

namespace newPSG.PMS.Sessions
{
    public class SessionAppService : PMSAppServiceBase, ISessionAppService
    {
        private readonly IUiThemeCustomizerFactory _uiThemeCustomizerFactory;
        private readonly IRepository<CanBoChucVu, long> _canboChucVuRepos;
        private readonly IRepository<CanBo, long> _canboRepos;
        public SessionAppService(IUiThemeCustomizerFactory uiThemeCustomizerFactory,
            IRepository<CanBoChucVu, long> canboChucVuRepos,
            IRepository<CanBo, long> canboRepos)
        {
            _uiThemeCustomizerFactory = uiThemeCustomizerFactory;
            _canboChucVuRepos = canboChucVuRepos;
            _canboRepos = canboRepos;
        }

    
        private long GethucVuChinhId(long canBoId)
        {
            var chucVuChinh = _canboChucVuRepos.FirstOrDefault(x => x.CanBoId == canBoId && x.ChucVuChinh == 1);
            if (chucVuChinh != null)
                return chucVuChinh.ChucVuId;

            return -1;
        }
        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                Application = new ApplicationInfoDto
                {
                    Version = AppVersionHelper.Version,
                    ReleaseDate = AppVersionHelper.ReleaseDate,
                    Features = new Dictionary<string, bool>(),
                    Currency = PMSConsts.Currency,
                    CurrencySign = PMSConsts.CurrencySign,
                    AllowTenantsToChangeEmailSettings = PMSConsts.AllowTenantsToChangeEmailSettings
                }
            };

            var uiCustomizer = await _uiThemeCustomizerFactory.GetCurrentUiCustomizer();
            output.Theme = await uiCustomizer.GetUiSettings();

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = ObjectMapper
                    .Map<TenantLoginInfoDto>(await TenantManager
                        .Tenants
                        .Include(t => t.Edition)
                        .FirstAsync(t => t.Id == AbpSession.GetTenantId()));
            }

            if (AbpSession.UserId.HasValue)
            {
                output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
            }
            var canBo = _canboRepos.FirstOrDefault(x => x.UserId == (long)AbpSession.UserId);
            output.CanBo = new CanBoInfo();
            if (canBo != null)
            {
                output.CanBo.CanBoId = canBo.Id;
                output.CanBo.DonViId = canBo.DonViId;
                output.CanBo.ChucVuChinhId = GethucVuChinhId(canBo.Id);               
            }
            output.CanBo.MacDinhTinhId = await SettingManager.GetSettingValueAsync(AppSettings.TenantManagement.TinhMacDinh);
            output.CanBo.MacDinhHuyenId = await SettingManager.GetSettingValueAsync(AppSettings.TenantManagement.HuyenMacDinh);

            if (output.Tenant == null)
            {
                return output;
            }

            if (output.Tenant.Edition != null)
            {
                output.Tenant.Edition.IsHighestEdition = IsEditionHighest(output.Tenant.Edition.Id);
            }

            output.Tenant.SubscriptionDateString = GetTenantSubscriptionDateString(output);
            output.Tenant.CreationTimeString = output.Tenant.CreationTime.ToString("d");


            
            return output;

        }

        private bool IsEditionHighest(int editionId)
        {
            var topEdition = GetHighestEditionOrNullByMonthlyPrice();
            if (topEdition == null)
            {
                return false;
            }

            return editionId == topEdition.Id;
        }

        private SubscribableEdition GetHighestEditionOrNullByMonthlyPrice()
        {
            var editions = TenantManager.EditionManager.Editions;
            if (editions == null || !editions.Any())
            {
                return null;
            }

            return editions.Cast<SubscribableEdition>()
                .OrderByDescending(e => e.MonthlyPrice)
                .FirstOrDefault();
        }

        private string GetTenantSubscriptionDateString(GetCurrentLoginInformationsOutput output)
        {
            return output.Tenant.SubscriptionEndDateUtc == null
                ? L("Unlimited")
                : output.Tenant.SubscriptionEndDateUtc?.ToString("d");
        }

        public async Task<UpdateUserSignInTokenOutput> UpdateUserSignInToken()
        {
            if (AbpSession.UserId <= 0)
            {
                throw new Exception(L("ThereIsNoLoggedInUser"));
            }

            var user = await UserManager.GetUserAsync(AbpSession.ToUserIdentifier());
            user.SetSignInToken();
            return new UpdateUserSignInTokenOutput
            {
                SignInToken = user.SignInToken,
                EncodedUserId = Convert.ToBase64String(Encoding.UTF8.GetBytes(user.Id.ToString())),
                EncodedTenantId = user.TenantId.HasValue
                    ? Convert.ToBase64String(Encoding.UTF8.GetBytes(user.TenantId.Value.ToString()))
                    : ""
            };
        }
    }
}