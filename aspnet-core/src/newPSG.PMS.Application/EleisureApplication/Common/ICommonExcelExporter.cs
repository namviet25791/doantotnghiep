using System.Collections.Generic;
using newPSG.PMS.Authorization.Users.Dto;
using newPSG.PMS.Dto;
using newPSG.PMS.EleisureApplicationShared;

namespace newPSG.PMS.Authorization.Users.Exporting
{
    public interface ICommonExcelExporter
    {
        FileDto ExportToFileThongKeTinhTrangPhieuDanhGiaCanBo(List<ThongKeCanBoDto> canBoDto);
    }
}