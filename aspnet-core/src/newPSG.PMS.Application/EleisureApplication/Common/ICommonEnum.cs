﻿using System;
using System.Collections.Generic;
using System.Text;
using static newPSG.PMS.CommonENum;

namespace newPSG.PMS.EleisureApplication
{
    public interface ICommonEnum
    {
        List<EnumObj> GetCapDonViEnum();
        List<EnumObj> GetKhoiHanhChinhEnum();
        List<EnumObj> GetKhoiTruongHocEnum();
        List<EnumObj> GetLoaiNhomDoiTuongEnum();
    }
}
