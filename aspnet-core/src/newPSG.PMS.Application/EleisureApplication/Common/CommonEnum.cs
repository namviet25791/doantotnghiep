﻿using System;
using System.Collections.Generic;
using System.Text;
using static newPSG.PMS.CommonENum;

namespace newPSG.PMS.EleisureApplication
{
   public class CommonEnum : PMSAppServiceBase, ICommonEnum
    {
        public List<EnumObj> GetCapDonViEnum()
        {
            return CommonENum.EnumToList(typeof(AppConsts.CapDonVi));
        }
        public List<EnumObj> GetKhoiHanhChinhEnum()
        {
            return CommonENum.EnumToList(typeof(AppConsts.KhoiHanhChinh));
        }
        public List<EnumObj> GetKhoiTruongHocEnum()
        {
            return CommonENum.EnumToList(typeof(AppConsts.KhoiTruongHoc));
        }
        public List<EnumObj> GetLoaiNhomDoiTuongEnum()
        {
            return CommonENum.EnumToList(typeof(AppConsts.LoaiNhomDoiTuong));
        }
        
    }
}
