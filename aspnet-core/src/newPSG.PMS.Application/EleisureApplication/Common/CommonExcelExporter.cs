﻿using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Collections.Extensions;
using Abp.Runtime.Session;
using Abp.Timing.Timezone;
using newPSG.PMS.Authorization.Users.Dto;
using newPSG.PMS.DataExporting.Excel.EpPlus;
using newPSG.PMS.Dto;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.Storage;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace newPSG.PMS.Authorization.Users.Exporting
{
    public class CommonExcelExporter : EpPlusExcelExporterBase, ICommonExcelExporter
    {
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly IAbpSession _abpSession;

        public CommonExcelExporter(
            ITimeZoneConverter timeZoneConverter,
            IAbpSession abpSession,
            ITempFileCacheManager tempFileCacheManager)
            : base(tempFileCacheManager)
        {
            _timeZoneConverter = timeZoneConverter;
            _abpSession = abpSession;
        }

        public FileDto ExportToFileThongKeTinhTrangPhieuDanhGiaCanBo(List<ThongKeCanBoDto> canBoListDto)
        {
            string fileName = "ThongKeTinhTrangPhieuDanhGiaCanBo" + DateTime.Now.Date + ".xlsx";

            return CreateExcelPackage(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.Workbook.Worksheets.Add(L("Cán bộ"));
                    sheet.OutLineApplyStyle = true;

                    AddHeader(
                        sheet,
                        "STT",
                        "Tên cán bộ",
                        "Chức vụ",
                        "Đơn vị",
                        "Số điện thoại",
                        "Trạng thái"
                        );

                    AddObjects(
                        sheet, 2, canBoListDto,
                        _ => _.Stt,
                        _ => _.TenCanBo,
                        _ => _.TenChucVu,
                        _ => _.PhongBan,
                        _ => _.SoDienThoai,
                        _ => _.TenTrangThai
                        );

                    var colum1 = sheet.Column(1);
                    colum1.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    var colum3 = sheet.Column(5);
                    colum3.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;


                    sheet.Cells[1, 1, 1, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    for (var i = 1; i <= 6; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }
                });
        }
    }
}
