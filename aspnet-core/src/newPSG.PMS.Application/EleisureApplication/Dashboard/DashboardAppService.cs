﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Notifications;
using Abp.Runtime.Caching;
using Abp.Threading;
using Microsoft.AspNetCore.Identity;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Authorization.Users.Exporting;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static newPSG.PMS.AppConsts;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
   public partial class DashboardAppService : PMSAppServiceBase, IDashboard
    {
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhGiaCanBoRepos;
        private readonly IRepository<PhieuDanhGia, long> _phieuDanhGiaRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<TieuChiDanhGia, long> _tieuChiDanhGiaRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaChiTietRepos;
        private readonly IRepository<DonViChuyenTrach, long> _donViChuyenTrachRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly ICache _mainCache;
        private readonly INotificationPublisher _notificationPublisher;
        private readonly ICommonExcelExporter _iCommonExcelExporter;
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly UserManager _userManager;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canBoChucVuDanhGiaRepos;
        private readonly IRepository<ChucVuDanhGia, long> _chucVuDanhGiaRepos;
        private readonly IRepository<DonVi, long> _donViRepos;
        public DashboardAppService(
            IIocResolver iocResolver,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhGiaCanBoRepos,
             IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
              IRepository<ThangDiem, long> thangDiemRepos,
               IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
               IRepository<TieuChiDanhGia, long> tieuChiDanhGiaRepos,
               IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaChiTietRepos,
               IRepository<PhieuDanhGia, long> phieuDanhGiaRepos,
               INotificationPublisher notificationPublisher,
               IRepository<DonViChuyenTrach, long> donViChuyenTrachRepos,
               IRepository<CanBo, long> canBoRepos,
                UserManager userManager,
                IRepository<ChucVu, long> chucVuRepos,
                IRepository<CanBoChucVu, long> canBoChucVuRepos,
                IRepository<CanBoChucVuDanhGia, long> canBoChucVuDanhGiaRepos,
                IRepository<ChucVuDanhGia, long> chucVuDanhGiaRepos,
                IRepository<DonVi, long> donViRepos,
                ICommonExcelExporter iCommonExcelExporter)
        {
            _iocResolver = iocResolver;
            _phieuDanhGiaCanBoRepos = phieuDanhGiaCanBoRepos;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _tieuChiDanhGiaRepos = tieuChiDanhGiaRepos;
            _lichSuKetQuaDanhGiaChiTietRepos = lichSuKetQuaDanhGiaChiTietRepos;
            _phieuDanhGiaRepos = phieuDanhGiaRepos;
            _notificationPublisher = notificationPublisher;
            _donViChuyenTrachRepos = donViChuyenTrachRepos;
            _canBoRepos = canBoRepos;
            _userManager = userManager;
            _chucVuRepos = chucVuRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
            _canBoChucVuDanhGiaRepos = canBoChucVuDanhGiaRepos;
            _chucVuDanhGiaRepos = chucVuDanhGiaRepos;
            _donViRepos = donViRepos;
            _iCommonExcelExporter = iCommonExcelExporter;
        }

        public List<DiemXepLoaiTheoThangDto> Dashboard_DiemXepLoaiTheoThang(DashboardInput input)
        {
            var lstData = new List<DiemXepLoaiTheoThangDto>();

            int yearInput = Convert.ToInt32(input.Date);
            int maxMonth = 12;

            DateTime currentDate = DateTime.Now.Date;
            if (yearInput == currentDate.Year)
            {
                maxMonth = currentDate.Month;
            }

            var query = from item in _phieuDanhGiaCanBoRepos.GetAll()
                        where (item.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh && item.CanBoId == input.CanBoId
                         && item.ChucVuId == input.ChucVuId
                         && (item.Thang <= maxMonth && item.Nam == yearInput))
                        join ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && x.IsKetQuaCuoiCung) on item.Id equals ls.PhieuDanhGiaCanBoId
                        join loaiThangDiem in _thangDiemChiTietRepos.GetAll() on ls.ThangDiemChiTietId equals loaiThangDiem.Id
                        join ktpdgct in _ketQuaDanhGiaChiTietRepos.GetAll() on ls.Id equals ktpdgct.LichSuKetQuaDanhGiaChiTietId

                        select new
                        {
                            PhieuDanhGiaCanBoId = item.Id,
                            Diem = ktpdgct.Diem,
                            Thang = item.Thang,
                            PhieuDanhGiaId = item.PhieuDanhGiaId,
                            NhanXet = ktpdgct.NhanXet,
                            LoaiThangDiemChiTiet= loaiThangDiem.LoaiThangDiemChiTiet,
                            TieuChiId = ktpdgct.TieuChiId,
                        };

            lstData = query.GroupBy(x => new { x.PhieuDanhGiaCanBoId }).Select(y => new DiemXepLoaiTheoThangDto
            {
                Thang = y.FirstOrDefault() != null ? y.FirstOrDefault().Thang : 0,
                Diem = y.Sum(s => s.Diem),
                DanhGia = y.Where(x => x.PhieuDanhGiaId != 0 && x.TieuChiId == -1).FirstOrDefault() != null ? y.Where(x => x.PhieuDanhGiaId != 0 && x.TieuChiId == -1).FirstOrDefault().NhanXet : "",
                LoaiThangDiemChiTietEnum =(LoaiThangDiemChiTiet) (y.Where(x => x.PhieuDanhGiaId != 0 && x.TieuChiId == -1).FirstOrDefault() != null ? y.Where(x => x.PhieuDanhGiaId != 0 && x.TieuChiId == -1).FirstOrDefault().LoaiThangDiemChiTiet:5),
                PhieuDanhGiaId = y.FirstOrDefault() != null ? y.FirstOrDefault().PhieuDanhGiaId : 0,
            }).OrderBy(x => x.Thang).ToList();

            return lstData;
        }

        public List<PhieuDanhGiaChiTietDto> TinhTongDiemCapCha(List<PhieuDanhGiaChiTietDto> arrs, long? parentId = null)
        {
            var arrByParentIds = arrs.Where(item => item.TieuChiChaId == parentId).ToList();

            arrByParentIds.ForEach(item =>
            {
                var arr2s = arrs.Where(item2 => item2.TieuChiChaId == item.Id).ToList();
                item.IsCapCha = true;
                item.TongDiemLanhDaoCham = item.DiemLanhDaoCham;

                if (arr2s.Count == 0)
                {
                    item.IsCapCha = false;
                    return;
                }

                TinhTongDiemCapCha(arrs, item.Id);
                item.TongDiemLanhDaoCham = arr2s.Sum(m => m.TongDiemLanhDaoCham);
            });

            return arrs;
        }

        public static IList<TieuChiDanhGiaDto> GetAllTietChiCapThapNhat(IList<TieuChiDanhGiaDto> arrs, long? parentId = null)
        {
            var arrByParentIds = arrs.Where(item => item.TieuChiChaId == parentId)
                .Select(x => new TieuChiDanhGiaDto
                {
                    Id = x.Id,
                    Ten = x.Ten,
                    DiemToiDa = x.DiemToiDa,
                    TieuChiChaId = x.TieuChiChaId
                }).ToList();
            var results = new List<TieuChiDanhGiaDto>();

            arrByParentIds.ForEach(item =>
            {
                var arr2s = arrs.Where(item2 => item2.TieuChiChaId == item.Id).ToList();

                if (arr2s.Count == 0)
                {
                    results.Add(item);

                    return;
                }

                var arr3s = GetAllTietChiCapThapNhat(arrs, item.Id);
                results = results.Concat(arr3s).ToList();
            });

            return results;
        }
        public List<PhanBoDiemTheoNhomTieuChiDto> Dashboard_PhanBoDiemTheoNhomTieuChi(DashboardInput input)
        {
            var lstData = new List<PhanBoDiemTheoNhomTieuChiDto>();
            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.Date))
            {
                String inputString = input.Date;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }
            int month = dtStartDate.Month;
            int year = dtStartDate.Year;

            var phieuDanhGia = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == input.CanBoId
            && x.ChucVuId == input.ChucVuId && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
            && x.Thang == month && x.Nam == year).OrderByDescending(x => x.Thang).FirstOrDefault();

            if (phieuDanhGia != null)
            {

                var lstTieuChi = _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGia.PhieuDanhGiaId)
                    .Select(x => new TieuChiDanhGiaDto
                    {
                        Id = x.Id,
                        TieuChiChaId = x.TieuChiChaId,
                        Ten = x.Ten
                    }).ToList();

                var lstTieuChiCha = lstTieuChi.Where(x => x.TieuChiChaId == null);

                var ketQuaDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                     from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                     where ls.PhieuDanhGiaCanBoId == phieuDanhGia.Id
                                     && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                                     && kq.Diem != null
                                     select new
                                     {
                                         TieuChiId = kq.TieuChiId,
                                         Diem = kq.Diem.Value
                                     });



                foreach (var item in lstTieuChiCha)
                {
                    var lstTieuChiTinhTiem = GetAllTietChiCapThapNhat(lstTieuChi, item.Id);

                    var diemLanhDaoCham = ketQuaDanhGia.Where(x => lstTieuChiTinhTiem.Any(d => d.Id == x.TieuChiId)).ToList().Sum(x => x.Diem);

                    var obj = new PhanBoDiemTheoNhomTieuChiDto();
                    obj.NhomTieuChi = Regex.Replace(item.Ten, "<[^>]*>", string.Empty).ToLower();
                    obj.Diem = diemLanhDaoCham;
                    lstData.Add(obj);
                }
            }

            return lstData;
        }
        public List<TyLeHoanThanhNhomTieuChiDto> Dashboard_TyLeHoanThanhTheoNhomTieuChi(DashboardInput input)
        {
            var lstData = new List<TyLeHoanThanhNhomTieuChiDto>();

            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.Date))
            {
                String inputString = input.Date;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }
            int month = dtStartDate.Month;
            int year = dtStartDate.Year;

            var phieuDanhGia = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
                    x.CanBoId == input.CanBoId
                    && x.ChucVuId == input.ChucVuId
                    && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                    && x.Thang == month
                    && x.Nam == year).FirstOrDefault();

            if (phieuDanhGia != null)
            {

                var lstTieuChi = _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGia.PhieuDanhGiaId)
                    .Select(x => new TieuChiDanhGiaDto
                    {
                        Id = x.Id,
                        TieuChiChaId = x.TieuChiChaId,
                        Ten = x.Ten,
                        DiemToiDa = x.DiemToiDa
                    }).ToList();

                var lstTieuChiCha = lstTieuChi.Where(x => x.TieuChiChaId == null);

                var ketQuaDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                     from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                                     where ls.PhieuDanhGiaCanBoId == phieuDanhGia.Id
                                     && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                                     select new
                                     {
                                         TieuChiId = kq.TieuChiId,
                                         Diem = kq.Diem.Value
                                     });

                foreach (var item in lstTieuChiCha)
                {
                    var lstTieuChiTinhTiem = GetAllTietChiCapThapNhat(lstTieuChi, item.Id);

                    var diemLanhDaoCham = ketQuaDanhGia.Where(x => lstTieuChiTinhTiem.Any(d => d.Id == x.TieuChiId)).ToList().Sum(x => x.Diem);

                    var obj = new TyLeHoanThanhNhomTieuChiDto();
                    obj.DiemToiDaPhanTram = 100;
                    obj.NhomTieuChi = Regex.Replace(item.Ten, "<[^>]*>", string.Empty).ToLower();
                    obj.Diem = diemLanhDaoCham;
                    obj.DiemToiDa = item.DiemToiDa;
                    obj.DiemPhanTram = (obj.Diem / obj.DiemToiDa) * obj.DiemToiDaPhanTram;

                    lstData.Add(obj);
                }
            }

            return lstData;
        }

        public List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiTienBo(DashboardInput input)
        {
            var lstData = new List<TopDanhSachTieuChiDto>();

            var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
                    x.CanBoId == input.CanBoId
                    && x.ChucVuId == input.ChucVuId
                    && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                .OrderByDescending(x => x.Nam)
                .ThenByDescending(x => x.Thang).Take(2);

            if (phieuDanhGiaCanBo.Count() == 1)
            {
                var pdgcb = phieuDanhGiaCanBo.FirstOrDefault();

                var lstKetQuaDanhGia = _ketQuaDanhGiaChiTietRepos.GetAll()
                    .Where(x => x.PhieuDanhGiaCanBoId == pdgcb.Id)
                    .ToList();

                lstData = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                           from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                           from tc in _tieuChiDanhGiaRepos.GetAll().Where(x => x.Id == kq.TieuChiId)
                           where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.FirstOrDefault().Id
                           && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.Diem != null
                           select new TopDanhSachTieuChiDto
                           {
                               TieuChiId = kq.TieuChiId,
                               TenTieuChi = tc.Ten,
                               DiemChenhLech = kq.Diem.Value
                           }).OrderByDescending(x => x.DiemChenhLech).Take(6).ToList();
            }
            else if (phieuDanhGiaCanBo.Count() == 2)
            {
                var pdgcb1 = phieuDanhGiaCanBo.FirstOrDefault();
                var pdgcb2 = phieuDanhGiaCanBo.LastOrDefault();
                var lstKetQua1 = new List<KetQuaDanhGiaChiTietDto>();
                var lstKetQua2 = new List<KetQuaDanhGiaChiTietDto>();

                lstKetQua1 = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                              from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                              where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.FirstOrDefault().Id
                              && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                              select new KetQuaDanhGiaChiTietDto
                              {
                                  TieuChiId = kq.TieuChiId,
                                  Diem = kq.Diem.Value
                              }).ToList();

                if (pdgcb1.PhieuDanhGiaId == pdgcb2.PhieuDanhGiaId)
                {
                    lstKetQua2 = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                  from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                                  where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.LastOrDefault().Id
                                  && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                                  select new KetQuaDanhGiaChiTietDto
                                  {
                                      TieuChiId = kq.TieuChiId,
                                      Diem = kq.Diem.Value
                                  }).ToList();
                }

                var query = (from kq1 in lstKetQua1
                             from kq2 in lstKetQua2.Where(x => x.TieuChiId == kq1.TieuChiId)
                             select new
                             {
                                 TieuChiId = kq1.TieuChiId,
                                 DiemChenhLech = kq1.Diem - kq2.Diem
                             });

                var querygroup = query.GroupBy(x => x.TieuChiId)
                     .Select(g => new
                     {
                         TieuChiId = g.Key,
                         DiemChenhLech = g.FirstOrDefault().DiemChenhLech
                     }).OrderByDescending(x => x.DiemChenhLech).Take(6).ToList();


                lstData = (from data in querygroup
                           join tc in _tieuChiDanhGiaRepos.GetAll() on data.TieuChiId equals tc.Id
                           select new TopDanhSachTieuChiDto
                           {
                               TieuChiId = data.TieuChiId,
                               TenTieuChi = tc.Ten,
                               DiemChenhLech = data.DiemChenhLech
                           }).ToList();
            }
            return lstData;
        }

        public List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiDiXuong(DashboardInput input)
        {
            var lstData = new List<TopDanhSachTieuChiDto>();

            var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
                    x.CanBoId == input.CanBoId
                    && x.ChucVuId == input.ChucVuId
                    && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                .OrderByDescending(x => x.Nam)
                .ThenByDescending(x => x.Thang).Take(2);

            if (phieuDanhGiaCanBo.Count() == 1)
            {
                var lstKetQuaDanhGia = _ketQuaDanhGiaChiTietRepos.GetAll()
                    .Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.FirstOrDefault().Id)
                    .ToList();


                lstData = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                           from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                           from tc in _tieuChiDanhGiaRepos.GetAll().Where(x => x.Id == kq.TieuChiId)
                           where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.FirstOrDefault().Id
                           && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
                           select new TopDanhSachTieuChiDto
                           {
                               TieuChiId = kq.TieuChiId,
                               TenTieuChi = tc.Ten,
                               DiemChenhLech = kq.Diem.Value
                           }).OrderBy(x => x.DiemChenhLech).Take(6).ToList();
            }
            else if (phieuDanhGiaCanBo.Count() == 2)
            {
                var pdgcb1 = phieuDanhGiaCanBo.FirstOrDefault();
                var pdgcb2 = phieuDanhGiaCanBo.LastOrDefault();
                var lstKetQua1 = new List<KetQuaDanhGiaChiTietDto>();
                var lstKetQua2 = new List<KetQuaDanhGiaChiTietDto>();

                lstKetQua1 = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                              from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                              where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.FirstOrDefault().Id
                              && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                              select new KetQuaDanhGiaChiTietDto
                              {
                                  TieuChiId = kq.TieuChiId,
                                  Diem = kq.Diem.Value
                              }).ToList();


                if (pdgcb1.PhieuDanhGiaId == pdgcb2.PhieuDanhGiaId)
                {
                    lstKetQua2 = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                  from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.Diem != null)
                                  where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.LastOrDefault().Id
                                  && ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && kq.PhieuDanhGiaId != 0
                                  select new KetQuaDanhGiaChiTietDto
                                  {
                                      TieuChiId = kq.TieuChiId,
                                      Diem = kq.Diem.Value
                                  }).ToList();
                }


                var query = (from kq1 in lstKetQua1
                             join kq2 in lstKetQua2 on kq1.TieuChiId equals kq2.TieuChiId
                             select new
                             {
                                 TieuChiId = kq1.TieuChiId,
                                 DiemChenhLech = kq1.Diem - kq2.Diem
                             });

                var querygroup = query.GroupBy(x => x.TieuChiId)
                .Select(g => new
                {
                    TieuChiId = g.Key,
                    DiemChenhLech = g.FirstOrDefault().DiemChenhLech
                }).OrderBy(x => x.DiemChenhLech).Take(6).ToList();

                lstData = (from data in querygroup
                           join tc in _tieuChiDanhGiaRepos.GetAll() on data.TieuChiId equals tc.Id
                           select new TopDanhSachTieuChiDto
                           {
                               TieuChiId = data.TieuChiId,
                               TenTieuChi = tc.Ten,
                               DiemChenhLech = data.DiemChenhLech
                           }).ToList();
            }
            return lstData;
        }
        public List<TopDanhSachTieuChiDto> Dashboard_DanhSachTieuChiHayMatDiem(DashboardInput input)
        {
            var lstData = new List<TopDanhSachTieuChiDto>();

            var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
                x.CanBoId == input.CanBoId
                && x.ChucVuId == input.ChucVuId
                && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
            .OrderByDescending(x => x.Nam)
            .ThenByDescending(x => x.Thang).Take(6);


            var lstPhieuDanhGiaId = phieuDanhGiaCanBo.Select(x => x.PhieuDanhGiaId).Distinct();

            var dsTieuChi = (from p in lstPhieuDanhGiaId
                             from ct in _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == p)
                             select new TieuChiDanhGiaDto
                             {
                                 Id = ct.Id,
                                 Ten = ct.Ten,
                                 DiemToiDa = ct.DiemToiDa,
                                 TieuChiChaId = ct.TieuChiChaId
                             }).ToList();

            var lstTieuChiId = GetAllTietChiCapThapNhat(dsTieuChi);

            var query = (from tc in lstTieuChiId
                         from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.TieuChiId == tc.Id)
                         select new
                         {
                             TieuChiId = tc.Id,
                             TenTieuChi = tc.Ten,
                             DiemToiDa = tc.DiemToiDa,
                             DiemTuCham = kqct.Diem,
                             IsMatDiem = kqct.Diem < tc.DiemToiDa ? 1 : 0,
                         });

            lstData = query.GroupBy(x => x.TieuChiId).Select(y => new TopDanhSachTieuChiDto
            {
                TieuChiId = y.Key,
                TenTieuChi = y.FirstOrDefault().TenTieuChi,
                SoLanMatDiem = y.Count(c => c.IsMatDiem == 1)
            }).OrderByDescending(o => o.SoLanMatDiem).Take(6).ToList();

            return lstData;
        }


        #region Dashboard don vi 
        public List<KetQuaDanhGiaCacThangDto> Dashboard_KetQuaDanhGiaCacThang(DashboardInput input)
        {
            var lstData = new List<KetQuaDanhGiaCacThangDto>();

            int yearInput = Convert.ToInt32(input.Date);
            int maxMonth = 12;

            DateTime currentDate = DateTime.Now.Date;
            if (yearInput == currentDate.Year)
            {
                maxMonth = currentDate.Month;
            }

            var lstCanBoCanQuanLy = GetDanhSachCanBoThucDonViQuanLy(input);
            var lstCanBo = (from cbId in lstCanBoCanQuanLy
                            from cb in _canBoRepos.GetAll().Where(x => x.Id == cbId.CanBoId)
                            select new
                            {
                                CanBoId = cb.Id,
                                UserId = cb.UserId
                            });

            var query = from cb in lstCanBo
                        from item in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.CanBoId)
                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == item.Id
                        && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && x.IsKetQuaCuoiCung == true)
                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)

                        where (item.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                        && (item.Thang <= maxMonth && item.Nam == yearInput))
                        select new
                        {
                            Thang = item.Thang,
                            LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet
                        };

            lstData = query.GroupBy(x => new { x.Thang }).Select(y => new KetQuaDanhGiaCacThangDto
            {
                Thang = y.Key.Thang,
                HoanThanhXuatSac = y.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count(),
                HoanThanhTot = y.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count(),
                HoanThanh = y.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count(),
                KhongHoanThanh = y.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count()
            }).OrderBy(x => x.Thang).ToList();
            return lstData;
        }

        public List<TyLeThangDiemDto> Dashboard_TyLeThangDiem(DashboardInput input)
        {
            var lstData = new List<TyLeThangDiemDto>();

            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.Date))
            {
                String inputString = input.Date;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }
            int month = dtStartDate.Month;
            int year = dtStartDate.Year;


            var lstCanBoCanQuanLy = GetDanhSachCanBoThucDonViQuanLy(input);
            var lstCanBo = (from cbId in lstCanBoCanQuanLy
                            from cb in _canBoRepos.GetAll().Where(x => x.Id == cbId.CanBoId)
                            select new
                            {
                                CanBoId = cb.Id,
                                UserId = cb.UserId
                            });

            var query = from cb in lstCanBo
                        from item in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.CanBoId)
                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == item.Id
                        && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren && x.IsKetQuaCuoiCung == true)
                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)

                        where (item.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                        && (item.Thang == month && item.Nam == year))
                        select new
                        {
                            LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet
                        };

            lstData = query.GroupBy(x => new { x.LoaiThangDiemChiTiet }).Select(y => new TyLeThangDiemDto
            {
                ThangDiem = GetTenLoaiThangDiem(y.Key.LoaiThangDiemChiTiet),
                SoLuong = y.Count(),
            }).ToList();

            return lstData;
        }

        public string GetTenLoaiThangDiem(int loaiThangDiem)
        {
            string ten = "";
            if (loaiThangDiem == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV)
            {
                ten = "Hoàn thành xuất sắc nhiệm vụ";
            }
            else if (loaiThangDiem == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV)
            {
                ten = "Hoàn thành tốt nhiệm vụ";
            }
            else if (loaiThangDiem == (int)AppConsts.LoaiThangDiemChiTiet.HTNV)
            {
                ten = "Hoàn thành nhiệm vụ";
            }
            else if (loaiThangDiem == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV)
            {
                ten = "Không hoàn thành nhiệm vụ";
            }
            else
            {
                ten = "Khác";
            }
            return ten;
        }

        public List<DsDiemTrungBinhDto> Dashboard_DsCanBoDiemTrungBinhCaoNhatTrongNam(DashboardInput input)
        {
            var lstData = new List<DsDiemTrungBinhDto>();
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            var lstCanBoCanQuanLy = GetDanhSachCanBoThucDonViQuanLy(input);
            var lstCanBo = (from cbId in lstCanBoCanQuanLy
                            from cb in _canBoRepos.GetAll().Where(x => x.Id == cbId.CanBoId)
                            select new
                            {
                                CanBoId = cb.Id,
                                UserId = cb.UserId
                            });

            var dsPhieuDanhGiaCanBoTheoLoaiThangDiem = (from cb in lstCanBo
                                                        from item in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.CanBoId)
                                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == item.Id
                                                        && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                                                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                                        where (item.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                                                       && (item.Thang <= month && item.Nam == year)
                                                       && (tdct.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV
                                                       || tdct.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV)
                                                       && ls.IsKetQuaCuoiCung == true)
                                                        orderby tdct.LoaiThangDiemChiTiet
                                                        select new
                                                        {
                                                            ChucVuId = item.ChucVuId,
                                                            UserId = cb.UserId,
                                                            CanBoId = cb.CanBoId,
                                                            Thang = item.Thang,
                                                            LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                                            PhieuDanhGiaCanBoId = item.Id,
                                                            LichSuId = ls.Id
                                                        });

            var query = (from pdg in dsPhieuDanhGiaCanBoTheoLoaiThangDiem
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                         from u in _userManager.Users.Where(x => x.Id == pdg.UserId)
                         from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == pdg.LichSuId)
                         where kqct.Diem > 0
                         select new
                         {
                             CanBoId = pdg.CanBoId,
                             TenCanBo = u.Surname + " " + u.Name,
                             Diem = kqct.Diem.Value,
                             TenChucVu = cv.Ten
                         });

            lstData = query.GroupBy(x => new { x.CanBoId }).Select(y => new DsDiemTrungBinhDto
            {
                TenChucVu = y.FirstOrDefault().TenChucVu,
                TenCanBo = y.FirstOrDefault().TenCanBo,
                DiemTrungBinh = (y.Sum(x => x.Diem) / month)
            }).OrderBy(x => x.DiemTrungBinh).ToList();

            return lstData;
        }

        public List<DsDiemTrungBinhDto> Dashboard_DsCanBoDiemTrungBinhThapNhatTrongNam(DashboardInput input)
        {
            var lstData = new List<DsDiemTrungBinhDto>();

            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            var lstCanBoCanQuanLy = GetDanhSachCanBoThucDonViQuanLy(input);
            var lstCanBo = (from cbId in lstCanBoCanQuanLy
                            from cb in _canBoRepos.GetAll().Where(x => x.Id == cbId.CanBoId)
                            select new
                            {
                                CanBoId = cb.Id,
                                UserId = cb.UserId
                            });

            var dsPhieuDanhGiaCanBoTheoLoaiThangDiem = (from cb in lstCanBo
                                                        from item in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.CanBoId)
                                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == item.Id
                                                        && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                                                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                                        where (item.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                                                        && (item.Thang <= month && item.Nam == year)
                                                         && (tdct.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV
                                                       || tdct.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV) && ls.IsKetQuaCuoiCung == true)
                                                        orderby tdct.LoaiThangDiemChiTiet descending
                                                        select new
                                                        {
                                                            ChucVuId = item.ChucVuId,
                                                            UserId = cb.UserId,
                                                            CanBoId = cb.CanBoId,
                                                            Thang = item.Thang,
                                                            LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                                            PhieuDanhGiaCanBoId = item.Id,
                                                            LichSuId = ls.Id
                                                        });

            var query = (from pdg in dsPhieuDanhGiaCanBoTheoLoaiThangDiem
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                         from u in _userManager.Users.Where(x => x.Id == pdg.UserId)
                         from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == pdg.LichSuId)
                         where kqct.Diem > 0
                         select new
                         {
                             CanBoId = pdg.CanBoId,
                             TenCanBo = u.Surname + " " + u.Name,
                             Diem = kqct.Diem.Value,
                             TenChucVu = cv.Ten
                         });

            lstData = query.GroupBy(x => new { x.CanBoId }).Select(y => new DsDiemTrungBinhDto
            {
                TenChucVu = y.FirstOrDefault().TenChucVu,
                TenCanBo = y.FirstOrDefault().TenCanBo,
                DiemTrungBinh = (y.Sum(x => x.Diem) / month)
            }).OrderBy(x => x.DiemTrungBinh).ToList();

            return lstData;
        }


        public List<ObjectTemp> GetDanhSachCanBoThucDonViQuanLy(DashboardInput input)
        {
            var lstDonViChuyenTrach = _donViChuyenTrachRepos.GetAll()
                .Where(x => x.CanBoId == input.CanBoId && x.ChucVuId == input.ChucVuId).Select(x => x.DonViId);

            // Lấy ra tất cả các chức vụ cần đánh giá
            var lstCanBoCanDanhGia = new List<long>();
            var lstChucVuCanDanhGia = new List<long>();
            var lstTatCaCanBoCanDanhGia = new List<ObjectTemp>();
            var isChucVuChinh = _canBoChucVuRepos.GetAll().Any(x => x.ChucVuId == input.ChucVuId && x.ChucVuChinh == 1);
            if (isChucVuChinh)
            {
                lstCanBoCanDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDanhGiaId == input.ChucVuId)
                    .Select(x => x.CanBoId).ToList();

                lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                           from cbcv in lstCanBoCanDanhGia.Where(x => x == cb.Id)
                                           select new ObjectTemp
                                           {
                                               CanBoId = cb.Id
                                           }).ToList();
            }
            else
            {
                lstChucVuCanDanhGia = _chucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuThucHienDanhGiaId == input.ChucVuId)
                    .Select(x => x.ChucVuDuocDanhGiaId).ToList();

                lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                           from cv in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
                                           from cbcv in lstChucVuCanDanhGia.Where(x => x == cv.ChucVuId)
                                           select new ObjectTemp
                                           {
                                               CanBoId = cb.Id
                                           }).ToList();
            }

            return lstTatCaCanBoCanDanhGia;
        }

        public List<TyLeHoanThanhPhieuDto> Dashboard_TyLeHoanThanhPhieu(DashboardInput input)
        {
            var lstData = new List<TyLeHoanThanhPhieuDto>();

            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.Date))
            {
                String inputString = input.Date;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }
            int month = dtStartDate.Month;
            int year = dtStartDate.Year;

            var lstDonViChuyenTrach = _donViChuyenTrachRepos.GetAll().Where(x => x.CanBoId == input.CanBoId
            && x.ChucVuId == input.ChucVuId).Select(x => x.DonViId);

            // Lấy ra tất cả các chức vụ cần đánh giá
            var lstCanBoCanDanhGia = new List<long>();
            var lstChucVuCanDanhGia = new List<long>();
            var lstTatCaCanBoCanDanhGia = new List<ObjectTemp>();
            var lstCanBoDaGuiPhieu = new List<ObjectTemp>();
            var isChucVuChinh = _canBoChucVuRepos.GetAll().Any(x => x.ChucVuId == input.ChucVuId && x.ChucVuChinh == 1);
            if (isChucVuChinh)
            {
                lstCanBoCanDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDanhGiaId == input.ChucVuId)
                    .Select(x => x.CanBoId).ToList();

                lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                           from cbcv in lstCanBoCanDanhGia.Where(x => x == cb.Id)
                                           select new ObjectTemp
                                           {
                                               CanBoId = cb.Id,
                                               ChucVuDanhGiaId = cbcv
                                           }).ToList();

                lstCanBoDaGuiPhieu = (from tccbdg in lstTatCaCanBoCanDanhGia
                                      from pdg in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == tccbdg.CanBoId
                                      && x.Thang == month && x.Nam == year).DefaultIfEmpty()
                                      select new ObjectTemp
                                      {
                                          DaHoanThanh = (pdg != null) ? pdg.DaHoanThanh : -1
                                      }).ToList();
            }
            else
            {
                lstChucVuCanDanhGia = _chucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuThucHienDanhGiaId == input.ChucVuId)
                    .Select(x => x.ChucVuDuocDanhGiaId).ToList();

                lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                           from cv in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
                                           from cbcv in lstChucVuCanDanhGia.Where(x => x == cv.ChucVuId)
                                           select new ObjectTemp
                                           {
                                               ChucVuDanhGiaId = cbcv
                                           }).ToList();

                lstCanBoDaGuiPhieu = (from tccbdg in lstTatCaCanBoCanDanhGia
                                      from pdg in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.ChucVuId == tccbdg.ChucVuDanhGiaId
                                       && x.Thang == month && x.Nam == year).DefaultIfEmpty()
                                      select new ObjectTemp
                                      {
                                          DaHoanThanh = (pdg == null) ? pdg.DaHoanThanh : -1
                                      }).ToList();
            }


            var obj1 = new TyLeHoanThanhPhieuDto();
            obj1.Ten = "Hoàn thành";
            obj1.TyLe = lstCanBoDaGuiPhieu.Where(x => x.DaHoanThanh == 1).Count();
            lstData.Add(obj1);

            var obj2 = new TyLeHoanThanhPhieuDto();
            obj2.Ten = "Chưa được đánh giá";
            obj2.TyLe = lstCanBoDaGuiPhieu.Where(x => x.DaHoanThanh == 0).Count();
            lstData.Add(obj2);

            var obj3 = new TyLeHoanThanhPhieuDto();
            obj3.Ten = "Chưa gửi đánh giá";
            obj3.TyLe = lstCanBoDaGuiPhieu.Where(x => x.DaHoanThanh == -1).Count();
            lstData.Add(obj3);

            return lstData;
        }
        #endregion

        public DateTime ConvertDateStringToDate(string date)
        {
            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(date))
            {
                String inputString = date;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }

            return dtStartDate;
        }
    }
}
