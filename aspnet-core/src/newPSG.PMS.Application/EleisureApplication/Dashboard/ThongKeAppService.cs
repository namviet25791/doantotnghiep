﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Notifications;
using Abp.Runtime.Caching;
using Abp.Threading;
using Microsoft.AspNetCore.Identity;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Dto;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static newPSG.PMS.AppConsts;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    public partial class DashboardAppService
    {

        public List<ObjectTemp> ThongKePhieuDanhGiaCanBoCore(ThongKeCanBoInput input)
        {
            DateTime dtStartDate = ConvertDateStringToDate(input.Date);
            int month = dtStartDate.Month;
            int year = dtStartDate.Year;

            var lstDonViChuyenTrach = _donViChuyenTrachRepos.GetAll().Where(x => x.CanBoId == input.CanBoId
                     && x.ChucVuId == input.ChucVuId).Select(x => x.DonViId);

            // Lấy ra tất cả các chức vụ cần đánh giá
            var lstCanBoDaGuiPhieu = new List<ObjectTemp>();

            var lstCanBoCanDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDanhGiaId == input.ChucVuId
            && (x.CanBoDanhGiaId == null || x.CanBoDanhGiaId == input.CanBoId))
                    .Select(x => new {
                        CanBoId = x.CanBoId,
                        ChucVuCanBoId = x.ChucVuCanBoId,
                    });

            var lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                       from cbcv in lstCanBoCanDanhGia.Where(x => x.CanBoId == cb.Id)
                                       select new 
                                       {
                                           CanBoId = cb.Id,
                                           ChucVuCanBoId = cbcv.ChucVuCanBoId,
                                           UserId = cb.UserId,
                                           DonViId = cb.DonViId,
                                       });

            lstCanBoDaGuiPhieu = (from tccbdg in lstTatCaCanBoCanDanhGia
                                  from pdg in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == tccbdg.CanBoId
                                  && x.Thang == month && x.Nam == year).DefaultIfEmpty()
                                  select new ObjectTemp
                                  {
                                      CanBoId = tccbdg.CanBoId,
                                      ChucVuCanBoId = tccbdg.ChucVuCanBoId,
                                      UserId = tccbdg.UserId,
                                      DonViId = tccbdg.DonViId,
                                      DaHoanThanh = (pdg != null) ? pdg.DaHoanThanh : 2
                                  }).ToList();

            return lstCanBoDaGuiPhieu;
        }

        public PagedResultDto<ThongKeCanBoDto> ThongKeTinhTrangPhieuDanhGiaCanBo(ThongKeCanBoInput input)
        {
            var lstCanBoDaGuiPhieu = ThongKePhieuDanhGiaCanBoCore(input);

            var query = (from cb in lstCanBoDaGuiPhieu
                         from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == cb.ChucVuCanBoId)
                         from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                         where (input.TrangThai == -1 || cb.DaHoanThanh == input.TrangThai)
                         select new ThongKeCanBoDto
                         {
                             TenCanBo = u.Surname + " " + u.Name,
                             TenChucVu = cv.Ten,
                             PhongBan = dv.Ten,
                             SoDienThoai = u.PhoneNumber,
                             TrangThai = cb.DaHoanThanh
                         }).OrderBy(x => x.TrangThai);

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ThongKeCanBoDto>(totalCount, items);
        }

        public async Task<FileDto> ExportCanBoToExcel(ThongKeCanBoInput input)
        {
            var lstCanBoDaGuiPhieu = ThongKePhieuDanhGiaCanBoCore(input);

            var query =  (from cb in lstCanBoDaGuiPhieu
                         from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == cb.ChucVuCanBoId)
                         from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                         where (input.TrangThai == -1 || cb.DaHoanThanh == input.TrangThai)
                         select new ThongKeCanBoDto
                         {
                             TenCanBo = u.Surname + " " + u.Name,
                             TenChucVu = cv.Ten,
                             PhongBan = dv.Ten,
                             SoDienThoai = u.PhoneNumber,
                             TrangThai = cb.DaHoanThanh
                         }).OrderBy(x => x.TrangThai).ToList();


            int index = 1;
            foreach (var item in query)
            {
                item.Stt = index++;
                if (item.TrangThai == 1)
                {
                    item.TenTrangThai = "Đã hoàn thành";
                }
                else if (item.TrangThai == 0)
                {
                    item.TenTrangThai = "Đang thực hiện đánh giá";
                }
                else
                {
                    item.TenTrangThai = "Chưa gửi đánh giá";
                }
            }


            return _iCommonExcelExporter.ExportToFileThongKeTinhTrangPhieuDanhGiaCanBo(query);
        }
    }
}
