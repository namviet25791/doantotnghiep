﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Runtime.Security;
using Abp.Threading;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class ChucVuAppService : PMSAppServiceBase, IChucVu
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<ThanhVienNhomChucVu, long> _thanhVienNhomChucVuRepos;
        private readonly ICache _mainCache;
        private readonly IRepository<CanBo, long> _canBo;
        private readonly IRepository<DonVi, long> _donviRepos;
        private readonly IRepository<CanBoChucVu, long> _canboChucVuRepos;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canBoChucVuDanhGiaRepos;
        private readonly IRepository<BaoCaoNhomDoiTuongChiTiet, long> _baoCaoNhomDoiTuongChiTietRepos;
        public ChucVuAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<ChucVu, long> chucVuRepos,
            IRepository<CanBoChucVu, long> canBoChucVuRepos,
            IRepository<ThanhVienNhomChucVu, long> thanhVienNhomChucVuRepos,
            IRepository<CanBo, long> canBo,
            IRepository<DonVi, long> donviRepos,
            IRepository<CanBoChucVu, long> canboChucVuRepos,
            IRepository<CanBoChucVuDanhGia, long> canBoChucVuDanhGiaRepos,
            IRepository<BaoCaoNhomDoiTuongChiTiet, long> baoCaoNhomDoiTuongChiTietRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _chucVuRepos = chucVuRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
            _thanhVienNhomChucVuRepos = thanhVienNhomChucVuRepos;
            _canBo = canBo;
            _donviRepos = donviRepos;
            _canboChucVuRepos = canboChucVuRepos;
            _canBoChucVuDanhGiaRepos = canBoChucVuDanhGiaRepos;
            _baoCaoNhomDoiTuongChiTietRepos = baoCaoNhomDoiTuongChiTietRepos;
            _mainCache = _cacheManager.GetCache("ChucVuAppService");
        }

        //[AbpAuthorize(AppPermissions.Pages_Admin_ChucVu)]

        [AbpAllowAnonymous]
        public PagedResultDto<ChucVuDto> GetByFilter(ChucVuInput input)
        {
            //input.Format();
            var query = from x in _chucVuRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && (!input.LaLanhDao.HasValue || x.LaLanhDao == input.LaLanhDao.Value)
                        && (!input.Khoi.HasValue || x.Khoi == input.Khoi.Value)
                        && (!input.LoaiChucVu.HasValue || x.LoaiChucVu == input.LoaiChucVu.Value)
                        select new ChucVuDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            Khoi = x.Khoi,
                            LaLanhDao = x.LaLanhDao,
                            LoaiChucVu = x.LoaiChucVu,
                            TenHienThiTrenBieuMau = x.TenHienThiTrenBieuMau,
                            CapLanhDao = x.CapLanhDao
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ChucVuDto>(totalCount, items);
        }

        public bool CheckChucVuTrungTen(string name, long id = 0)
        {
            bool _result = false;
            if (id > 0)
            {
                var temp = _chucVuRepos.GetAll()
                    .Where(x => x.Ten.Equals(name) && x.Id != id);
                _result = temp.Any();
            }
            else
            {
                var temp = _chucVuRepos.GetAll()
                    .Where(x => x.Ten.Equals(name));
                _result = temp.Any();
            }

            return _result;
        }
        public async Task<ErroMsg> Upsert(ChucVuDto input)
        {
            //var emailpass = SimpleStringCipher.Instance.Decrypt("37lfxSI9EoYVRbybyoAf7ZpkZlVZMs2+V0PkOfvikMU=");
            var rs = new ErroMsg();
            try
            {
                if (!CheckChucVuTrungTen(input.Ten, input.Id))
                {
                    if (input.Id > 0)
                    {
                        var category = _chucVuRepos.Get(input.Id);
                        ObjectMapper.Map(input, category);
                        rs.Value = input.Id;
                    }
                    else
                    {
                        var entity = ObjectMapper.Map<ChucVu>(input);
                        entity.TenantId = AbpSession.TenantId;
                        rs.Value = await _chucVuRepos.InsertAndGetIdAsync(entity);
                    }
                    rs.Result = true;
                    CurrentUnitOfWork.SaveChanges();
                    _mainCache.Clear();
                }
                else
                {
                    rs.Msg = "Chức vụ đã tồn tại!";
                    rs.Result = false;
                }
            }
            catch (Exception ex)
            {
                rs.Result = false;
            }

            return rs;
        }

        public List<DanhSachCanBoChucVu> GetChucVuTheoCanBo(long canBoId)
        {
            var lstData = new List<DanhSachCanBoChucVu>();
            lstData = (from cvcb in _canBoChucVuRepos.GetAll()
                       from cv in _chucVuRepos.GetAll().Where(x => x.Id == cvcb.ChucVuId)
                       where cvcb.CanBoId == canBoId
                       select new DanhSachCanBoChucVu
                       {
                           Id = cvcb.Id,
                           CanBoId = cvcb.CanBoId,
                           ChucVuId = cvcb.ChucVuId,
                           ChucVuChinh = cvcb.ChucVuChinh,
                           TenChucVu = cv.Ten
                       }).ToList();

            var lstCanBoChucVuDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == canBoId);
            foreach (var item in lstData)
            {
                item.IsCauHinh = lstCanBoChucVuDanhGia.Any(x => x.ChucVuCanBoId == item.ChucVuId);
            }

            return lstData;
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_ChucVu_Create)]
        public void Delete(long id)
        {
            _chucVuRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        #region Thanh vien chu vu
        public List<CanBoResponse> GetThanhVienNhomCanBoTheoChucVuId(CanBoInput input)
        {
            var lstData = new List<CanBoResponse>();

            var lstThanhVien = _thanhVienNhomChucVuRepos.GetAll().Where(x => x.ChucVuId == input.ChucVuId)
                .Select(x => new { Id = x.Id, CanBoId = x.CanBoId, ChucVuCanBoId = x.ChucVuCanBoId });

            lstData = (from tv in lstThanhVien
                       from cb in _canBo.GetAll().Where(x => x.Id == tv.CanBoId)
                       from dv in _donviRepos.GetAll().Where(x => x.Id == cb.DonViId)
                       from user in UserManager.Users.Where(x => x.Id == cb.UserId)
                       from cbcv in _canboChucVuRepos.GetAll().Where(x => x.ChucVuId == tv.ChucVuCanBoId && x.CanBoId == tv.CanBoId).DefaultIfEmpty()
                       from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcv.ChucVuId).DefaultIfEmpty()
                       where (string.IsNullOrEmpty(input.Filter) || user.Name.Contains(input.Filter)
                       || user.Surname.Contains(input.Filter) || user.PhoneNumber.Contains(input.Filter)
                       || user.EmailAddress.Contains(input.Filter) || cb.Address.Contains(input.Filter))
                       select new CanBoResponse
                       {
                           Id = tv.Id,
                           TenantId = cb.TenantId,
                           Ten = (user != null) ? user.Name : "",
                           Ho = (user != null) ? user.Surname : "",
                           TenDonVi = (dv != null) ? dv.Ten : "",
                           ChucVuId = (cv != null) ? cv.Id : 0,
                           TenChucVu = (cv != null) ? cv.Ten : "",
                           ChucVuChinh = (cbcv != null) ? cbcv.ChucVuChinh : 2
                       }).ToList();

            return lstData;
        }

        public bool ThemThanhVienNhomChucVu(List<ObjectTemp> lstData, long chucVuId)
        {
            bool result = false;
            int? tenantId = AbpSession.TenantId;
            try
            {
                foreach (var item in lstData)
                {
                    var obj = new ThanhVienNhomChucVu();
                    obj.TenantId = tenantId;
                    obj.ChucVuId = chucVuId;
                    obj.CanBoId = item.CanBoId;
                    obj.ChucVuCanBoId = item.ChucVuCanBoId;
                    _thanhVienNhomChucVuRepos.Insert(obj);
                }
                CurrentUnitOfWork.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public bool XoaThanhVienNhomChucVu(long id)
        {
            bool result = false;
            try
            {
                _thanhVienNhomChucVuRepos.Delete(x => x.Id == id);
                CurrentUnitOfWork.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }


        #endregion


        [AbpAllowAnonymous]
        public PagedResultDto<ChucVuDto> GetChucVuBaoCaoDoiTuongChiTiet(ChucVuBaoCaoDoiTuongChiTieInput input)
        {
            //input.Format();

            var dsBcChucVuChiTiet = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId).Select(x => x.ChucVuId);

            var query = from x in _chucVuRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && x.LoaiChucVu == (int)AppConsts.LoaiHinhChucVu.ChucVu
                        && !dsBcChucVuChiTiet.Any(d => d == x.Id)
                        select new ChucVuDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            Khoi = x.Khoi,
                            LaLanhDao = x.LaLanhDao,
                            LoaiChucVu = x.LoaiChucVu,
                            TenHienThiTrenBieuMau = x.TenHienThiTrenBieuMau,
                            CapLanhDao = x.CapLanhDao
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ChucVuDto>(totalCount, items);
        }
    }
}
