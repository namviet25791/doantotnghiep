﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class ChucVuDanhGiaAppService : PMSAppServiceBase, IChucVuDanhGia
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<ChucVuDanhGia, long> _chucVuDanhGiaRepos;
        private readonly ICache _mainCache;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IPhieuDanhGiaCanBo _iPhieuDanhGiaCanBoService;
        public ChucVuDanhGiaAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<ChucVuDanhGia, long> chucVuDanhGiaRepos,
            IPhieuDanhGiaCanBo iPhieuDanhGiaCanBoService, IRepository<ChucVu, long> chucVuRepos,
            IRepository<CanBo, long> canBoRepos, IRepository<CanBoChucVu, long> canBoChucVuRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _chucVuDanhGiaRepos = chucVuDanhGiaRepos;
            _mainCache = _cacheManager.GetCache("ChucVuDanhGiaAppService");
            _iPhieuDanhGiaCanBoService = iPhieuDanhGiaCanBoService;
            _chucVuRepos = chucVuRepos;
            _canBoRepos = canBoRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
        }

        public PagedResultDto<ChucVuDanhGiaDto> GetByFilter(ChucVuDanhGiaInput input)
        {
            input.Format();
            var query = from x in _chucVuDanhGiaRepos.GetAll()
                        where (!input.ChucVuDuocDanhGiaId.HasValue || x.ChucVuDuocDanhGiaId == input.ChucVuDuocDanhGiaId.Value)
                        && (!input.ChucVuThucHienDanhGiaId.HasValue || x.ChucVuThucHienDanhGiaId == input.ChucVuThucHienDanhGiaId.Value)
                        select new ChucVuDanhGiaDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            ChucVuDuocDanhGiaId = x.ChucVuDuocDanhGiaId,
                            ChucVuThucHienDanhGiaId = x.ChucVuThucHienDanhGiaId,
                            SoThuTu = x.SoThuTu,
                            NguoiCham = x.NguoiCham
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ChucVuDanhGiaDto>(totalCount, items);
        }
        public List<ChucVuDanhGiaDto> GetByChucVuDuocDanhGia(long chucVuId)
        {
            var query = (from x in _chucVuDanhGiaRepos.GetAll()
                         from cv in _chucVuRepos.GetAll().Where(d => d.Id == x.ChucVuThucHienDanhGiaId)
                         where x.ChucVuDuocDanhGiaId == chucVuId
                         select new ChucVuDanhGiaDto
                         {
                             Id = x.Id,
                             TenantId = x.TenantId,
                             ChucVuDuocDanhGiaId = x.ChucVuDuocDanhGiaId,
                             ChucVuThucHienDanhGiaId = x.ChucVuThucHienDanhGiaId,
                             SoThuTu = x.SoThuTu,
                             NguoiCham = x.NguoiCham,
                             LoaiChucVu = cv.LoaiChucVu,
                         }).ToList();

            return query;
        }

        public List<CauHinhChucVuCanBoDanhGiaDto> GetChucVuDanhGiaTheoChucVuCanBoId(long canBoId, long chucVuId)
        {
            var lstChucVu = (from x in _chucVuDanhGiaRepos.GetAll()
                             from cv in _chucVuRepos.GetAll().Where(d => d.Id == x.ChucVuThucHienDanhGiaId)
                             where x.ChucVuDuocDanhGiaId == chucVuId
                             select new CauHinhChucVuCanBoDanhGiaDto
                             {
                                 ThuTu = x.SoThuTu,
                                 ChucVuDanhGiaId = x.ChucVuThucHienDanhGiaId,
                                 NguoiCham = x.NguoiCham,
                                 LoaiChucVu = cv.LoaiChucVu
                             }).ToList();


            var getDonViCanBo = _canBoRepos.GetAll().FirstOrDefault(x => x.Id == canBoId);

            var lstCanBoThuocDonVi = (from cb in _canBoRepos.GetAll()
                                      from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                      from cvcb in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
                                      where cb.DonViId == getDonViCanBo.DonViId
                                      select new
                                      {
                                          CanBoId = cb.Id,
                                          TenCanBo = u.Surname + " " + u.Name,
                                          ChucVuId = cvcb.ChucVuId
                                      });

            foreach (var item in lstChucVu)
            {
                if (item.LoaiChucVu == (int)AppConsts.LoaiHinhChucVu.ChucVu)
                {
                    item.ListCanBoChucVu = lstCanBoThuocDonVi.Where(x => x.ChucVuId == item.ChucVuDanhGiaId).Select(x => new
                    CanBoChucVuTemp
                    {
                        CanBoId = x.CanBoId,
                        TenCanBo = x.TenCanBo
                    }).ToList();

                    if (item.ListCanBoChucVu.Count == 0)
                    {
                        item.ListCanBoChucVu = (from cv in _canBoChucVuRepos.GetAll()
                                                from cb in _canBoRepos.GetAll().Where(x => x.Id == cv.CanBoId)
                                                from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                                where cv.ChucVuId == item.ChucVuDanhGiaId
                                                select new CanBoChucVuTemp
                                                {
                                                    CanBoId = cb.Id,
                                                    TenCanBo = u.Surname + " " + u.Name,
                                                }).ToList();

                    }

                    item.CanBoDanhGiaId = (item.ListCanBoChucVu.Count() > 0) ? item.ListCanBoChucVu.FirstOrDefault().CanBoId : 0;
                }
                item.CanBoId = canBoId;
                item.ChucVuCanBoId = chucVuId;
            }

            return lstChucVu;
        }

        public async Task<ErroMsg> Upsert(List<ChucVuDanhGiaDto> input)
        {
            var erroMsg = new ErroMsg();
            try
            {
                //if (_iPhieuDanhGiaCanBoService.KiemTraThoiGianKetThucDanhGia(DateTime.Now))
                //{
                _chucVuDanhGiaRepos.Delete(x => x.ChucVuDuocDanhGiaId == input[0].ChucVuDuocDanhGiaId);
                foreach (var item in input)
                {
                    if (item.ChucVuThucHienDanhGiaId > 0)
                    {
                        item.Id = 0;
                        var entity = ObjectMapper.Map<ChucVuDanhGia>(item);
                        entity.TenantId = AbpSession.TenantId;
                        await _chucVuDanhGiaRepos.InsertAndGetIdAsync(entity);
                    }
                }
                erroMsg.Result = true;
                //}
                //else
                //{
                //    erroMsg.Result = false;
                //    erroMsg.Msg = "Đang trong thời gian đánh giá không được thay đổi.";
                //}
            }
            catch (Exception ex)
            {
                erroMsg.Result = false;
                erroMsg.Msg = "Có lỗi xảy ra vui lòng thao tác lại.";
                throw ex;
            }

            return erroMsg;
        }
        public void Delete(long id)
        {
            _chucVuDanhGiaRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
