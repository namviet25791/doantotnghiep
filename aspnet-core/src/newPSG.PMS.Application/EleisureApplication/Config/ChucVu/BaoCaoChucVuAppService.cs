﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class BaoCaoChucVuAppService : PMSAppServiceBase, IBaoCaoChucVu
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<BaoCaoChucVu, long> _repos; 
        private readonly ICache _mainCache;
        public BaoCaoChucVuAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<BaoCaoChucVu, long> repos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos; 
            _mainCache = _cacheManager.GetCache("BaoCaoChucVuAppService");
        }

        public PagedResultDto<BaoCaoChucVuDto> GetByFilter(BaoCaoChucVuInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll() 
                        where  (!input.BaoCaoId.HasValue || x.BaoCaoId == input.BaoCaoId.Value)
                        && (!input.ChucVuId.HasValue|| x.ChucVuId == input.ChucVuId.Value)
                        select new BaoCaoChucVuDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,  
                            BaoCaoId = x.BaoCaoId,
                            ChucVuId = x.ChucVuId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList(); 
            
            return new PagedResultDto<BaoCaoChucVuDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<long> Upsert(BaoCaoChucVuDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _repos.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            {
               
                var entity =  ObjectMapper.Map<BaoCaoChucVu>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = _repos.InsertAndGetId(entity);  
            }
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
            return input.Id;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
