﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class ThangDiemChiTietAppService : PMSAppServiceBase, IThangDiemChiTiet
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<ThangDiemChiTiet, long> _repos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly ICache _mainCache;
        public ThangDiemChiTietAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<ThangDiemChiTiet, long> repos,
            IRepository<ThangDiem, long> thangDiemRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _thangDiemRepos = thangDiemRepos;
            _mainCache = _cacheManager.GetCache("ThangDiemChiTietAppService");
        }

        public PagedResultDto<ThangDiemChiTietDto> GetByFilter(ThangDiemChiTietInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                            //join y in _thangDiemRepos.GetAll()
                            //on x.ThangDiemId equals y.Id
                        where (String.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && (!input.ThangDiemId.HasValue || (x.ThangDiemId == input.ThangDiemId.Value))
                        select new ThangDiemChiTietDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            ThangDiemId = x.ThangDiemId,
                            DiemSan = x.DiemSan,
                            DiemTran = x.DiemTran,
                            Ten = x.Ten
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<ThangDiemChiTietDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<bool> Upsert(List<ThangDiemChiTietDto> input)
        {
            bool result = false;
            try
            {
                var lstThangDiemSort = input.OrderByDescending(x => x.DiemTran);
                int index = 0;
                foreach (var item in lstThangDiemSort)
                {
                    if(index <= 4)
                    {
                        index++;
                    }

                    item.LoaiThangDiemChiTiet = index;
                    if (item.Id > 0)
                    {
                        var category = _repos.Get(item.Id);
                        ObjectMapper.Map(item, category);
                    }
                    else
                    {
                        var entity = ObjectMapper.Map<ThangDiemChiTiet>(item);
                        entity.TenantId = AbpSession.TenantId;
                        item.Id = _repos.InsertAndGetId(entity);
                    }
                    CurrentUnitOfWork.SaveChanges();
                }
                _mainCache.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw;
            }
            return result;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        public void CopyByThangDiem(CopyThangDiemChiTietInput input)
        {
            var items = _repos.GetAllList(x => x.ThangDiemId == input.ThangDiemTruocDoId);
            foreach (var item in items)
            {
                _repos.Insert(new ThangDiemChiTiet()
                {
                    ThangDiemId = item.ThangDiemId,
                    TenantId = item.TenantId,
                    DiemSan = item.DiemSan,
                    DiemTran = item.DiemTran
                });
            }
        }
    }
}
