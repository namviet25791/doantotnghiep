﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static newPSG.PMS.CommonENum;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class ThangDiemAppService : PMSAppServiceBase, IThangDiem
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<ThangDiem, long> _repos;
        private readonly IRepository<PhieuDanhGia, long> _phieuDanhGiaRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly ICache _mainCache;
        public ThangDiemAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<ThangDiem, long> repos,
            IRepository<PhieuDanhGia, long> phieuDanhGiaRepos,
            IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _phieuDanhGiaRepos = phieuDanhGiaRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _mainCache = _cacheManager.GetCache("ThangDiemAppService");
        }
        public List<ThangDiemDto> GetAllThangDiemCombo()
        {
            var date = DateTime.Now.Date;

            var query = (from x in _repos.GetAll()
                         where (x.HieuLucTuNgay == null || x.HieuLucTuNgay <= date)
                         && (x.HieuLucToiNgay == null || x.HieuLucToiNgay >= date)
                         select new ThangDiemDto
                         {
                             Id = x.Id,
                             Ten = x.Ten
                         }).ToList();

            return query;
        }
        [AbpAuthorize(AppPermissions.Pages_Admin_ThangDiem)]
        public PagedResultDto<ThangDiemResponse> GetByFilter(ThangDiemInput input)
        {
            //input.Format();
            var query = from x in _repos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && (!input.StartDate.HasValue || x.HieuLucTuNgay >= input.StartDate.Value)
                        && (!input.EndDate.HasValue || x.HieuLucToiNgay <= input.EndDate.Value)
                        select new ThangDiemResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            HieuLucTuNgay = x.HieuLucTuNgay,
                            HieuLucToiNgay = x.HieuLucToiNgay
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            foreach (var item in items)
            {
                //item.SoLuongThangDiemChiTiet = _thangDiemChiTietRepos.Count(x => x.ThangDiemId == item.Id);
                item.ThangDiemChiTiet = _thangDiemChiTietRepos.GetAllList(x => x.ThangDiemId == item.Id).Select(x => new ThangDiemChiTietDto
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    ThangDiemId = x.ThangDiemId,
                    DiemSan = x.DiemSan,
                    DiemTran = x.DiemTran,
                    Ten = x.Ten
                }).ToList();
                item.SoLuongThangDiemChiTiet = item.ThangDiemChiTiet.Count;
            }
            return new PagedResultDto<ThangDiemResponse>(totalCount, items);
        }
        public List<EnumObj> getLoaiThangDiemChiTiet()
        {
            return CommonENum.EnumToList(typeof(AppConsts.LoaiThangDiemChiTiet));
        }
        public async Task<long> Upsert(ThangDiemDto input)
        {
            long id = 0;
            try
            {
                if (input.Id > 0)
                {
                    var category = _repos.Get(input.Id);
                    category.Ten = input.Ten;
                    category.HieuLucToiNgay = input.HieuLucToiNgay;
                    category.HieuLucTuNgay = input.HieuLucTuNgay;
                }
                else
                {
                    var entity = ObjectMapper.Map<ThangDiem>(input);
                    entity.TenantId = AbpSession.TenantId;
                    id = _repos.InsertAndGetId(entity);
                }
                CurrentUnitOfWork.SaveChanges();
                _mainCache.Clear();
            }
            catch (Exception ex)
            {
                id = 0;
                throw;
            }
            return id;
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_ThangDiem)]
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
        [AbpAuthorize(AppPermissions.Pages_Admin_ThangDiem_Clone)]
        public void CopyThangDiem(CopyThangDiemInput input)
        {
            var thangDiem = _repos.Get(input.ThangDiemTruocDoId);
            if (thangDiem != null)
            {
                var newThangDiem = new ThangDiem()
                {
                    HieuLucToiNgay = thangDiem.HieuLucToiNgay,
                    HieuLucTuNgay = thangDiem.HieuLucTuNgay,
                    Ten = thangDiem.Ten,
                    TenantId = thangDiem.TenantId
                };
                var id = _repos.InsertAndGetId(newThangDiem);
                var items = _thangDiemChiTietRepos.GetAllList(x => x.ThangDiemId == input.ThangDiemTruocDoId);
                foreach (var item in items)
                {
                    _thangDiemChiTietRepos.Insert(new ThangDiemChiTiet()
                    {
                        ThangDiemId = id,
                        TenantId = item.TenantId,
                        DiemSan = item.DiemSan,
                        DiemTran = item.DiemTran,
                        Ten = item.Ten,
                        LoaiThangDiemChiTiet = item.LoaiThangDiemChiTiet
                    });
                }
            }
        }
    }
}
