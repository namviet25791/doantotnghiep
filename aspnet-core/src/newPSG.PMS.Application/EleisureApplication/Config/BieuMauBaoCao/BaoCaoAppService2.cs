﻿using newPSG.PMS.EleisureApplicationShared;
using Stimulsoft.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace newPSG.PMS.EleisureApplication
{
    public partial class BaoCaoAppService
    {
        #region Bao cao cho quan Hoan Kiem
        public string TongHopKetQuaDanhGiaXepLoaiMau01(FilterBaoCaoInput input)
        {
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);


            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll()
                .Where(x => (x.Nam >= nam && x.Thang >= thang)
            && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            int index = 1;
            var dtData = new List<BaoCaoTongHopXepLoai>();
            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var getPdgcb = (from cv in lstChucVuByDoiTuong
                                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.TieuChiId == -1)
                                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                select new
                                {
                                    ChucVuId = cv,
                                    LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                    CanBoId = pdg.CanBoId
                                }).ToList();

                if (tenDoiTuong.TenHienThiTrenBaoCao == "Công chức, viên chức")
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    var getCanBoTheoLoaiHD = (from pdg in getPdgcb
                                              from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                              where cb.LoaiHopDong == (int)AppConsts.LoaiHopDong.VienChuc
                                              || cb.LoaiHopDong == (int)AppConsts.LoaiHopDong.CongChuc
                                              select new
                                              {
                                                  LoaiHopDong = cb.LoaiHopDong,
                                                  LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet
                                              });
                    obj.SL_DanhGia = getCanBoTheoLoaiHD.Count();
                    obj.SL_HTXSNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.SL_HTTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.SL_HTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.SL_KHTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj);
                }
                else if (tenDoiTuong.TenHienThiTrenBaoCao == "LĐHĐ theo NĐ số 68/2000/NĐ-CP")
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    var getCanBoTheoLoaiHD = (from pdg in getPdgcb
                                              from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                              where cb.LoaiHopDong == (int)AppConsts.LoaiHopDong.LDHDND68
                                              select new
                                              {
                                                  LoaiHopDong = cb.LoaiHopDong,
                                                  LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet
                                              });
                    obj.SL_DanhGia = getCanBoTheoLoaiHD.Count();
                    obj.SL_HTXSNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.SL_HTTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.SL_HTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.SL_KHTNV = getCanBoTheoLoaiHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj);
                }
                else if (tenDoiTuong.TenHienThiTrenBaoCao == "LĐHĐ làm công tác chuyên môn")
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj.SL_DanhGia = 0;
                    obj.SL_HTXSNV = 0;
                    obj.SL_HTTNV = 0;
                    obj.SL_HTNV = 0;
                    obj.SL_KHTNV = 0;
                    dtData.Add(obj);
                }
                else
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj.SL_DanhGia = getPdgcb.Count();
                    obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj);
                }

                index++;
            }

            report.RegData("dtTable", dtData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = thang.ToString() + "/" + nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["TenPhieuBaoCao"].Value = getBaoCao.TenBaoCao;

            var tongSoNguoiDanhGia = (decimal)dtData.Sum(x => x.SL_DanhGia);
            var tyleHTXSNV = "0";
            var tyleHTTNV = "0";
            var tyleHTNV = "0";
            var tyleKHTNV = "0";

            if (tongSoNguoiDanhGia > 0)
            {
                tyleHTXSNV = ((decimal)dtData.Sum(x => x.SL_HTXSNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTTNV = ((decimal)dtData.Sum(x => x.SL_HTTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTNV = ((decimal)dtData.Sum(x => x.SL_HTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleKHTNV = ((decimal)dtData.Sum(x => x.SL_KHTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
            }

            report.Dictionary.Variables["TongHTXSNV"].Value = tyleHTXSNV;
            report.Dictionary.Variables["TongHTTNV"].Value = tyleHTTNV;
            report.Dictionary.Variables["TongHTNV"].Value = tyleHTNV;
            report.Dictionary.Variables["TongKHTNV"].Value = tyleKHTNV;
            report.Render(false);
            return report.SaveDocumentJsonToString();
        }
        public string TongHopKetQuaDanhGiaXepLoaiMau02(FilterBaoCaoInput input)
        {
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);


            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll()
                .Where(x => (x.Nam >= nam && x.Thang >= thang)
            && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            int index = 1;
            var dtData = new List<BaoCaoTongHopXepLoai>();
            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var getPdgcb = (from cv in lstChucVuByDoiTuong
                                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.TieuChiId == -1)
                                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                select new
                                {
                                    ChucVuId = cv,
                                    LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                    CanBoId = pdg.CanBoId
                                }).ToList();


                if (tenDoiTuong.TenHienThiTrenBaoCao == "LĐHĐ làm công tác chuyên môn"
                    || tenDoiTuong.TenHienThiTrenBaoCao == "Công chức phường theo NĐ số 92/2009/NĐ-CP")
                {
                    var obj2 = new BaoCaoTongHopXepLoai();
                    obj2.STT = index.ToString();
                    obj2.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj2.SL_DanhGia = 0;
                    obj2.SL_HTXSNV = 0;
                    obj2.SL_HTTNV = 0;
                    obj2.SL_HTNV = 0;
                    obj2.SL_KHTNV = 0;
                    dtData.Add(obj2);
                    index++;
                    continue;
                }

                var obj = new BaoCaoTongHopXepLoai();
                obj.STT = index.ToString();
                obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                obj.SL_DanhGia = getPdgcb.Count();
                obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                dtData.Add(obj);
                index++;
            }



            report.RegData("dtTable", dtData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = thang.ToString() + "/" + nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["TenPhieuBaoCao"].Value = getBaoCao.TenBaoCao;

            var tongSoNguoiDanhGia = (decimal)dtData.Sum(x => x.SL_DanhGia);
            var tyleHTXSNV = "0";
            var tyleHTTNV = "0";
            var tyleHTNV = "0";
            var tyleKHTNV = "0";

            if (tongSoNguoiDanhGia > 0)
            {
                tyleHTXSNV = ((decimal)dtData.Sum(x => x.SL_HTXSNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTTNV = ((decimal)dtData.Sum(x => x.SL_HTTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTNV = ((decimal)dtData.Sum(x => x.SL_HTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleKHTNV = ((decimal)dtData.Sum(x => x.SL_KHTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
            }

            report.Dictionary.Variables["TongHTXSNV"].Value = tyleHTXSNV;
            report.Dictionary.Variables["TongHTTNV"].Value = tyleHTTNV;
            report.Dictionary.Variables["TongHTNV"].Value = tyleHTNV;
            report.Dictionary.Variables["TongKHTNV"].Value = tyleKHTNV;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }
        public string TongHopKetQuaDanhGiaXepLoaiMau03(FilterBaoCaoInput input)
        {
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);


            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll()
                .Where(x => (x.Nam >= nam && x.Thang >= thang)
            && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            int index = 1;
            var dtData = new List<BaoCaoTongHopXepLoai>();
            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var getPdgcb = (from cv in lstChucVuByDoiTuong
                                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.TieuChiId == -1)
                                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                select new
                                {
                                    ChucVuId = cv,
                                    LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                    CanBoId = pdg.CanBoId
                                }).ToList();

                if (tenDoiTuong.TenHienThiTrenBaoCao == "Giáo viên"
                    || tenDoiTuong.TenHienThiTrenBaoCao == "Nhân viên hành chính")
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    dtData.Add(obj);

                    var getCanBoTheoChucVu = (from pdg in getPdgcb
                                              from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                              select new
                                              {
                                                  LoaiHopDong = cb.LoaiHopDong,
                                                  LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet
                                              });

                    var dsVienChuc = getCanBoTheoChucVu.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.VienChuc);
                    var obj2 = new BaoCaoTongHopXepLoai();
                    obj2.TenDoiTuong = "- Viên chức";
                    obj2.SL_DanhGia = dsVienChuc.Count();
                    obj2.SL_HTXSNV = dsVienChuc.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj2.SL_HTTNV = dsVienChuc.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj2.SL_HTNV = dsVienChuc.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj2.SL_KHTNV = dsVienChuc.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj2);

                    var dsHopDong = getCanBoTheoChucVu.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HopDong);
                    var obj3 = new BaoCaoTongHopXepLoai();
                    obj3.TenDoiTuong = "- Hợp đồng";
                    obj3.SL_DanhGia = dsHopDong.Count();
                    obj3.SL_HTXSNV = dsHopDong.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj3.SL_HTTNV = dsHopDong.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj3.SL_HTNV = dsHopDong.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj3.SL_KHTNV = dsHopDong.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj3);
                }
                else if (tenDoiTuong.TenHienThiTrenBaoCao == "LĐHĐ NĐ 68")
                {
                    var getCanBoLDHD = (from pdg in getPdgcb
                                        from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                        where cb.LoaiHopDong == (int)AppConsts.LoaiHopDong.LDHDND68
                                        select new
                                        {
                                            LoaiHopDong = cb.LoaiHopDong,
                                            LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet
                                        });

                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj.SL_DanhGia = getCanBoLDHD.Count();
                    obj.SL_HTXSNV = getCanBoLDHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.SL_HTTNV = getCanBoLDHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.SL_HTNV = getCanBoLDHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.SL_KHTNV = getCanBoLDHD.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();

                    dtData.Add(obj);
                }
                else
                {
                    var obj = new BaoCaoTongHopXepLoai();
                    obj.STT = index.ToString();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj.SL_DanhGia = getPdgcb.Count();
                    obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    dtData.Add(obj);
                }

                index++;
            }

            report.RegData("dtTable", dtData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = thang.ToString() + "/" + nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["TenPhieuBaoCao"].Value = getBaoCao.TenBaoCao;

            var tongSoNguoiDanhGia = (decimal)dtData.Sum(x => x.SL_DanhGia);
            var tyleHTXSNV = "0";
            var tyleHTTNV = "0";
            var tyleHTNV = "0";
            var tyleKHTNV = "0";

            if (tongSoNguoiDanhGia > 0)
            {
                tyleHTXSNV = ((decimal)dtData.Sum(x => x.SL_HTXSNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTTNV = ((decimal)dtData.Sum(x => x.SL_HTTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleHTNV = ((decimal)dtData.Sum(x => x.SL_HTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
                tyleKHTNV = ((decimal)dtData.Sum(x => x.SL_KHTNV) / tongSoNguoiDanhGia * 100).ToString("N0");
            }

            report.Dictionary.Variables["TongHTXSNV"].Value = tyleHTXSNV;
            report.Dictionary.Variables["TongHTTNV"].Value = tyleHTTNV;
            report.Dictionary.Variables["TongHTNV"].Value = tyleHTNV;
            report.Dictionary.Variables["TongKHTNV"].Value = tyleKHTNV;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }

        public string TongHopKetQuaDanhGiaXepLoaiHangThangMau10(FilterBaoCaoInput input)
        {
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => input.LstDonVi.Any(d => d == x.DonViId));


            var lstPhieuDanhGia = (from pdgcb in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Nam == nam && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                                   from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdgcb.Id && x.IsKetQuaCuoiCung == true)
                                   from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                   from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdgcb.ChucVuId)
                                   from cb in lstCanBoThuocDonVi.Where(x => x.Id == pdgcb.CanBoId)
                                   from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                                   from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                                   select new
                                   {
                                       CanBoId = pdgcb.CanBoId,
                                       ChucVuId = pdgcb.ChucVuId,
                                       Thang = pdgcb.Thang,
                                       TenCanBo = user.Surname + " " + user.Name,
                                       TenChucVu = cv.Ten,
                                       LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                       TenDonvi = dv.Ten,
                                   });

            var lstData = new List<BaoCaoTongHopKetQuaDanhGiaHangThang>();
            int index = 1;
            foreach (var item in lstPhieuDanhGia)
            {
                string TenLoaiThangDiemChiTiet = "";
                bool isNew = false;
                var obj = lstData.Where(x => x.CanBoId == item.CanBoId && x.ChucVuId == item.ChucVuId).FirstOrDefault();
                if (obj == null)
                {
                    isNew = true;
                    obj = new BaoCaoTongHopKetQuaDanhGiaHangThang();
                    obj.CanBoId = item.CanBoId;
                    obj.ChucVuId = item.ChucVuId;
                    obj.TenCanBo = item.TenCanBo;
                    obj.TenChucVu = item.TenChucVu;
                    obj.TenDonvi = item.TenDonvi;
                    obj.LoaiA = 0;
                    obj.LoaiB = 0;
                    obj.LoaiC = 0;
                    obj.LoaiD = 0;
                    obj.STT = index.ToString();
                }

                switch (item.LoaiThangDiemChiTiet)
                {
                    case (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV:
                        TenLoaiThangDiemChiTiet = "A";
                        obj.LoaiA += 1;
                        break;
                    case (int)AppConsts.LoaiThangDiemChiTiet.HTTNV:
                        TenLoaiThangDiemChiTiet = "B";
                        obj.LoaiB += 1;
                        break;
                    case (int)AppConsts.LoaiThangDiemChiTiet.HTNV:
                        TenLoaiThangDiemChiTiet = "C";
                        obj.LoaiC += 1;
                        break;
                    case (int)AppConsts.LoaiThangDiemChiTiet.KHTNV:
                        TenLoaiThangDiemChiTiet = "D";
                        obj.LoaiD += 1;
                        break;
                }

                switch (item.Thang)
                {
                    case 1:
                        obj.T1 = TenLoaiThangDiemChiTiet;
                        break;
                    case 2:
                        obj.T2 = TenLoaiThangDiemChiTiet;
                        break;
                    case 3:
                        obj.T3 = TenLoaiThangDiemChiTiet;
                        break;
                    case 4:
                        obj.T4 = TenLoaiThangDiemChiTiet;
                        break;
                    case 5:
                        obj.T5 = TenLoaiThangDiemChiTiet;
                        break;
                    case 6:
                        obj.T6 = TenLoaiThangDiemChiTiet;
                        break;
                    case 7:
                        obj.T7 = TenLoaiThangDiemChiTiet;
                        break;
                    case 8:
                        obj.T8 = TenLoaiThangDiemChiTiet;
                        break;
                    case 9:
                        obj.T9 = TenLoaiThangDiemChiTiet;
                        break;
                    case 10:
                        obj.T10 = TenLoaiThangDiemChiTiet;
                        break;
                    case 11:
                        obj.T11 = TenLoaiThangDiemChiTiet;
                        break;
                    case 12:
                        obj.T12 = TenLoaiThangDiemChiTiet;
                        break;
                }
                if (isNew)
                {
                    lstData.Add(obj);
                    index++;
                }
            }

            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["TenPhieuBaoCao"].Value = getBaoCao.TenBaoCao;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }

        public string TongHopKetQuaDanhGiaXepLoaiTheoThangMau07(FilterBaoCaoInput input)
        {
            var dDate = ConvertDateStringToDateTime(input.ThoiGianBatDau);
            var thang = dDate.Month;
            var nam = dDate.Year;

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll()
              .Where(x => (x.Nam >= nam && x.Thang >= thang)
          && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            int index = 1;
            var lstData = new List<BaoCaoTongHopKetQuaDanhGiaXepLoaiThang>();
            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var test1 = (from cv in lstChucVuByDoiTuong
                             from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                             select new
                             {
                                 pdg.Id,
                                 pdg.CanBoId,
                                 ChucVuId = cv,
                             }).ToList();
                var test2 = (from pdg in test1
                             from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                             from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                             select new
                             {
                                 pdg.CanBoId,
                                 pdg.ChucVuId,
                                 tdct.LoaiThangDiemChiTiet,
                                 TenLoaiThangDiemChiTiet = tdct.Ten,
                             }).ToList();

                //var getPdgcb = (from cv in lstChucVuByDoiTuong
                //                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                //                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                //                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                var getPdgcb = (from pdg in test2
                                from cvu in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                                from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                                select new
                                {
                                    CanBoId = pdg.CanBoId,
                                    ChucVuId = pdg.ChucVuId,
                                    TenCanBo = user.Surname + " " + user.Name,
                                    TenChucVu = cvu.Ten,
                                    LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet,
                                    TenLoaiThangDiemChiTiet = pdg.TenLoaiThangDiemChiTiet,
                                }).ToList();

                foreach (var item in getPdgcb)
                {
                    var obj = new BaoCaoTongHopKetQuaDanhGiaXepLoaiThang();
                    obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                    obj.CanBoId = item.CanBoId;
                    obj.ChucVuId = item.ChucVuId;
                    obj.TenCanBo = item.TenCanBo;
                    obj.TenChucVu = item.TenChucVu;
                    obj.TenLoaiThangDiemChiTiet = item.TenLoaiThangDiemChiTiet;
                    obj.STT = index.ToString();
                    switch (item.LoaiThangDiemChiTiet)
                    {
                        case (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV:
                            obj.LoaiThangDiemChiTiet = "A";
                            break;
                        case (int)AppConsts.LoaiThangDiemChiTiet.HTTNV:
                            obj.LoaiThangDiemChiTiet = "B";
                            break;
                        case (int)AppConsts.LoaiThangDiemChiTiet.HTNV:
                            obj.LoaiThangDiemChiTiet = "C";
                            break;
                        case (int)AppConsts.LoaiThangDiemChiTiet.KHTNV:
                            obj.LoaiThangDiemChiTiet = "D";
                            break;
                    }
                    lstData.Add(obj);
                    index++;
                }
            }

            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = thang.ToString() + "/" + nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["TenPhieuBaoCao"].Value = getBaoCao.TenBaoCao;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }

        public string TongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11(FilterBaoCaoInput input)
        {
            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }

            DateTime dtEndDate = dtStartDate;
            string loaiThoiGianView = string.Empty;

            if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Thang)
            {
                dtStartDate = new DateTime(dtStartDate.Year, dtStartDate.Month, 1);
                dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
                loaiThoiGianView = "THÁNG " + dtStartDate.Month + " NĂM " + dtStartDate.Year;
            }
            else if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Quy)
            {
                var quy = "";
                int month = dtStartDate.Month;
                if (month == 1 || month == 2 || month == 3)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 1, 1);
                    quy = "QUÝ 1";
                }
                else if (month == 4 || month == 5 || month == 6)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 4, 1);
                    quy = "QUÝ 2";
                }
                else if (month == 7 || month == 8 || month == 9)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 7, 1);
                    quy = "QUÝ 3";
                }
                else if (month == 10 || month == 11 || month == 12)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 10, 1);
                    quy = "QUÝ 4";
                }
                dtEndDate = dtStartDate.AddMonths(3).AddDays(-1);
                loaiThoiGianView = quy + " NĂM " + dtStartDate.Year;
            }
            else if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Nam)
            {
                dtStartDate = new DateTime(dtStartDate.Year, 1, 1);
                dtEndDate = dtStartDate.AddMonths(12).AddDays(-1);
                loaiThoiGianView = "NĂM " + dtStartDate.Year;
            }


            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }
            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
            (x.Nam >= dtStartDate.Year && x.Thang >= dtStartDate.Month)
            && (x.Nam <= dtEndDate.Year && x.Thang <= dtEndDate.Month) && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            var lstData = new List<BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong>();

            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var getPdgcb = (from cv in lstChucVuByDoiTuong
                                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                select new
                                {
                                    ChucVu = cv,
                                    LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet,
                                }).ToList();
                var tongSo = (from cv in lstChucVuByDoiTuong
                              from cbcv in _canBoChucVuRepos.GetAll().Where(x => x.ChucVuId == cv)
                              select new
                              {
                                  ChucVu = cv,
                                  CanBoId = cbcv.CanBoId
                              }).Count();
                var obj = new BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong();
                obj.TenDoiTuong = tenDoiTuong.TenHienThiTrenBaoCao;
                obj.SL_DanhGia = getPdgcb.Select(x => x.ChucVu).Count();
                obj.TongSo = tongSo;
                if (obj.SL_DanhGia > 0)
                {
                    obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Select(x => x.ChucVu).Count();
                    obj.TiLe_HTXSNV = ((decimal)obj.SL_HTXSNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Select(x => x.ChucVu).Count();
                    obj.TiLe_HTTNV = ((decimal)obj.SL_HTTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Select(x => x.ChucVu).Count();
                    obj.TiLe_HTNV = ((decimal)obj.SL_HTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Select(x => x.ChucVu).Count();
                    obj.TiLe_KHTNV = ((decimal)obj.SL_KHTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");
                }
                else
                {
                    obj.SL_HTXSNV = 0;
                    obj.TiLe_HTXSNV = "0";

                    obj.SL_HTTNV = 0;
                    obj.TiLe_HTTNV = "0";

                    obj.SL_HTNV = 0;
                    obj.TiLe_HTNV = "0";

                    obj.SL_KHTNV = 0;
                    obj.TiLe_KHTNV = "0";
                }
                lstData.Add(obj);
            }

            //tinh tong
            int thamgia = 0;
            int HTXS = 0;
            int HTT = 0;
            int HT = 0;
            int KHT = 0;

            foreach (var data in lstData)
            {
                thamgia = thamgia + data.SL_DanhGia;
                HTXS = HTXS + data.SL_HTXSNV;
                HTT = HTT + data.SL_HTTNV;
                HT = HT + data.SL_HTNV;
                KHT = KHT + data.SL_KHTNV;
            }

            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = loaiThoiGianView;
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName;

            report.Dictionary.Variables["TileHTXS"].Value = ((decimal)HTXS / (decimal)thamgia * 100).ToString("#0");
            report.Dictionary.Variables["TileHTT"].Value = ((decimal)HTT / (decimal)thamgia * 100).ToString("#0");
            report.Dictionary.Variables["TileHT"].Value = ((decimal)HT / (decimal)thamgia * 100).ToString("#0");
            report.Dictionary.Variables["TileKHT"].Value = ((decimal)KHT / (decimal)thamgia * 100).ToString("#0");

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }

        public TongHopKhoiCacPhuong04Dto TongHopKhoiCacPhuong04(FilterBaoCaoInput input)
        {
            var data = new TongHopKhoiCacPhuong04Dto();

            var dDate = ConvertDateStringToDateTime(input.ThoiGianBatDau);
            var thang = dDate.Month;
            var nam = dDate.Year;

            var lstChucVu = (from n in _baoCaoNhomDoiTuongRepos.GetAll()
                             from ct in _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == n.Id)
                             from cv in _chucVuRepos.GetAll().Where(x => x.Id == ct.ChucVuId)
                             where input.LstNhomChucVu.Any(d => d == n.Id)
                             select new
                             {
                                 TenNhom = n.TenHienThiTrenBaoCao,
                                 NhomId = n.Id,
                                 ChucVuId = cv.Id,
                                 TenChucVu = cv.Ten,
                                 ThuTu = n.ThuTu
                             });

            data.LstNhomChucVu = lstChucVu.GroupBy(x => new { x.NhomId, x.TenNhom })
                .Select(g => new NhomChucVuDto
                {
                    NhomChucVuId = g.Key.NhomId,
                    TenNhom = g.Key.TenNhom,
                    ThuTu = g.FirstOrDefault().ThuTu,
                    IsNhomChucVu = (g.Key.TenNhom == "Công chức (NĐ số 92/2009/NĐ-CP)" || g.Key.TenNhom == "LĐHĐ làm công tác chuyên môn") ? true : false,
                    LstChucVu = g.Select(y => new ChucVuChiTietDto
                    {
                        ChucVuId = y.ChucVuId,
                        TenChucVu = y.TenChucVu,
                    }).OrderBy(x => x.ChucVuId).ToList()
                }).OrderBy(x => x.ThuTu).ToList();



            var lstPhieuDanhGiaCanBo = (from pdg in _phieuDanhGiaCanBoRepos.GetAll()
                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                        where (pdg.Nam >= nam && pdg.Thang >= thang)
                                         && pdg.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                                        select new
                                        {
                                            ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                            ChucVuId = pdg.ChucVuId,
                                            CanBoId = pdg.CanBoId,
                                            PhieuDanhGiaId = pdg.PhieuDanhGiaId
                                        });


            var dsDonVi = _donViRepos.GetAll().Where(x => x.CapDonViId == (int)AppConsts.CapDonVi.Xa_Phuong);

            long thangDiemId = 0;
            var thangDiem = _thangDiemRepos.GetAll().FirstOrDefault();
            if (thangDiem != null)
            {
                thangDiemId = thangDiem.Id;
            }

            data.ThangDiemChiTiet = _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == thangDiemId)
                  .Select(x => new KetQuaThangDiemChiTietDto
                  {
                      ThangDiemChiTietId = x.Id,
                      TenThangDiem = x.Ten,
                      SoLuong = 0
                  }).ToList();


            data.LstKetQuaDonVi = new List<KetQuaDonVi>();
            foreach (var dv in dsDonVi)
            {
                var getAllCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => x.DonViId == dv.Id).Select(x => new
                {
                    CanBoId = x.Id,
                    LoaiHopDong = x.LoaiHopDong
                });

                int soLuongDuocDanhGia = 0;
                var kqDonVi = new KetQuaDonVi();
                kqDonVi.LstNhomChucVu = data.LstNhomChucVu.Select(x => x.Clone()).ToList();

                foreach (var ncv in kqDonVi.LstNhomChucVu)
                {
                    ncv.IsNhomChucVu = true;
                    if (ncv.TenNhom == "Công chức (NĐ số 92/2009/NĐ-CP)" || ncv.TenNhom == "LĐHĐ làm công tác chuyên môn")
                    {
                        var getAllCanBoThuocDonVi1 = getAllCanBoThuocDonVi.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HDND92);

                        var kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi1.Any(d => d.CanBoId == x.CanBoId)
                        && ncv.LstChucVu.Any(d => d.ChucVuId == x.ChucVuId));

                        ncv.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                        foreach (var td in ncv.LstThangDiem)
                        {
                            td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                        }
                        soLuongDuocDanhGia += kq.Count();
                    }
                    else if (ncv.TenNhom == "LĐHĐ làm công tác chuyên môn")
                    {
                        var getAllCanBoThuocDonVi1 = getAllCanBoThuocDonVi.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HopDong);

                        var kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi1.Any(d => d.CanBoId == x.CanBoId)
                        && ncv.LstChucVu.Any(d => d.ChucVuId == x.ChucVuId));

                        ncv.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();

                        foreach (var td in ncv.LstThangDiem)
                        {
                            td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                        }
                        soLuongDuocDanhGia += kq.Count();
                    }
                    else
                    {
                        ncv.IsNhomChucVu = false;

                        foreach (var item in ncv.LstChucVu)
                        {
                            var kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi.Any(d => d.CanBoId == x.CanBoId)
                            && x.ChucVuId == item.ChucVuId);

                            item.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                            foreach (var td in item.LstThangDiem)
                            {
                                td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                            }
                            soLuongDuocDanhGia += kq.Count();
                        }
                    }
                }


                kqDonVi.TenDonVi = dv.Ten;
                kqDonVi.TongSo = getAllCanBoThuocDonVi.Count();
                kqDonVi.SoDuocDanhGia = soLuongDuocDanhGia;
                kqDonVi.SoKhongDanhGia = getAllCanBoThuocDonVi.Count() - soLuongDuocDanhGia;
                kqDonVi.LyDoKhongDanhGia = "";
                data.LstKetQuaDonVi.Add(kqDonVi);
            }
            return data;
        }

        public TongHopKhoiDang06Dto TongHopKhoiCacDang06(FilterBaoCaoInput input)
        {
            var data = new TongHopKhoiDang06Dto();

            var dDate = ConvertDateStringToDateTime(input.ThoiGianBatDau);
            var thang = dDate.Month;
            var nam = dDate.Year;

            var lstNhomDoiTuong = _baoCaoNhomDoiTuongRepos.GetAll().Where(x => input.LstNhomChucVu.Any(d => d == x.Id));
            var lstNhomChucVu = lstNhomDoiTuong.Where(x => x.LoaiNhomDoiTuong == (int)AppConsts.LoaiNhomDoiTuong.NhomChucVu);
            var lstNhomDonVi = lstNhomDoiTuong.Where(x => x.LoaiNhomDoiTuong == (int)AppConsts.LoaiNhomDoiTuong.NhomDonVi);

            #region thang diem
            long thangDiemId = 0;
            var thangDiem = _thangDiemRepos.GetAll().FirstOrDefault();
            if (thangDiem != null)
            {
                thangDiemId = thangDiem.Id;
            }

            data.ThangDiemChiTiet = _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == thangDiemId)
                  .Select(x => new KetQuaThangDiemChiTietDto
                  {
                      ThangDiemChiTietId = x.Id,
                      TenThangDiem = x.Ten,
                      SoLuong = 0
                  }).ToList();
            #endregion

            var lstHeader = new List<ObjHeader>();
            lstHeader.Add(new ObjHeader { Key = "CapTruong" });
            lstHeader.Add(new ObjHeader { Key = "CapPho" });
            lstHeader.Add(new ObjHeader { Key = "CongChuc" });
            lstHeader.Add(new ObjHeader { Key = "LaoDongHopDong" });
            lstHeader.Add(new ObjHeader { Key = "LDHD68" });

            var lstPhieuDanhGiaCanBo = (from pdg in _phieuDanhGiaCanBoRepos.GetAll()
                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                        where (pdg.Nam >= nam && pdg.Thang >= thang)
                                         && pdg.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                                        select new
                                        {
                                            ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                            ChucVuId = pdg.ChucVuId,
                                            CanBoId = pdg.CanBoId,
                                            PhieuDanhGiaId = pdg.PhieuDanhGiaId
                                        });

            data.LstKetQuaDonVi = new List<KetQuaDonVi06>();

            int indexLv1 = 1;
            int indexLv2 = 1;
            foreach (var ncv in lstNhomChucVu)
            {
                #region Lv 1              
                var lv1 = new KetQuaDonVi06();
                lv1.TenDonVi = ncv.TenHienThiTrenBaoCao;
                lv1.SttLv1 = indexLv1;
                lv1.Lv1 = 1;
                data.LstKetQuaDonVi.Add(lv1);
                indexLv1++;
                #endregion

                var lstChucVu = (from ct in _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == ncv.Id)
                                 from cv in _chucVuRepos.GetAll().Where(x => x.Id == ct.ChucVuId)
                                 select new
                                 {
                                     TenChucVu = cv.Ten,
                                     ChucVuId = cv.Id
                                 });

                foreach (var cv in lstChucVu)
                {
                    var lv2 = new KetQuaDonVi06();
                    lv2.SttLv2 = indexLv2;
                    lv2.Lv2 = 2;
                    var lstCanBoChucVu = (from cbcv in _canBoChucVuRepos.GetAll()
                                          from cva in _chucVuRepos.GetAll().Where(x => x.Id == cbcv.ChucVuId)
                                          where cbcv.ChucVuId == cv.ChucVuId
                                          select new
                                          {
                                              CanBoId = cbcv.CanBoId,
                                              ChucVuId = cbcv.ChucVuId,
                                              CapLanhDao = cva.CapLanhDao
                                          });

                    var lstKetQua = (from pdg in lstPhieuDanhGiaCanBo
                                     from cbcv in lstCanBoChucVu.Where(x => x.CanBoId == x.CanBoId && x.ChucVuId == pdg.ChucVuId)
                                     select new
                                     {
                                         CapLanhDao = cbcv.CapLanhDao,
                                         CanBoId = pdg.CanBoId,
                                         ThangDiemChiTietId = pdg.ThangDiemChiTietId
                                     });

                    lv2.LstNhomChucVu = new List<NhomChucVu06Dto>();
                    foreach (var item in lstHeader)
                    {
                        var nvckq = new NhomChucVu06Dto();

                        if (item.Key == "CapTruong")
                        {
                            var kq = lstKetQua.Where(x => x.CapLanhDao == (int)AppConsts.CapLanhDao.CapTruong);

                            nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                            foreach (var td in nvckq.LstThangDiem)
                            {
                                td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                            }
                        }
                        else if (item.Key == "CapPho")
                        {
                            var kq = lstKetQua.Where(x => x.CapLanhDao == (int)AppConsts.CapLanhDao.CapPho);

                            nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                            foreach (var td in nvckq.LstThangDiem)
                            {
                                td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                            }
                        }
                        else if (item.Key == "CongChuc" || item.Key == "LaoDongHopDong" || item.Key == "LDHD68")
                        {
                            var kq = (from pdg in lstKetQua
                                      from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                      select new
                                      {
                                          ThangDiemChiTietId = pdg.ThangDiemChiTietId,
                                          LoaiHopDong = cb.LoaiHopDong
                                      });

                            if (item.Key == "CongChuc")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.CongChuc);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                            else if (item.Key == "LaoDongHopDong")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HopDong);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                            else if (item.Key == "LDHD68")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.LDHDND68);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }

                            nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                            foreach (var td in nvckq.LstThangDiem)
                            {
                                td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                            }
                        }

                        lv2.LstNhomChucVu.Add(nvckq);
                        indexLv2++;
                    }



                    //soLuongDuocDanhGia += kq.Count();
                    lv2.TenDonVi = cv.TenChucVu;
                    lv2.TongSo = lstCanBoChucVu.Count();
                    lv2.SoDuocDanhGia = lstKetQua.Count();
                    lv2.SoKhongDanhGia = lv2.TongSo - lv2.SoDuocDanhGia;
                    lv2.LyDoKhongDanhGia = "";
                    data.LstKetQuaDonVi.Add(lv2);
                }
            }


            foreach (var ndv in lstNhomDonVi)
            {
                // I
                var lv1 = new KetQuaDonVi06();
                lv1.TenDonVi = ndv.TenHienThiTrenBaoCao;
                lv1.SttLv1 = indexLv1;
                lv1.Lv1 = 1;
                data.LstKetQuaDonVi.Add(lv1);
                indexLv1++;

                var lstDonVi = (from ct in _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == ndv.Id)
                                from cv in _donViRepos.GetAll().Where(x => x.Id == ct.DonViId)
                                select new
                                {
                                    TenDonVi = cv.Ten,
                                    DonViId = cv.Id
                                });

                foreach (var dv in lstDonVi)
                {
                    var lv2 = new KetQuaDonVi06();
                    lv1.SttLv2 = indexLv2;
                    lv2.Lv2 = 2;
                    var lstCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => x.DonViId == dv.DonViId);

                    var lstKetQua = (from pdg in lstPhieuDanhGiaCanBo
                                     from cbdv in lstCanBoThuocDonVi.Where(x => x.Id == pdg.CanBoId)
                                     select new
                                     {
                                         CanBoId = pdg.CanBoId,
                                         ChucVuId = pdg.ChucVuId,
                                         ThangDiemChiTietId = pdg.ThangDiemChiTietId
                                     });

                    lv2.LstNhomChucVu = new List<NhomChucVu06Dto>();
                    foreach (var item in lstHeader)
                    {
                        var nvckq = new NhomChucVu06Dto();

                        if (item.Key == "CapTruong" || item.Key == "CapPho")
                        {
                            var kqcv = (from pdg in lstKetQua
                                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                                        select new
                                        {
                                            CapLanhDao = cv.CapLanhDao,
                                            ThangDiemChiTietId = pdg.ThangDiemChiTietId
                                        });
                            if (item.Key == "CapTruong")
                            {
                                var kq = kqcv.Where(x => x.CapLanhDao == (int)AppConsts.CapLanhDao.CapTruong);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                            else
                            {
                                var kq = kqcv.Where(x => x.CapLanhDao == (int)AppConsts.CapLanhDao.CapPho);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                        }
                        else if (item.Key == "CongChuc" || item.Key == "LaoDongHopDong" || item.Key == "LDHD68")
                        {
                            var kq = (from pdg in lstKetQua
                                      from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                                      select new
                                      {
                                          ThangDiemChiTietId = pdg.ThangDiemChiTietId,
                                          LoaiHopDong = cb.LoaiHopDong
                                      });

                            if (item.Key == "CongChuc")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.CongChuc);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                            else if (item.Key == "LaoDongHopDong")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HopDong);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }
                            else if (item.Key == "LDHD68")
                            {
                                var kq2 = kq.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.LDHDND68);

                                nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                                foreach (var td in nvckq.LstThangDiem)
                                {
                                    td.SoLuong = kq2.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                                }
                            }

                            nvckq.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                            foreach (var td in nvckq.LstThangDiem)
                            {
                                td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                            }
                        }

                        lv2.LstNhomChucVu.Add(nvckq);
                        indexLv2++;
                    }

                    //soLuongDuocDanhGia += kq.Count();
                    lv2.TenDonVi = dv.TenDonVi;
                    lv2.TongSo = lstCanBoThuocDonVi.Count();
                    lv2.SoDuocDanhGia = lstKetQua.Count();
                    lv2.SoKhongDanhGia = lv2.TongSo - lv2.SoDuocDanhGia;
                    lv2.LyDoKhongDanhGia = "";
                    data.LstKetQuaDonVi.Add(lv2);
                }
            }

            return data;
        }

        #endregion

        #region TrieuMinhHa TruongHoc
        public TongHopKhoiTruongHoc05Dto TongHopKhoiTruongHoc05(FilterBaoCaoInput input)
        {
            var data = new TongHopKhoiTruongHoc05Dto();
            data.LstKetQuaDonViTHCS = new List<KetQuaDonVi>();
            data.LstKetQuaDonViTieuHoc = new List<KetQuaDonVi>();
            data.LstKetQuaDonViMamNon = new List<KetQuaDonVi>();
            data.SoLuongThangDiemDanhGia = 0;
            var dDate = ConvertDateStringToDateTime(input.ThoiGianBatDau);
            var thang = dDate.Month;
            var nam = dDate.Year;

            var lstChucVu = (from n in _baoCaoNhomDoiTuongRepos.GetAll()
                             from ct in _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == n.Id)
                             from cv in _chucVuRepos.GetAll().Where(x => x.Id == ct.ChucVuId)
                             where input.LstNhomChucVu.Any(d => d == n.Id)
                             select new
                             {
                                 TenNhom = n.TenHienThiTrenBaoCao,
                                 NhomId = n.Id,
                                 ChucVuId = cv.Id,
                                 TenChucVu = cv.Ten,
                                 ThuTu = n.ThuTu
                             });

            data.LstNhomChucVu = lstChucVu.GroupBy(x => new { x.NhomId, x.TenNhom })
                .Select(g => new NhomChucVuDto
                {
                    NhomChucVuId = g.Key.NhomId,
                    TenNhom = g.Key.TenNhom,
                    ThuTu = g.FirstOrDefault().ThuTu,
                    IsNhomChucVu = (g.Key.TenNhom == "Công chức (NĐ số 92/2009/NĐ-CP)" || g.Key.TenNhom == "LĐHĐ làm công tác chuyên môn") ? true : false,
                    LstChucVu = g.Select(y => new ChucVuChiTietDto
                    {
                        ChucVuId = y.ChucVuId,
                        TenChucVu = y.TenChucVu,
                    }).OrderBy(x => x.ChucVuId).ToList()
                }).OrderBy(x => x.ThuTu).ToList();



            var lstPhieuDanhGiaCanBo = (from pdg in _phieuDanhGiaCanBoRepos.GetAll()
                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                        where (pdg.Nam >= nam && pdg.Thang >= thang)
                                         && pdg.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh
                                        select new
                                        {
                                            ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                            ChucVuId = pdg.ChucVuId,
                                            CanBoId = pdg.CanBoId,
                                            PhieuDanhGiaId = pdg.PhieuDanhGiaId
                                        });


            var dsDonVi = _donViRepos.GetAll().Where(x => x.CapDonViId == (int)AppConsts.KhoiHanhChinh.TruongHoc);

            long thangDiemId = 0;
            if (lstPhieuDanhGiaCanBo.Count() > 0)
            {
                thangDiemId = _thangDiemChiTietRepos.GetAll().Where(x => x.Id == lstPhieuDanhGiaCanBo.FirstOrDefault().ThangDiemChiTietId)
                    .FirstOrDefault().ThangDiemId;
            }

            data.ThangDiemChiTiet = _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == thangDiemId)
                .Select(x => new KetQuaThangDiemChiTietDto
                {
                    ThangDiemChiTietId = x.Id,
                    TenThangDiem = x.Ten,
                    SoLuong = 0
                }).ToList();


            foreach (var dv in dsDonVi)
            {
                var getAllCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => x.DonViId == dv.Id).Select(x => new
                {
                    CanBoId = x.Id,
                    LoaiHopDong = x.LoaiHopDong
                });

                int soLuongDuocDanhGia = 0;
                var kqDonVi = new KetQuaDonVi();
                kqDonVi.LstNhomChucVu = data.LstNhomChucVu.Select(x => x.Clone()).ToList();

                foreach (var ncv in kqDonVi.LstNhomChucVu)
                {
                    ncv.IsNhomChucVu = true;
                    if (ncv.TenNhom == "Giáo viên" || ncv.TenNhom == "Nhân viên hành chính")
                    {
                        var getAllCanBoThuocDonVi1 = getAllCanBoThuocDonVi.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.VienChuc);

                        var kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi1.Any(d => d.CanBoId == x.CanBoId)
                        && ncv.LstChucVu.Any(d => d.ChucVuId == x.ChucVuId));

                        ncv.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                        foreach (var td in ncv.LstThangDiem)
                        {
                            td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                        }
                        soLuongDuocDanhGia += kq.Count();
                         getAllCanBoThuocDonVi1 = getAllCanBoThuocDonVi.Where(x => x.LoaiHopDong == (int)AppConsts.LoaiHopDong.HopDong);

                         kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi1.Any(d => d.CanBoId == x.CanBoId)
                        && ncv.LstChucVu.Any(d => d.ChucVuId == x.ChucVuId));
                        var listDataHopDong = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();
                        foreach (var td in listDataHopDong)
                        {
                            td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                        }
                        ncv.LstThangDiem.AddRange(listDataHopDong);
                        soLuongDuocDanhGia += kq.Count();
                        data.SoLuongThangDiemDanhGia += 8;
                    }
                    else
                    {
                        var kq = lstPhieuDanhGiaCanBo.Where(x => getAllCanBoThuocDonVi.Any(d => d.CanBoId == x.CanBoId)
                        && ncv.LstChucVu.Any(d => d.ChucVuId == x.ChucVuId));

                        ncv.LstThangDiem = data.ThangDiemChiTiet.Select(x => x.Clone()).ToList();

                        foreach (var td in ncv.LstThangDiem)
                        {
                            td.SoLuong = kq.Where(x => x.ThangDiemChiTietId == td.ThangDiemChiTietId).Count();
                        }
                        soLuongDuocDanhGia += kq.Count();
                        data.SoLuongThangDiemDanhGia += 4;
                    }
                }


                kqDonVi.TenDonVi = dv.Ten;
                kqDonVi.TongSo = getAllCanBoThuocDonVi.Count();
                kqDonVi.SoDuocDanhGia = soLuongDuocDanhGia;
                kqDonVi.SoKhongDanhGia = getAllCanBoThuocDonVi.Count() - soLuongDuocDanhGia;
                kqDonVi.LyDoKhongDanhGia = "";
                if(dv.KhoiTruongHocId == (int)AppConsts.KhoiTruongHoc.THCS)
                {
                    data.LstKetQuaDonViTHCS.Add(kqDonVi);
                }
                if (dv.KhoiTruongHocId == (int)AppConsts.KhoiTruongHoc.TieuHoc)
                {
                    data.LstKetQuaDonViTieuHoc.Add(kqDonVi);
                }
                if (dv.KhoiTruongHocId == (int)AppConsts.KhoiTruongHoc.MamNon)
                {
                    data.LstKetQuaDonViMamNon.Add(kqDonVi);
                }
            }






            //report.RegData("dtTable", lstData);
            //report.Dictionary.Variables["LoaiThoiGian"].Value = loaiThoiGianView;
            //report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName;

            //report.Dictionary.Variables["TileHTXS"].Value = ((decimal)HTXS / (decimal)thamgia * 100).ToString("#0");
            //report.Dictionary.Variables["TileHTT"].Value = ((decimal)HTT / (decimal)thamgia * 100).ToString("#0");
            //report.Dictionary.Variables["TileHT"].Value = ((decimal)HT / (decimal)thamgia * 100).ToString("#0");
            //report.Dictionary.Variables["TileKHT"].Value = ((decimal)KHT / (decimal)thamgia * 100).ToString("#0");

            return data;
        }
        #endregion
    }
}
