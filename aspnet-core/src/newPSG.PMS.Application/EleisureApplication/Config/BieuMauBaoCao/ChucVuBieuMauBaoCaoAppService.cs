﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class ChucVuBieuMauBaoCaoAppService : PMSAppServiceBase, IChucVuBieuMauBaoCao
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<ChucVuBieuMauBaoCao, long> _repos; 
        private readonly ICache _mainCache;
        public ChucVuBieuMauBaoCaoAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<ChucVuBieuMauBaoCao, long> repos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos; 
            _mainCache = _cacheManager.GetCache("ChucVuBieuMauBaoCaoAppService");
        }

        public PagedResultDto<ChucVuBieuMauBaoCaoDto> GetByFilter(ChucVuBieuMauBaoCaoInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (!input.ChucVuId.HasValue || x.ChucVuId == input.ChucVuId.Value)
                        && (!input.BieuMauBaoCaoId.HasValue || x.BIeuMauBaoCaoId == input.BieuMauBaoCaoId.Value)
                        select new ChucVuBieuMauBaoCaoDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,  
                            ChucVuId = x.ChucVuId, 
                            BieuMauBaoCaoId = x.BIeuMauBaoCaoId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList(); 
            
            return new PagedResultDto<ChucVuBieuMauBaoCaoDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<long> Upsert(ChucVuBieuMauBaoCaoDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _repos.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            { 
                var entity =  ObjectMapper.Map<ChucVuBieuMauBaoCao>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = _repos.InsertAndGetId(entity);  
            }
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
            return input.Id;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
