﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using Stimulsoft.Report;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
namespace newPSG.PMS.EleisureApplication
{
    public partial class BaoCaoAppService
    {

        public List<BaoCaoDto> GetBaoCaoTheoChucVu(BaoCaoInput input)
        {
            List<BaoCaoDto> data = (from bccv in _baoCaoChucVuRepos.GetAll()
                                    from bc in _baoCaoRepos.GetAll().Where(x => x.Id == bccv.BaoCaoId)
                                    where bccv.ChucVuId == input.ChucVuId
                                    select new BaoCaoDto
                                    {
                                        Id = bc.Id,
                                        TenBaoCao = bc.TenBaoCao,
                                        Source = bc.Source,
                                    }).OrderBy(x=>x.Id).ToList();
            return data;
        }
        public List<BaoCaoChucVuCauHinhDto> GetBaoCaoChucVuCauHinhTheoChucVu(BaoCaoChucVuCauHinhInput input)
        {
            var query = (from n in _baoCaoChucVuCauHinhRepos.GetAll()
                         from dt in _baoCaoNhomDoiTuongRepos.GetAll().Where(x => x.Id == n.BaoCaoNhomDoiTuongId)
                         where n.ChucVuId == input.ChucVuId
                         select new BaoCaoChucVuCauHinhDto
                         {
                             Id = n.Id,
                             TenantId = n.TenantId,
                             BaoCaoNhomDoiTuongId = n.BaoCaoNhomDoiTuongId,
                             TenNhomDoiTuong = dt.TenNhomDoiTuong,
                         }).OrderBy(x => x.Id).ToList();

            return query;
        }

        #region Bao Cao Nhom Doi Tuong
        public PagedResultDto<BaoCaoNhomDoiTuongDto> GetBaoCaoNhomDoiTuong(BaoCaoInput input)
        {
            var query = (from n in _baoCaoNhomDoiTuongRepos.GetAll()
                         where (input.Filter == null || input.Filter == "" || n.TenNhomDoiTuong.Contains(input.Filter))
                         && (n.LoaiNhomDoiTuong == input.LoaiNhomDoiTuong)
                         select new BaoCaoNhomDoiTuongDto
                         {
                             Id = n.Id,
                             TenantId = n.TenantId,
                             TenNhomDoiTuong = n.TenNhomDoiTuong,
                             TenHienThiTrenBaoCao = n.TenHienThiTrenBaoCao,
                             LoaiNhomDoiTuong = n.LoaiNhomDoiTuong
                         }).OrderByDescending(x => x.Id).ToList();

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<BaoCaoNhomDoiTuongDto>(totalCount, items);
        }
        public async Task<long> InsertOrUpdateBaoCaoNhomDoiTuong(BaoCaoNhomDoiTuongDto input)
        {
            long result = 0;

            try
            {
                if (input.Id > 0)
                {
                    var category = _baoCaoNhomDoiTuongRepos.Get(input.Id);
                    category.TenNhomDoiTuong = input.TenNhomDoiTuong;
                    category.TenHienThiTrenBaoCao = input.TenHienThiTrenBaoCao;
                    category.LoaiNhomDoiTuong = input.LoaiNhomDoiTuong;
                    result = input.Id;
                }
                else
                {
                    var entity = new BaoCaoNhomDoiTuong();
                    entity.TenNhomDoiTuong = input.TenNhomDoiTuong;
                    entity.TenHienThiTrenBaoCao = input.TenHienThiTrenBaoCao;
                    entity.LoaiNhomDoiTuong = input.LoaiNhomDoiTuong;
                    entity.TenantId = AbpSession.TenantId;

                    result = await _baoCaoNhomDoiTuongRepos.InsertAndGetIdAsync(entity);
                }
                CurrentUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                result = 0;
                throw;
            }

            return result;
        }
        public void DeleteBaoCaoNhomDoiTuong(long id)
        {
            _baoCaoNhomDoiTuongRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
        }
        #endregion

        #region bao cao nhom doi tuong chi tiet
        public List<BaoCaoNhomDoiTuongChiTietDto> GetBaoCaoNhomDoiTuongChiTiet(BaoCaoInput input)
        {
            var data = new List<BaoCaoNhomDoiTuongChiTietDto>();

            //baoCaoNhomDoiTuongChiTiet (chucVuId có thể là chucVuId hoac donViId) do chưa


            if (input.LoaiNhomDoiTuong == (int)AppConsts.LoaiNhomDoiTuong.NhomChucVu)
            {
                data = (from n in _baoCaoNhomDoiTuongChiTietRepos.GetAll()
                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == n.ChucVuId)
                        where n.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId
                        select new BaoCaoNhomDoiTuongChiTietDto
                        {
                            Id = n.Id,
                            TenantId = n.TenantId,
                            BaoCaoNhomDoiTuongId = n.BaoCaoNhomDoiTuongId,
                            ChucVuId = n.ChucVuId,
                            TenChucVu = cv.Ten
                        }).ToList();
            }
            else
            {
                data = (from n in _baoCaoNhomDoiTuongChiTietRepos.GetAll()
                        from cv in _donViRepos.GetAll().Where(x => x.Id == n.ChucVuId)
                        where n.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId
                        select new BaoCaoNhomDoiTuongChiTietDto
                        {
                            Id = n.Id,
                            TenantId = n.TenantId,
                            BaoCaoNhomDoiTuongId = n.BaoCaoNhomDoiTuongId,
                            ChucVuId = n.ChucVuId,
                            TenChucVu = cv.Ten
                        }).ToList();
            }
           
            return data;
        }
        public async Task<bool> InsertBaoCaoNhomDoiTuongChiTiet(BaoCaoNhomDoiTuongChiTietInsert input)
        {
            bool result = false;

            try
            {
                //_baoCaoNhomDoiTuongChiTietRepos.Delete(x => x.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId);
                foreach (var chucVuId in input.LstChucVu)
                {
                    var newObj = new BaoCaoNhomDoiTuongChiTiet();
                    newObj.ChucVuId = chucVuId;
                    newObj.BaoCaoNhomDoiTuongId = input.BaoCaoNhomDoiTuongId;
                    newObj.TenantId = AbpSession.TenantId;
                    await _baoCaoNhomDoiTuongChiTietRepos.InsertAndGetIdAsync(newObj);
                }
                result = true;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }

        public async Task<bool> InsertBaoCaoNhomDoiTuongChiTietDonVi(BaoCaoNhomDoiTuongChiTietInsert input)
        {
            bool result = false;

            try
            {
                //_baoCaoNhomDoiTuongChiTietRepos.Delete(x => x.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId);
                foreach (var chucVuId in input.LstChucVu)
                {
                    var newObj = new BaoCaoNhomDoiTuongChiTiet();
                    newObj.DonViId = chucVuId;
                    newObj.BaoCaoNhomDoiTuongId = input.BaoCaoNhomDoiTuongId;
                    newObj.TenantId = AbpSession.TenantId;
                    await _baoCaoNhomDoiTuongChiTietRepos.InsertAndGetIdAsync(newObj);
                }
                result = true;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }
        public void DeleteBaoCaoNhomDoiTuongChiTiet(long id)
        {
            _baoCaoNhomDoiTuongChiTietRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
        }
        #endregion

        #region Bao Cao Chuc Vu Cau Hinh

        public List<BaoCaoChucVuCauHinhDto> GetBaoCaoChucVuCauHinh(BaoCaoChucVuCauHinhInput input)
        {
            var query = (from n in _baoCaoChucVuCauHinhRepos.GetAll()
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == n.ChucVuId)
                         where n.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId
                         select new BaoCaoChucVuCauHinhDto
                         {
                             Id = n.Id,
                             TenantId = n.TenantId,
                             BaoCaoNhomDoiTuongId = n.BaoCaoNhomDoiTuongId,
                             ChucVuId = n.ChucVuId,
                             TenChucVu = cv.Ten
                         }).ToList();

            return query;
        }

        public async Task<bool> InsertOrUpdateBaoCaoChucVuCauHinh(BaoCaoChucVuCauHinhInsert input)
        {
            bool result = false;

            try
            {
                _baoCaoChucVuCauHinhRepos.Delete(x => x.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId);
                foreach (var cvId in input.LstChucVuId)
                {
                    var newObj = new BaoCaoChucVuCauHinh();
                    newObj.ChucVuId = cvId;
                    newObj.BaoCaoNhomDoiTuongId = input.BaoCaoNhomDoiTuongId;
                    newObj.TenantId = AbpSession.TenantId;
                    await _baoCaoChucVuCauHinhRepos.InsertAndGetIdAsync(newObj);
                }
                CurrentUnitOfWork.SaveChanges();
                result = true;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            return result;
        }
        public void DeleteBaoCaoChucVuCauHinh(long id)
        {
            _baoCaoChucVuCauHinhRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
        }
        #endregion


        #region bao cao      
        public PagedResultDto<BaoCaoDto> GetAllBaoCao(BaoCaoInput input)
        {
            input.Format();
            var query = from x in _baoCaoRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.TenBaoCao.Contains(input.Filter) || x.Mota.Contains(input.Filter) || x.MaBaoCao.Contains(input.Filter) || x.DuongDanBieuMau.Contains(input.Filter))

                        select new BaoCaoDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            DuongDanBieuMau = x.DuongDanBieuMau,
                            MaBaoCao = x.MaBaoCao,
                            Mota = x.Mota,
                            TenBaoCao = x.TenBaoCao,
                            Source = x.Source
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<BaoCaoDto>(totalCount, items);
        }
        public async Task<long> InsertOrUpdateBaoCao(BaoCaoDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _baoCaoRepos.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            {
                if (_baoCaoRepos.FirstOrDefault(x => x.TenBaoCao == input.TenBaoCao) != null)
                {
                    return -1;
                }
                var entity = ObjectMapper.Map<BaoCao>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = await _baoCaoRepos.InsertAndGetIdAsync(entity);
            }
            CurrentUnitOfWork.SaveChanges();
            return input.Id;
        }
        public void DeleteBaoCao(long id)
        {
            _baoCaoRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
        }
        #endregion

        #region Bao cao chuc vu
        public List<BaoCaoDto> GetChucVuTheoBaoCao(long baoCaoId)
        {
            var query = (from bccv in _baoCaoChucVuRepos.GetAll()
                         from bc in _baoCaoRepos.GetAll().Where(x => x.Id == bccv.BaoCaoId)
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == bccv.ChucVuId)
                         where bccv.BaoCaoId == baoCaoId
                         select new BaoCaoDto
                         {
                             Id = bccv.Id,
                             TenantId = bc.TenantId,
                             DuongDanBieuMau = bc.DuongDanBieuMau,
                             MaBaoCao = bc.MaBaoCao,
                             Mota = bc.Mota,
                             TenBaoCao = bc.TenBaoCao,
                             Source = bc.Source,
                             TenChucVu = cv.Ten,
                             ChucVuId = cv.Id
                         }).ToList();

            return query;
        }

        public async Task<bool> InsertBaoCaoChucVu(BaoCaoChucVuInsert input)
        {
            bool result = false;
            try
            {
                _baoCaoChucVuRepos.Delete(x => x.BaoCaoId == input.BaoCaoId);
                foreach (var cvId in input.LstChucVuId)
                {
                    var newObj = new BaoCaoChucVu();
                    newObj.ChucVuId = cvId;
                    newObj.BaoCaoId = input.BaoCaoId;
                    newObj.TenantId = AbpSession.TenantId;
                    await _baoCaoChucVuRepos.InsertAndGetIdAsync(newObj);
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }

            return result;
        }
        public void DeleteBaoCaoChucVu(long id)
        {
            _baoCaoChucVuRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
        }
        #endregion

    }
}