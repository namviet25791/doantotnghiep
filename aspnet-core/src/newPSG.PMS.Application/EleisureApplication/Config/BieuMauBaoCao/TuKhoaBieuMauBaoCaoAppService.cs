﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class TuKhoaBieuMauBaoCaoAppService : PMSAppServiceBase, ITuKhoaBieuMauBaoCao
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<TuKhoaBieuMauBaoCao, long> _repos;
        private readonly IRepository<TuKhoa, long> _tukhoaRepos;
        private readonly ICache _mainCache;
        public TuKhoaBieuMauBaoCaoAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<TuKhoaBieuMauBaoCao, long> repos,
            IRepository<TuKhoa, long> tukhoaRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _tukhoaRepos = tukhoaRepos;
            _mainCache = _cacheManager.GetCache("TuKhoaBieuMauBaoCaoAppService");
        }

        public PagedResultDto<TuKhoaBieuMauBaoCaoResponse> GetByFilter(TuKhoaBieuMauBaoCaoInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        join cv in _tukhoaRepos.GetAll() on x.TuKhoaId equals cv.Id into cvt
                        from cv in cvt.DefaultIfEmpty()
                        where (!input.BieuMauBaoCaoId.HasValue || x.BieuMauBaoCaoId == input.BieuMauBaoCaoId.Value)
                        && (!input.TuKhoaId.HasValue || x.BieuMauBaoCaoId == input.TuKhoaId.Value)
                        select new TuKhoaBieuMauBaoCaoResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            BieuMauBaoCaoId = x.BieuMauBaoCaoId,
                            TuKhoaId = x.TuKhoaId,
                            MaTuKhoa= (cv != null) ? cv.Ma : ""
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<TuKhoaBieuMauBaoCaoResponse>(totalCount, items);
        }

        public async System.Threading.Tasks.Task<List<long>> Upsert(TuKhoaBieuMauBaoCaoUpdate input)
        {
           
            List<long> ids = new List<long>();

            var existedEntities = _repos.GetAllList(x => x.BieuMauBaoCaoId == input.BieuMauBaoCaoId);
            _repos.Delete(x => x.BieuMauBaoCaoId == input.BieuMauBaoCaoId && !input.TuKhoaIds.Contains(x.TuKhoaId));
            CurrentUnitOfWork.SaveChanges();
            foreach (var tukhoa in input.TuKhoaIds)
            {
                if (!existedEntities.Exists(x => x.TuKhoaId == tukhoa))
                {
                    var entity = ObjectMapper.Map<TuKhoaBieuMauBaoCao>(new TuKhoaBieuMauBaoCaoDto()
                    {
                        BieuMauBaoCaoId = input.BieuMauBaoCaoId,
                        TuKhoaId = tukhoa,
                        TenantId = input.TenantId
                    });

                    entity.TenantId = AbpSession.TenantId;
                    var id = _repos.InsertAndGetId(entity);

                    CurrentUnitOfWork.SaveChanges();
                    ids.Add(id);
                }
            }

            _mainCache.Clear();
            return ids;
        }


        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
