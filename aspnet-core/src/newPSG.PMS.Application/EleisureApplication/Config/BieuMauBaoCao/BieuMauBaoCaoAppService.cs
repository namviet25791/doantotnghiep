﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class BieuMauBaoCaoAppService : PMSAppServiceBase, IBieuMauBaoCao
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<BieuMauBaoCao, long> _repos; 
        private readonly ICache _mainCache;
        public BieuMauBaoCaoAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<BieuMauBaoCao, long> repos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos; 
            _mainCache = _cacheManager.GetCache("BieuMauBaoCaoAppService");
        }

        public PagedResultDto<BieuMauBaoCaoDto> GetByFilter(BieuMauBaoCaoInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter)
                        || x.Ma.Contains(input.Filter) || x.NoiDung.Contains(input.Filter))  
                        select new BieuMauBaoCaoDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId, 
                            Ten  = x.Ten,   
                            Ma = x.Ma,
                            NoiDung = x.NoiDung
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList(); 
            
            return new PagedResultDto<BieuMauBaoCaoDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<long> Upsert(BieuMauBaoCaoDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _repos.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            {
                if (_repos.FirstOrDefault(x => x.Ten == input.Ten) != null)
                {
                    return -1;
                }
                var entity =  ObjectMapper.Map<BieuMauBaoCao>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = _repos.InsertAndGetId(entity);  
            }
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
            return input.Id;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
