﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using Stimulsoft.Report;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public partial class BaoCaoAppService : PMSAppServiceBase, IBaoCao
    {
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<BaoCao, long> _baoCaoRepos;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhGiaCanBoRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaChiTietRepos;
        private readonly IRepository<LoaiDonVi, long> _loaiDonViRepos;
        private readonly IRepository<DonVi, long> _donViRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<BaoCaoChucVu, long> _baoCaoChucVuRepos;
        private readonly IRepository<BaoCaoChucVuCauHinh, long> _baoCaoChucVuCauHinhRepos;
        private readonly IRepository<BaoCaoNhomDoiTuong, long> _baoCaoNhomDoiTuongRepos;
        private readonly IRepository<BaoCaoNhomDoiTuongChiTiet, long> _baoCaoNhomDoiTuongChiTietRepos;

        private readonly UserManager _userManager;
        public BaoCaoAppService(IRepository<CanBo, long> canBoRepos,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhGiaCanBoRepos,
            IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaChiTietRepos,
          UserManager userManager,
          IRepository<DonVi, long> donViRepos,
          IRepository<BaoCao, long> baoCaoRepos,
          IRepository<LoaiDonVi, long> loaiDonViRepos,
          IRepository<ChucVu, long> chucVuRepos,
          IRepository<CanBoChucVu, long> canBoChucVuRepos,
          IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
          IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
          IRepository<BaoCaoNhomDoiTuong, long> baoCaoNhomDoiTuongRepos,
          IRepository<BaoCaoChucVu, long> baoCaoChucVuRepos,
          IRepository<BaoCaoChucVuCauHinh, long> baoCaoChucVuCauHinhRepos,
          IRepository<BaoCaoNhomDoiTuongChiTiet, long> baoCaoNhomDoiTuongChiTietRepos,
            IRepository<ThangDiem, long> thangDiemRepos)
        {
            _canBoRepos = canBoRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
            _phieuDanhGiaCanBoRepos = phieuDanhGiaCanBoRepos;
            _lichSuKetQuaDanhGiaChiTietRepos = lichSuKetQuaDanhGiaChiTietRepos;
            _loaiDonViRepos = loaiDonViRepos;
            _userManager = userManager;
            _donViRepos = donViRepos;
            _baoCaoRepos = baoCaoRepos;
            _chucVuRepos = chucVuRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _baoCaoNhomDoiTuongRepos = baoCaoNhomDoiTuongRepos;
            _baoCaoChucVuRepos = baoCaoChucVuRepos;
            _baoCaoChucVuCauHinhRepos = baoCaoChucVuCauHinhRepos;
            _baoCaoNhomDoiTuongChiTietRepos = baoCaoNhomDoiTuongChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
        }

        public IList<long> GetAllDonViCapThapNhat(IList<DonViDto> arrs, long? parentId = null)
        {
            var arrByParentIds = arrs.Where(item => item.DonViQuanLyId == parentId).ToList();
            var results = new List<long>();

            arrByParentIds.ForEach(item =>
            {
                var arr2s = arrs.Where(item2 => item2.DonViQuanLyId == item.Id).ToList();

                if (arr2s.Count == 0)
                {
                    results.Add(item.Id);

                    return;
                }

                var arr3s = GetAllDonViCapThapNhat(arrs, item.Id);
                results = results.Concat(arr3s).ToList();
            });

            return results;
        }



        #region Báo cáo tổng hợp kết quả đánh giá xếp loại theo loại đối tượng
        public string BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong(FilterBaoCaoInput input)
        {
            DateTime dtStartDate = DateTime.Now.Date;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                dtStartDate = oDate.Date;
            }

            DateTime dtEndDate = dtStartDate;
            string loaiThoiGianView = string.Empty;

            if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Thang)
            {
                dtStartDate = new DateTime(dtStartDate.Year, dtStartDate.Month, 1);
                dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);
                loaiThoiGianView = "THÁNG " + dtStartDate.Month + " NĂM " + dtStartDate.Year;
            }
            else if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Quy)
            {
                var quy = "";
                int month = dtStartDate.Month;
                if (month == 1 || month == 2 || month == 3)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 1, 1);
                    quy = "QUÝ 1";
                }
                else if (month == 4 || month == 5 || month == 6)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 4, 1);
                    quy = "QUÝ 2";
                }
                else if (month == 7 || month == 8 || month == 9)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 7, 1);
                    quy = "QUÝ 3";
                }
                else if (month == 10 || month == 11 || month == 12)
                {
                    dtStartDate = new DateTime(dtStartDate.Year, 10, 1);
                    quy = "QUÝ 4";
                }
                dtEndDate = dtStartDate.AddMonths(3).AddDays(-1);
                loaiThoiGianView = quy + " NĂM " + dtStartDate.Year;
            }
            else if (input.LoaiThoiGian == (int)AppConsts.LoaiThoiGian.Nam)
            {
                dtStartDate = new DateTime(dtStartDate.Year, 1, 1);
                dtEndDate = dtStartDate.AddMonths(12).AddDays(-1);
                loaiThoiGianView = "NĂM " + dtStartDate.Year;
            }


            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }
            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstPhieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x =>
            (x.Nam >= dtStartDate.Year && x.Thang >= dtStartDate.Month)
            && (x.Nam <= dtEndDate.Year && x.Thang <= dtEndDate.Month) && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh);

            var lstData = new List<BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong>();

            foreach (var id in input.LstNhomChucVu)
            {
                var tenDoiTuong = _baoCaoNhomDoiTuongRepos.Get(id);
                var lstChucVuByDoiTuong = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == id)
                  .Select(x => x.ChucVuId).Distinct();

                var getPdgcb = (from cv in lstChucVuByDoiTuong
                                from pdg in lstPhieuDanhGiaCanBo.Where(x => x.ChucVuId == cv)
                                from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id && x.IsKetQuaCuoiCung == true)
                                from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                select new
                                {
                                    ChucVu = cv,
                                    NhanXet = kq.NhanXet,
                                    LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet
                                }).ToList();

                var obj = new BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong();
                obj.TenDoiTuong = tenDoiTuong.TenNhomDoiTuong;
                obj.SL_DanhGia = getPdgcb.Select(x => x.ChucVu).Distinct().Count();
                if (obj.SL_DanhGia > 0)
                {
                    obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Select(x => x.ChucVu).Distinct().Count();
                    obj.TiLe_HTXSNV = ((decimal)obj.SL_HTXSNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Select(x => x.ChucVu).Distinct().Count();
                    obj.TiLe_HTTNV = ((decimal)obj.SL_HTTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Select(x => x.ChucVu).Distinct().Count();
                    obj.TiLe_HTNV = ((decimal)obj.SL_HTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Select(x => x.ChucVu).Distinct().Count();
                    obj.TiLe_KHTNV = ((decimal)obj.SL_KHTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");
                }
                else
                {
                    obj.SL_HTXSNV = 0;
                    obj.TiLe_HTXSNV = "0";

                    obj.SL_HTTNV = 0;
                    obj.TiLe_HTTNV = "0";

                    obj.SL_HTNV = 0;
                    obj.TiLe_HTNV = "0";

                    obj.SL_KHTNV = 0;
                    obj.TiLe_KHTNV = "0";
                }
                lstData.Add(obj);
            }
            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = loaiThoiGianView;
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }
        #endregion

        #region Báo cáo tổng hợp kết quả đánh giá xếp loại theo đơn vị
        public string BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi(FilterBaoCaoInput input)
        {
            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            //report.Load("./TemplateBaoCao/BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi.mrt");
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstDonVi = new List<long>();

            var lstCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => input.LstDonVi.Any(d => d == x.DonViId))
                .Select(x => new { Id = x.Id, DonViId = x.DonViId });

            var lstPhieuDanhGia = (from cb in lstCanBoThuocDonVi
                                   from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                                   from pdgcb in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.Id && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                                   from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdgcb.Id && x.IsKetQuaCuoiCung == true)
                                   from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                   where pdgcb.Nam == nam && pdgcb.Thang == thang
                                   select new
                                   {
                                       DonViId = cb.DonViId,
                                       TenDonVi = dv.Ten,
                                       LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet
                                   });

            var lstData = lstPhieuDanhGia.GroupBy(x => x.DonViId)
                .Select(group => new BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi
                {
                    DonViId = group.Key,
                    TenDonVi = group.FirstOrDefault().TenDonVi,
                    GhiChu = "",
                }).ToList();


            foreach (var obj in lstData)
            {
                var getPdgcb = lstPhieuDanhGia.Where(x => x.DonViId == obj.DonViId).ToList();

                obj.SL_DanhGia = getPdgcb.Count;
                if (obj.SL_DanhGia > 0)
                {
                    obj.SL_HTXSNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTXSNV).Count();
                    obj.TiLe_HTXSNV = ((decimal)obj.SL_HTXSNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTTNV).Count();
                    obj.TiLe_HTTNV = ((decimal)obj.SL_HTTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_HTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.HTNV).Count();
                    obj.TiLe_HTNV = ((decimal)obj.SL_HTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");

                    obj.SL_KHTNV = getPdgcb.Where(x => x.LoaiThangDiemChiTiet == (int)AppConsts.LoaiThangDiemChiTiet.KHTNV).Count();
                    obj.TiLe_KHTNV = ((decimal)obj.SL_KHTNV / (decimal)obj.SL_DanhGia * 100).ToString("N0");
                }
                else
                {
                    obj.SL_HTXSNV = 0;
                    obj.TiLe_HTXSNV = "0";

                    obj.SL_HTTNV = 0;
                    obj.TiLe_HTTNV = "0";

                    obj.SL_HTNV = 0;
                    obj.TiLe_HTNV = "0";

                    obj.SL_KHTNV = 0;
                    obj.TiLe_KHTNV = "0";
                }

            }

            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = "THÁNG " + thang + " NĂM " + nam;
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName;

            report.Render(false);
            return report.SaveDocumentJsonToString();
            // OR report.SaveDocumentToString();
        }
        #endregion

        #region Báo cáo chi tiết kết quả đánh giá theo đơn vị
        public string BaoCaoChiTietKeQuaDanhGiaTheoDonVi(FilterBaoCaoInput input)
        {

            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }

            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstCanBoThuocDonVi = _canBoRepos.GetAll().Where(x => input.LstDonVi.Any(d => d == x.DonViId))
                .Select(x => new { Id = x.Id, DonViId = x.DonViId, UserId = x.UserId });

            var lstPhieuDanhGia = (from cb in lstCanBoThuocDonVi
                                   from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                                   from ldv in _loaiDonViRepos.GetAll().Where(x => x.Id == dv.LoaiDonViId)
                                   from pdgcb in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.CanBoId == cb.Id && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                                   from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                                   from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdgcb.ChucVuId)
                                   where pdgcb.Nam == nam && pdgcb.Thang == thang
                                   select new
                                   {
                                       LoaiDonViId = ldv.Id,
                                       TenLoaiDonVi = ldv.Ten,
                                       DonViId = cb.DonViId,
                                       TenDonVi = dv.Ten,
                                       HoVaTen = user.Surname + " " + user.Name,
                                       ChucVu = cv.Ten,
                                       PhieuDanhGiaCanBoId = pdgcb.Id
                                   });

            var lstData = new List<BaoCaoChiTietKeQuaDanhGiaTheoDonVi>();
            foreach (var item in lstPhieuDanhGia)
            {
                var obj = new BaoCaoChiTietKeQuaDanhGiaTheoDonVi();
                obj.LoaiDonViId = item.LoaiDonViId;
                obj.TenLoaiDonVi = item.TenLoaiDonVi;
                obj.DonViId = item.DonViId;
                obj.TenDonVi = item.TenDonVi;
                obj.HoVaTen = item.HoVaTen;
                obj.ChucVu = item.ChucVu;

                var getKetQuaTuDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                          from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                          where ls.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                                          && ls.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia
                                          select new
                                          {
                                              ten = tdct.Ten
                                          }).FirstOrDefault();
                if (getKetQuaTuDanhGia != null)
                {
                    obj.XepLoaiCuaCaNhan = getKetQuaTuDanhGia.ten.ToString();
                }

                var getKetQuaCuoiCung = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                         from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                         from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                         where ls.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                                         && ls.IsKetQuaCuoiCung == true
                                         select new
                                         {
                                             NhanXet = kq.NhanXet,
                                             LoaiThangDiemChiTiet = tdct.LoaiThangDiemChiTiet
                                         }).FirstOrDefault();

                if (getKetQuaCuoiCung != null)
                {
                    obj.NhanXet = getKetQuaCuoiCung.NhanXet;
                    obj.XepLoaiCapTren = getKetQuaCuoiCung.LoaiThangDiemChiTiet.ToString();
                }

                lstData.Add(obj);
            }

            report.RegData("dtTable", lstData);
            report.Dictionary.Variables["LoaiThoiGian"].Value = "THÁNG " + thang + " NĂM " + nam;
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }
        #endregion

        #region Thông báo kết quả đánh giá
        public string BaoCaoThongBaoKetQuaDanhGia(FilterBaoCaoInput input)
        {

            var thang = DateTime.Now.Month;
            var nam = DateTime.Now.Year;
            if (!string.IsNullOrWhiteSpace(input.ThoiGianBatDau))
            {
                String inputString = input.ThoiGianBatDau;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                DateTime oDate = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);

                thang = oDate.Month;
                nam = oDate.Year;
            }
            string sDoiTuongXepLoai = "";
            var getBaoCao = _baoCaoRepos.Get(input.BaoCaoId);
            if (getBaoCao == null)
            {
                return "";
            }

            StiReport report = new StiReport();
            report.Load(getBaoCao.DuongDanBieuMau);

            var lstChucVuOrg = new List<ChucVuDto>();
            var lstChucVu = new List<ChucVuDto>();
            foreach (var id in input.LstNhomChucVu)
            {
                var doiTuong = _baoCaoNhomDoiTuongRepos.Get(id);


                if (doiTuong != null)
                {
                    if (!string.IsNullOrWhiteSpace(sDoiTuongXepLoai))
                    {
                        sDoiTuongXepLoai += ", " + doiTuong.TenNhomDoiTuong;
                    }
                    else
                    {
                        sDoiTuongXepLoai += doiTuong.TenNhomDoiTuong;
                    }
                }

                var lstChucVuByDoiTuong = (from bcch in _baoCaoNhomDoiTuongChiTietRepos.GetAll()
                                           from cv in _chucVuRepos.GetAll().Where(x => x.Id == bcch.ChucVuId)
                                           where bcch.BaoCaoNhomDoiTuongId == id
                                           select new ChucVuDto
                                           {
                                               Id = cv.Id,
                                               Ten = cv.Ten,
                                           });

                if (lstChucVuByDoiTuong.Count() > 0)
                {
                    lstChucVuOrg.AddRange(lstChucVuByDoiTuong);
                }
            }
            lstChucVu = lstChucVuOrg.Distinct().ToList();

            var lstPhieuDanhGiaCanBoByChucVu = (from cv in lstChucVu
                                                from pdgcb in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.ChucVuId == cv.Id
                                                && x.Nam == nam && x.Thang == thang && x.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
                                                from cb in _canBoRepos.GetAll().Where(x => x.Id == pdgcb.CanBoId)
                                                from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                                                from dv in _donViRepos.GetAll().Where(x => x.Id == cb.DonViId)
                                                select new BaoCaoThongBaoKetQuaDanhGia
                                                {
                                                    HoVaTen = user.Surname + " " + user.Name,
                                                    ChucVu = cv.Ten,
                                                    TenDonVi = dv.Ten,
                                                    PhieuDanhGiaCanBoId = pdgcb.Id
                                                }).ToList();

            foreach (var item in lstPhieuDanhGiaCanBoByChucVu)
            {
                var getKetQuaTuDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                          from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                          where ls.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                                          && ls.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia
                                          select new
                                          {
                                              KetQuaCuoiCung = tdct.Ten,
                                              LichSuId = ls.Id
                                          }).FirstOrDefault();
                if (getKetQuaTuDanhGia != null)
                {
                    item.CaNhanTuXepLoai = getKetQuaTuDanhGia.KetQuaCuoiCung;
                    item.DiemCaNhanCham = _ketQuaDanhGiaChiTietRepos.GetAll()
                        .Where(x => x.LichSuKetQuaDanhGiaChiTietId == getKetQuaTuDanhGia.LichSuId && x.Diem != null)
                        .ToList().Sum(x => x.Diem.Value);
                }

                var getKetQuaCuoiCung = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                         from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId)
                                         from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                         where ls.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                                         && ls.IsKetQuaCuoiCung == true
                                         select new
                                         {
                                             NhanXet = kq.NhanXet,
                                             LichSuId = ls.Id,
                                             KetQuaCuoiCung = tdct.Ten,
                                         }).FirstOrDefault();

                if (getKetQuaCuoiCung != null)
                {
                    item.NhanXet = getKetQuaCuoiCung.NhanXet;
                    item.LanhDaoXepLoai = getKetQuaCuoiCung.KetQuaCuoiCung;
                    var diemLanhDao = _ketQuaDanhGiaChiTietRepos.GetAll()
                          .Where(x => x.LichSuKetQuaDanhGiaChiTietId == getKetQuaCuoiCung.LichSuId).ToList().Sum(x => x.Diem);
                    item.DiemLanhDaoCham = (diemLanhDao > 0) ? diemLanhDao.ToString() : "";
                }
            }


            report.RegData("dtTable", lstPhieuDanhGiaCanBoByChucVu);
            report.Dictionary.Variables["Thang"].Value = thang.ToString();
            report.Dictionary.Variables["Nam"].Value = nam.ToString();
            report.Dictionary.Variables["TenDonVi"].Value = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName; ;
            report.Dictionary.Variables["DoiTuongXepLoai"].Value = sDoiTuongXepLoai;

            report.Render(false);
            return report.SaveDocumentJsonToString();
        }

        #endregion


        public DateTime ConvertDateStringToDateTime(string sDate)
        {
            DateTime dDateTime = DateTime.Now;

            if (!string.IsNullOrWhiteSpace(sDate))
            {
                String inputString = sDate;
                inputString = Regex.Replace(inputString, " \\(.*\\)$", "");
                dDateTime = DateTime.ParseExact(inputString, "ddd MMM dd yyyy HH:mm:ss 'GMT'zzz",
                    System.Globalization.CultureInfo.InvariantCulture);
            }

            return dDateTime;
        }



    }
}
