﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using System;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    [AbpAuthorize(AppPermissions.Pages_Admin_DonVi)]
    public class DonViAppService : PMSAppServiceBase, IDonVi
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<DonVi, long> _donViRepos;
        private readonly ICache _mainCache;
        private readonly IDonViChuyenTrach _iDonViChuyenTrach;
        private readonly ICommonEnum _iCommonEnum;
        private readonly IRepository<BaoCaoNhomDoiTuongChiTiet, long> _baoCaoNhomDoiTuongChiTietRepos;
        public DonViAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<DonVi, long> donViRepos,
            IDonViChuyenTrach iDonViChuyenTrach,
            ICommonEnum iCommonEnum,
            IRepository<BaoCaoNhomDoiTuongChiTiet, long> baoCaoNhomDoiTuongChiTietRepos
            )
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _donViRepos = donViRepos;
            _iDonViChuyenTrach = iDonViChuyenTrach;
            _iCommonEnum = iCommonEnum;
            _baoCaoNhomDoiTuongChiTietRepos = baoCaoNhomDoiTuongChiTietRepos;
            _mainCache = _cacheManager.GetCache("DonViAppService");
        }

       
        [AbpAllowAnonymous]
        public PagedResultDto<DonViResponse> GetByFilter(DonViInput input)
        {
            //input.Format();

            var _capDonViRepos = _iCommonEnum.GetCapDonViEnum();
            var _khoiRepos = _iCommonEnum.GetKhoiHanhChinhEnum();


            var query = from x in _donViRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        //&& (!input.LoaiDonViId.HasValue || input.LoaiDonViId.Value == x.LoaiDonViId)
                        && (!input.DonViQuanLyId.HasValue || input.DonViQuanLyId.Value == x.DonViQuanLyId)
                        && (!input.CapDonVi.HasValue || input.CapDonVi.Value == x.CapDonViId)
                        && (!input.Khoi.HasValue || input.Khoi.Value == x.KhoiId)
                        && (string.IsNullOrEmpty(input.TinhId) || input.TinhId == x.TinhId)
                        && (string.IsNullOrEmpty(input.HuyenId) || input.HuyenId == x.HuyenId)
                        && (string.IsNullOrEmpty(input.XaId) || input.TinhId == x.XaId)
                        join cd in _capDonViRepos on x.CapDonViId equals cd.Id into cdt
                        from cd in cdt.DefaultIfEmpty()
                        join kd in _khoiRepos on x.KhoiId equals kd.Id into kdt
                        from kd in kdt.DefaultIfEmpty()
                        join ql in _donViRepos.GetAll() on x.DonViQuanLyId equals ql.Id into qlt
                        from ql in qlt.DefaultIfEmpty()
                        select new DonViResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            CapDonViId = x.CapDonViId,
                            DonViQuanLyId = x.DonViQuanLyId,
                            XaId = x.XaId,
                            HuyenId = x.HuyenId,
                            TinhId = x.TinhId,
                            KhoiId = x.KhoiId,
                            KhoiTruongHocId = x.KhoiTruongHocId,
                            TenCapDonVi = (cd != null) ? cd.Name : "",
                            TenKhoi = (kd != null) ? kd.Name : "",
                            TenDonViQuanLy = (ql != null) ? ql.Ten : "",
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<DonViResponse>(totalCount, items);
        }

        [AbpAllowAnonymous]
        public PagedResultDto<DonViResponse> GetDonViChoCanBoChucVu(DonViInput input)
        {
            var _capDonViRepos = _iCommonEnum.GetCapDonViEnum();
            var _khoiRepos = _iCommonEnum.GetKhoiHanhChinhEnum();
            var lstDonViDaChon = _iDonViChuyenTrach.LoadDonViChuyenTrachTheoCannBoChucVu(input.CanBoId, input.ChucVuId);

            var query = from x in _donViRepos.GetAll()
                        where !lstDonViDaChon.Any(d => d == x.Id)
                        && (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && (!input.DonViQuanLyId.HasValue || input.DonViQuanLyId.Value == x.DonViQuanLyId)
                        && (!input.CapDonVi.HasValue || input.CapDonVi.Value == x.CapDonViId)
                        && (!input.Khoi.HasValue || input.Khoi.Value == x.KhoiId)
                        && (string.IsNullOrEmpty(input.TinhId) || input.TinhId == x.TinhId)
                        && (string.IsNullOrEmpty(input.HuyenId) || input.HuyenId == x.HuyenId)
                        && (string.IsNullOrEmpty(input.XaId) || input.TinhId == x.XaId)
                        join cd in _capDonViRepos on x.CapDonViId equals cd.Id into cdt
                        from cd in cdt.DefaultIfEmpty()
                        join kd in _khoiRepos on x.KhoiId equals kd.Id into kdt
                        from kd in kdt.DefaultIfEmpty()
                        join ql in _donViRepos.GetAll() on x.DonViQuanLyId equals ql.Id into qlt
                        from ql in qlt.DefaultIfEmpty()
                        select new DonViResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            CapDonViId = x.CapDonViId,
                            DonViQuanLyId = x.DonViQuanLyId,
                            XaId = x.XaId,
                            HuyenId = x.HuyenId,
                            TinhId = x.TinhId,
                            KhoiId = x.KhoiId,
                            TenCapDonVi = (cd != null) ? cd.Name : "",
                            TenKhoi = (kd != null) ? kd.Name : "",
                            TenDonViQuanLy = (ql != null) ? ql.Ten : "",
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<DonViResponse>(totalCount, items);
        }

        public bool CheckDonViTrungTen(string name, long id = 0)
        {
            bool _result = false;
            if (id > 0)
            {
                var temp = _donViRepos.GetAll()
                    .Where(x => x.Ten.Equals(name) && x.Id != id);
                _result = temp.Any();
            }
            else
            {
                var temp = _donViRepos.GetAll()
                    .Where(x => x.Ten.Equals(name));
                _result = temp.Any();
            }

            return _result;
        }

        public async System.Threading.Tasks.Task<long> Upsert(DonViDto input)
        {
            long result = 0;
            try
            {
                input.DonViQuanLyId = (input.DonViQuanLyId == 0) ? null : input.DonViQuanLyId;

                if (!CheckDonViTrungTen(input.Ten, input.Id))
                {
                    if (input.Id > 0)
                    {
                        var category = _donViRepos.Get(input.Id);
                        ObjectMapper.Map(input, category);
                    }
                    else
                    {
                        var entity = ObjectMapper.Map<DonVi>(input);
                        entity.TenantId = AbpSession.TenantId;
                        input.Id = _donViRepos.InsertAndGetId(entity);
                    }
                    CurrentUnitOfWork.SaveChanges();
                    _mainCache.Clear();
                    result = 1;
                }
                else
                {
                    result = -1;
                }
            }
            catch (Exception ex)
            {
                result = 0;
                throw;
            }
            return result;
        }
        //[AbpAuthorize(AppPermissions.Pages_Admin_CanBo_Delete)]
        public void Delete(long id)
        {
            _donViRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        [AbpAllowAnonymous]
        public PagedResultDto<DonViResponse> GetDonViBaoCaoDoiTuongChiTiet(DonViBaoCaoDoiTuongChiTieInput input)
        {
            //input.Format();

            var _capDonViRepos = _iCommonEnum.GetCapDonViEnum();
            var _khoiRepos = _iCommonEnum.GetKhoiHanhChinhEnum();
            var dsBcDonViChiTiet = _baoCaoNhomDoiTuongChiTietRepos.GetAll().Where(x => x.BaoCaoNhomDoiTuongId == input.BaoCaoNhomDoiTuongId).Select(x => x.ChucVuId);


            var query = from x in _donViRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        && (!input.CapDonVi.HasValue || input.CapDonVi.Value == x.CapDonViId)
                        && (!input.Khoi.HasValue || input.Khoi.Value == x.KhoiId)
                        && !dsBcDonViChiTiet.Any(d => d == x.Id)

                        join cd in _capDonViRepos on x.CapDonViId equals cd.Id into cdt
                        from cd in cdt.DefaultIfEmpty()
                        join kd in _khoiRepos on x.KhoiId equals kd.Id into kdt
                        from kd in kdt.DefaultIfEmpty()
                        join ql in _donViRepos.GetAll() on x.DonViQuanLyId equals ql.Id into qlt
                        from ql in qlt.DefaultIfEmpty()                        
                        select new DonViResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten,
                            CapDonViId = x.CapDonViId,
                            DonViQuanLyId = x.DonViQuanLyId,
                            KhoiId = x.KhoiId,
                            KhoiTruongHocId = x.KhoiTruongHocId,
                            TenCapDonVi = (cd != null) ? cd.Name : "",
                            TenKhoi = (kd != null) ? kd.Name : "",
                            TenDonViQuanLy = (ql != null) ? ql.Ten : "",
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<DonViResponse>(totalCount, items);
        }
    }
}
