﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using Microsoft.AspNetCore.Identity;
using newPSG.PMS.Authorization;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    public class DonViChuyenTrachAppService : PMSAppServiceBase, IDonViChuyenTrach
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<DonViChuyenTrach, long> _donViChuyenTrachrepos;
        private readonly IRepository<CanBo, long> _canboRepos;
        private readonly IRepository<DonVi, long> _donviRepos;
        private readonly ICache _mainCache;

        private readonly UserManager _userManager;
        public DonViChuyenTrachAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<DonViChuyenTrach, long> donViChuyenTrachrepos,
            IRepository<DonVi, long> donviRepos,
            IRepository<CanBo, long> canboRepos,
            UserManager userManager)

        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _donViChuyenTrachrepos = donViChuyenTrachrepos;
            _donviRepos = donviRepos;
            _canboRepos = canboRepos;

            _userManager = userManager;
            _mainCache = _cacheManager.GetCache("DonViChuyenTrachAppService");
        }

        public PagedResultDto<DonViChuyenTrachResponse> GetByFilter(DonViChuyenTrachInput input)
        {
            input.Format();
            var query = from x in _donViChuyenTrachrepos.GetAll()
                        where (!input.CanBoId.HasValue || input.CanBoId.Value == x.CanBoId)
                        && (!input.DonViId.HasValue || input.DonViId.Value == x.DonViId)
                        && (!input.ChucVuId.HasValue || input.ChucVuId.Value == x.ChucVuId)
                        join cd in _canboRepos.GetAll() on x.CanBoId equals cd.Id into cdt
                        from cd in cdt.DefaultIfEmpty()
                        join kd in _donviRepos.GetAll() on x.DonViId equals kd.Id into kdt
                        from kd in kdt.DefaultIfEmpty()

                        join user in _userManager.Users.ToList() on cd.UserId equals user.Id into userdt
                        from user in userdt.DefaultIfEmpty()
                        select new DonViChuyenTrachResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            CanBoId = x.CanBoId,
                            DonViId = x.DonViId,
                            TenCanBo = (user != null) ? user.Name : "",
                            TenDonVi = (kd != null) ? kd.Ten : "",
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<DonViChuyenTrachResponse>(totalCount, items);
        }
        public bool InsertDonViChuyenTrach(DonViChuyenTrachUpdate input)
        {
            bool result = false;
            try
            {
                foreach (var dv in input.LstDonVi)
                {
                    var obj = new DonViChuyenTrach();
                    obj.DonViId = dv;
                    obj.CanBoId = input.CanBoId;
                    obj.ChucVuId = input.ChucVuId;
                    obj.TenantId = AbpSession.TenantId;

                    _donViChuyenTrachrepos.InsertAndGetId(obj);
                    //CurrentUnitOfWork.SaveChanges();
                    result = true;
                }
                _mainCache.Clear();
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }
        public bool DeleteDonViChuyenTrach(DonViChuyenTrachUpdate input)
        {
            bool result = false;
            try
            {
                var existedEntities = _donViChuyenTrachrepos.GetAllList(x => x.Id == input.Id);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }

            return result;
        }

        public void Delete(long id)
        {
            _donViChuyenTrachrepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        public List<DonViChuyenTrachResponse> LoadDonViChuyenTrach(long canBoId, long chucVuId)
        {
            var lstData = new List<DonViChuyenTrachResponse>();

            lstData = (from dvct in _donViChuyenTrachrepos.GetAll().Where(x => x.CanBoId == canBoId && x.ChucVuId == chucVuId)
                       from dv in _donviRepos.GetAll().Where(x => x.Id == dvct.DonViId)
                       select new DonViChuyenTrachResponse
                       {
                           Id = dv.Id,
                           TenDonVi = dv.Ten
                       }).ToList();
            return lstData;
        }

        public List<long> LoadDonViChuyenTrachTheoCannBoChucVu(long canBoId, long chucVuId)
        {
            var lstData = _donViChuyenTrachrepos.GetAll()
                .Where(x => x.CanBoId == canBoId && x.ChucVuId == chucVuId).Select(x => x.DonViId).ToList();
            return lstData;
        }
    }
}
