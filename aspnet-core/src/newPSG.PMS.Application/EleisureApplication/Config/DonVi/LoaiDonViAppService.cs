﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    [AbpAuthorize(AppPermissions.Pages_Admin_LoaiDonVi)]
    public class LoaiDonViAppService : PMSAppServiceBase, ILoaiDonVi
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<LoaiDonVi, long> _repos;
        private readonly ICache _mainCache;
        public LoaiDonViAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<LoaiDonVi, long> repos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _mainCache = _cacheManager.GetCache("LoaiDonViAppService");
        }
        [AbpAllowAnonymous]
        public PagedResultDto<LoaiDonViDto> GetByFilter(LoaiDonViInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter))
                        select new LoaiDonViDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            Ten = x.Ten
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<LoaiDonViDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<bool> Upsert(LoaiDonViDto input)
        {
            bool result = false;
            try
            {
                if (input.Id > 0)
                {
                    var category = _repos.Get(input.Id);
                    ObjectMapper.Map(input, category);
                }
                else
                {
                    var entity = ObjectMapper.Map<LoaiDonVi>(input);
                    entity.TenantId = AbpSession.TenantId;
                    input.Id = _repos.InsertAndGetId(entity);
                }
                CurrentUnitOfWork.SaveChanges();
                _mainCache.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }
        [AbpAuthorize(AppPermissions.Pages_Admin_LoaiDonVi_Delete)]
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
    }
}
