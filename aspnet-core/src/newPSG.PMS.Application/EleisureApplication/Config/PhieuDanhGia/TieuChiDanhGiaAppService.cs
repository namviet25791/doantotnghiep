﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class TieuChiDanhGiaAppService : PMSAppServiceBase, ITieuChiDanhGia
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<TieuChiDanhGia, long> _repos;
        private readonly ICache _mainCache;
        public TieuChiDanhGiaAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<TieuChiDanhGia, long> repos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _mainCache = _cacheManager.GetCache("TieuChiDanhGiaAppService");
        }

        public PagedResultDto<TieuChiDanhGiaResponse> GetByFilter(TieuChiDanhGiaInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (
                        (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter) || x.KyHieu.Contains(input.Filter))
                        && (!input.CanBoId.HasValue || input.CanBoId.Value == x.CanBoId)
                        && (!input.PhieuDanhGiaId.HasValue || input.PhieuDanhGiaId.Value == x.PhieuDanhGiaId)
                        && (!input.TieuChiChaId.HasValue || (x.TieuChiChaId.HasValue && input.TieuChiChaId.Value == x.TieuChiChaId.Value))
                        )
                        select new TieuChiDanhGiaResponse
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            KyHieu = x.KyHieu,
                            Ten = x.Ten,
                            CanBoId = x.CanBoId,
                            DiemToiDa = x.DiemToiDa,
                            DiemToiThieu = x.DiemToiThieu,
                            NhomTieuChi = x.NhomTieuChi,
                            PhieuDanhGiaId = x.PhieuDanhGiaId,
                            ThuTu = x.ThuTu,
                            TieuChiChaId = x.TieuChiChaId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).OrderBy(x => x.ThuTu).ToList();
            //foreach(var item in items)
            //{
            //    item.MangTieuChiCon = GetTieuChiDanhGiaCon(item.Id);
            //}
            return new PagedResultDto<TieuChiDanhGiaResponse>(totalCount, items);
        }
        private List<TieuChiDanhGiaResponse> GetTieuChiDanhGiaCon(long id)
        {
            List<TieuChiDanhGiaResponse> results = new List<TieuChiDanhGiaResponse>();
            var objs = _repos.GetAllList(x => x.TieuChiChaId == id).ToList();
            foreach (var x in objs)
            {
                results.Add(new TieuChiDanhGiaResponse()
                {
                    Id = x.Id,
                    TenantId = x.TenantId,
                    KyHieu = x.KyHieu,
                    Ten = x.Ten,
                    CanBoId = x.CanBoId,
                    DiemToiDa = x.DiemToiDa,
                    NhomTieuChi = x.NhomTieuChi,
                    PhieuDanhGiaId = x.PhieuDanhGiaId,
                    ThuTu = x.ThuTu,
                    TieuChiChaId = x.TieuChiChaId,
                    //MangTieuChiCon = GetTieuChiDanhGiaCon(x.Id)
                });
            }
            return results;

        }
        public async System.Threading.Tasks.Task<bool> Upsert(TieuChiDanhGiaDto input)
        {
            bool result = false;
            try
            {
                input.TieuChiChaId = (input.TieuChiChaId == 0) ? null : input.TieuChiChaId;
                if (input.Id > 0)
                {
                    var category = _repos.Get(input.Id);
                    ObjectMapper.Map(input, category);
                }
                else
                {
                    var entity = ObjectMapper.Map<TieuChiDanhGia>(input);
                    entity.TenantId = AbpSession.TenantId;
                    input.Id = _repos.InsertAndGetId(entity);
                }
                CurrentUnitOfWork.SaveChanges();
                _mainCache.Clear();
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw;
            }
            return result;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        public void CopyByPhieuDanhGia(CopyTieuChiDanhGiaInput input)
        {
            var items = _repos.GetAllList(x => x.PhieuDanhGiaId == input.PhieuDanhGiaTruocDoId);
            foreach (var item in items)
            {
                _repos.Insert(new TieuChiDanhGia()
                {
                    PhieuDanhGiaId = input.PhieuDanhGiaId,
                    CanBoId = item.CanBoId,
                    DiemToiDa = item.DiemToiDa,
                    DiemToiThieu = item.DiemToiThieu,
                    KyHieu = item.KyHieu,
                    NhomTieuChi = item.NhomTieuChi,
                    Ten = item.Ten,
                    TenantId = item.TenantId,
                    ThuTu = item.ThuTu,
                    TieuChiChaId = item.TieuChiChaId
                });
            }
        }
    }
}
