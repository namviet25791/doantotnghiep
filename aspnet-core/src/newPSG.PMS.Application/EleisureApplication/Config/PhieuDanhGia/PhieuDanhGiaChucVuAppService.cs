﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class PhieuDanhGiaChucVuAppService : PMSAppServiceBase, IPhieuDanhGiaChucVu
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<PhieuDanhGiaChucVu, long> _repos;
        private readonly ICache _mainCache;

        private readonly IRepository<PhieuDanhGia, long> _phieuDanhGiaRepos;
        private readonly IRepository<TieuChiDanhGia, long> _tieuChiDanhGiaRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<CanBoChucVu, long> _canboChucVuRepos;
        private readonly IRepository<CanBo, long> _canboRepos;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhGiaCanBoRepos;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaChiTietRepos;
        public PhieuDanhGiaChucVuAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<PhieuDanhGiaChucVu, long> repos,
            IRepository<PhieuDanhGia, long> phieuDanhGiaRepos,
            IRepository<TieuChiDanhGia, long> tieuChiDanhGiaRepos,
            IRepository<ThangDiem, long> thangDiemRepos,
            IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
            IRepository<CanBoChucVu, long> canboChucVuRepos,
            IRepository<CanBo, long> canboRepos,
            IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
            IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaChiTietRepos,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhGiaCanBoRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _phieuDanhGiaRepos = phieuDanhGiaRepos;
            _tieuChiDanhGiaRepos = tieuChiDanhGiaRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
            _canboChucVuRepos = canboChucVuRepos;
            _canboRepos = canboRepos;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _lichSuKetQuaDanhGiaChiTietRepos = lichSuKetQuaDanhGiaChiTietRepos;
            _phieuDanhGiaCanBoRepos = phieuDanhGiaCanBoRepos;
            _mainCache = _cacheManager.GetCache("PhieuDanhGiaChucVuAppService");
        }

        public PagedResultDto<PhieuDanhGiaChucVuDto> GetByFilter(PhieuDanhGiaChucVuInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (!input.StartDate.HasValue || x.HieuLucTuNgay >= input.StartDate.Value)
                        && (!input.EndDate.HasValue || x.HieuLucToiNgay <= input.EndDate.Value)
                        && (!input.PhieuDanhGiaId.HasValue || input.PhieuDanhGiaId.Value == x.PhieuDanhGiaId)
                        && (!input.ChucVuId.HasValue || input.ChucVuId.Value == x.ChucVuId)
                        select new PhieuDanhGiaChucVuDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            ChucVuId = x.ChucVuId,
                            PhieuDanhGiaId = x.PhieuDanhGiaId,
                            HieuLucTuNgay = x.HieuLucTuNgay,
                            HieuLucToiNgay = x.HieuLucToiNgay
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<PhieuDanhGiaChucVuDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<bool> Upsert(PhieuDanhGiaChucVuDto input)
        {
            bool result = true;
            try
            {
                var pdgcv = _repos.GetAll().Where(x => x.ChucVuId == input.ChucVuId).FirstOrDefault();
                if (pdgcv != null)
                {
                    pdgcv.PhieuDanhGiaId = input.PhieuDanhGiaId;
                    await _repos.UpdateAsync(pdgcv);
                    result = true;
                }
                else
                {
                    var entity = new PhieuDanhGiaChucVu();
                    entity.TenantId = AbpSession.TenantId;
                    entity.PhieuDanhGiaId = input.PhieuDanhGiaId;
                    entity.ChucVuId = input.ChucVuId;
                    await _repos.InsertAsync(entity);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
                throw;
            }
            return result;
        }
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        //private long GetCurrentCanBoId()
        //{ 
        //    var canBo = _canboRepos.FirstOrDefault(x => x.UserId == AbpSession.UserId);
        //    if (canBo != null)
        //        return canBo.Id; 
        //    return -1;
        //}


    }
}
