﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.Authorization;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    [AbpAuthorize]
    public class PhieuDanhGiaAppService : PMSAppServiceBase, IPhieuDanhGia
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<PhieuDanhGia, long> _repos;
        private readonly IRepository<TieuChiDanhGia, long> _tieuChiRepos;
        private readonly ICache _mainCache;
        public PhieuDanhGiaAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<PhieuDanhGia, long> repos,
            IRepository<TieuChiDanhGia, long> tieuChiRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _repos = repos;
            _tieuChiRepos = tieuChiRepos;
            _mainCache = _cacheManager.GetCache("PhieuDanhGiaAppService");
        }
        [AbpAuthorize(AppPermissions.Pages_Admin_PhieuDanhGia)]
        public PagedResultDto<PhieuDanhGiaDto> GetByFilter(PhieuDanhGiaInput input)
        {
            input.Format();
            var query = from x in _repos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter) || x.TieuDe.Contains(input.Filter))
                        && (!input.StartDate.HasValue || x.HieuLucTuNgay >= input.StartDate.Value)
                        && (!input.EndDate.HasValue || x.HieuLucToiNgay <= input.EndDate.Value)
                        select new PhieuDanhGiaDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            TieuDe = x.TieuDe,
                            Ten = x.Ten,
                            GhiChu = x.GhiChu,
                            HieuLucTuNgay = x.HieuLucTuNgay,
                            HieuLucToiNgay = x.HieuLucToiNgay,
                            ThangDiemId = x.ThangDiemId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<PhieuDanhGiaDto>(totalCount, items);
        }
        public async Task<long> Upsert(PhieuDanhGiaDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _repos.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            {
                if (_repos.FirstOrDefault(x => x.Ten == input.Ten) != null)
                {
                    return -1;
                }
                var entity = ObjectMapper.Map<PhieuDanhGia>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = _repos.InsertAndGetId(entity);
            }
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
            return input.Id;
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_PhieuDanhGia_Delete)]
        public void Delete(long id)
        {
            _repos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_PhieuDanhGia_Clone)]
        public bool CopyPhieuDanhGia(CopyPhieuDanhGiaInput input)
        {
            bool result = false;
            try
            {
                var phieuDanhGia = _repos.Get(input.PhieuDanhGiaTruocDoId);
                if (phieuDanhGia != null)
                {
                    var newPhieuDanhGia = new PhieuDanhGia()
                    {
                        HieuLucToiNgay = phieuDanhGia.HieuLucToiNgay,
                        HieuLucTuNgay = phieuDanhGia.HieuLucTuNgay,
                        TieuDe = phieuDanhGia.TieuDe,
                        Ten = phieuDanhGia.Ten + " - Copy",
                        TenantId = phieuDanhGia.TenantId,
                        ThangDiemId = phieuDanhGia.ThangDiemId
                    };
                    var id = _repos.InsertAndGetId(newPhieuDanhGia);
                    var items = _tieuChiRepos.GetAllList(x => x.PhieuDanhGiaId == input.PhieuDanhGiaTruocDoId);
                    InsertChilds(items, null,null, id);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

        private void InsertChilds(List<TieuChiDanhGia> tieuChiDanhGias, long? parentId, long? newParentId,  long phieuDanhGiaId)
        {
            var childNodes = tieuChiDanhGias.Where(x => x.TieuChiChaId == parentId);
            foreach (var item in childNodes)
            {
                var newId = _tieuChiRepos.InsertAndGetId(new TieuChiDanhGia()
                {
                    PhieuDanhGiaId = phieuDanhGiaId,
                    CanBoId = item.CanBoId,
                    DiemToiDa = item.DiemToiDa,
                    KyHieu = item.KyHieu,
                    NhomTieuChi = item.NhomTieuChi,
                    Ten = item.Ten,
                    TenantId = item.TenantId,
                    ThuTu = item.ThuTu,
                    TieuChiChaId = newParentId
                });

                var childNodeInItem = tieuChiDanhGias.Where(x => x.TieuChiChaId == item.Id);
                if (!childNodeInItem.Any())
                {
                    continue;
                }

                InsertChilds(tieuChiDanhGias, item.Id, newId, phieuDanhGiaId);
            }
        }
    }
}
