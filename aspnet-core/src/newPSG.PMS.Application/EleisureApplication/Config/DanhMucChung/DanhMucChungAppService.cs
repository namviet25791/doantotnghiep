﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    public class DanhMucChungAppService : PMSAppServiceBase, IDanhMucChung
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<Tinh, string> _tinhRepos;
        private readonly IRepository<Huyen, string> _huyenRepos;
        private readonly IRepository<Xa, string> _xaRepos;
        private readonly ICache _mainCache;
        public DanhMucChungAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<Tinh, string> tinhRepos, IRepository<Huyen, string> huyenRepos, IRepository<Xa, string> xaRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _tinhRepos = tinhRepos;
            _huyenRepos = huyenRepos;
            _xaRepos = xaRepos;
            _mainCache = _cacheManager.GetCache("DanhMucChungAppService");
        }

        public PagedResultDto<HuyenDto> GetHuyenByFilter(HuyenInput input)
        {
            input.Format();
            var query = from x in _huyenRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter) || x.Cap.Contains(input.Filter))
                        && (string.IsNullOrEmpty(input.TinhId) || x.TinhId ==  input.TinhId)
                        select new HuyenDto
                        {
                            Id = x.Id,
                            Ten = x.Ten,
                            Cap = x.Cap,
                            TinhId = x.TinhId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<HuyenDto>(totalCount, items);
        }

        public PagedResultDto<TinhDto> GetTinhByFilter(TinhInput input)
        {
            input.Format();
            var query = from x in _tinhRepos.GetAll() 
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter) || x.Cap.Contains(input.Filter))  
                        select new TinhDto
                        {
                            Id = x.Id,
                            Ten  = x.Ten,
                            Cap = x.Cap
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList(); 
            
            return new PagedResultDto<TinhDto>(totalCount, items);
        }

        public PagedResultDto<XaDto> GetXaByFilter(XaInput input)
        {
            input.Format();
            var query = from x in _xaRepos.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.Ten.Contains(input.Filter) || x.Cap.Contains(input.Filter))
                        && (string.IsNullOrEmpty(input.HuyenId) || x.HuyenId == input.HuyenId)
                        select new XaDto
                        {
                            Id = x.Id,
                            Ten = x.Ten,
                            Cap = x.Cap,
                            HuyenId = x.HuyenId
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<XaDto>(totalCount, items);
        }
    }
}
