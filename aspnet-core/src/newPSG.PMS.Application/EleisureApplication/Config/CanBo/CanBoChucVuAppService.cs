﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{

    public class CanBoChucVuAppService : PMSAppServiceBase, ICanBoChucVu
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<ChucVuDanhGia, long> _chucvuDanhGiaRepos;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canboChucvuDanhGiaRepos;
        private readonly ICache _mainCache;
        public CanBoChucVuAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<CanBoChucVu, long> canBoChucVuRepos,
            IRepository<ChucVuDanhGia, long> chucvuDanhGiaRepos,
            IRepository<CanBoChucVuDanhGia, long> canboChucvuDanhGiaRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _canBoChucVuRepos = canBoChucVuRepos;
            _chucvuDanhGiaRepos = chucvuDanhGiaRepos;
            _canboChucvuDanhGiaRepos = canboChucvuDanhGiaRepos;
            _mainCache = _cacheManager.GetCache("CanBoChucVuAppService");
        }
        public PagedResultDto<CanBoChucVuDto> GetByFilter(CanBoChucVuInput input)
        {
            var query = from x in _canBoChucVuRepos.GetAll()
                        where (!input.CanBoId.HasValue || x.CanBoId == input.CanBoId.Value)
                        && (!input.ChucVuId.HasValue || x.ChucVuId == input.ChucVuId.Value)
                        && (!input.ChucVuChinh.HasValue || x.ChucVuChinh == input.ChucVuChinh.Value)
                        select new CanBoChucVuDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            CanBoId = x.CanBoId,
                            ChucVuId = x.ChucVuId,
                            ChucVuChinh = x.ChucVuChinh
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<CanBoChucVuDto>(totalCount, items);
        }

        public List<CanBoChucVuDto> GetByFilterWithoutPaging(CanBoChucVuInput input)
        {
            var lstData = (from x in _canBoChucVuRepos.GetAll()
                           where x.CanBoId == input.CanBoId.Value
                           select new CanBoChucVuDto
                           {
                               Id = x.Id,
                               TenantId = x.TenantId,
                               CanBoId = x.CanBoId,
                               ChucVuId = x.ChucVuId,
                               ChucVuChinh = x.ChucVuChinh
                           }).ToList();

            return lstData;
        }
        public bool Upsert(List<CanBoChucVuDto> input)
        {
            bool result = false;
            try
            {
                _canBoChucVuRepos.Delete(x => x.CanBoId == input[0].CanBoId);

                var lstItemInsert = input.Where(x => x.ChucVuId >= 0).Select(x => x.ChucVuId).Distinct();
                foreach (var item in lstItemInsert)
                {
                    var lstCV = input.Where(x => x.ChucVuId == item);
                    if (lstCV.Count() > 1)
                    {
                        var chucVuChinh = lstCV.Where(x => x.ChucVuChinh == 1).FirstOrDefault();
                        if (chucVuChinh != null)
                        {
                            var entity1 = ObjectMapper.Map<CanBoChucVu>(item);
                            entity1.TenantId = AbpSession.TenantId;
                            _canBoChucVuRepos.Insert(entity1);
                            continue;
                        }
                    }
                    var itemCV = lstCV.FirstOrDefault();
                    var entity = ObjectMapper.Map<CanBoChucVu>(itemCV);
                    entity.TenantId = AbpSession.TenantId;
                    _canBoChucVuRepos.Insert(entity);
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        private void UpdateCanBoChucVuDanhGia(long canboId, long oldChucVuChinhId, long newChucVuChinhId)
        {
            return;
            var oldChucVuDanhGias = _chucvuDanhGiaRepos.GetAllList(x => x.ChucVuDuocDanhGiaId == oldChucVuChinhId).ToList();
            foreach (var oldChucVuDanhGia in oldChucVuDanhGias)
            {
                _canboChucvuDanhGiaRepos.Delete(x => x.CanBoId == canboId && x.ChucVuDanhGiaId == oldChucVuDanhGia.ChucVuThucHienDanhGiaId);
            }
            var chucVuDanhGias = _chucvuDanhGiaRepos.GetAllList(x => x.ChucVuDuocDanhGiaId == newChucVuChinhId).ToList();
            int index = 0;
            foreach (var chucVuDanhGia in chucVuDanhGias)
            {
                index++;
                _canboChucvuDanhGiaRepos.Insert(new CanBoChucVuDanhGia()
                {
                    CanBoId = canboId,
                    ChucVuDanhGiaId = chucVuDanhGia.ChucVuThucHienDanhGiaId,
                    ThuTu = index,
                    TenantId = chucVuDanhGia.TenantId
                });
            }
            CurrentUnitOfWork.SaveChanges();
        }

        public void Delete(long id, long canBoId, long chucVuId)
        {
            _canBoChucVuRepos.Delete(x => x.Id == id);
            _canboChucvuDanhGiaRepos.Delete(x => x.CanBoId == canBoId && x.ChucVuCanBoId == chucVuId);

            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

    }
}
