﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using newPSG.PMS.Authorization;
using newPSG.PMS.Authorization.Roles;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Authorization.Users.Dto;
using newPSG.PMS.Authorization.Users.Profile;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    [AbpAuthorize(AppPermissions.Pages_Admin_CanBo)]
    public class CanBoAppService : PMSAppServiceBase, ICanBo
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<CanBo, long> _canBo;
        private readonly IRepository<DonVi, long> _donviRepos;
        private readonly IRepository<CanBoChucVu, long> _canboChucVuRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canboChucVuDanhGiaRepos;
        private readonly IRepository<DonViChuyenTrach, long> _donViChuyenTrachRepos;
        private readonly IRepository<PhieuDanhGiaChucVu, long> _phieuDanhGiaChucVuRepos;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhCanBoRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<LoaiDonVi, long> _loaiDonViRepos;
        private readonly RoleManager _roleManager;
        private readonly ICache _mainCache;
        private readonly UserManager _userManager;
        private readonly IUserAppService _iUserAppService;
        private readonly IProfileAppService _iProfileAppService;
        private readonly IRepository<ThanhVienNhomChucVu, long> _thanhVienNhomChucVuRepos;
        public CanBoAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<CanBo, long> canBo,
            IRepository<DonVi, long> donviRepos,
            IRepository<CanBoChucVu, long> canboChucVuRepos,
            IRepository<ChucVu, long> chucVuRepos,
            IRepository<CanBoChucVuDanhGia, long> canboChucVuDanhGiaRepos,
            IRepository<DonViChuyenTrach, long> donViChuyenTrachRepos,
            IRepository<PhieuDanhGiaChucVu, long> phieuDanhGiaChucVuRepos,
            IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaChiTietRepos,
            IRepository<ThangDiem, long> thangDiemRepos,
            IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
            IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhCanBoRepos,
            IRepository<LoaiDonVi, long> loaiDonViRepos,
            IProfileAppService iProfileAppService,
             RoleManager roleManager,
        UserManager userManager,
        IUserAppService iUserAppService,
        IRepository<ThanhVienNhomChucVu, long> thanhVienNhomChucVuRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _canBo = canBo;
            _donviRepos = donviRepos;
            _canboChucVuRepos = canboChucVuRepos;
            _chucVuRepos = chucVuRepos;
            _canboChucVuDanhGiaRepos = canboChucVuDanhGiaRepos;
            _donViChuyenTrachRepos = donViChuyenTrachRepos;
            _phieuDanhGiaChucVuRepos = phieuDanhGiaChucVuRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
            _lichSuKetQuaDanhGiaRepos = lichSuKetQuaDanhGiaChiTietRepos;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _phieuDanhCanBoRepos = phieuDanhCanBoRepos;
            _mainCache = _cacheManager.GetCache("CanBoAppService");
            _userManager = userManager;
            _roleManager = roleManager;
            _iUserAppService = iUserAppService;
            _loaiDonViRepos = loaiDonViRepos;
            _iProfileAppService = iProfileAppService;
            _thanhVienNhomChucVuRepos = thanhVienNhomChucVuRepos;
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_CanBo)]
        public PagedResultDto<CanBoResponse> GetByFilter(CanBoInput input)
        {
            input.Format();
            var items = new List<CanBoResponse>();
            int totalCount = 0;
            // kiem tra ng dung
            var kiemTraNguoiDung = _userManager.Users.Where(x => x.Id == AbpSession.UserId).FirstOrDefault();
            var lstDonVi = new List<DonVi>();
            if (kiemTraNguoiDung.UserName == "admin")
            {
                lstDonVi = _donviRepos.GetAll().ToList();
            }
            else
            {
                var cb = _canBo.GetAll().Where(x => x.UserId == AbpSession.UserId).FirstOrDefault();
                var getDonViQuanLy = _donViChuyenTrachRepos.GetAll().Where(x => x.CanBoId == cb.Id && x.ChucVuId == input.ChucVuId).Select(x => x.DonViId);

                lstDonVi = (from dvql in getDonViQuanLy
                            from dv in _donviRepos.GetAll().Where(x => x.Id == dvql)
                            select new DonVi
                            {
                                Id = dv.Id,
                                Ten = dv.Ten,
                                DonViQuanLyId = dv.DonViQuanLyId,
                                LoaiDonViId = dv.LoaiDonViId
                            }).ToList();

                input.ChucVuId = null;
            }

            var query = from cb in _canBo.GetAll()
                        from dv in lstDonVi.Where(x => x.Id == cb.DonViId)
                        from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                        from cbcv in _canboChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id).DefaultIfEmpty()
                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcv.ChucVuId).DefaultIfEmpty()
                        where (!input.DonViId.HasValue || cb.DonViId == input.DonViId.Value)
                        && (!input.ChucVuId.HasValue || cbcv.ChucVuId == input.ChucVuId.Value)
                        && (input.Filter == null || input.Filter == "" || user.Name.Contains(input.Filter)
                        || user.Surname.Contains(input.Filter) || user.PhoneNumber.Contains(input.Filter)
                        || user.EmailAddress.Contains(input.Filter))
                        select new CanBoResponse
                        {
                            Id = cb.Id,
                            TenantId = cb.TenantId,
                            Ten = (user != null) ? user.Name : "",
                            DonViId = cb.DonViId,
                            Ho = (user != null) ? user.Surname : "",
                            UserId = cb.UserId,
                            PhoneNumber = (user != null) ? user.PhoneNumber : "",
                            EmailAddress = (user != null) ? user.EmailAddress : "",
                            TenDonVi = (dv != null) ? dv.Ten : "",
                            Avatar = (user != null) ? user.ProfilePictureId : null,
                            TinhId = cb.TinhId,
                            HuyenId = cb.HuyenId,
                            XaId = cb.XaId,
                            Address = cb.Address,
                            ChucVuId = (cv != null) ? cv.Id : 0,
                            TenChucVu = (cv != null) ? cv.Ten : "",
                            DonViQuanLyId = (dv != null) ? dv.DonViQuanLyId : 0,
                            LoaiDonViId = (dv != null) ? dv.LoaiDonViId : 0,
                            LoaiHopDong = cb.LoaiHopDong,
                            ChucVuChinh = (cbcv == null) ? 0 : cbcv.ChucVuChinh
                        };

            totalCount = query.Count();
            items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            //input.Format(); 

            foreach (var item in items)
            {
                if (item.DonViQuanLyId != null && item.DonViQuanLyId != 0)
                {
                    var donViql = _donviRepos.Get(item.DonViQuanLyId.Value);
                    item.TenDonViQuanLy = donViql.Ten;
                }

                if (item.LoaiDonViId != 0)
                {
                    var ldv = _loaiDonViRepos.Get(item.LoaiDonViId);
                    item.TenLoaiDonVi = (ldv != null) ? ldv.Ten : "";
                }
            }
            return new PagedResultDto<CanBoResponse>(totalCount, items);
        }
        public async Task<CanBoDto> GetChiTietCanBo(long CanBoId)
        {
            var canBoInfo = new CanBoDto();

            var getCanBo = (from cb in _canBo.GetAll()
                            from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                            where cb.Id == CanBoId
                            select new CanBoDto
                            {
                                Id = cb.Id,
                                TenantId = cb.TenantId,
                                Ten = (user != null) ? user.Name : "",
                                DonViId = cb.DonViId,
                                Ho = (user != null) ? user.Surname : "",
                                UserId = cb.UserId,
                                PhoneNumber = (user != null) ? user.PhoneNumber : "",
                                EmailAddress = (user != null) ? user.EmailAddress : "",
                                ProfilePictureId = (user != null) ? user.ProfilePictureId : null,
                                TinhId = cb.TinhId,
                                HuyenId = cb.HuyenId,
                                XaId = cb.XaId,
                                Address = cb.Address,
                                LoaiHopDong = cb.LoaiHopDong
                            }).FirstOrDefault();

            //Getting all available roles
            var userRoleDtos = await _roleManager.Roles
                .OrderBy(r => r.DisplayName)
                .Select(r => new UserRoleDto
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                    RoleDisplayName = r.DisplayName
                })
                .ToArrayAsync();

            if (getCanBo != null)
            {
                canBoInfo = getCanBo;
                var user = await UserManager.GetUserByIdAsync(getCanBo.UserId);

                foreach (var userRoleDto in userRoleDtos)
                {
                    userRoleDto.IsAssigned = await UserManager.IsInRoleAsync(user, userRoleDto.RoleName);
                }
            }
            else
            {
                // tạo mới
                foreach (var defaultRole in await _roleManager.Roles.Where(r => r.IsDefault).ToListAsync())
                {
                    var defaultUserRole = userRoleDtos.FirstOrDefault(ur => ur.RoleName == defaultRole.Name);
                    if (defaultUserRole != null)
                    {
                        defaultUserRole.IsAssigned = true;
                    }
                }
            }

            canBoInfo.Roles = userRoleDtos;


            return canBoInfo;
        }

        public string KiemTraNguoiDung(long userId, string email, string phoneNumber)
        {
            if (userId > 0)
            {
                var checkEmail = _userManager.Users.Any(x => x.Id != userId && x.EmailAddress == email);
                if (checkEmail)
                {
                    return "Email đã tồn tại vui lòng kiểm tra lại.";
                }

                var checkPhoneNumber = _userManager.Users.Any(x => x.Id != userId && x.PhoneNumber == phoneNumber);
                if (checkPhoneNumber)
                {
                    return "Số điện thoại đã tồn tại vui lòng kiểm tra lại";
                }
            }
            else
            {
                var checkEmail = _userManager.Users.Any(x => x.EmailAddress == email);
                if (checkEmail)
                {
                    return "Email đã tồn tại vui lòng kiểm tra lại.";
                }

                var checkPhoneNumber = _userManager.Users.Any(x => x.PhoneNumber == phoneNumber);
                if (checkPhoneNumber)
                {
                    return "Số điện thoại đã tồn tại vui lòng kiểm tra lại";
                }
            }

            return "";
        }

        public async Task<CanBoDto> Upsert(CanBoDto input)
        {
            var erroMsg = KiemTraNguoiDung(input.UserId, input.EmailAddress, input.PhoneNumber);

            if (!string.IsNullOrWhiteSpace(erroMsg))
            {
                input.erroMsg = erroMsg;
                return input;
            }

            var user = new CreateOrUpdateUserInput();
            user.SetRandomPassword = false;
            user.SendActivationEmail = false;

            user.User = new UserEditDto();
            user.User.Id = input.UserId;
            user.User.EmailAddress = input.EmailAddress;
            user.User.IsActive = true;
            user.User.IsLockoutEnabled = false;
            user.User.IsTwoFactorEnabled = false;
            user.User.Name = input.Ten;
            user.User.PhoneNumber = input.PhoneNumber;
            user.User.ShouldChangePasswordOnNextLogin = false;
            user.User.UserName = input.PhoneNumber;
            user.User.Surname = input.Ho;
            user.User.Password = input.Password;
            user.AssignedRoleNames = input.AssignedRoleNames;

            var userId = await _iUserAppService.CreateOrUpdateUser(user);

            long canBoId = 0;
            if (input.Id > 0)
            {
                var canbo = _canBo.Get(input.Id);
                canbo.DonViId = input.DonViId;
                canbo.Address = input.Address ?? "";
                canbo.TinhId = input.TinhId;
                canbo.HuyenId = input.HuyenId;
                canbo.XaId = input.XaId;
                canbo.LoaiHopDong = input.LoaiHopDong;
                await _canBo.UpdateAsync(canbo);
                canBoId = input.Id;
            }
            else
            {
                //if (AbpSession.TenantId.HasValue)
                var canbo = new CanBo();
                canbo.DonViId = input.DonViId;
                canbo.Address = input.Address;
                canbo.TinhId = input.TinhId;
                canbo.HuyenId = input.HuyenId;
                canbo.XaId = input.XaId;
                canbo.UserId = userId;
                canbo.LoaiHopDong = input.LoaiHopDong;
                canbo.TenantId = AbpSession.TenantId;
                canBoId = await _canBo.InsertAndGetIdAsync(canbo);
            }

            input.UserId = userId;
            input.Id = canBoId;

            return input;
        }

        [AbpAuthorize(AppPermissions.Pages_Admin_CanBo_Delete)]
        public async Task<int> Delete(long id, long userId)
        {
            int result = 0;
            try
            {
                _canBo.Delete(x => x.Id == id);
                CurrentUnitOfWork.SaveChanges();
                _mainCache.Clear();

                result = await _iUserAppService.DeleteCanBoUser(userId);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return result;
        }

        public PagedResultDto<CanBoResponse> TimKiemThanhVienNhomChucVu(CanBoInput input)
        {
            var items = new List<CanBoResponse>();
            int totalCount = 0;

            var lstThanhVien = _thanhVienNhomChucVuRepos.GetAll().Where(x => x.ChucVuId == input.ChucVuId)
                .Select(x => new { CanBoId = x.CanBoId, ChucVuId = x.ChucVuCanBoId });

            var query = from cb in _canBo.GetAll()
                        from dv in _donviRepos.GetAll().Where(x => x.Id == cb.DonViId)
                        from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                        from cbcv in _canboChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id).DefaultIfEmpty()
                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcv.ChucVuId).DefaultIfEmpty()
                        where (!input.DonViId.HasValue || cb.DonViId == input.DonViId.Value)
                        && (string.IsNullOrEmpty(input.Filter) || user.Name.Contains(input.Filter)
                        || user.Surname.Contains(input.Filter) || user.PhoneNumber.Contains(input.Filter)
                        || user.EmailAddress.Contains(input.Filter) || cb.Address.Contains(input.Filter))
                        && (!lstThanhVien.Any(d => d.CanBoId == cb.Id && d.ChucVuId == cv.Id))
                        select new CanBoResponse
                        {
                            Id = cb.Id,
                            TenantId = cb.TenantId,
                            Ten = (user != null) ? user.Name : "",
                            Ho = (user != null) ? user.Surname : "",
                            TenDonVi = (dv != null) ? dv.Ten : "",
                            ChucVuId = (cv != null) ? cv.Id : 0,
                            TenChucVu = (cv != null) ? cv.Ten : "",
                            ChucVuChinh = (cbcv != null) ? cbcv.ChucVuChinh : 2
                        };

            totalCount = query.Count();
            items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<CanBoResponse>(totalCount, items);
        }

        public List<CanBoResponse> GetDsNhanVienTheoDonViId(long donViId)
        {
            var items = new List<CanBoResponse>();

            items = (from cb in _canBo.GetAll()
                        from cbcv in _canboChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id).DefaultIfEmpty()
                        from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                        where cb.DonViId == donViId
                        select new CanBoResponse
                        {
                            Id = cb.Id,
                            TenantId = cb.TenantId,
                            Ten =  (user == null) ? "" : (user.Surname + " " + user.Name),
                            ChucVuId = (cbcv == null) ? 0  : cbcv.ChucVuId
                        }).ToList();

            return items;
        }
}
}
