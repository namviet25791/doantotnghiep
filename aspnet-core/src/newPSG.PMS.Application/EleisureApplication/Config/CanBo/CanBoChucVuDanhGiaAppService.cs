﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Runtime.Caching;
using Abp.Threading;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Storage;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{

    public class CanBoChucVuDanhGiaAppService : PMSAppServiceBase, ICanBoChucVuDanhGia
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canBoChucVuDanhGiaRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly ICache _mainCache;
        private readonly IPhieuDanhGiaCanBo _iPhieuDanhGiaCanBoService;
        public CanBoChucVuDanhGiaAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<CanBoChucVuDanhGia, long> canBoChucVuDanhGiaRepos,
            IPhieuDanhGiaCanBo iPhieuDanhGiaCanBoService,
            IRepository<CanBo, long> canBoRepos,
            IRepository<ChucVu, long> chucVuRepos,
            IRepository<CanBoChucVu, long> canBoChucVuRepos)
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _canBoChucVuDanhGiaRepos = canBoChucVuDanhGiaRepos;
            _mainCache = _cacheManager.GetCache("CanBoChucVuDanhGiaAppService");
            _iPhieuDanhGiaCanBoService = iPhieuDanhGiaCanBoService;
            _canBoRepos = canBoRepos;
            _chucVuRepos = chucVuRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
        }

        public PagedResultDto<CanBoChucVuDanhGiaDto> GetByFilter(CanBoChucVuDanhGiaInput input)
        {
            var query = from x in _canBoChucVuDanhGiaRepos.GetAll()
                        from cv in _chucVuRepos.GetAll().Where(d => d.Id == x.ChucVuDanhGiaId)
                        where (!input.CanBoId.HasValue || x.CanBoId == input.CanBoId.Value)
                        && (!input.ChucVuCanBoId.HasValue || x.ChucVuCanBoId == input.ChucVuCanBoId.Value)
                        select new CanBoChucVuDanhGiaDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            CanBoId = x.CanBoId,
                            ChucVuCanBoId = x.ChucVuCanBoId,
                            ChucVuDanhGiaId = x.ChucVuDanhGiaId,
                            NguoiCham = x.NguoiCham,
                            CanBoDanhGiaId = x.CanBoDanhGiaId,
                            ThuTu = x.ThuTu,
                            LoaiChucVu = cv.LoaiChucVu
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<CanBoChucVuDanhGiaDto>(totalCount, items);
        }
        public async Task<ErroMsg> Upsert(List<CanBoChucVuDanhGiaDto> input)
        {
            var erroMsg = new ErroMsg();
            erroMsg.Result = false;
            try
            {
                //if (_iPhieuDanhGiaCanBoService.KiemTraThoiGianKetThucDanhGia(DateTime.Now))
                //{
                _canBoChucVuDanhGiaRepos.Delete(x => x.CanBoId == input[0].CanBoId && x.ChucVuCanBoId == input[0].ChucVuCanBoId);

                var lstItemInsert = input.Where(x => x.ChucVuDanhGiaId >= 0).Select(x => x.ChucVuDanhGiaId).Distinct();
                foreach (var item in lstItemInsert)
                {
                    var lstNgCham = input.Where(x => x.ChucVuDanhGiaId == item);
                    if (lstNgCham.Count() > 1)
                    {
                        var ngCham = lstNgCham.Where(x => x.NguoiCham == true).FirstOrDefault();
                        if (ngCham != null)
                        {
                            var entity1 = ObjectMapper.Map<CanBoChucVuDanhGia>(ngCham);
                            entity1.TenantId = AbpSession.TenantId;
                            await _canBoChucVuDanhGiaRepos.InsertAndGetIdAsync(entity1);
                            continue;
                        }
                    }

                    var itemChamDuyet = lstNgCham.FirstOrDefault();
                    var entity = ObjectMapper.Map<CanBoChucVuDanhGia>(itemChamDuyet);
                    entity.TenantId = AbpSession.TenantId;
                    await _canBoChucVuDanhGiaRepos.InsertAndGetIdAsync(entity);
                }
                erroMsg.Result = true;
                //}
                //else
                //{
                //    erroMsg.Result = false;
                //    erroMsg.Msg = "Đang trong thời gian đánh giá không được thay đổi.";                    
                //}
            }
            catch (Exception ex)
            {
                erroMsg.Msg = "Có lỗi xảy ra vui lòng thao tác lại.";
                erroMsg.Result = false;
            }
            return erroMsg;
        }

        public void Delete(long id)
        {
            _canBoChucVuDanhGiaRepos.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }

        public List<CauHinhChucVuCanBoDanhGiaDto> GetCanBoChucVuDanhGiaByCanBoId(long canBoId, long chucVuId)
        {
            var lstChucVu = (from x in _canBoChucVuDanhGiaRepos.GetAll()
                             from cv in _chucVuRepos.GetAll().Where(d => d.Id == x.ChucVuDanhGiaId)
                             where x.ChucVuCanBoId == chucVuId && x.CanBoId == canBoId
                             select new CauHinhChucVuCanBoDanhGiaDto
                             {
                                 Id = x.Id,
                                 ThuTu = x.ThuTu,
                                 ChucVuDanhGiaId = x.ChucVuDanhGiaId,
                                 NguoiCham = x.NguoiCham,
                                 LoaiChucVu = cv.LoaiChucVu,
                                 CanBoDanhGiaId=x.CanBoDanhGiaId,
                             }).ToList();


            var getDonViCanBo = _canBoRepos.GetAll().FirstOrDefault(x => x.Id == canBoId);


            var lstCanBoThuocDonVi = (from cb in _canBoRepos.GetAll()
                                      from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                      from cvcb in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
                                      where cb.DonViId == getDonViCanBo.DonViId
                                      select new
                                      {
                                          CanBoId = cb.Id,
                                          TenCanBo = u.Surname + " " + u.Name,
                                          ChucVuId = cvcb.ChucVuId
                                      });

            foreach (var item in lstChucVu)
            {
                if (item.LoaiChucVu == (int)AppConsts.LoaiHinhChucVu.ChucVu)
                {
                    item.ListCanBoChucVu = lstCanBoThuocDonVi.Where(x => x.ChucVuId == item.ChucVuDanhGiaId).Select(x => new
                    CanBoChucVuTemp
                    {
                        CanBoId = x.CanBoId,
                        TenCanBo = x.TenCanBo
                    }).ToList();

                    if (item.ListCanBoChucVu.Count == 0)
                    {
                        item.ListCanBoChucVu = (from cv in _canBoChucVuRepos.GetAll()
                                                from cb in _canBoRepos.GetAll().Where(x => x.Id == cv.CanBoId)
                                                from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                                where cv.ChucVuId == item.ChucVuDanhGiaId
                                                select new CanBoChucVuTemp
                                                {
                                                    CanBoId = cb.Id,
                                                    TenCanBo = u.Surname + " " + u.Name,
                                                }).ToList();

                    }

                    if (item.CanBoDanhGiaId == null)
                    {
                        item.CanBoDanhGiaId = (item.ListCanBoChucVu.Count() > 0) ? item.ListCanBoChucVu.FirstOrDefault().CanBoId : 0;
                    }
                   
                }
                item.CanBoId = canBoId;
                item.ChucVuCanBoId = chucVuId;
                
            }

            return lstChucVu;
        }

        public List<CanBoChucVuTemp> GetCanBoTheoChucVu(long chucVuDanhGiaId, long donViId)
        {
            var data = new List<CanBoChucVuTemp>();

            var lstCanBoThuocDonVi = (from cb in _canBoRepos.GetAll()
                                      from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                      from cvcb in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
                                      where cb.DonViId == donViId
                                      select new
                                      {
                                          CanBoId = cb.Id,
                                          TenCanBo = u.Surname + " " + u.Name,
                                          ChucVuId = cvcb.ChucVuId
                                      });

            data = lstCanBoThuocDonVi.Where(x => x.ChucVuId == chucVuDanhGiaId).Select(x => new
                  CanBoChucVuTemp
            {
                CanBoId = x.CanBoId,
                TenCanBo = x.TenCanBo
            }).ToList();

            if (data.Count == 0)
            {
                data = (from cv in _canBoChucVuRepos.GetAll()
                        from cb in _canBoRepos.GetAll().Where(x => x.Id == cv.CanBoId)
                        from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                        where cv.ChucVuId == chucVuDanhGiaId
                        select new CanBoChucVuTemp
                        {
                            CanBoId = cb.Id,
                            TenCanBo = u.Surname + " " + u.Name,
                        }).ToList();
            }

            return data;
        }

    }
}
