﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Runtime.Caching;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Configuration;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    public class KetQuaDanhGiaChiTietAppService : PMSAppServiceBase, IKetQuaDanhGiaChiTiet
    {
        private readonly ICacheManager _cacheManager;
        private readonly IIocResolver _iocResolver;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTiet;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhGiaCanBoRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaRepos;
        private readonly IRepository<ThongTinDanhGiaKeTiepTemp, long> _thongTinDanhGiaKeTiepTempRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canBoChucVuDanhGiaRepos;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<PhieuDanhGia, long> _phieuDanhGiaRepos;
        private readonly IRepository<ThanhVienNhomChucVu, long> _thanhVienNhomChucVuRepos;
        private readonly UserManager _userManager;
        private readonly ICache _mainCache;
        //private readonly IPhieuDanhGiaCanBo _iPhieuDanhGiaCanBo;
        private readonly IRepository<ChucVuDanhGia, long> _chucVuDanhGiaRepos;

        public KetQuaDanhGiaChiTietAppService(ICacheManager cacheManager,
            IIocResolver iocResolver,
            IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTiet,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhGiaCanBoRepos,
            IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaRepos,
            IRepository<ThangDiem, long> thangDiemRepos,
            IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
            IRepository<CanBoChucVu, long> canBoChucVuRepos,
            IRepository<CanBoChucVuDanhGia, long> canBoChucVuDanhGiaRepos,
            IRepository<CanBo, long> canBoRepos,
            IRepository<ChucVu, long> chucVuRepos,
            IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
             IRepository<PhieuDanhGia, long> phieuDanhGiaRepos,
            UserManager userManager,
            IRepository<ThongTinDanhGiaKeTiepTemp, long> thongTinDanhGiaKeTiepTempRepos,
            IRepository<ChucVuDanhGia, long> chucVuDanhGiaRepos,
            IRepository<ThanhVienNhomChucVu, long>thanhVienNhomChucVuRepos

           )
        {
            _cacheManager = cacheManager;
            _iocResolver = iocResolver;
            _ketQuaDanhGiaChiTiet = ketQuaDanhGiaChiTiet;
            _phieuDanhGiaCanBoRepos = phieuDanhGiaCanBoRepos;
            _lichSuKetQuaDanhGiaRepos = lichSuKetQuaDanhGiaRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
            _canBoChucVuRepos = canBoChucVuRepos;
            _canBoChucVuDanhGiaRepos = canBoChucVuDanhGiaRepos;
            _canBoRepos = canBoRepos;
            _chucVuRepos = chucVuRepos;
            _userManager = userManager;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _phieuDanhGiaRepos = phieuDanhGiaRepos;
            _mainCache = _cacheManager.GetCache("KetQuaDanhGiaChiTietAppService");
            _thongTinDanhGiaKeTiepTempRepos = thongTinDanhGiaKeTiepTempRepos;
            _chucVuDanhGiaRepos = chucVuDanhGiaRepos;
            _thanhVienNhomChucVuRepos = thanhVienNhomChucVuRepos;
        }


        #region A Huong
        public PagedResultDto<KetQuaDanhGiaChiTietDto> GetByFilter(KetQuaDanhGiaChiTietInput input)
        {
            input.Format();
            var query = from x in _ketQuaDanhGiaChiTiet.GetAll()
                        where (string.IsNullOrEmpty(input.Filter) || x.NhanXet.Contains(input.Filter))
                        //&& (!input.CanBoId.HasValue || x.CanBoId == input.CanBoId.Value)
                        //&& (!input.PhieuDanhGiaId.HasValue || x.PhieuDanhGiaId == input.PhieuDanhGiaId.Value)
                        && (!input.TieuChiId.HasValue || x.TieuChiId == input.TieuChiId.Value)
                        && x.Diem != null
                        select new KetQuaDanhGiaChiTietDto
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            TieuChiId = x.TieuChiId,
                            NhanXet = x.NhanXet,
                            //Diem = x.Diem.Value,
                            PhieuDanhGiaCanBoId = x.PhieuDanhGiaCanBoId,
                            PhieuDanhGiaId = x.PhieuDanhGiaId,
                            //  LichSuKetQuaDanhGiaChiTiet = x.LichSuKetQuaDanhGiaChiTiet
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<KetQuaDanhGiaChiTietDto>(totalCount, items);
        }
        public async System.Threading.Tasks.Task<long> Upsert(KetQuaDanhGiaChiTietDto input)
        {
            //input.Format();
            if (input.Id > 0)
            {
                var category = _ketQuaDanhGiaChiTiet.Get(input.Id);
                ObjectMapper.Map(input, category);
            }
            else
            {
                var entity = ObjectMapper.Map<KetQuaDanhGiaChiTiet>(input);
                entity.TenantId = AbpSession.TenantId;
                input.Id = _ketQuaDanhGiaChiTiet.InsertAndGetId(entity);
            }
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
            return input.Id;
        }
        public void Delete(long id)
        {
            _ketQuaDanhGiaChiTiet.Delete(x => x.Id == id);
            CurrentUnitOfWork.SaveChanges();
            _mainCache.Clear();
        }
        public async Task ThemDanhGia(KetQuaDanhGiaUpdate input)
        {
            input.TenantId = AbpSession.TenantId;
            var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.FirstOrDefault(x => x.CanBoId == input.CanBoId
            && x.PhieuDanhGiaId == input.PhieuDanhGiaId
            && x.Thang == DateTime.Today.Month
            && x.Nam == DateTime.Today.Year);
            if (phieuDanhGiaCanBo == null)
            {
                phieuDanhGiaCanBo = new PhieuDanhGiaCanBo()
                {
                    PhieuDanhGiaId = input.PhieuDanhGiaId,
                    CanBoId = input.CanBoId,
                    DaHoanThanh = (int)AppConsts.TrangThaiPhieuCaNhan.Moi,
                    Ky = 0,
                    Nam = DateTime.Today.Year,
                    Quy = DateTime.Today.Month / 3,
                    Thang = DateTime.Today.Month,
                    LichSuKetQuaDanhGiaChiTietHienTaiId = 0
                };
                phieuDanhGiaCanBo.Id = _phieuDanhGiaCanBoRepos.InsertAndGetId(phieuDanhGiaCanBo);
            }
            if (phieuDanhGiaCanBo != null)
            {
                input.ThangDiemChiTietId = GetThangDiemChiTietId(input.PhieuDanhGiaId, input.TieuChis.Sum(x => x.Diem));
                var newId = _lichSuKetQuaDanhGiaRepos.InsertAndGetId(new LichSuKetQuaDanhGiaChiTiet()
                {
                    PhieuDanhGiaCanBoId = phieuDanhGiaCanBo.Id,
                    TrangThai = input.TrangThai,
                    LicSuPhieuDanhGiaChiTietTruocDoId = phieuDanhGiaCanBo.LichSuKetQuaDanhGiaChiTietHienTaiId,
                    ThangDiemChiTietId = input.ThangDiemChiTietId,
                    CanBoId = input.CanBoId,
                    CanBoDanhGiaId = input.CanBoDanhGiaId,
                    NgayDanhGia = DateTime.Today,
                    CapDanhGia = (input.CanBoDanhGiaId == input.CanBoId) ? (int)AppConsts.CapDanhGia.TuDanhGia : (int)AppConsts.CapDanhGia.CapTren,
                });
                phieuDanhGiaCanBo.LichSuKetQuaDanhGiaChiTietHienTaiId = newId;
                foreach (var tieuChi in input.TieuChis)
                {
                    _ketQuaDanhGiaChiTiet.Insert(new KetQuaDanhGiaChiTiet()
                    {
                        PhieuDanhGiaCanBoId = phieuDanhGiaCanBo.Id,
                        Diem = tieuChi.Diem,
                        PhieuDanhGiaId = phieuDanhGiaCanBo.PhieuDanhGiaId,
                        NhanXet = tieuChi.NhanXet,
                        TieuChiId = tieuChi.TieuChiId,
                        //LichSuKetQuaDanhGiaChiTiet = newId
                    });
                }
                CurrentUnitOfWork.SaveChanges();
                CapNhatTrangThaiPhieu(phieuDanhGiaCanBo);
            }
        }


        private void CapNhatTrangThaiPhieu(PhieuDanhGiaCanBo phieuDanhGiaCanBo)
        {
            if (phieuDanhGiaCanBo.DaHoanThanh != (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh)
            {
                if (_lichSuKetQuaDanhGiaRepos.FirstOrDefault(x =>
                      x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.Id
                      && x.CanBoDanhGiaId == phieuDanhGiaCanBo.CanBoId
                      && x.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui) == null)

                {
                    return;
                }

                var chucVuDanhGiaIds = _canBoChucVuDanhGiaRepos.GetAllList(x => x.CanBoId == phieuDanhGiaCanBo.CanBoId).Select(xx => xx.ChucVuDanhGiaId).ToList();
                var canBoDanhGiaIds = _canBoChucVuRepos.GetAllList(x => chucVuDanhGiaIds.Contains(x.ChucVuId)).Select(xx => xx.CanBoId).ToList();
                if (canBoDanhGiaIds != null && canBoDanhGiaIds.Count > 0)
                    canBoDanhGiaIds.Remove(phieuDanhGiaCanBo.CanBoId);
                foreach (var canBoDanhGia in canBoDanhGiaIds)
                {
                    // Nếu tìm thấy chưa có ai duyệt
                    if (_lichSuKetQuaDanhGiaRepos.FirstOrDefault(x =>
                     x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBo.Id
                     && x.CanBoDanhGiaId == canBoDanhGia
                     && x.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui) == null)
                    {
                        phieuDanhGiaCanBo.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.ChuaHoanThanh;
                        CurrentUnitOfWork.SaveChanges();
                        return;
                    }
                }
                phieuDanhGiaCanBo.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;
                CurrentUnitOfWork.SaveChanges();
            }
        }


        public PagedResultDto<KetQuaDanhGiaTongHop> GetKetQuaTongHop(KetQuaDanhGiaTongHopInput input)
        {
            input.Format();
            var query = from x in _phieuDanhGiaCanBoRepos.GetAll()
                        where (input.CanBoId == x.CanBoId)
                          && (!input.Nam.HasValue || input.Nam.Value == x.Nam)
                        select new KetQuaDanhGiaTongHop
                        {
                            Id = x.Id,
                            TenantId = x.TenantId,
                            CanBoId = x.CanBoId,
                            Nam = x.Nam,
                            DaHoanThanh = x.DaHoanThanh,
                            Ky = x.Ky,
                            LichSuKetQuaDanhGiaChiTietHienTaiId = x.LichSuKetQuaDanhGiaChiTietHienTaiId,
                            PhieuDanhGiaId = x.PhieuDanhGiaId,
                            Quy = x.Quy,
                            Thang = x.Thang,
                            TrangThai = x.DaHoanThanh
                        };

            var totalCount = query.Count();
            var items = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
            foreach (var item in items)
            {
                var lichSuKetQuaDanhGiaIds = _lichSuKetQuaDanhGiaRepos.GetAllList(x => x.PhieuDanhGiaCanBoId == item.Id && x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia).Select(xx => xx.Id).ToList();
                // item.TongDiem = _ketQuaDanhGiaChiTiet.GetAllList(x => x.PhieuDanhGiaCanBoId == item.Id && lichSuKetQuaDanhGiaIds.Contains(x.LichSuKetQuaDanhGiaChiTiet)).Sum(xx => xx.Diem);
                item.XepLoai = GetXepLoai(item.PhieuDanhGiaId, item.TongDiem);
                //  var tongDiemCuoiCung = _ketQuaDanhGiaChiTiet.GetAllList(x => x.PhieuDanhGiaCanBoId == item.Id && x.LichSuKetQuaDanhGiaChiTiet == item.LichSuKetQuaDanhGiaChiTietHienTaiId).Sum(xx => xx.Diem);
                // item.XepLoaiCuoiCung = GetXepLoai(item.PhieuDanhGiaId, tongDiemCuoiCung);
            }
            return new PagedResultDto<KetQuaDanhGiaTongHop>(totalCount, items);
        }
        //private string GetXepLoai(KetQuaDanhGiaTongHop input,float diem)
        //{
        //    //var date = new DateTime(input.Nam, input.Thang, 1);
        //    var thangDiem = _thangDiemRepos.FirstOrDefault(x => x.PhieuDanhGiaId == input.PhieuDanhGiaId);
        //    //&& x.HieuLucTuNgay <= date && x.HieuLucToiNgay >= date);
        //    if(thangDiem!=null)
        //    {
        //        var thangDiemChiTiet = _thangDiemChiTietRepos.FirstOrDefault(x => x.ThangDiemId == thangDiem.Id && x.DiemSan <= diem && x.DiemTran > diem);
        //        if (thangDiemChiTiet != null)
        //            return thangDiemChiTiet.Ten;
        //    }
        //    return "";
        //}
        private string GetXepLoai(long phieuDanhGiaId, float diem)
        {
            ////var date = new DateTime(input.Nam, input.Thang, 1);
            //var thangDiem = _thangDiemRepos.FirstOrDefault(x => x.PhieuDanhGiaId == phieuDanhGiaId);
            ////&& x.HieuLucTuNgay <= date && x.HieuLucToiNgay >= date);
            //if (thangDiem != null)
            //{
            //    var thangDiemChiTiet = _thangDiemChiTietRepos.FirstOrDefault(x => x.ThangDiemId == thangDiem.Id && x.DiemSan <= diem && x.DiemTran > diem);
            //    if (thangDiemChiTiet != null)
            //        return thangDiemChiTiet.Ten;
            //}
            return "";
        }
        private long GetThangDiemChiTietId(long phieuDanhGiaId, float diem)
        {
            ////var date = new DateTime(input.Nam, input.Thang, 1);
            //var thangDiem = _thangDiemRepos.FirstOrDefault(x => x.PhieuDanhGiaId == phieuDanhGiaId);
            ////&& x.HieuLucTuNgay <= date && x.HieuLucToiNgay >= date);
            //if (thangDiem != null)
            //{
            //    var thangDiemChiTiet = _thangDiemChiTietRepos.FirstOrDefault(x => x.ThangDiemId == thangDiem.Id && x.DiemSan <= diem && x.DiemTran > diem);
            //    if (thangDiemChiTiet != null)
            //        return thangDiemChiTiet.Id;
            //}
            return 0;
        }
        public void UpdateDanhGia(KetQuaDanhGiaChiTietUpdate input)
        {
            var lichSuTuDanhGia = _lichSuKetQuaDanhGiaRepos.FirstOrDefault(x => x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId && x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia);
            if (lichSuTuDanhGia != null)
            {
                var phieuDanhGiaId = _phieuDanhGiaCanBoRepos.Get(lichSuTuDanhGia.PhieuDanhGiaCanBoId).PhieuDanhGiaId;
                lichSuTuDanhGia.ThangDiemChiTietId = GetThangDiemChiTietId(phieuDanhGiaId, input.TieuChis.Sum(x => x.Diem));
                lichSuTuDanhGia.TrangThai = input.TrangThai;
                foreach (var item in input.TieuChis)
                {
                    //var obj = _ketQuaDanhGiaChiTiet.FirstOrDefault(x => x.TieuChiId == item.TieuChiId
                    //&& x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId
                    //&& x.LichSuKetQuaDanhGiaChiTiet == lichSuTuDanhGia.Id);
                    //if (obj != null)
                    //{
                    //    obj.Diem = item.Diem;
                    //    obj.NhanXet = item.NhanXet;
                    //}
                }
                CurrentUnitOfWork.SaveChanges();
                var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.FirstOrDefault(x => x.Id == input.PhieuDanhGiaCanBoId);
                if (phieuDanhGiaCanBo != null)
                    CapNhatTrangThaiPhieu(phieuDanhGiaCanBo);
            }
        }

        public bool GetDaGuiPhieu(long canBoId)
        {
            bool result = false;

            var thoiGianKetThucDanhGia = SettingManager.GetSettingValue(AppSettings.TenantManagement.ThoiGianKetThucDanhGia);

            DateTime ngHienTai = DateTime.Now.Date;
            DateTime thoiGianGuiPhieu = ngHienTai.AddMonths(-1);

            var phieuDanhGia = _phieuDanhGiaCanBoRepos.GetAll().Any(x => x.CanBoId == canBoId
            && x.Nam == thoiGianGuiPhieu.Year
            && x.Thang == thoiGianGuiPhieu.Month);

            if (phieuDanhGia)
            {
                // đã có phiếu
                result = true;
            }
            else
            {
                // chưa có phiếu kiểm tra tiếp đã quá ngày duyệt phiếu hàng tháng hay chưa
                int ngayKetThuc = 1;
                int.TryParse(thoiGianKetThucDanhGia, out ngayKetThuc);

                DateTime ngBatDau = new DateTime(ngHienTai.Year, ngHienTai.Month, 1);
                int nkt = DateTime.DaysInMonth(ngBatDau.Year, ngBatDau.Month);

                ngayKetThuc = (nkt < ngayKetThuc) ? nkt : ngayKetThuc;

                DateTime ngKetThuc = new DateTime(ngHienTai.Year, ngHienTai.Month, ngayKetThuc);

                if (ngHienTai >= ngBatDau && ngHienTai <= ngKetThuc)
                {
                    //được tạo phiếu
                    result = false;
                }
                else
                {
                    // quá hạn tạo phiếu
                    result = true;
                }
            }
            return result;
        }
        #endregion

        #region TrangQT
        public List<KetQuaTongHopDto> GetKetQuaChiTietAsync(long phieuDanhGiaCanBoId)
        {
            var lstKetQuaTongHop = new List<KetQuaTongHopDto>();

            var query = (from lskq in _lichSuKetQuaDanhGiaRepos.GetAll()
                         from cb in _canBoRepos.GetAll().Where(x => x.Id == lskq.CanBoDanhGiaId).DefaultIfEmpty()
                         from cv in _chucVuRepos.GetAll().Where(x => x.Id == lskq.ChucVuCanBoDanhGiaId).DefaultIfEmpty()
                         where lskq.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                         && lskq.KetQuaTongHop == true
                         select new
                         {
                             LichSuId = lskq.Id,
                             UserId = (cb != null) ? cb.UserId : 0,
                             TenChucVu = (cv != null) ? cv.Ten : "",
                             ChucVuCanBoDanhGiaID = (cv != null) ? cv.Id : 0,
                             ThangDiemChiTietId = lskq.ThangDiemChiTietId,
                             CanBoDanhGiaId = lskq.CanBoDanhGiaId,
                             CapDanhGia = lskq.CapDanhGia
                         }).OrderBy(x => x.LichSuId);

            var phieuDanhGia = (from pdgcb in _phieuDanhGiaCanBoRepos.GetAll()
                                from pdg in _phieuDanhGiaRepos.GetAll().Where(x => x.Id == pdgcb.PhieuDanhGiaId)
                                where pdgcb.Id == phieuDanhGiaCanBoId
                                select new
                                {
                                    TenPhieuDanhGia = pdg.Ten
                                }).FirstOrDefault();


            foreach (var item in query)
            {
                var obj = new KetQuaTongHopDto();

                var user = _userManager.Users.Where(x => x.Id == item.UserId).FirstOrDefault();
                if (user == null)
                {
                    obj.TenCanBo = "Tổ chuyên môn đánh giá";
                }
                else
                {
                    obj.TenCanBo = user.Surname + " " + user.Name;
                }

                obj.TenChucVu = item.TenChucVu;
                obj.CanBoDanhGiaId = item.CanBoDanhGiaId;
                obj.CapDanhGia = item.CapDanhGia;
                obj.ChucVuCanBoDanhGiaID = item.ChucVuCanBoDanhGiaID;

                var lsKetQua = _ketQuaDanhGiaChiTietRepos.GetAll()
                    .Where(x => x.LichSuKetQuaDanhGiaChiTietId == item.LichSuId && x.Diem != null);

                var tongDiem = (decimal)lsKetQua.Where(x => x.Diem.HasValue)
                    .ToList()
                    .Sum(x => x.Diem.Value);
                obj.TongDiem = Math.Round(tongDiem, 1);

                obj.ThangDiemChiTietId = item.ThangDiemChiTietId;

                var nx = lsKetQua.Where(x => x.TieuChiId == -1).FirstOrDefault();

                string nhanXet = "";
                if (nx != null)
                {
                    nhanXet = nx.NhanXet;
                    //var lstNhanXet = nx.NhanXet.Split(';').ToList();

                    //foreach (var n in lstNhanXet)
                    //{
                    //    nhanXet += n + "\n";
                    //}
                }

                obj.NhanXet = nhanXet;

                obj.TenPhieuDanhGia = (phieuDanhGia != null) ? phieuDanhGia.TenPhieuDanhGia : "";

                lstKetQuaTongHop.Add(obj);
            }

            return lstKetQuaTongHop;
        }
        public long ThemMoiLichSuVaKetQuaPhieuDanhGia(long phieuDanhGiaCanBoId, long lichSuKetQuaDanhGiaChiTietId, int loaiChucVuDanhGia)
        {
            long lichSuKetQuaDanhGiaId = 0;

            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);

            var lsTruocDo = _lichSuKetQuaDanhGiaRepos.Get(lichSuKetQuaDanhGiaChiTietId);

            #region kiem tra can bo chuc vu hien tai la ca nhan hay nhom danh gia
            long chucVuId = 0;
            string tenChucVu = "";
            var tmp = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.CanBoDanhGiaKeTiepId == lsTruocDo.CanBoDanhGiaId && x.ChucVuId == lsTruocDo.ChucVuCanBoDanhGiaId).FirstOrDefault();
            if (tmp != null)
            {
                var cv = GetNhomChucVu(pdgcb.CanBoId, pdgcb.ChucVuId, tmp.ThuTu);
                chucVuId = cv.Id;
                tenChucVu = cv.Ten;
            }
            #endregion

            var dsLichSu = _lichSuKetQuaDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.CapDoDanhGia == lsTruocDo.CapDoDanhGia && x.Id != lichSuKetQuaDanhGiaId).Select(x => new
            {
                Id = x.Id,
                CanBoDanhGiaId = x.CanBoDanhGiaId
            });
            int totalItem = dsLichSu.Count();


            var lskq = new LichSuKetQuaDanhGiaChiTiet();
            lskq.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
            lskq.LicSuPhieuDanhGiaChiTietTruocDoId = 0;
            //            
            lskq.LicSuPhieuDanhGiaChiTietTruocDoId = pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId;
            lskq.ThangDiemChiTietId = lsTruocDo.ThangDiemChiTietId;
            lskq.CanBoId = 0;
            lskq.CanBoDanhGiaId = (totalItem == 1) ? dsLichSu.FirstOrDefault().CanBoDanhGiaId : 0;
            lskq.CapDanhGia = lsTruocDo.CapDanhGia;
            lskq.NgayDanhGia = DateTime.Now;
            lskq.TrangThai = lsTruocDo.TrangThai;
            lskq.ChucVuCanBoDanhGiaId = chucVuId;
            lskq.IsKetQuaCuoiCung = false;
            lskq.TenantId = lsTruocDo.TenantId;
            lskq.LoaiChucVuDanhGia = lsTruocDo.LoaiChucVuDanhGia;
            lskq.KetQuaTongHop = true;
            lskq.CapDoDanhGia = lsTruocDo.CapDoDanhGia;
            lichSuKetQuaDanhGiaId = _lichSuKetQuaDanhGiaRepos.InsertAndGetId(lskq);

            pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId = lichSuKetQuaDanhGiaId;


            // tao ket qua chi tiet
            var kqChiTiet = (from ls in dsLichSu
                             from kq in _ketQuaDanhGiaChiTiet.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                             select new
                             {
                                 TieuChiId = kq.TieuChiId,
                                 Diem = kq.Diem,
                                 PhieuDanhGiaId = kq.PhieuDanhGiaId,
                                 PhieuDanhGiaCanBoId = kq.PhieuDanhGiaCanBoId,
                                 NhanXet = kq.NhanXet,
                                 CanBoDanhGiaId = ls.CanBoDanhGiaId
                             }).ToList();

            var groupKetQua = kqChiTiet.Where(x => x.TieuChiId != -1)
                .GroupBy(x => new { x.TieuChiId, x.PhieuDanhGiaId, x.PhieuDanhGiaCanBoId })
                .Select(g => new
                {
                    PhieuDanhGiaId = g.Key.PhieuDanhGiaId,
                    PhieuDanhGiaCanBoId = g.Key.PhieuDanhGiaCanBoId,
                    TieuChiId = g.Key.TieuChiId,
                    Diem = g.Sum(x => x.Diem)
                }).ToList();

            var nx = "";
            var tongHopNhanXet = kqChiTiet.Where(x => x.TieuChiId == -1 && x.NhanXet != "");

            int i = 1;
            foreach (var item in tongHopNhanXet)
            {
                if (tenChucVu.Contains("PCT"))
                {
                    string thuTuText = "";
                    if (i == 1)
                    {
                        thuTuText = "thứ nhất: ";
                    }
                    else if (i == 2)
                    {
                        thuTuText = "thứ hai: ";
                    }
                    else if (i == 3)
                    {
                        thuTuText = "thứ ba: ";
                    }
                    else if (i == 4)
                    {
                        thuTuText = "thứ tư: ";
                    }
                    else if (i == 5)
                    {
                        thuTuText = "thứ năm: ";
                    }


                    if (string.IsNullOrWhiteSpace(nx))
                    {
                        nx += "Phó chủ tịch " + thuTuText + item.NhanXet;
                    }
                    else
                    {
                        nx += "\n" + "Phó chủ tịch " + thuTuText + item.NhanXet;
                    }
                    i++;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(nx))
                    {
                        nx += item.NhanXet;
                    }
                    else
                    {
                        nx += "\n" + item.NhanXet;
                    }
                }
            }

            float? tongDiem = 0;

            foreach (var item in groupKetQua)
            {
                var obj = new KetQuaDanhGiaChiTiet();
                obj.PhieuDanhGiaId = item.PhieuDanhGiaId;
                obj.PhieuDanhGiaCanBoId = lsTruocDo.PhieuDanhGiaCanBoId;
                obj.TieuChiId = item.TieuChiId;

                var diemTB = (decimal)item.Diem / totalItem;
                if (item.Diem > 0)
                {
                    obj.Diem = (float)Math.Round(diemTB, 1);
                }
                else
                {
                    obj.Diem = 0;
                }
                tongDiem += (float)diemTB;

                obj.NhanXet = "";
                obj.LichSuKetQuaDanhGiaChiTietId = lichSuKetQuaDanhGiaId;
                obj.TenantId = lsTruocDo.TenantId;
                _ketQuaDanhGiaChiTietRepos.Insert(obj);
            }

            var obj2 = new KetQuaDanhGiaChiTiet();
            obj2.PhieuDanhGiaId = 0;
            obj2.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
            obj2.TieuChiId = -1;
            obj2.Diem = 0;
            obj2.NhanXet = nx;
            obj2.LichSuKetQuaDanhGiaChiTietId = lichSuKetQuaDanhGiaId;
            obj2.TenantId = lsTruocDo.TenantId;
            _ketQuaDanhGiaChiTietRepos.Insert(obj2);


            //tinh lai thang diem
            var tddg = (from pdg in _phieuDanhGiaRepos.GetAll().Where(x => x.Id == pdgcb.PhieuDanhGiaId)
                        from td in _thangDiemRepos.GetAll().Where(x => x.Id == pdg.ThangDiemId)
                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == td.Id)
                        where (tongDiem >= tdct.DiemSan && tongDiem <= tdct.DiemTran)
                        select new
                        {
                            Id = tdct.Id
                        }).FirstOrDefault();

            if (tddg != null)
            {
                lskq.Id = lichSuKetQuaDanhGiaId;
                lskq.ThangDiemChiTietId = tddg.Id;
                _lichSuKetQuaDanhGiaRepos.Update(lskq);
            }

            return lichSuKetQuaDanhGiaId;
        }
        public long CapNhatLichSuVaKetQuaPhieuDanhGia(long phieuDanhGiaCanBoId, long lsHienTaiId,
            long lsKetQuaTongHopId, List<TieuChiItem> LstTieuChi)
        {
            long lichSuKetQuaDanhGiaId = 0;

            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
            var lsTongHop = _lichSuKetQuaDanhGiaRepos.Get(lsKetQuaTongHopId);

            //var lsTruocDo = _lichSuKetQuaDanhGiaRepos.Get(lichSuKetQuaDanhGiaChiTietId);

            var dsLichSuKetQuaTongHop = _ketQuaDanhGiaChiTiet.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == lsKetQuaTongHopId);

            // tao ket qua chi tiet
            var dsLichSuNhom = _lichSuKetQuaDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
           && x.CapDoDanhGia == lsTongHop.CapDoDanhGia && x.KetQuaTongHop == false && x.Id != lsHienTaiId).Select(x => new
           {
               Id = x.Id,
               CanBoDanhGiaId = x.CanBoDanhGiaId,
               KetQuaTongHop = x.KetQuaTongHop,
           });

            var kqChiTiet = new List<TieuChiItem>();
            kqChiTiet = (from ls in dsLichSuNhom
                         from kq in _ketQuaDanhGiaChiTiet.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                         where (ls.KetQuaTongHop == false)
                         select new TieuChiItem
                         {
                             TieuChiId = kq.TieuChiId,
                             Diem = kq.Diem,
                             NhanXet = kq.NhanXet,
                             PhieuDanhGiaId = kq.PhieuDanhGiaId,
                             PhieuDanhGiaCanBoId = kq.PhieuDanhGiaCanBoId,
                             CanBoDanhGiaId = ls.CanBoDanhGiaId
                         }).ToList();

            var thongTinCT = kqChiTiet.FirstOrDefault();

            if (thongTinCT != null)
            {
                foreach (var item in LstTieuChi)
                {
                    item.PhieuDanhGiaId = thongTinCT.PhieuDanhGiaId;
                    item.PhieuDanhGiaCanBoId = thongTinCT.PhieuDanhGiaCanBoId;
                    item.CanBoDanhGiaId = thongTinCT.CanBoDanhGiaId;
                }
                kqChiTiet = kqChiTiet.Concat(LstTieuChi).ToList();
            }
            else
            {
                kqChiTiet = LstTieuChi;
            }

            var groupKetQua = kqChiTiet
                .GroupBy(x => new { x.TieuChiId, x.PhieuDanhGiaId, x.PhieuDanhGiaCanBoId })
                .Select(g => new
                {
                    PhieuDanhGiaId = g.Key.PhieuDanhGiaId,
                    PhieuDanhGiaCanBoId = g.Key.PhieuDanhGiaCanBoId,
                    TieuChiId = g.Key.TieuChiId,
                    Diem = g.Sum(x => x.Diem)
                }).ToList();

            var getChucVuTongHop = _chucVuRepos.Get(lsTongHop.ChucVuCanBoDanhGiaId);

            var nx = "";
            var tongHopNhanXet = kqChiTiet.Where(x => x.TieuChiId == -1 && x.NhanXet != "");

            int i = 1;
            foreach (var item in tongHopNhanXet)
            {
                if (getChucVuTongHop.Ten.Contains("PCT"))
                {
                    string thuTuText = "";
                    if (i == 1)
                    {
                        thuTuText = "thứ nhất: ";
                    }
                    else if (i == 2)
                    {
                        thuTuText = "thứ hai: ";
                    }
                    else if (i == 3)
                    {
                        thuTuText = "thứ ba: ";
                    }
                    else if (i == 4)
                    {
                        thuTuText = "thứ tư: ";
                    }
                    else if (i == 5)
                    {
                        thuTuText = "thứ năm: ";
                    }


                    if (string.IsNullOrWhiteSpace(nx))
                    {
                        nx += "Phó chủ tịch " + thuTuText + item.NhanXet;
                    }
                    else
                    {
                        nx += "\n" + "Phó chủ tịch " + thuTuText + item.NhanXet;
                    }
                    i++;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(nx))
                    {
                        nx += item.NhanXet;
                    }
                    else
                    {
                        nx += "\n" + item.NhanXet;
                    }
                }
            }

            float? tongDiem = 0;
            int totalItem = dsLichSuNhom.Count() + 1;
            foreach (var item in groupKetQua)
            {
                var checkObj = dsLichSuKetQuaTongHop.Where(x => x.TieuChiId == item.TieuChiId).FirstOrDefault();
                if (checkObj == null || item.TieuChiId == 0)
                {
                    continue;
                }
                checkObj.TieuChiId = item.TieuChiId;

                var diemTB = (decimal)item.Diem / totalItem;
                if (item.Diem > 0)
                {
                    checkObj.Diem = (float)Math.Round(diemTB, 1);
                }
                else
                {
                    checkObj.Diem = 0;
                }
                tongDiem += (float)diemTB;
                //
                checkObj.NhanXet = "";

                if (checkObj.TieuChiId == -1)
                {
                    checkObj.NhanXet = nx;
                }
                _ketQuaDanhGiaChiTietRepos.Update(checkObj);
            }

            //tinh lai thang diem
            var tddg = (from pdg in _phieuDanhGiaRepos.GetAll().Where(x => x.Id == pdgcb.PhieuDanhGiaId)
                        from td in _thangDiemRepos.GetAll().Where(x => x.Id == pdg.ThangDiemId)
                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == td.Id)
                        where (tongDiem >= tdct.DiemSan && tongDiem <= tdct.DiemTran)
                        select new
                        {
                            Id = tdct.Id
                        }).FirstOrDefault();

            if (tddg != null)
            {
                lsTongHop.ThangDiemChiTietId = tddg.Id;
                _lichSuKetQuaDanhGiaRepos.Update(lsTongHop);
            }

            return lichSuKetQuaDanhGiaId;
        }


        public ChucVuDto GetNhomChucVu(long canBoId, long chucVuCanBoId, int thuTu)
        {
            var chucVu = new ChucVuDto();
            var isChucVuChinh = _canBoChucVuRepos.GetAll().Any(x => x.CanBoId == canBoId
            && x.ChucVuChinh == (int)AppConsts.LoaiChucVu.ChucVuChinh);
            if (isChucVuChinh)
            {
                var getChucVuNhomDanhGiaId = (from cbcvdg in _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == canBoId)
                                              from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcvdg.ChucVuDanhGiaId)
                                              where cbcvdg.ThuTu == thuTu
                                              select new
                                              {
                                                  ChucVuId = cv.Id,
                                                  Ten = cv.Ten
                                              }).FirstOrDefault();

                chucVu.Ten = (getChucVuNhomDanhGiaId == null) ? "" : getChucVuNhomDanhGiaId.Ten;
                chucVu.Id = (getChucVuNhomDanhGiaId == null) ? 0 : getChucVuNhomDanhGiaId.ChucVuId;
            }
            else
            {
                var getChucVuNhomDanhGiaId = (from cbcvdg in _chucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDuocDanhGiaId == chucVuCanBoId)
                                              from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcvdg.ChucVuThucHienDanhGiaId)
                                              where cbcvdg.SoThuTu == thuTu
                                              select new
                                              {
                                                  ChucVuId = cv.Id,
                                                  Ten = cv.Ten
                                              }).FirstOrDefault();

                chucVu.Ten = (getChucVuNhomDanhGiaId == null) ? "" : getChucVuNhomDanhGiaId.Ten;
                chucVu.Id = (getChucVuNhomDanhGiaId == null) ? 0 : getChucVuNhomDanhGiaId.ChucVuId;
            }

            return chucVu;
        }
        #endregion

        #region HongAnh
        public List<ChiTietLuongDanhGiaDto> GetChiTietLuongDanhGia(long phieuDanhGiaId, long canBoId, long chucVuCanBoId )
        {
            var chiTiet = new List<ChiTietLuongDanhGiaDto>();
            // get list
            var main = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == canBoId && x.ChucVuCanBoId == chucVuCanBoId);

            var caNhan = (from m in main
                          from cv in _chucVuRepos.GetAll().Where(x => x.Id == m.ChucVuDanhGiaId && x.LoaiChucVu == 1)
                          from cb in _canBoRepos.GetAll().Where(x => x.Id == m.CanBoDanhGiaId)
                          from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                          select new CanBoDanhGia
                          {
                              ChucVuDanhGiaId = m.ChucVuDanhGiaId,
                              CanBoDanhGiaId = m.CanBoDanhGiaId,
                              ChucVuDanhGia = cv.Ten,
                              CanBoDanhgia = user.Surname + " " + user.Name,
                              ThuTu = m.ThuTu,
                              NguoiCham= m.NguoiCham
                          });

            var nhom = (from m in main
                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == m.ChucVuDanhGiaId && x.LoaiChucVu == 2)
                          from tvncv in _thanhVienNhomChucVuRepos.GetAll().Where(x=>x.ChucVuId == cv.Id)
                        select new CanBoDanhGia
                          {
                              ChucVuDanhGiaId = tvncv.ChucVuCanBoId,
                              CanBoDanhGiaId = tvncv.CanBoId,
                              ThuTu = m.ThuTu,
                              NguoiCham = m.NguoiCham
                          });

            var nhom2=( from n in nhom
                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == n.ChucVuDanhGiaId)
                        from cb in _canBoRepos.GetAll().Where(x => x.Id == n.CanBoDanhGiaId)
                        from user in _userManager.Users.Where(x => x.Id == cb.UserId)
                        select new CanBoDanhGia
                        {
                            ChucVuDanhGiaId = n.ChucVuDanhGiaId,
                            CanBoDanhGiaId = n.CanBoDanhGiaId,
                            ChucVuDanhGia = cv.Ten,
                            CanBoDanhgia = user.Surname + " " + user.Name,
                            ThuTu = n.ThuTu,
                            NguoiCham = n.NguoiCham
                        });

            var lstCanBoDanhGia = caNhan.Union(nhom2).OrderBy(x => x.ThuTu);

            var lichsu = (from lst in lstCanBoDanhGia
                          from lskqdgct in _lichSuKetQuaDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaId && x.CanBoDanhGiaId == lst.CanBoDanhGiaId && x.ChucVuCanBoDanhGiaId == lst.ChucVuDanhGiaId).DefaultIfEmpty()
                          from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == lskqdgct.ThangDiemChiTietId).DefaultIfEmpty()
                          select new
                          {
                              phieuDanhGiaId = (lskqdgct != null) ? lskqdgct.PhieuDanhGiaCanBoId : 0,
                              lichSuKetQuaDanhGiaChiTietID = (lskqdgct != null) ? lskqdgct.Id : 0,
                              TrangThai = (lskqdgct != null) ? lskqdgct.TrangThai : 0,
                              ChucVuDanhGiaId = lst.ChucVuDanhGiaId,
                              CanBoDanhGiaId = lst.CanBoDanhGiaId,
                              ChucVuDanhGia = lst.ChucVuDanhGia,
                              CanBoDanhgia = lst.CanBoDanhgia,
                              ThuTu = lst.ThuTu,
                              NguoiCham = lst.NguoiCham,
                              XepLoai = (tdct != null) ? tdct.Ten : "",
                          }).ToList();
           
            if (lichsu.Count > 0)
            {
                foreach (var item in lichsu)
                {
                    var ct = new ChiTietLuongDanhGiaDto();
                    ct.PhieuDanhGiaID = item.phieuDanhGiaId;
                    ct.ChucVuDanhGiaId = item.ChucVuDanhGiaId;
                    ct.CanBoDanhGiaId = item.ChucVuDanhGiaId;
                    ct.TenChucVu = item.ChucVuDanhGia;
                    ct.TenCanBo = item.CanBoDanhgia;
                    ct.TrangThai = item.TrangThai;
                    ct.XepLoai = item.XepLoai;
                    if (item.NguoiCham == true && item.lichSuKetQuaDanhGiaChiTietID != 0)
                    {
                        ct.TongDiem = _ketQuaDanhGiaChiTietRepos.GetAll()
                       .Where(x => x.TieuChiId != -1 && x.LichSuKetQuaDanhGiaChiTietId == item.lichSuKetQuaDanhGiaChiTietID)
                       .ToList()
                       .Sum(x => x.Diem);
                    }
                    else
                    {
                        ct.TongDiem = null;
                    }

                    chiTiet.Add(ct);
                }

            }

            return chiTiet;
        }
        #endregion
    }
}
