﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Notifications;
using Abp.Runtime.Caching;
using Microsoft.Extensions.Configuration;
using newPSG.PMS.Authorization;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Configuration;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EntityDB;
using newPSG.PMS.Notifications;
using newPSG.PMS.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Documents.Flow.Model.Fields;

namespace newPSG.PMS.EleisureApplication
{
    //[AbpAuthorize]
    public partial class PhieuDanhGiaCanBoAppService : PMSAppServiceBase, IPhieuDanhGiaCanBo
    {
        private readonly IAppNotifier _appNotifier;
        private readonly IConfigurationRoot _appConfiguration;
        private readonly ICacheManager _cacheManager;
        private readonly IRepository<CanBoChucVu, long> _canBoChucVuRepos;
        private readonly IRepository<PhieuDanhGiaChucVu, long> _phieuDanhGiaChucVuRepos;
        private readonly IRepository<PhieuDanhGiaCanBo, long> _phieuDanhGiaCanBoRepos;
        private readonly IRepository<PhieuDanhGia, long> _phieuDanhGiaRepos;
        private readonly IRepository<TieuChiDanhGia, long> _tieuChiDanhGiaRepos;
        private readonly IRepository<ThangDiem, long> _thangDiemRepos;
        private readonly IRepository<ThangDiemChiTiet, long> _thangDiemChiTietRepos;
        private readonly IRepository<LichSuKetQuaDanhGiaChiTiet, long> _lichSuKetQuaDanhGiaChiTietRepos;
        private readonly IRepository<KetQuaDanhGiaChiTiet, long> _ketQuaDanhGiaChiTietRepos;
        private readonly IRepository<DonViChuyenTrach, long> _donViChuyenTrachRepos;
        private readonly IRepository<CanBoChucVuDanhGia, long> _canBoChucVuDanhGiaRepos;
        private readonly IRepository<ThanhVienNhomChucVu, long> _thanhVienNhomChucVuRepos;
        private readonly IRepository<CanBo, long> _canBoRepos;
        private readonly IRepository<DonVi, long> _donViRepos;
        private readonly IRepository<ChucVu, long> _chucVuRepos;
        private readonly IRepository<ChucVuDanhGia, long> _chucVuDanhGiaRepos;
        private readonly IRepository<ThongTinDanhGiaKeTiepTemp, long> _thongTinDanhGiaKeTiepTempRepos;
        private readonly IRepository<Huyen, string> _huyenRepos;
        private readonly ICache _mainCache;
        private readonly UserManager _userManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ITempFileCacheManager _tempFileCacheManager;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IKetQuaDanhGiaChiTiet _iKetQuaDanhGiaChiTiet;
        private readonly IRepository<Setting,long> _settingRepos;
        public PhieuDanhGiaCanBoAppService(ICacheManager cacheManager,
            IRepository<CanBoChucVu, long> canBoChucVuRepos,
            IRepository<PhieuDanhGiaCanBo, long> phieuDanhGiaCanBoRepos,
            IRepository<PhieuDanhGia, long> phieuDanhGiaRepos,
            IRepository<TieuChiDanhGia, long> tieuChiDanhGiaRepos,
            IRepository<ThangDiem, long> thangDiemRepos,
            IRepository<ThangDiemChiTiet, long> thangDiemChiTietRepos,
            IRepository<LichSuKetQuaDanhGiaChiTiet, long> lichSuKetQuaDanhGiaChiTietRepos,
            IRepository<KetQuaDanhGiaChiTiet, long> ketQuaDanhGiaChiTietRepos,
             IRepository<PhieuDanhGiaChucVu, long> phieuDanhGiaChucVuRepos,
             IRepository<DonViChuyenTrach, long> donViChuyenTrachRepos,
             IRepository<CanBoChucVuDanhGia, long> canBoChucVuDanhGiaRepos,
             IRepository<ThanhVienNhomChucVu, long> thanhVienNhomChucVuRepos,
             IRepository<ChucVu, long> chucVuRepos,
             IRepository<CanBo, long> canBoRepos,
             UserManager userManager,
             IRepository<DonVi, long> donViRepos,
             IUnitOfWorkManager unitOfWorkManager,
             IRepository<ChucVuDanhGia, long> chucVuDanhGiaRepos,
             IRepository<ThongTinDanhGiaKeTiepTemp, long> thongTinDanhGiaKeTiepTempRepos,
             IRepository<Huyen, string> huyenRepos,
             ITempFileCacheManager tempFileCacheManager,
              IUnitOfWork unitOfWork,
              IKetQuaDanhGiaChiTiet iKetQuaDanhGiaChiTiet,
              IRepository<Setting, long> settingRepos,
              IAppNotifier appNotifier)
        {
            _cacheManager = cacheManager;
            _canBoChucVuRepos = canBoChucVuRepos;
            _phieuDanhGiaCanBoRepos = phieuDanhGiaCanBoRepos;
            _phieuDanhGiaRepos = phieuDanhGiaRepos;
            _tieuChiDanhGiaRepos = tieuChiDanhGiaRepos;
            _thangDiemChiTietRepos = thangDiemChiTietRepos;
            _thangDiemRepos = thangDiemRepos;
            _lichSuKetQuaDanhGiaChiTietRepos = lichSuKetQuaDanhGiaChiTietRepos;
            _ketQuaDanhGiaChiTietRepos = ketQuaDanhGiaChiTietRepos;
            _phieuDanhGiaChucVuRepos = phieuDanhGiaChucVuRepos;
            _donViChuyenTrachRepos = donViChuyenTrachRepos;
            _canBoRepos = canBoRepos;
            _canBoChucVuDanhGiaRepos = canBoChucVuDanhGiaRepos;
            _userManager = userManager;
            _donViRepos = donViRepos;
            _chucVuRepos = chucVuRepos;
            _unitOfWorkManager = unitOfWorkManager;
            _chucVuDanhGiaRepos = chucVuDanhGiaRepos;
            _thongTinDanhGiaKeTiepTempRepos = thongTinDanhGiaKeTiepTempRepos;
            _mainCache = _cacheManager.GetCache("PhieuDanhGiaCanBoAppService");
            _appConfiguration = AppConfigurations.Get(Directory.GetCurrentDirectory());
            _tempFileCacheManager = tempFileCacheManager;
            _huyenRepos = huyenRepos;
            _unitOfWork = unitOfWork;
            _thanhVienNhomChucVuRepos = thanhVienNhomChucVuRepos;
            _iKetQuaDanhGiaChiTiet = iKetQuaDanhGiaChiTiet;
            _appNotifier = appNotifier;
            _settingRepos = settingRepos;
        }

        public ErroMsg KiemTraThongTinPhieuTuUrl(long phieuDanhGiaCanBoId, long chucVuDanhGiaId)
        {
            ErroMsg rst = new ErroMsg();
            rst.Result = false;

            // kiểm tra cán bộ đang đăng nhập có chức vụ đc truyền vào hay ko?
            var canbo = _canBoRepos.GetAll().FirstOrDefault(x => x.UserId == AbpSession.UserId.Value);
            if (canbo == null)
            {
                rst.Result = false;
                rst.Msg = "Bạn không có quyền xem phiếu này!";
                return rst;
            }

            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
            if (pdgcb.Id == phieuDanhGiaCanBoId && pdgcb.CanBoId == canbo.Id)
            {
                rst.Result = false;
                rst.Msg = "";
                return rst;
            }


            var ktChucVu = _canBoChucVuRepos.GetAll().Any(x => x.CanBoId == canbo.Id && x.ChucVuId == chucVuDanhGiaId);
            if (!ktChucVu)
            {
                rst.Result = false;
                rst.Msg = "Bạn không có quyền xem phiếu này!";
                return rst;
            }

            // kiem tra chuc vu nay co dc xem phieu nay hay khong
            var isCheckView = _thongTinDanhGiaKeTiepTempRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.ChucVuId == chucVuDanhGiaId);

            if (isCheckView)
            {

                var date = new DateTime(pdgcb.Nam, pdgcb.Thang, 1);
                var isCheckHetHan = KiemTraThoiGianKetThucDanhGia(date);

                var lstThongTin = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId)
                    .OrderBy(x => x.Id).ToList();
                if (lstThongTin.Count > 0)
                {
                    int index = lstThongTin.FindIndex(a => a.ChucVuId == chucVuDanhGiaId);

                    var thongTinDanhGiaKeTiep = new ThongTinDanhGiaKeTiepTemp();
                    if (lstThongTin.Count > index + 1)
                    {
                        thongTinDanhGiaKeTiep = lstThongTin[index + 1];
                        if (pdgcb.DaHoanThanh == (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.ChuaHoanThanh
                        && isCheckHetHan == false && thongTinDanhGiaKeTiep.TrangThai == 0)
                        {
                            rst.Result = true;
                        }
                        else
                        {
                            rst.Result = false;
                        }
                    }

                    if (thongTinDanhGiaKeTiep.Id == 0)
                    {
                        if (isCheckHetHan == false)
                        {
                            rst.Result = true;
                            var checkThongTinNguoiChamDuyet = lstThongTin.Where(x => x.ChucVuId == chucVuDanhGiaId).FirstOrDefault();
                            if (checkThongTinNguoiChamDuyet != null)
                            {
                                rst.IsNguoiCham = checkThongTinNguoiChamDuyet.NguoiCham;
                            }
                        }
                        else
                        {
                            rst.Result = false;
                        }
                    }
                }
                else
                {
                    rst.Result = false;
                    rst.Msg = "Bạn không có quyền xem phiếu này!";
                }
            }
            else
            {
                rst.Result = false;
                rst.Msg = "Bạn không có quyền xem phiếu này!";
            }
            return rst;
        }

        public List<ThangDiemChiTietDto> getThangDiemTheoPhieuDanhGiaId(long thangDiemId)
        {
            var lstData = new List<ThangDiemChiTietDto>();
            var currDate = new DateTime().Date;
            lstData = (from td in _thangDiemRepos.GetAll()
                       from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.ThangDiemId == td.Id)
                       where td.Id == thangDiemId
                        && (td.HieuLucTuNgay == null || td.HieuLucTuNgay < currDate)
                         && (td.HieuLucToiNgay == null || td.HieuLucToiNgay > currDate)
                       select new ThangDiemChiTietDto
                       {
                           Id = tdct.Id,
                           Ten = tdct.Ten,
                           DiemSan = tdct.DiemSan,
                           DiemTran = tdct.DiemTran,
                       }).ToList();
            return lstData;
        }

        public List<PhieuDanhGiaChiTietDto> TinhTongDiemCapCha(List<PhieuDanhGiaChiTietDto> arrs, long? parentId = null)
        {
            var arrByParentIds = arrs.Where(item => item.TieuChiChaId == parentId).ToList();

            arrByParentIds.ForEach(item =>
            {
                var arr2s = arrs.Where(item2 => item2.TieuChiChaId == item.Id).ToList();
                item.IsCapCha = true;
                item.TongDiemCaNhanTuCham = item.DiemTuCham; // tu cham
                item.TongDiemLanhDaoCham = item.DiemLanhDaoCham; // ng dang danh gia

                item.LichSuTongDiemCapDanhGia1 = item.LichSuDiemCapDanhGia1; // lich su cham 1
                item.LichSuTongDiemCapDanhGia2 = item.LichSuDiemCapDanhGia2; // lich su cham 2
                item.LichSuTongDiemCapDanhGia3 = item.LichSuDiemCapDanhGia3; // lich su cham 3
                item.LichSuTongDiemCapDanhGia4 = item.LichSuDiemCapDanhGia4; // lich su cham 4

                if (arr2s.Count == 0)
                {
                    item.IsCapCha = false;
                    return;
                }

                TinhTongDiemCapCha(arrs, item.Id);
                //item.Value2 = arr2s.Sum(m => m.Value2);
                item.TongDiemCaNhanTuCham = arr2s.Sum(m => m.TongDiemCaNhanTuCham);
                item.TongDiemLanhDaoCham = arr2s.Sum(m => m.TongDiemLanhDaoCham);
                item.LichSuTongDiemCapDanhGia1 = arr2s.Sum(m => m.LichSuTongDiemCapDanhGia1);
                item.LichSuTongDiemCapDanhGia2 = arr2s.Sum(m => m.LichSuTongDiemCapDanhGia2);
                item.LichSuTongDiemCapDanhGia3 = arr2s.Sum(m => m.LichSuTongDiemCapDanhGia3);
                item.LichSuTongDiemCapDanhGia4 = arr2s.Sum(m => m.LichSuTongDiemCapDanhGia4);
            });

            return arrs;
        }

        public PhieuDanhGiaDto getPhieuDanhGiaIdByChucVuIdOrPhieuDanhGiaCanBoId(long? canBoId, long? chucVuId, long? phieuDanhGiaCanBoId)
        {
            var chiTietPdg = new PhieuDanhGiaDto();
            long phieuDanhGiaId = 0;

            if (phieuDanhGiaCanBoId != null && phieuDanhGiaCanBoId != 0)
            {
                var getPhieuDanhGia = _phieuDanhGiaCanBoRepos.GetAll().FirstOrDefault(x => x.Id == phieuDanhGiaCanBoId);
                if (getPhieuDanhGia != null)
                {
                    phieuDanhGiaId = getPhieuDanhGia.PhieuDanhGiaId;
                }
            }
            else
            {
                var checkChucVuCanBo = _canBoChucVuRepos.GetAll().Any(x => x.CanBoId == canBoId
             && x.ChucVuId == chucVuId);
                var currDate = new DateTime().Date;

                var getPhieuDanhGia = (from pdgcv in _phieuDanhGiaChucVuRepos.GetAll()
                                       from pdg in _phieuDanhGiaRepos.GetAll().Where(x => x.Id == pdgcv.PhieuDanhGiaId)
                                       where pdgcv.ChucVuId == chucVuId
                                       && (pdg.HieuLucTuNgay == null || pdg.HieuLucTuNgay < currDate)
                                       && (pdg.HieuLucToiNgay == null || pdg.HieuLucToiNgay > currDate)
                                       select new
                                       {
                                           PhieuDanhGiaId = pdg.Id
                                       }).FirstOrDefault();

                if (getPhieuDanhGia != null)
                {
                    phieuDanhGiaId = getPhieuDanhGia.PhieuDanhGiaId;
                }
            }

            if (phieuDanhGiaId > 0)
            {
                chiTietPdg = _phieuDanhGiaRepos.GetAll().Where(x => x.Id == phieuDanhGiaId)
                    .Select(x => new PhieuDanhGiaDto
                    {
                        Id = x.Id,
                        Ten = x.Ten,
                        GhiChu = x.GhiChu,
                        TieuDe = x.TieuDe,
                        ThangDiemId = x.ThangDiemId
                    }).FirstOrDefault();
            }

            return chiTietPdg;
        }

        public bool KiemTraThoiGianKetThucDanhGia(DateTime dThoiGian)
        {
            var curDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            bool isHetHanDanhGia = true;

            var thoiGianKetThucDanhGia = SettingManager.GetSettingValue(AppSettings.TenantManagement.ThoiGianKetThucDanhGia);
            if (!string.IsNullOrWhiteSpace(thoiGianKetThucDanhGia))
            {
                int ngayKetThuc = 1;
                int.TryParse(thoiGianKetThucDanhGia, out ngayKetThuc);

                var inputDate = new DateTime(dThoiGian.Year, dThoiGian.Month, 1);

                int nkt = DateTime.DaysInMonth(curDate.Year, curDate.Month);
                ngayKetThuc = (nkt < ngayKetThuc) ? nkt : ngayKetThuc;

                var thangNay = new DateTime(curDate.Year, curDate.Month, ngayKetThuc);
                var thangTruoc = curDate.AddMonths(-1);

                if (inputDate >= thangTruoc && inputDate <= thangNay)
                {
                    isHetHanDanhGia = false;
                }
            }

            return isHetHanDanhGia;
        }

        [AbpAuthorize(AppPermissions.Pages_User_DanhGiaCaNhan)]
        public PagedResultDto<DanhSachPhieuDanhGiaCanBoDto> GetAllPhieuDanhGiaCanBo(PhieuDanhGiaCanBoInput input)
        {
            try
            {

                int trangThaiHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;

                var query = _phieuDanhGiaCanBoRepos.GetAll()
                             .Where(x => x.CanBoId == input.CanBoId
                             && x.Nam == input.Nam
                             && x.ChucVuId == input.ChucVuId)
                             .Select(x => new
                             {
                                 Id = x.Id,
                                 PhieuDanhGiaId = x.PhieuDanhGiaId,
                                 PhieuDanhGiaCanBoId = x.Id,
                                 Nam = x.Nam,
                                 Thang = x.Thang,
                                 LichSuKetQuaDanhGiaChiTietHienTaiId = x.LichSuKetQuaDanhGiaChiTietHienTaiId,
                                 DaHoanThanh = x.DaHoanThanh,
                                 ChucVuId = x.ChucVuId,
                                 CanBoId = x.CanBoId
                             });

                var totalCount = query.Count();
                var lstData = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

                var items = new List<DanhSachPhieuDanhGiaCanBoDto>();
                foreach (var item in lstData)
                {
                    var obj = new DanhSachPhieuDanhGiaCanBoDto();
                    obj.Id = item.PhieuDanhGiaCanBoId;
                    obj.Nam = item.Nam;
                    obj.Thang = item.Thang;
                    obj.CanBoId = item.CanBoId;

                    var date = new DateTime(item.Nam, item.Thang, 1);
                    obj.HetHanDanhGia = KiemTraThoiGianKetThucDanhGia(date);

                    var user = (from cb in _canBoRepos.GetAll().Where(x => x.Id == obj.CanBoId)
                                from u in _userManager.Users.Where(x => x.Id == cb.UserId)
                                select new
                                {
                                    tenNhanVien = u.Surname + " " + u.Name,
                                }).FirstOrDefault();
                    obj.TenNhanVien = (user == null) ? "" : user.tenNhanVien;

                    var chucVu = _chucVuRepos.GetAll().Where(x => x.Id == item.ChucVuId).FirstOrDefault();
                    obj.TenChucVu = (chucVu == null) ? "" : chucVu.Ten;

                    obj.LichSuKetQuaDanhGiaChiTietId = _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                        .Where(x => x.PhieuDanhGiaCanBoId == item.Id && x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia).FirstOrDefault().Id;

                    var ketQua = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                  from kq in _ketQuaDanhGiaChiTietRepos.GetAll()
                                  .Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.PhieuDanhGiaId != 0)
                                  where ls.PhieuDanhGiaCanBoId == item.Id
                                  select new
                                  {
                                      LichSuId = ls.Id,
                                      TrangThai = ls.TrangThai,
                                      CapDanhGia = ls.CapDanhGia,
                                      Diem = (kq.Diem != null) ? kq.Diem.Value : 0,
                                      ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                      KetQuaTongHop = ls.KetQuaTongHop
                                  });


                    int trangThai = 3;
                    float tongDiem = 0;
                    #region tinh diem ca nhan tu danh gia
                    var tongDiemTuDanhGia = ketQua.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia);
                    tongDiem = (float)Math.Round(tongDiemTuDanhGia.ToList().Sum(x => x.Diem), 1);

                    var thangDiemChiTiet = tongDiemTuDanhGia.FirstOrDefault().ThangDiemChiTietId;
                    obj.XepLoaiCaNhan = _thangDiemChiTietRepos.GetAll().Where(x => x.Id == thangDiemChiTiet).FirstOrDefault().Ten;

                    trangThai = tongDiemTuDanhGia.FirstOrDefault().TrangThai;


                    var capTrenDanhGia = ketQua.Any(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren);
                    if (capTrenDanhGia)
                    {
                        obj.IsSuaKetQua = false;
                    }
                    else
                    {
                        obj.IsSuaKetQua = true;
                    }

                    #endregion

                    var diemToiDa = _tieuChiDanhGiaRepos.GetAll()
                        .Where(x => x.PhieuDanhGiaId == item.PhieuDanhGiaId && x.TieuChiChaId == null).ToList().Sum(x => x.DiemToiDa);
                    obj.TongDiemCaNhan = tongDiem.ToString() + "/" + diemToiDa.ToString();

                    var lsCuoiCung = _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.Id == item.LichSuKetQuaDanhGiaChiTietHienTaiId).FirstOrDefault();
                    if (lsCuoiCung != null)
                    {
                        if (lsCuoiCung.IsKetQuaCuoiCung)
                        {
                            obj.XepLoaiCuoiCung = _thangDiemChiTietRepos.GetAll().Where(x => x.Id == lsCuoiCung.ThangDiemChiTietId).FirstOrDefault().Ten;
                        }
                    }

                    var isLoaiHinhChucVu = _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == item.Id
                    && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu);
                    if (isLoaiHinhChucVu)
                    {
                        obj.LoaiHienThiChiTiet = 2;
                    }
                    else
                    {
                        obj.LoaiHienThiChiTiet = 1;
                    }

                    obj.TrangThai = (item.DaHoanThanh == trangThaiHoanThanh) ? item.DaHoanThanh : trangThai;
                    items.Add(obj);
                }

                return new PagedResultDto<DanhSachPhieuDanhGiaCanBoDto>(totalCount, items);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [AbpAuthorize(AppPermissions.Pages_User_DanhGiaNhanVien)]
        public PagedResultDto<DanhSachPhieuDanhGiaCanBoDto> GetAllPhieuDanhGiaNhanVienTheoChucVuLanhDao(PhieuDanhGiaCanBoInput input)
        {
            try
            {
                var curDate = DateTime.Now;

                bool isHetHanDanhGia = KiemTraThoiGianKetThucDanhGia(input.ThoiGian.Value);

                //int trangThaiHoanThanh =(int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;
                // get các phiếu đang chờ duyệt
                var getPhieuDangCho = _thongTinDanhGiaKeTiepTempRepos
                    .GetAll()
                    .Where(x => x.CanBoDanhGiaKeTiepId == input.CanBoId && x.ChucVuId == input.ChucVuId
                     && (input.TrangThaiPhieu == -1 || x.TrangThai == input.TrangThaiPhieu));

                var query = (from temp in getPhieuDangCho
                             from pdgcb in _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Id == temp.PhieuDanhGiaCanBoId)
                             from cb in _canBoRepos.GetAll().Where(x => x.Id == pdgcb.CanBoId)
                             where pdgcb.Nam == input.ThoiGian.Value.Year
                                   && pdgcb.Thang == input.ThoiGian.Value.Month
                             select new
                             {
                                 PhieuDanhGiaCanBoId = pdgcb.Id,
                                 Thang = pdgcb.Thang,
                                 Nam = pdgcb.Nam,
                                 NhanVienId = pdgcb.CanBoId,
                                 LichSuKetQuaDanhGiaChiTietHienTaiId = pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId,
                                 DaHoanThanh = pdgcb.DaHoanThanh,
                                 UserId = cb.UserId,
                                 ChucVuNhanVienID = pdgcb.ChucVuId,
                                 DonViCongTacId = cb.DonViId,
                                 IsNguoiCham = temp.NguoiCham,
                                 LoaiChucVuDanhGia = temp.LoaiChucVuDanhGia,
                                 ThuTu = temp.ThuTu,
                             });


                var totalCount = query.Count();
                var data = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

                var items = new List<DanhSachPhieuDanhGiaCanBoDto>();
                foreach (var item in data)
                {
                    var obj = new DanhSachPhieuDanhGiaCanBoDto();
                    obj.HetHanDanhGia = isHetHanDanhGia;
                    obj.Id = item.PhieuDanhGiaCanBoId;
                    obj.Thang = item.Thang;
                    obj.Nam = item.Nam;
                    var user = _userManager.Users.Where(x => x.Id == item.UserId).FirstOrDefault();
                    obj.TenNhanVien = user.Surname + " " + user.Name;
                    obj.IsNguoiCham = item.IsNguoiCham;
                    obj.LoaiChucVuDanhGia = item.LoaiChucVuDanhGia;  // AppConsts.LoaiHinhChucVu

                    if (item.ThuTu == 2 && item.IsNguoiCham == true)
                    {
                        obj.IsChanDiemLan2 = true;
                    }
                    else
                    {
                        obj.IsChanDiemLan2 = false;
                    }

                    obj.TenChucVu = "";
                    var chucVu = (from cbcv in _canBoChucVuRepos.GetAll()
                                  from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcv.ChucVuId)
                                  where cbcv.CanBoId == item.NhanVienId && cbcv.ChucVuId == item.ChucVuNhanVienID
                                  select new
                                  {
                                      Ten = cv.Ten
                                  }).FirstOrDefault();

                    if (chucVu != null)
                    {
                        obj.TenChucVu = chucVu.Ten;
                    }
                    obj.DonViCongTac = _donViRepos.Get(item.DonViCongTacId).Ten;
                    obj.CanBoId = item.NhanVienId;

                    //

                    var checkLichSuChamDiem = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                               from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id
                                               && x.Diem != null)
                                               where ls.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                                               select new
                                               {
                                                   CapDoDanhGia = ls.CapDoDanhGia,
                                                   LichSuId = ls.Id,
                                                   CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                                   PhieuDanhGiaId = kqct.PhieuDanhGiaId,
                                                   CapDanhGia = ls.CapDanhGia,
                                                   Diem = kqct.Diem.Value,
                                                   ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                                   KetQuaCuoiCung = ls.IsKetQuaCuoiCung,
                                                   ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                                   LoaiChucVuDanhGia = ls.LoaiChucVuDanhGia,
                                                   KetQuaTongHop = ls.KetQuaTongHop
                                               });

                    int trangThaiPhieuDanhGiaCanBo = 2;
                    long phieuDanhGiaId = 0;
                    float tongDiem = 0;

                    #region tinh diem ca nhan tu danh gia

                    var tongDiemTuCham = checkLichSuChamDiem.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia
                    && x.PhieuDanhGiaId != 0).ToList();
                    tongDiem = (float)Math.Round(tongDiemTuCham.Sum(x => x.Diem), 1);

                    if (tongDiemTuCham.Count > 0)
                    {
                        long thangDiemChiTietId = tongDiemTuCham.FirstOrDefault().ThangDiemChiTietId;
                        obj.XepLoaiCaNhan = _thangDiemChiTietRepos.GetAll().Where(x => x.Id == thangDiemChiTietId).FirstOrDefault().Ten;
                    }
                    #endregion

                    var getTrangThaiPhieuCuaCanBoHienTai = checkLichSuChamDiem.FirstOrDefault(x => x.CanBoDanhGiaId == input.CanBoId
                    && x.ChucVuCanBoDanhGiaId == input.ChucVuId);
                    if (getTrangThaiPhieuCuaCanBoHienTai != null)
                    {
                        obj.LichSuKetQuaDanhGiaChiTietId = getTrangThaiPhieuCuaCanBoHienTai.LichSuId;
                        trangThaiPhieuDanhGiaCanBo = (int)AppConsts.TrangThaiPhieuCaNhan.DaGui;
                    }

                    phieuDanhGiaId = checkLichSuChamDiem.FirstOrDefault().PhieuDanhGiaId;
                    var diemToiDa = _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGiaId
                    && x.TieuChiChaId == null).ToList().Sum(x => x.DiemToiDa);

                    obj.TongDiemCaNhan = tongDiem.ToString() + "/" + diemToiDa.ToString();
                    //

                    var checkKetQuaCuoiCung = checkLichSuChamDiem.Where(x => x.KetQuaCuoiCung == true).FirstOrDefault();
                    if (checkKetQuaCuoiCung != null)
                    {
                        //obj.IsSuaKetQua = false;
                        obj.XepLoaiCuoiCung = _thangDiemChiTietRepos.GetAll().Where(x => x.Id == checkKetQuaCuoiCung.ThangDiemChiTietId).FirstOrDefault().Ten;
                    }

                    #region kiem tra xem ban ghi co dc edit hay ko
                    // kiem tra ls can bo dang dang nhap
                    var getLsCanBo = checkLichSuChamDiem.Where(x => x.CanBoDanhGiaId == input.CanBoId
                    && x.ChucVuCanBoDanhGiaId == input.ChucVuId).FirstOrDefault();
                    if (getLsCanBo != null)
                    {
                        // get ls cuoi cung
                        var lsMax = checkLichSuChamDiem.OrderByDescending(x => x.LichSuId).FirstOrDefault().CapDoDanhGia;
                        if (getLsCanBo.CapDoDanhGia < lsMax)
                        {
                            obj.IsSuaKetQua = false;
                        }
                        else
                        {
                            obj.IsSuaKetQua = true;
                        }
                    }
                    else
                    {
                        obj.IsSuaKetQua = true;
                    }
                    #endregion

                    obj.TrangThai = (item.DaHoanThanh == 1) ? item.DaHoanThanh : trangThaiPhieuDanhGiaCanBo;

                    var isLoaiHinhChucVu = _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                    && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu);
                    if (isLoaiHinhChucVu)
                    {
                        obj.LoaiHienThiChiTiet = 2;
                    }
                    else
                    {
                        obj.LoaiHienThiChiTiet = 1;
                    }

                    items.Add(obj);
                }
                return new PagedResultDto<DanhSachPhieuDanhGiaCanBoDto>(totalCount, items);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public PagedResultDto<DanhSachCanBoChuaGuiDanhGiaDto> GetAllNhanVienChuaDanhGia(PhieuDanhGiaCanBoInput input)
        {
            try
            {
                var thoiGianDanhGia = input.ThoiGian.Value.AddMonths(-1);
                int month = thoiGianDanhGia.Month;
                int year = thoiGianDanhGia.Year;

                var lstDonViChuyenTrach = _donViChuyenTrachRepos.GetAll().Where(x => x.CanBoId == input.CanBoId
                     && x.ChucVuId == input.ChucVuId).Select(x => x.DonViId);


                var lstCanBoCanDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDanhGiaId == input.ChucVuId
                && (x.CanBoDanhGiaId == null || x.CanBoDanhGiaId == input.CanBoId))
                        .Select(x => new
                        {
                            CanBoId = x.CanBoId,
                            ChucVuCanBoId = x.ChucVuCanBoId,
                        });

                var lstTatCaCanBoCanDanhGia = (from cb in _canBoRepos.GetAll().Where(x => lstDonViChuyenTrach.Any(d => d == x.DonViId))
                                               from cbcv in lstCanBoCanDanhGia.Where(x => x.CanBoId == cb.Id)
                                               select new
                                               {
                                                   CanBoId = cb.Id,
                                                   ChucVuCanBoId = cbcv.ChucVuCanBoId,
                                                   UserId = cb.UserId
                                               });

                #region kiem tra tron ds thanh vien nhom chức vụ
                var lstNhomChucVuDanhGia = _thanhVienNhomChucVuRepos.GetAll().Where(x => x.CanBoId == input.CanBoId
                   && x.ChucVuCanBoId == input.ChucVuId).Select(x => new
                   {
                       ChucVuId = x.ChucVuId,
                   });

                if (lstNhomChucVuDanhGia.Count() > 0)
                {
                    var lstCanBoCanDanhGia2 = _canBoChucVuDanhGiaRepos.GetAll().Where(x =>
                    lstNhomChucVuDanhGia.Any(d => d.ChucVuId == x.ChucVuDanhGiaId) && (x.CanBoDanhGiaId == null || x.CanBoDanhGiaId == input.CanBoId))
                           .Select(x => new
                           {
                               CanBoId = x.CanBoId,
                               ChucVuCanBoId = x.ChucVuCanBoId,
                           });

                    var lstTatCaCanBoCanDanhGia2 = (from cb in _canBoRepos.GetAll()
                                                    from cbcv in lstCanBoCanDanhGia2.Where(x => x.CanBoId == cb.Id)
                                                    select new
                                                    {
                                                        CanBoId = cb.Id,
                                                        ChucVuCanBoId = cbcv.ChucVuCanBoId,
                                                        UserId = cb.UserId
                                                    });

                    lstTatCaCanBoCanDanhGia = lstTatCaCanBoCanDanhGia.Concat(lstTatCaCanBoCanDanhGia2);
                }
                #endregion



                var dsPdg = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Thang == month && x.Nam == year);

                var query = (from tccbdg in lstTatCaCanBoCanDanhGia
                             from u in UserManager.Users.Where(x => x.Id == tccbdg.UserId)
                             from cv in _chucVuRepos.GetAll().Where(x => x.Id == tccbdg.ChucVuCanBoId)
                             where !dsPdg.Any(d => d.CanBoId == tccbdg.CanBoId && d.ChucVuId == tccbdg.ChucVuCanBoId)
                             select new DanhSachCanBoChuaGuiDanhGiaDto
                             {
                                 CanBoId = tccbdg.CanBoId,
                                 ChucVuCanBoId = tccbdg.ChucVuCanBoId,
                                 TenChucVu = cv.Ten,
                                 TenCanBo = u.Surname + " " + u.Name
                             }).ToList();


                var totalCount = query.Count();

                query = query.Skip(input.SkipCount).Take(input.MaxResultCount).ToList();
                return new PagedResultDto<DanhSachCanBoChuaGuiDanhGiaDto>(totalCount, query);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool TaoMoiPhieuDanhGiaCanBo(PhieuDanhGiaCanBoUpdateDto input)
        {
            var tenantId = AbpSession.TenantId;
            bool result = false;
            long phieuDanhGiaCanBoId = 0;
            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                //using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                //{
                try
                {

                    long lichSuKetQuaDanhGiaChiTietId = 0;
                    bool isAddNew = true;
                    #region Tao Phieu Danh Gia Can Bo
                    var model = new PhieuDanhGiaCanBo();
                    if (input.PhieuDanhGiaCanBoId == null || input.PhieuDanhGiaCanBoId == 0)
                    {

                        model.CanBoId = input.CanBoId;
                        model.ChucVuId = input.ChucVuId;
                        model.PhieuDanhGiaId = input.PhieuDanhGiaId;
                        model.Thang = input.ThoiGianTaoPhieu.Month;
                        model.Nam = input.ThoiGianTaoPhieu.Year;
                        model.Quy = 0;
                        model.Ky = 0;
                        model.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.ChuaHoanThanh;
                        model.TenantId = tenantId;
                        phieuDanhGiaCanBoId = _phieuDanhGiaCanBoRepos.InsertAndGetId(model);
                        isAddNew = true;
                    }
                    else
                    {
                        isAddNew = false;
                        phieuDanhGiaCanBoId = input.PhieuDanhGiaCanBoId.Value;

                    }
                    #endregion

                    #region Tao Lich Su Ket Qua Danh Gia Chi Tiet

                    var lskq = new LichSuKetQuaDanhGiaChiTiet();
                    lskq.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                    lskq.LicSuPhieuDanhGiaChiTietTruocDoId = 0;
                    if (phieuDanhGiaCanBoId > 0 && isAddNew == false)
                    {
                        var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                        lskq.LicSuPhieuDanhGiaChiTietTruocDoId = pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId;
                    }
                    lskq.ThangDiemChiTietId = input.ThangDiemChiTietId;
                    lskq.CanBoId = input.CanBoId;
                    lskq.CanBoDanhGiaId = input.CanBoDanhGiaId;
                    lskq.CapDanhGia = input.CapDanhGia;
                    lskq.NgayDanhGia = DateTime.Now;
                    lskq.TrangThai = input.TrangThai;
                    lskq.ChucVuCanBoDanhGiaId = input.ChucVuId;
                    lskq.IsKetQuaCuoiCung = false;
                    lskq.TenantId = tenantId;
                    lskq.LoaiChucVuDanhGia = input.LoaiChucVuDanhGia;
                    if (input.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia)
                    {
                        lskq.KetQuaTongHop = true;
                        lskq.CapDoDanhGia = (int)AppConsts.CapDoDanhGia.Cap_1;
                    }
                    else
                    {
                        lskq.KetQuaTongHop = (input.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.ChucVu) ? true : false;

                        var layTTCanBo = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                        && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId && x.ChucVuId == input.ChucVuId).FirstOrDefault();

                        if (layTTCanBo != null)
                        {
                            lskq.CapDoDanhGia = layTTCanBo.ThuTu + 1;
                        }
                    }
                    lichSuKetQuaDanhGiaChiTietId = _lichSuKetQuaDanhGiaChiTietRepos.InsertAndGetId(lskq);

                    // update PhieuDanhGiaCanBo
                    if (!isAddNew && phieuDanhGiaCanBoId > 0)
                    {
                        var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                        pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId = lichSuKetQuaDanhGiaChiTietId;
                        _phieuDanhGiaCanBoRepos.Update(pdgcb);
                    }
                    else
                    {
                        model.LichSuKetQuaDanhGiaChiTietHienTaiId = lichSuKetQuaDanhGiaChiTietId;
                        _phieuDanhGiaCanBoRepos.Update(model);
                    }
                    #endregion

                    #region Tao Lich Su Ket Qua Danh Gia Chi Tiet
                    foreach (var item in input.LstTieuChi)
                    {
                        if (item.TieuChiId == 0)
                        {
                            continue;
                        }
                        var obj = new KetQuaDanhGiaChiTiet();
                        obj.PhieuDanhGiaId = input.PhieuDanhGiaId;
                        obj.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                        obj.TieuChiId = item.TieuChiId;
                        obj.Diem = item.Diem;
                        obj.NhanXet = item.NhanXet ?? "";
                        obj.LichSuKetQuaDanhGiaChiTietId = lichSuKetQuaDanhGiaChiTietId;
                        obj.TenantId = tenantId;
                        _ketQuaDanhGiaChiTietRepos.Insert(obj);
                    }
                    #endregion

                    #region Đóng thông tin đánh giá kế tiếp hiện tại
                    long tempId = 0;
                    if (isAddNew == false)
                    {
                        var checkTemp = _thongTinDanhGiaKeTiepTempRepos.GetAll()
                            .Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                            && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId
                            && x.ChucVuId == input.ChucVuId
                            && x.TrangThai == (int)AppConsts.TrangThaiTemp.Cho).FirstOrDefault();

                        if (checkTemp != null)
                        {
                            tempId = checkTemp.Id;
                            checkTemp.TrangThai = (int)AppConsts.TrangThaiTemp.HoanThanh;
                            _thongTinDanhGiaKeTiepTempRepos.Update(checkTemp);
                        }

                    }
                    #endregion

                    #region update ban ghi cuoi cung
                    var isKetQuaCuoiCung = false;
                    if (input.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui)
                    {

                        #region Kiem tra ng danh gia hien tai co phai trong nhom danh gia hay ko
                        // neu la nhom danh gia sau moi lan cham diem phai kiem tra cac thanh vien da cham het chua, 
                        //neu da cham het roi thi tao ban ghi tong hop diem
                        long lichSuDanhGiaId = 0;
                        if (input.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu && input.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                        {
                            var checkThongTinDanhGia = true;
                            if (tempId == 0)
                            {
                                checkThongTinDanhGia = _thongTinDanhGiaKeTiepTempRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu
                                && x.TrangThai == (int)AppConsts.TrangThaiTemp.Cho);
                            }
                            else
                            {
                                checkThongTinDanhGia = _thongTinDanhGiaKeTiepTempRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu
                                && x.TrangThai == (int)AppConsts.TrangThaiTemp.Cho
                                && x.Id != tempId);
                            }

                            if (!checkThongTinDanhGia)
                            {
                                lichSuDanhGiaId = _iKetQuaDanhGiaChiTiet.ThemMoiLichSuVaKetQuaPhieuDanhGia(phieuDanhGiaCanBoId,
                                    lichSuKetQuaDanhGiaChiTietId, (int)AppConsts.LoaiHinhChucVu.NhomChucVu);
                            }
                        }
                        else
                        {
                            lichSuDanhGiaId = lichSuKetQuaDanhGiaChiTietId;
                        }

                        long thongTinDanhGiaKeTiepId = 0;
                        var ttDanhGiaHienTai = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId
                        && x.ChucVuId == input.ChucVuId
                        && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId).FirstOrDefault();
                        if (ttDanhGiaHienTai != null)
                        {
                            thongTinDanhGiaKeTiepId = ttDanhGiaHienTai.Id;
                        }

                        isKetQuaCuoiCung = GetThongTinDanhGiaKeTiep(phieuDanhGiaCanBoId, input.CapDanhGia, thongTinDanhGiaKeTiepId);
                        if (isKetQuaCuoiCung)
                        {
                            var lscc = _lichSuKetQuaDanhGiaChiTietRepos.Get(lichSuDanhGiaId);
                            lscc.IsKetQuaCuoiCung = true;
                            _lichSuKetQuaDanhGiaChiTietRepos.Update(lscc);

                            var pdg = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                            pdg.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;
                            _phieuDanhGiaCanBoRepos.Update(pdg);
                        }
                        #endregion

                    }

                    #endregion
                    result = true;
                    unitOfWork.Complete();
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    unitOfWork.Dispose();
                }
                //}                
            }

            return result;
        }
        public bool CapNhatPhieuDanhGiaCanBoCham(PhieuDanhGiaCanBoUpdateDto input)
        {
            var tenantId = AbpSession.TenantId;
            bool result = false;

            using (var unitOfWork = _unitOfWorkManager.Begin())
            {
                try
                {
                    #region Cap nhat Lich Su
                    var lsdg = _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                        .Where(x => x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId && x.Id == input.LichSuKetQuaDanhGiaChiTietId)
                        .FirstOrDefault();

                    if (lsdg != null)
                    {
                        if (input.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui)
                        {
                            // đã gửi phiếu nhưng chưa đc chấm - duyệt
                            lsdg.TrangThai = input.TrangThai;
                        }

                        lsdg.ThangDiemChiTietId = input.ThangDiemChiTietId;
                        lsdg.NgayDanhGia = DateTime.Now;
                        _lichSuKetQuaDanhGiaChiTietRepos.Update(lsdg);
                    }
                    #endregion

                    #region Cap Nhat Lich Su Ket Qua Danh Gia Chi Tiet
                    var ketQuaPdg = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                     from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                     where ls.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId
                                     && ls.CapDanhGia == input.CapDanhGia && ls.Id == input.LichSuKetQuaDanhGiaChiTietId
                                     select new KetQuaDanhGiaChiTiet
                                     {
                                         Id = kq.Id,
                                         PhieuDanhGiaCanBoId = kq.PhieuDanhGiaCanBoId,
                                         PhieuDanhGiaId = kq.PhieuDanhGiaId,
                                         TieuChiId = kq.TieuChiId,
                                         Diem = kq.Diem,
                                         NhanXet = kq.NhanXet,
                                         LichSuKetQuaDanhGiaChiTietId = kq.LichSuKetQuaDanhGiaChiTietId,
                                         TenantId = kq.TenantId
                                     });

                    foreach (var item in input.LstTieuChi)
                    {
                        var checkObj = ketQuaPdg.Where(x => x.TieuChiId == item.TieuChiId).FirstOrDefault();
                        if (checkObj == null || item.TieuChiId == 0)
                        {
                            continue;
                        }

                        checkObj.TieuChiId = item.TieuChiId;
                        checkObj.Diem = item.Diem;
                        checkObj.NhanXet = item.NhanXet ?? "";
                        _ketQuaDanhGiaChiTietRepos.Update(checkObj);
                    }
                    #endregion

                    #region Tạo thông tin đánh giá kế tiếp
                    if (input.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui && input.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia)
                    {
                        var isKetQuaCuoiCung = GetThongTinDanhGiaKeTiep(input.PhieuDanhGiaCanBoId.Value, input.CapDanhGia, 0);
                        if (isKetQuaCuoiCung)
                        {
                            var lskq = _lichSuKetQuaDanhGiaChiTietRepos.Get(input.LichSuKetQuaDanhGiaChiTietId.Value);
                            lskq.IsKetQuaCuoiCung = true;
                            _lichSuKetQuaDanhGiaChiTietRepos.Update(lskq);

                            var pdg = _phieuDanhGiaCanBoRepos.Get(input.PhieuDanhGiaCanBoId.Value);
                            pdg.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;
                            _phieuDanhGiaCanBoRepos.Update(pdg);
                        }
                    }
                    #endregion

                    #region update phieu danh gia tong hop
                    if (lsdg.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu)
                    {
                        var ktLichSuKqTongHop = _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                       .Where(x => x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId && x.CapDoDanhGia == lsdg.CapDoDanhGia
                       && x.KetQuaTongHop == true).FirstOrDefault();

                        if (ktLichSuKqTongHop != null)
                        {
                            _iKetQuaDanhGiaChiTiet.CapNhatLichSuVaKetQuaPhieuDanhGia(input.PhieuDanhGiaCanBoId.Value,
                                input.LichSuKetQuaDanhGiaChiTietId.Value, ktLichSuKqTongHop.Id, input.LstTieuChi);
                        }
                    }
                    #endregion


                    result = true;
                    unitOfWork.Complete();
                    CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    _unitOfWork.Dispose();
                }
            }

            return result;
        }

        public bool TaoMoiHoacCapNhatPhieuDanhGiaCanBo(PhieuDanhGiaCanBoUpdateDto input)
        {
            bool result = false;
            bool isEdit = false;
            if (input.PhieuDanhGiaCanBoId != null && input.PhieuDanhGiaCanBoId > 0)
            {
                if (input.LichSuKetQuaDanhGiaChiTietId != null && input.LichSuKetQuaDanhGiaChiTietId > 0)
                {
                    result = CapNhatPhieuDanhGiaCanBoCham(input);
                }
                else
                {
                    result = TaoMoiPhieuDanhGiaCanBo(input);
                }
            }
            else
            {
                result = TaoMoiPhieuDanhGiaCanBo(input);
            }


            // Gửi thông báo sau khi tạo các bản ghi đánh giá thành công
            //if (result && input.TrangThai == (int)AppConsts.TrangThaiPhieuCaNhan.DaGui)
            //{
            //    if (input.LichSuKetQuaDanhGiaChiTietId != null && input.LichSuKetQuaDanhGiaChiTietId > 0)
            //    {
            //        isEdit = true;
            //    }
            //    else
            //    {
            //        isEdit = false;
            //    }

            //    await TaoThongBaoChoCanbo(input.PhieuDanhGiaCanBoId.Value, isEdit, input.CapDanhGia);
            //}
            return result;
        }

        public ThongTinDuyetPhieuDanhGiaCanBoDto GetThongTinDuyetPhieuTheoCanBoDanhGia(PhieuDuyetInput input)
        {
            var data = new ThongTinDuyetPhieuDanhGiaCanBoDto();

            data = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                    from kq in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                    where ls.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId
                    && ls.CanBoId == input.CanBoId && ls.CanBoDanhGiaId == input.CanBoDanhGiaId
                    && ls.ChucVuCanBoDanhGiaId == input.ChucVuCanBoDanhGiaId
                    select new ThongTinDuyetPhieuDanhGiaCanBoDto
                    {
                        LichSuKetQuaDanhGiaChiTietId = ls.Id,
                        CanBoDanhGiaId = ls.CanBoDanhGiaId,
                        NhanXet = kq.NhanXet,
                        KetQuaChiTietId = kq.Id,
                        ThangDiemChiTietId = ls.ThangDiemChiTietId
                    }).FirstOrDefault();

            return data;
        }

        public bool GetThongTinDanhGiaKeTiep(long phieuDanhGiaCanBoId, int capDanhGia, long thongTinDanhGiaHienTaiId)
        {
            long nhanVienId = 0;
            long chucVuNhanVienId = 0;

            long chucVuCanBoDanhGiaKeTiepId = 0;

            var loaiHinhChucVu = 0;
            var isKetQuaCuoiCung = false;
            bool isNguoiCham = false;

            var phieuDanhGiaCanBo = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Id == phieuDanhGiaCanBoId).FirstOrDefault();

            // kiem tra xem trong bang thongTinDanhGiaKeTiep con ban ghi nao dang doi ko?
            var getTTKT = _thongTinDanhGiaKeTiepTempRepos.GetAll().Any(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.TrangThai == (int)AppConsts.TrangThaiTemp.Cho && x.Id != thongTinDanhGiaHienTaiId);
            if (getTTKT)
            {
                return isKetQuaCuoiCung;
            }

            if (phieuDanhGiaCanBo != null)
            {
                nhanVienId = phieuDanhGiaCanBo.CanBoId;
                chucVuNhanVienId = phieuDanhGiaCanBo.ChucVuId;

                var isChucVuChinh = _canBoChucVuRepos.GetAll()
                .Any(x => x.ChucVuId == chucVuNhanVienId
                && x.CanBoId == nhanVienId
                && x.ChucVuChinh == (int)AppConsts.LoaiChucVu.ChucVuChinh);

                int thuTuChucVuDanhGiaKeTiep = 1;
                var getThuTuDanhGiaHienTai = _thongTinDanhGiaKeTiepTempRepos.GetAll()
                      .Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId).OrderByDescending(x => x.ThuTu).FirstOrDefault();
                if (getThuTuDanhGiaHienTai != null)
                {
                    thuTuChucVuDanhGiaKeTiep = getThuTuDanhGiaHienTai.ThuTu + 1;
                }

                #region Tim kiem chuc vu danh gia ke tiep               
                var dsChucVuDanhGia = _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == nhanVienId).ToList();
                if (dsChucVuDanhGia.Count() == 0)
                {
                    isKetQuaCuoiCung = true;
                    return isKetQuaCuoiCung;
                }

                var chucVuCanBoDanhGiaKeTiep = dsChucVuDanhGia.Where(x => x.ThuTu == thuTuChucVuDanhGiaKeTiep).FirstOrDefault();
                if (chucVuCanBoDanhGiaKeTiep != null)
                {
                    if (chucVuCanBoDanhGiaKeTiep.CanBoDanhGiaId > 0)
                    {
                        var cv = _chucVuRepos.Get(chucVuCanBoDanhGiaKeTiep.ChucVuDanhGiaId);

                        // chỉ định cán bộ đánh giá kế tiếp; nếu có thì thêm vào bảng ThongTinDanhGiaKeTiepTemp và return luôn.
                        isKetQuaCuoiCung = false;

                        var temp = new ThongTinDanhGiaKeTiepTemp();
                        temp.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                        temp.CanBoDanhGiaKeTiepId = chucVuCanBoDanhGiaKeTiep.CanBoDanhGiaId.Value;
                        temp.ChucVuId = chucVuCanBoDanhGiaKeTiep.ChucVuDanhGiaId;
                        temp.TrangThai = (int)AppConsts.TrangThaiTemp.Cho;
                        temp.NguoiCham = chucVuCanBoDanhGiaKeTiep.NguoiCham;
                        temp.TenantId = AbpSession.TenantId;
                        temp.LoaiChucVuDanhGia = cv.LoaiChucVu;
                        temp.ThuTu = thuTuChucVuDanhGiaKeTiep;
                        _thongTinDanhGiaKeTiepTempRepos.Insert(temp);
                        return isKetQuaCuoiCung;
                    }

                    chucVuCanBoDanhGiaKeTiepId = chucVuCanBoDanhGiaKeTiep.ChucVuDanhGiaId;
                    isNguoiCham = chucVuCanBoDanhGiaKeTiep.NguoiCham;

                    loaiHinhChucVu = GetLoaiHinhNhomChucVu(chucVuCanBoDanhGiaKeTiepId);
                }
                else
                {
                    isKetQuaCuoiCung = true;
                    return isKetQuaCuoiCung;
                }
                #endregion

                #region Tìm kiếm cán bộ chấm tiếp theo                
                if (isKetQuaCuoiCung == false && chucVuCanBoDanhGiaKeTiepId != 0 && loaiHinhChucVu == (int)AppConsts.LoaiHinhChucVu.NhomChucVu)
                {
                    var dsCanBoToChuyenMon = _thanhVienNhomChucVuRepos.GetAll().Where(x => x.ChucVuId == chucVuCanBoDanhGiaKeTiepId);

                    foreach (var item in dsCanBoToChuyenMon)
                    {
                        var temp = new ThongTinDanhGiaKeTiepTemp();
                        temp.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                        temp.CanBoDanhGiaKeTiepId = item.CanBoId;
                        temp.ChucVuId = item.ChucVuCanBoId;
                        temp.TrangThai = (int)AppConsts.TrangThaiTemp.Cho;
                        temp.NguoiCham = isNguoiCham;
                        temp.TenantId = AbpSession.TenantId;
                        temp.LoaiChucVuDanhGia = (int)AppConsts.LoaiHinhChucVu.NhomChucVu;
                        temp.ThuTu = thuTuChucVuDanhGiaKeTiep;
                        _thongTinDanhGiaKeTiepTempRepos.Insert(temp);
                    }

                }
                #endregion
            }

            return isKetQuaCuoiCung;
        }
        //public bool KiemTraPhieuDanhGiaCuoiCung

        // Dành cho PCT
        public List<DanhSachDuyetPhieu> GetDanhSachPhieuCanDuyetPCT(PhieuDanhGiaCanBoInput input)
        {
            var date = new DateTime(input.ThoiGian.Value.Year, input.ThoiGian.Value.Month, 1);
            var isCheckHetHan = KiemTraThoiGianKetThucDanhGia(date);

            var dsPhieuDanhGiaThangHienTai = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Nam == input.ThoiGian.Value.Year
            && x.Thang == input.ThoiGian.Value.Month);

            var dsCacPhieuCanDanhGia = (from pdgcb in dsPhieuDanhGiaThangHienTai
                                        from temp in _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdgcb.Id)
                                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == temp.ChucVuId)
                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == temp.PhieuDanhGiaCanBoId &&
                                        x.ChucVuCanBoDanhGiaId == temp.ChucVuId && x.CanBoDanhGiaId == temp.CanBoDanhGiaKeTiepId).DefaultIfEmpty()
                                        from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == temp.PhieuDanhGiaCanBoId &&
                                       x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.TieuChiId == -1).DefaultIfEmpty()
                                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId).DefaultIfEmpty()
                                        where temp.CanBoDanhGiaKeTiepId == input.CanBoId
                                        && temp.ChucVuId == input.ChucVuId
                                        && temp.NguoiCham == false
                                        && (input.TrangThaiPhieu == -1 || temp.TrangThai == input.TrangThaiPhieu)
                                        select new
                                        {
                                            PhieuDanhGiaCanBoId = pdgcb.Id,
                                            CanBoId = pdgcb.CanBoId,
                                            ChucVuId = pdgcb.ChucVuId,
                                            LichSuKetQuaDanhGiaChiTietId = (ls == null) ? 0 : ls.Id,
                                            KetQuaDanhGiaChiTietId = (kqct == null) ? 0 : kqct.Id,
                                            NhanXet = (kqct == null) ? "" : kqct.NhanXet,
                                            TieuChiId = (kqct == null) ? 0 : kqct.TieuChiId,
                                            ThangDiemChiTietId = (ls == null) ? 0 : ls.ThangDiemChiTietId,
                                            LoaiThangDiemChiTiet = (tdct == null) ? 0 : tdct.LoaiThangDiemChiTiet,
                                            CapDoDanhGia = (ls == null) ? 0 : ls.CapDoDanhGia,
                                            LoaiChucVuDanhGia = cv.LoaiChucVu,
                                        });

            var _result = (from pdg in dsCacPhieuCanDanhGia
                           from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                           from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                           from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                           select new DanhSachDuyetPhieu
                           {
                               PhieuDanhGiaCanBoId = pdg.PhieuDanhGiaCanBoId,
                               CanBoId = pdg.CanBoId,
                               ChucVuId = pdg.ChucVuId,
                               TenChucVu = cv.Ten,
                               TenCanBo = u.Surname + " " + u.Name,
                               LichSuKetQuaDanhGiaChiTietId = pdg.LichSuKetQuaDanhGiaChiTietId,
                               KetQuaDanhGiaChiTietId = pdg.KetQuaDanhGiaChiTietId,
                               NhanXet = pdg.NhanXet,
                               TieuChiId = pdg.TieuChiId,
                               ThangDiemChiTietId = pdg.ThangDiemChiTietId,
                               LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet,
                               CapDoDanhGia = pdg.CapDoDanhGia,
                               HetHanDanhGia = isCheckHetHan,
                               IsSuaKetQua = false,
                               LoaiChucVuDanhGia = pdg.LoaiChucVuDanhGia
                           }).ToList();

            var dsLichSu = (from pdg in dsPhieuDanhGiaThangHienTai
                            from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id)
                            where ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
                            select new
                            {
                                PhieuDanhGiaCanBoId = pdg.Id,
                                CapDoDanhGia = ls.CapDoDanhGia
                            });

            var totalItemLs = dsLichSu.Count();

            foreach (var item in _result)
            {
                if (totalItemLs > 0)
                {
                    var lsMax = dsLichSu.Where(x => x.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId);
                    if (item.CapDoDanhGia > 0 && lsMax.Count() > 0 && item.CapDoDanhGia < lsMax.Max(x => x.CapDoDanhGia))
                    {
                        item.IsSuaKetQua = false;
                    }
                    else
                    {
                        item.IsSuaKetQua = true;
                    }
                }
                else
                {
                    item.IsSuaKetQua = true;
                }
            }
            return _result;
        }

        public bool TaoMoiVaCapNhatKetQuaNguoiDuyet(List<PhieuDanhGiaCanBoUpdateDto> lstData)
        {
            var _result = false;

            foreach (var item in lstData)
            {
                _result = TaoMoiVaCapNhatKetQuaNguoiDuyetProcess(item);
            }

            return _result;
        }
        public bool TaoMoiVaCapNhatKetQuaNguoiDuyetProcess(PhieuDanhGiaCanBoUpdateDto input)
        {
            bool result = false;
            using (var unitOfWork = _unitOfWorkManager.Begin(System.Transactions.TransactionScopeOption.RequiresNew))
            {
                try
                {
                    long ketQuaChiTietId = 0;

                    if (input.KetQuaChiTietId != null && input.KetQuaChiTietId > 0)
                    {// edit
                        ketQuaChiTietId = input.KetQuaChiTietId.Value;
                        var kq = _ketQuaDanhGiaChiTietRepos.Get(ketQuaChiTietId);
                        kq.NhanXet = input.NhanXet ?? "";
                        _ketQuaDanhGiaChiTietRepos.Update(kq);

                        var ls = _lichSuKetQuaDanhGiaChiTietRepos.Get(kq.LichSuKetQuaDanhGiaChiTietId);
                        ls.ThangDiemChiTietId = input.ThangDiemChiTietId;
                        _lichSuKetQuaDanhGiaChiTietRepos.Update(ls);
                    }
                    else
                    {
                        var tenantId = AbpSession.TenantId;
                        // addnew
                        long lichSuKetQuaDanhGiaChiTietId = 0;
                        long phieuDanhGiaCanBoId = input.PhieuDanhGiaCanBoId.Value;

                        var lskq = new LichSuKetQuaDanhGiaChiTiet();
                        lskq.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                        lskq.LicSuPhieuDanhGiaChiTietTruocDoId = 0;
                        if (input.PhieuDanhGiaCanBoId > 0)
                        {
                            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                            lskq.LicSuPhieuDanhGiaChiTietTruocDoId = pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId;
                        }
                        lskq.ThangDiemChiTietId = input.ThangDiemChiTietId;
                        lskq.CanBoId = input.CanBoId;
                        lskq.CanBoDanhGiaId = input.CanBoDanhGiaId;
                        lskq.CapDanhGia = input.CapDanhGia;
                        lskq.NgayDanhGia = DateTime.Now;
                        lskq.TrangThai = (int)AppConsts.TrangThaiPhieuCaNhan.DaGui;
                        lskq.ChucVuCanBoDanhGiaId = input.ChucVuId;
                        lskq.TenantId = tenantId;

                        //
                        lskq.KetQuaTongHop = true;
                        lskq.LoaiChucVuDanhGia = input.LoaiChucVuDanhGia;
                        var layTTCanBo = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                        && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId && x.ChucVuId == input.ChucVuId).FirstOrDefault();
                        if (layTTCanBo != null)
                        {
                            lskq.CapDoDanhGia = layTTCanBo.ThuTu + 1;
                        }

                        lichSuKetQuaDanhGiaChiTietId = _lichSuKetQuaDanhGiaChiTietRepos.InsertAndGetId(lskq);
                        // update PhieuDanhGiaCanBo
                        if (phieuDanhGiaCanBoId > 0)
                        {
                            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                            pdgcb.LichSuKetQuaDanhGiaChiTietHienTaiId = lichSuKetQuaDanhGiaChiTietId;
                            _phieuDanhGiaCanBoRepos.Update(pdgcb);
                        }

                        // tao ket qua danh gia chi tiet
                        var obj = new KetQuaDanhGiaChiTiet();
                        obj.PhieuDanhGiaId = 0;
                        obj.PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId;
                        obj.TieuChiId = -1;
                        obj.Diem = 0;
                        obj.NhanXet = input.NhanXet ?? "";
                        obj.LichSuKetQuaDanhGiaChiTietId = lichSuKetQuaDanhGiaChiTietId;
                        obj.TenantId = tenantId;
                        _ketQuaDanhGiaChiTietRepos.Insert(obj);

                        #region Đóng thông tin đánh giá kế tiếp hiện tại
                        var checkTemp = _thongTinDanhGiaKeTiepTempRepos.GetAll()
                               .Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                               && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId
                               && x.ChucVuId == input.ChucVuId
                               && x.TrangThai == (int)AppConsts.TrangThaiTemp.Cho).FirstOrDefault();

                        if (checkTemp != null)
                        {
                            checkTemp.TrangThai = (int)AppConsts.TrangThaiTemp.HoanThanh;
                            _thongTinDanhGiaKeTiepTempRepos.Update(checkTemp);
                        }
                        #endregion
                        #region update ban ghi cuoi cung       
                        long thongTinDanhGiaKeTiepId = 0;
                        var ttDanhGiaHienTai = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.ChucVuId == input.ChucVuId
                        && x.CanBoDanhGiaKeTiepId == input.CanBoDanhGiaId
                        && x.PhieuDanhGiaCanBoId == input.PhieuDanhGiaCanBoId).FirstOrDefault();
                        if (ttDanhGiaHienTai != null)
                        {
                            thongTinDanhGiaKeTiepId = ttDanhGiaHienTai.Id;
                        }

                        bool isKetQuaCuoiCung = GetThongTinDanhGiaKeTiep(phieuDanhGiaCanBoId, input.CapDanhGia, thongTinDanhGiaKeTiepId);
                        if (isKetQuaCuoiCung)
                        {
                            lskq.Id = lichSuKetQuaDanhGiaChiTietId;
                            lskq.IsKetQuaCuoiCung = true;
                            _lichSuKetQuaDanhGiaChiTietRepos.Update(lskq);

                            var pdg = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);
                            pdg.DaHoanThanh = (int)AppConsts.TrangThaiPhieuDanhGiaCanBo.HoanThanh;
                            _phieuDanhGiaCanBoRepos.Update(pdg);
                        }
                        #endregion
                    }

                    result = true;
                    unitOfWork.Complete();
                    //CurrentUnitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    unitOfWork.Dispose();
                }
            }

            return result;
        }

        // Danh Cho Chu Tich Quan
        public List<DanhSachDuyetPhieuChuTich> GetDanhSachPhieuCanDuyetCT(PhieuDanhGiaCanBoInput input)
        {
            var date = new DateTime(input.ThoiGian.Value.Year, input.ThoiGian.Value.Month, 1);
            var isCheckHetHan = KiemTraThoiGianKetThucDanhGia(date);

            var dsPhieuDanhGiaThangHienTai = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Nam == input.ThoiGian.Value.Year
            && x.Thang == input.ThoiGian.Value.Month);

            var dsCacPhieuCanDanhGia = (from pdgcb in dsPhieuDanhGiaThangHienTai
                                        from temp in _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdgcb.Id)
                                        from cv in _chucVuRepos.GetAll().Where(x => x.Id == temp.ChucVuId)
                                        from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == temp.PhieuDanhGiaCanBoId &&
                                        x.ChucVuCanBoDanhGiaId == temp.ChucVuId && x.CanBoDanhGiaId == temp.CanBoDanhGiaKeTiepId).DefaultIfEmpty()
                                        from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == temp.PhieuDanhGiaCanBoId &&
                                       x.LichSuKetQuaDanhGiaChiTietId == ls.Id && x.TieuChiId == -1).DefaultIfEmpty()
                                        from tdct in _thangDiemChiTietRepos.GetAll().Where(x => x.Id == ls.ThangDiemChiTietId).DefaultIfEmpty()
                                        where temp.CanBoDanhGiaKeTiepId == input.CanBoId
                                        && temp.ChucVuId == input.ChucVuId
                                        && temp.NguoiCham == false
                                        && (input.TrangThaiPhieu == -1 || temp.TrangThai == input.TrangThaiPhieu)
                                        select new
                                        {
                                            PhieuDanhGiaCanBoId = pdgcb.Id,
                                            CanBoId = pdgcb.CanBoId,
                                            ChucVuId = pdgcb.ChucVuId,
                                            LichSuKetQuaDanhGiaChiTietId = (ls == null) ? 0 : ls.Id,
                                            KetQuaDanhGiaChiTietId = (kqct == null) ? 0 : kqct.Id,
                                            NhanXet = (kqct == null) ? "" : kqct.NhanXet,
                                            TieuChiId = (kqct == null) ? 0 : kqct.TieuChiId,
                                            ThangDiemChiTietId = (ls == null) ? 0 : ls.ThangDiemChiTietId,
                                            LoaiThangDiemChiTiet = (tdct == null) ? 0 : tdct.LoaiThangDiemChiTiet,
                                            CapDoDanhGia = (ls == null) ? 0 : ls.CapDoDanhGia,
                                            LoaiChucVuDanhGia = cv.LoaiChucVu,
                                        });

            var _result = (from pdg in dsCacPhieuCanDanhGia
                           from cv in _chucVuRepos.GetAll().Where(x => x.Id == pdg.ChucVuId)
                           from cb in _canBoRepos.GetAll().Where(x => x.Id == pdg.CanBoId)
                           from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                           select new DanhSachDuyetPhieuChuTich
                           {
                               PhieuDanhGiaCanBoId = pdg.PhieuDanhGiaCanBoId,
                               CanBoId = pdg.CanBoId,
                               ChucVuId = pdg.ChucVuId,
                               TenChucVu = cv.Ten,
                               TenCanBo = u.Surname + " " + u.Name,
                               LichSuKetQuaDanhGiaChiTietId = pdg.LichSuKetQuaDanhGiaChiTietId,
                               KetQuaDanhGiaChiTietId = pdg.KetQuaDanhGiaChiTietId,
                               NhanXet = pdg.NhanXet,
                               TieuChiId = pdg.TieuChiId,
                               ThangDiemChiTietId = pdg.ThangDiemChiTietId,
                               LoaiThangDiemChiTiet = pdg.LoaiThangDiemChiTiet,
                               CapDoDanhGia = pdg.CapDoDanhGia,
                               HetHanDanhGia = isCheckHetHan,
                               IsSuaKetQua = false,
                               LoaiChucVuDanhGia = pdg.LoaiChucVuDanhGia
                           }).ToList();

            var dsLichSu = (from pdg in dsPhieuDanhGiaThangHienTai
                            from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == pdg.Id)
                            select new
                            {
                                PhieuDanhGiaCanBoId = pdg.Id,
                                CapDoDanhGia = ls.CapDoDanhGia,
                                CapDanhGia = ls.CapDanhGia,
                                ThangDiemChiTietId = ls.ThangDiemChiTietId
                            });

            var totalItemLs = dsLichSu.Count();

            foreach (var item in _result)
            {
                if (totalItemLs > 0)
                {
                    var lsMax = dsLichSu.Where(x => x.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                    && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren).Max(x => x.CapDoDanhGia);
                    if (item.CapDoDanhGia > 0 && item.CapDoDanhGia < lsMax)
                    {
                        item.IsSuaKetQua = false;
                    }
                    else
                    {
                        item.IsSuaKetQua = true;
                    }
                }
                else
                {
                    item.IsSuaKetQua = true;
                }

                item.ThangDiemDanhGiaCaNhanId = dsLichSu.Where(x => x.PhieuDanhGiaCanBoId == item.PhieuDanhGiaCanBoId
                && x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia).FirstOrDefault().ThangDiemChiTietId;

            }
            return _result;
        }

    }
}
