﻿using Abp;
using Abp.AspNetZeroCore.Net;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Microsoft.Extensions.Configuration;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Configuration;
using newPSG.PMS.Configuration.Host.Dto;
using newPSG.PMS.Dto;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EleisureApplicationShared.PhieuDanhGia.Dto.PhieuDanhGiaCanBo.Export;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using Telerik.Documents.Common.Model;
using Telerik.Documents.Core.Fonts;
using Telerik.Windows.Documents.Common.FormatProviders;
using Telerik.Windows.Documents.Fixed.Model.Fonts;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;

namespace newPSG.PMS.EleisureApplication
{
    public partial class PhieuDanhGiaCanBoAppService
    {

        public int GetLoaiHinhNhomChucVu(long chucVuId)
        {
            var value = (int)AppConsts.LoaiHinhChucVu.ChucVu;

            var isCheckNhomChucVu = _chucVuRepos.Get(chucVuId);
            if (isCheckNhomChucVu != null && isCheckNhomChucVu.LoaiChucVu == (int)AppConsts.LoaiHinhChucVu.NhomChucVu)
            {
                value = (int)AppConsts.LoaiHinhChucVu.NhomChucVu;
            }
            return value;
        }


        public PhieuDanhGiaTongHop GetKetQuaNhomDanhGia(long phieuDanhGiaCanBoId, long phieuDanhGiaId, long lichSuDanhGiaId)
        {
            var tongHopPhieuDanhGia = new PhieuDanhGiaTongHop();
            try
            {
                var phieuDanhGiaChiTiet = new List<PhieuDanhGiaChiTietDto>();
                var thongTinCanBoGuiDanhGia = new CanBoGuiDanhGiaDto();

                phieuDanhGiaChiTiet =
                   _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGiaId)
                   .Select(x => new PhieuDanhGiaChiTietDto
                   {
                       Id = x.Id,
                       Ten = x.Ten,
                       DiemToiDa = x.DiemToiDa,
                       TieuChiChaId = x.TieuChiChaId,
                       KyHieu = x.KyHieu,
                       ThuTu = x.ThuTu
                   }).ToList();

                #region Kiem tra su ton tai cua phieuDanhGiaCanBo trong Db
                var lvLichSuDanhGia = _lichSuKetQuaDanhGiaChiTietRepos.Get(lichSuDanhGiaId);
                var selectKetQuaDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                           from kqct in _ketQuaDanhGiaChiTietRepos.GetAll().Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                           where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                           && ls.CapDoDanhGia == lvLichSuDanhGia.CapDoDanhGia
                                           && ls.KetQuaTongHop == false
                                           select new
                                           {
                                               CapDanhGia = ls.CapDanhGia,
                                               TieuChiId = kqct.TieuChiId,
                                               Diem = kqct.Diem,
                                               NhanXet = kqct.NhanXet,
                                               ThangDiemId = ls.ThangDiemChiTietId,
                                               CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                               ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                               LichSuId = ls.Id,
                                               LoaiChucVuDanhGia = ls.LoaiChucVuDanhGia,
                                               KetQuaTongHop = ls.KetQuaTongHop,
                                               CapDoDanhGia = ls.CapDoDanhGia
                                           }).ToList();

                if (selectKetQuaDanhGia.Count() > 0)
                {
                    var lskqCap1 = new List<LichSuKetQuaDanhGiaDto>();
                    var lskqCap2 = new List<LichSuKetQuaDanhGiaDto>();
                    var lskqCap3 = new List<LichSuKetQuaDanhGiaDto>();
                    var lskqCap4 = new List<LichSuKetQuaDanhGiaDto>();
                    // lay thong tin nhung ket qua danh gia
                    var lstLvDb = selectKetQuaDanhGia.Select(x => x.LichSuId).Distinct().OrderBy(x => x);
                    int i = 1;
                    foreach (var lv in lstLvDb)
                    {
                        if (i == 1)
                        {
                            lskqCap1 = selectKetQuaDanhGia.Where(x => x.LichSuId == lv)
                                .Select(x => new LichSuKetQuaDanhGiaDto
                                {
                                    TieuChiId = x.TieuChiId,
                                    Diem = x.Diem,
                                    NhanXet = x.NhanXet,
                                    ThangDiemId = x.ThangDiemId,
                                    ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId
                                }).ToList();

                            tongHopPhieuDanhGia.LichSuDanhGiaId1 = 1;
                        }
                        else if (i == 2)
                        {
                            lskqCap2 = selectKetQuaDanhGia.Where(x => x.LichSuId == lv)
                                .Select(x => new LichSuKetQuaDanhGiaDto
                                {
                                    TieuChiId = x.TieuChiId,
                                    Diem = x.Diem,
                                    NhanXet = x.NhanXet,
                                    ThangDiemId = x.ThangDiemId,
                                    ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId
                                }).ToList();
                            tongHopPhieuDanhGia.LichSuDanhGiaId2 = 1;
                        }
                        else if (i == 3)
                        {
                            lskqCap3 = selectKetQuaDanhGia.Where(x => x.LichSuId == lv)
                                .Select(x => new LichSuKetQuaDanhGiaDto
                                {
                                    TieuChiId = x.TieuChiId,
                                    Diem = x.Diem,
                                    NhanXet = x.NhanXet,
                                    ThangDiemId = x.ThangDiemId,
                                    ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId
                                }).ToList();
                            tongHopPhieuDanhGia.LichSuDanhGiaId3 = 1;
                        }
                        else if (i == 4)
                        {
                            lskqCap4 = selectKetQuaDanhGia.Where(x => x.LichSuId == lv)
                                .Select(x => new LichSuKetQuaDanhGiaDto
                                {
                                    TieuChiId = x.TieuChiId,
                                    Diem = x.Diem,
                                    NhanXet = x.NhanXet,
                                    ThangDiemId = x.ThangDiemId,
                                    ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId
                                }).ToList();
                            tongHopPhieuDanhGia.LichSuDanhGiaId4 = 1;
                        }
                        i++;
                    }

                    //ket thuc select nhung cap da danh gia roi (danh gia roi hoac chua danh gia)
                    tongHopPhieuDanhGia.TongDiemCapDanhGia1 = lskqCap1.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                    tongHopPhieuDanhGia.TongDiemCapDanhGia2 = lskqCap2.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                    tongHopPhieuDanhGia.TongDiemCapDanhGia3 = lskqCap3.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                    tongHopPhieuDanhGia.TongDiemCapDanhGia4 = lskqCap4.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);


                    phieuDanhGiaChiTiet = (from pdgct in phieuDanhGiaChiTiet
                                           from c1 in lskqCap1.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                           from c2 in lskqCap2.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                           from c3 in lskqCap3.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                           from c4 in lskqCap4.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                           select new PhieuDanhGiaChiTietDto
                                           {
                                               Id = pdgct.Id,
                                               Ten = pdgct.Ten,
                                               TieuChiChaId = pdgct.TieuChiChaId,
                                               DiemToiDa = pdgct.DiemToiDa,
                                               KyHieu = pdgct.KyHieu,
                                               LichSuDiemCapDanhGia1 = (c1 != null) ? c1.Diem : 0,
                                               LichSuDiemCapDanhGia2 = (c2 != null) ? c2.Diem : 0,
                                               LichSuDiemCapDanhGia3 = (c3 != null) ? c3.Diem : 0,
                                               LichSuDiemCapDanhGia4 = (c4 != null) ? c4.Diem : 0,
                                           }).ToList();

                    tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                }
                #endregion


            }
            catch (Exception ex)
            {
                throw;
            }
            return tongHopPhieuDanhGia;
        }

        public PhieuDanhGiaTongHop GetChiTietPhieuDanhGia(long? canBoId, long? chucVuId, long? phieuDanhGiaCanBoId)
        {
            var tongHopPhieuDanhGia = new PhieuDanhGiaTongHop();
            try
            {
                long canBoGuiPhieuId = 0;
                long chuVuCanBoGuiPhieuId = 0;

                var phieuDanhGiaChiTiet = new List<PhieuDanhGiaChiTietDto>();
                var thongTinCanBoGuiDanhGia = new CanBoGuiDanhGiaDto();
                var phieuDanhGia = getPhieuDanhGiaIdByChucVuIdOrPhieuDanhGiaCanBoId(canBoId, chucVuId, phieuDanhGiaCanBoId);
                if (phieuDanhGia.Id > 0)
                {
                    if (phieuDanhGiaCanBoId != null)
                    {
                        var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId.Value);
                        tongHopPhieuDanhGia.TrangThai = pdgcb.DaHoanThanh;

                        var user = (from cb in _canBoRepos.GetAll().Where(x => x.Id == pdgcb.CanBoId)
                                    from u in _userManager.Users.Where(x => x.Id == cb.UserId)
                                    select new
                                    {
                                        TenCanBo = u.Surname + " " + u.Name,
                                    }).FirstOrDefault();

                        var chucVu = _chucVuRepos.GetAll().Where(x => x.Id == pdgcb.ChucVuId).FirstOrDefault();
                        thongTinCanBoGuiDanhGia.TenCanBo = (user == null) ? "" : user.TenCanBo;
                        thongTinCanBoGuiDanhGia.TenChucVu = (chucVu == null) ? "" : chucVu.Ten;
                        thongTinCanBoGuiDanhGia.ThoiGian = pdgcb.Thang + "/" + pdgcb.Nam;
                        canBoGuiPhieuId = pdgcb.CanBoId;
                        chuVuCanBoGuiPhieuId = pdgcb.ChucVuId;
                    }
                    else
                    {
                        var user = (from cb in _canBoRepos.GetAll().Where(x => x.Id == canBoId)
                                    from u in _userManager.Users.Where(x => x.Id == cb.UserId)
                                    select new
                                    {
                                        TenCanBo = u.Surname + " " + u.Name,
                                    }).FirstOrDefault();

                        var chucVu = _chucVuRepos.GetAll().Where(x => x.Id == chucVuId).FirstOrDefault();
                        thongTinCanBoGuiDanhGia.TenCanBo = (user == null) ? "" : user.TenCanBo;
                        thongTinCanBoGuiDanhGia.TenChucVu = (chucVu == null) ? "" : chucVu.Ten;
                    }
                    tongHopPhieuDanhGia.ThongTinCanBoDto = thongTinCanBoGuiDanhGia;
                    tongHopPhieuDanhGia.PhieuDanhGiaDto = phieuDanhGia;
                    tongHopPhieuDanhGia.ThangDiemChiTietDto = getThangDiemTheoPhieuDanhGiaId(phieuDanhGia.ThangDiemId.Value);

                    phieuDanhGiaChiTiet =
                       _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGia.Id)
                       .Select(x => new PhieuDanhGiaChiTietDto
                       {
                           Id = x.Id,
                           Ten = x.Ten,
                           DiemToiThieu = x.DiemToiThieu,
                           DiemToiDa = x.DiemToiDa,
                           TieuChiChaId = x.TieuChiChaId,
                           KyHieu = x.KyHieu,
                           ThuTu = x.ThuTu
                       }).ToList();

                    if (phieuDanhGiaCanBoId != null && phieuDanhGiaCanBoId > 0)
                    {
                        #region Kiem tra su ton tai cua phieuDanhGiaCanBo trong Db
                        var selectKetQuaDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                                   from kqct in _ketQuaDanhGiaChiTietRepos.GetAll()
                                                   .Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                                   where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                                   && kqct.PhieuDanhGiaId != 0
                                                   && (ls.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia
                                                   || ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                                                   select new
                                                   {
                                                       CapDanhGia = ls.CapDanhGia,
                                                       TieuChiId = kqct.TieuChiId,
                                                       Diem = kqct.Diem,
                                                       NhanXet = kqct.NhanXet,
                                                       ThangDiemId = ls.ThangDiemChiTietId,
                                                       CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                                       ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                                       LichSuId = ls.Id,
                                                       LoaiChucVuDanhGia = ls.LoaiChucVuDanhGia,
                                                       KetQuaTongHop = ls.KetQuaTongHop,
                                                       CapDoDanhGia = ls.CapDoDanhGia,
                                                       IsKetQuaCuoiCung = ls.IsKetQuaCuoiCung
                                                   }).ToList();

                        if (selectKetQuaDanhGia.Count() > 0)
                        {
                            var lstLv = new List<int>();
                            int lvLoaiHinhNhomChucVuTemp = 0;
                            int loaiHinhChucVu = (int)AppConsts.LoaiHinhChucVu.NhomChucVu;

                            // get lv nhom chuc vu
                            var getLvNhomDanhGia = selectKetQuaDanhGia.Where(x => x.LoaiChucVuDanhGia == loaiHinhChucVu).FirstOrDefault();
                            if (getLvNhomDanhGia != null)
                            {
                                lvLoaiHinhNhomChucVuTemp = getLvNhomDanhGia.CapDoDanhGia;
                            }

                            #region diem ca nhan tu cham                          
                            var kqTuDanhGia = selectKetQuaDanhGia.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia).ToList();
                            if (kqTuDanhGia.Count > 0)
                            {
                                lstLv.Add(kqTuDanhGia.FirstOrDefault().CapDoDanhGia);
                                tongHopPhieuDanhGia.ThangDiemCaNhanTuDanhGiaId = kqTuDanhGia.FirstOrDefault().ThangDiemId;
                            }
                            #endregion

                            #region diem ng dang dang nhap cham 
                            tongHopPhieuDanhGia.LichSuCanBoDangDanhGiaId = 0;
                            var kqlanhDaoDangChamDiem = selectKetQuaDanhGia.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
                                                   && x.CanBoDanhGiaId == canBoId && x.ChucVuCanBoDanhGiaId == chucVuId).ToList();

                            #region kiem tra can bo chuc vu hien tai la ca nhan hay nhom danh gia
                            var tmp = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                            && x.CanBoDanhGiaKeTiepId == canBoId && x.ChucVuId == chucVuId).FirstOrDefault();
                            if (tmp != null)
                            {
                                if (tmp.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu)
                                {
                                    var tenNhom = GetNhomChucVu(canBoGuiPhieuId, chuVuCanBoGuiPhieuId, tmp.ThuTu);
                                    if (!string.IsNullOrWhiteSpace(tenNhom.Ten))
                                    {
                                        tongHopPhieuDanhGia.TieuDeCot = tenNhom.Ten;
                                    }
                                    else
                                    {
                                        tongHopPhieuDanhGia.TieuDeCot = "Điểm cấp phó phụ trách trực tiếp hoặc tổ, nhóm chấm";
                                    }
                                }
                                else
                                {
                                    var cv2 = _chucVuRepos.Get(chucVuId.Value);
                                    tongHopPhieuDanhGia.TieuDeCot = cv2.TenHienThiTrenBieuMau;
                                }
                            }
                            #endregion

                            if (kqlanhDaoDangChamDiem.Count > 0)
                            {
                                tongHopPhieuDanhGia.LichSuCanBoDangDanhGiaId = kqlanhDaoDangChamDiem.FirstOrDefault().LichSuId;
                                lstLv.Add(kqlanhDaoDangChamDiem.FirstOrDefault().CapDoDanhGia);
                                tongHopPhieuDanhGia.ThangDiemLanhDaoDanhGiaId = kqlanhDaoDangChamDiem.FirstOrDefault().ThangDiemId;

                                var nxc = kqlanhDaoDangChamDiem.Where(x => x.TieuChiId == -1).FirstOrDefault();
                                if (nxc != null)
                                {
                                    tongHopPhieuDanhGia.NhanXetChung = nxc.NhanXet;
                                }
                                tongHopPhieuDanhGia.HienThiChiTietChoNguoiDangDangNhap = kqlanhDaoDangChamDiem.Any(x => x.IsKetQuaCuoiCung == true);
                            }
                            else
                            {
                                tongHopPhieuDanhGia.HienThiChiTietChoNguoiDangDangNhap = KiemTraChucVuCuoiCung(canBoGuiPhieuId, chuVuCanBoGuiPhieuId, chucVuId.Value);

                            }
                            #endregion

                            #region lay thong tin nhung ket qua danh gia cua nhung ng khac
                            var lstLvDb = selectKetQuaDanhGia.Select(x => x.CapDoDanhGia).Distinct()
                                .Where(x => !lstLv.Any(d => d == x)).OrderBy(x => x);

                            var lskqCap1 = new List<LichSuKetQuaDanhGiaDto>();
                            var lskqCap2 = new List<LichSuKetQuaDanhGiaDto>();

                            int i = 1;
                            foreach (var lv in lstLvDb)
                            {
                                if (i == 1)
                                {
                                    lskqCap1 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId
                                        }).ToList();

                                    if (lskqCap1.Count > 0)
                                    {
                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId1 =
                                            lskqCap1.FirstOrDefault().ThangDiemId;

                                        var cv1 = _chucVuRepos.Get(lskqCap1.FirstOrDefault().ChucVuCanBoDanhGiaId);
                                        tongHopPhieuDanhGia.TieuDeCot1 = cv1.TenHienThiTrenBieuMau;
                                        tongHopPhieuDanhGia.HienThiChiTietCot1 = (cv1.LoaiChucVu == loaiHinhChucVu) ? true : false;
                                        lvLoaiHinhNhomChucVuTemp = (cv1.LoaiChucVu == loaiHinhChucVu) ? lv : 0;
                                        tongHopPhieuDanhGia.LichSuDanhGiaId1 = lskqCap1.FirstOrDefault().LichSuDanhGiaId;
                                    }
                                }
                                else if (i == 2)
                                {
                                    lskqCap2 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId
                                        }).ToList();

                                    if (lskqCap2.Count > 0)
                                    {
                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId2 =
                                            lskqCap2.FirstOrDefault().ThangDiemId;

                                        var cv2 = _chucVuRepos.Get(lskqCap2.FirstOrDefault().ChucVuCanBoDanhGiaId);
                                        tongHopPhieuDanhGia.TieuDeCot2 = cv2.TenHienThiTrenBieuMau;
                                        tongHopPhieuDanhGia.HienThiChiTietCot2 = (cv2.LoaiChucVu == loaiHinhChucVu) ? true : false;
                                        lvLoaiHinhNhomChucVuTemp = (cv2.LoaiChucVu == loaiHinhChucVu) ? lv : 0;
                                        tongHopPhieuDanhGia.LichSuDanhGiaId2 = lskqCap2.FirstOrDefault().LichSuDanhGiaId;
                                    }
                                }
                                i++;
                            }
                            #endregion

                            tongHopPhieuDanhGia.TongDiemCaNhanCham = kqTuDanhGia.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemLanhDaoCham = kqlanhDaoDangChamDiem.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemCapDanhGia1 = lskqCap1.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemCapDanhGia2 = lskqCap2.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);


                            phieuDanhGiaChiTiet = (from pdgct in phieuDanhGiaChiTiet
                                                   from tdg in kqTuDanhGia.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from lddg in kqlanhDaoDangChamDiem.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from c1 in lskqCap1.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from c2 in lskqCap2.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   select new PhieuDanhGiaChiTietDto
                                                   {
                                                       Id = pdgct.Id,
                                                       Ten = pdgct.Ten,
                                                       TieuChiChaId = pdgct.TieuChiChaId,
                                                       DiemToiThieu = pdgct.DiemToiThieu,
                                                       DiemToiDa = pdgct.DiemToiDa,
                                                       DiemTuCham = (tdg != null) ? tdg.Diem : null,
                                                       DiemLanhDaoCham = (lddg != null) ? lddg.Diem : null,
                                                       TuNhanXet = (tdg != null) ? tdg.NhanXet : "",
                                                       LanhDaoNhanXet = (lddg != null) ? lddg.NhanXet : "",
                                                       KyHieu = pdgct.KyHieu,
                                                       LichSuDiemCapDanhGia1 = (c1 != null) ? c1.Diem : 0,
                                                       LichSuDiemCapDanhGia2 = (c2 != null) ? c2.Diem : null
                                                   }).ToList();

                            tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                        }
                        #endregion
                    }
                    else
                    {
                        tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Abp.UI.UserFriendlyException("Có lỗi xảy ra vui lòng thao tác lại!");
            }
            return tongHopPhieuDanhGia;
        }

        public PhieuDanhGiaTongHop GetChiTietPhieuDanhGiaTheoPhieuDanhGiaCanBoId(long? phieuDanhGiaCanBoId, long? canBoId,
            long? chucVuId, bool isExportFile)
        {
            var tongHopPhieuDanhGia = new PhieuDanhGiaTongHop();
            try
            {
                var phieuDanhGiaChiTiet = new List<PhieuDanhGiaChiTietDto>();
                var thongTinCanBoGuiDanhGia = new CanBoGuiDanhGiaDto();
                var phieuDanhGia = getPhieuDanhGiaIdByChucVuIdOrPhieuDanhGiaCanBoId(null, null, phieuDanhGiaCanBoId);
                if (phieuDanhGia.Id > 0)
                {
                    var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId.Value);
                    tongHopPhieuDanhGia.TrangThai = pdgcb.DaHoanThanh;

                    var user = (from cb in _canBoRepos.GetAll().Where(x => x.Id == pdgcb.CanBoId)
                                from u in _userManager.Users.Where(x => x.Id == cb.UserId)
                                select new
                                {
                                    TenCanBo = u.Surname + " " + u.Name,
                                }).FirstOrDefault();

                    var chucVu = _chucVuRepos.GetAll().Where(x => x.Id == pdgcb.ChucVuId).FirstOrDefault();
                    thongTinCanBoGuiDanhGia.TenCanBo = (user == null) ? "" : user.TenCanBo;
                    thongTinCanBoGuiDanhGia.TenChucVu = (chucVu == null) ? "" : chucVu.Ten;
                    thongTinCanBoGuiDanhGia.ThoiGian = pdgcb.Thang + "/" + pdgcb.Nam;

                    tongHopPhieuDanhGia.ThongTinCanBoDto = thongTinCanBoGuiDanhGia;
                    tongHopPhieuDanhGia.PhieuDanhGiaDto = phieuDanhGia;
                    tongHopPhieuDanhGia.ThangDiemChiTietDto = getThangDiemTheoPhieuDanhGiaId(phieuDanhGia.ThangDiemId.Value);

                    phieuDanhGiaChiTiet =
                       _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == phieuDanhGia.Id)
                       .Select(x => new PhieuDanhGiaChiTietDto
                       {
                           Id = x.Id,
                           Ten = x.Ten,
                           DiemToiDa = x.DiemToiDa,
                           DiemToiThieu = x.DiemToiThieu,
                           TieuChiChaId = x.TieuChiChaId,
                           KyHieu = x.KyHieu,
                           ThuTu = x.ThuTu
                       }).ToList();

                    if (phieuDanhGiaCanBoId != null && phieuDanhGiaCanBoId > 0)
                    {
                        #region Kiem tra su ton tai cua phieuDanhGiaCanBo trong Db

                        var getCanBoChamDiem = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                                from tmp in _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.CanBoDanhGiaKeTiepId == ls.CanBoDanhGiaId
                                                && x.ChucVuId == ls.ChucVuCanBoDanhGiaId).DefaultIfEmpty()
                                                where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                                && ls.KetQuaTongHop == true
                                                select new
                                                {
                                                    LichSuKetQuaDanhGiaChiTietId = ls.Id,
                                                    NguoiCham = (tmp == null) ? true : tmp.NguoiCham,
                                                    CapDanhGia = ls.CapDanhGia,
                                                    KetQuaTongHop = ls.KetQuaTongHop,
                                                    ThangDiemChiTietId = ls.ThangDiemChiTietId,
                                                    CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                                    ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                                    IsKetQuaCuoiCung = ls.IsKetQuaCuoiCung,
                                                    CapDoDanhGia = ls.CapDoDanhGia
                                                });

                        var selectKetQuaDanhGia = (from ls in getCanBoChamDiem
                                                   from kqct in _ketQuaDanhGiaChiTietRepos.GetAll()
                                                   .Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.LichSuKetQuaDanhGiaChiTietId)
                                                   select new
                                                   {
                                                       CapDanhGia = ls.CapDanhGia,
                                                       TieuChiId = kqct.TieuChiId,
                                                       Diem = kqct.Diem,
                                                       NhanXet = kqct.NhanXet,
                                                       ThangDiemId = ls.ThangDiemChiTietId,
                                                       CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                                       ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                                       LichSuId = ls.LichSuKetQuaDanhGiaChiTietId,
                                                       KetQuaTongHop = ls.KetQuaTongHop,
                                                       CapDoDanhGia = ls.CapDoDanhGia,
                                                       IsKetQuaCuoiCung = ls.IsKetQuaCuoiCung,
                                                       NguoiCham = ls.NguoiCham
                                                   }).ToList();

                        var getThongTinthongTinDanhGiaKeTiepTemp = _thongTinDanhGiaKeTiepTempRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId);


                        if (selectKetQuaDanhGia.Count() > 0)
                        {
                            int loaiHinhChucVu = (int)AppConsts.LoaiHinhChucVu.NhomChucVu;
                            int lvLoaiHinhNhomChucVuTemp = 0;

                            var lskqCap1 = new List<LichSuKetQuaDanhGiaDto>();
                            var lskqCap2 = new List<LichSuKetQuaDanhGiaDto>();
                            var lskqCap3 = new List<LichSuKetQuaDanhGiaDto>();
                            var lskqCap4 = new List<LichSuKetQuaDanhGiaDto>();


                            // get lv nhom chuc vu
                            if (!isExportFile)
                            {
                                tongHopPhieuDanhGia.HienThiChiTietChoNguoiDangDangNhap =
                                    selectKetQuaDanhGia.Any(x => x.IsKetQuaCuoiCung == true && x.CanBoDanhGiaId == canBoId.Value
                                    && x.ChucVuCanBoDanhGiaId == chucVuId.Value);
                            }
                            // lay thong tin nhung ket qua danh gia
                            var lstLvDb = selectKetQuaDanhGia.Select(x => x.CapDoDanhGia).Distinct().OrderBy(x => x);

                            int i = 1;
                            foreach (var lv in lstLvDb)
                            {
                                if (i == 1)
                                {
                                    lskqCap1 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId
                                        }).ToList();


                                    if (lskqCap1.Count > 0)
                                    {
                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId1 =
                                            lskqCap1.FirstOrDefault().ThangDiemId;

                                        tongHopPhieuDanhGia.TieuDeCot1 = "Điểm cá nhân tự chấm";
                                    }
                                    tongHopPhieuDanhGia.LichSuDanhGiaId1 = lskqCap1.FirstOrDefault().LichSuDanhGiaId;
                                }
                                else if (i == 2)
                                {
                                    lskqCap2 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId,
                                            NguoiCham = x.NguoiCham
                                        }).ToList();

                                    if (lskqCap2.Count > 0 && lskqCap2.FirstOrDefault().NguoiCham == true)
                                    {
                                        var thongTin = lskqCap2.FirstOrDefault();

                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId2 =
                                        lskqCap2.FirstOrDefault().ThangDiemId;

                                        var cv2 = _chucVuRepos.Get(thongTin.ChucVuCanBoDanhGiaId);
                                        tongHopPhieuDanhGia.TieuDeCot2 = cv2.TenHienThiTrenBieuMau;
                                        tongHopPhieuDanhGia.HienThiChiTietCot2 = (cv2.LoaiChucVu == loaiHinhChucVu) ? true : false;
                                        lvLoaiHinhNhomChucVuTemp = (cv2.LoaiChucVu == loaiHinhChucVu) ? lv : 0;

                                        var nx = lskqCap2.Where(x => x.TieuChiId == -1).FirstOrDefault();
                                        tongHopPhieuDanhGia.NhanXetCapDanhGia2 = (nx == null) ? "" : nx.NhanXet;
                                        tongHopPhieuDanhGia.LichSuDanhGiaId2 = lskqCap2.FirstOrDefault().LichSuDanhGiaId;

                                    }
                                }
                                else if (i == 3)
                                {
                                    lskqCap3 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId,
                                            NguoiCham = x.NguoiCham
                                        }).ToList();

                                    if (lskqCap3.Count > 0 && lskqCap3.FirstOrDefault().NguoiCham == true)
                                    {
                                        var thongTin = lskqCap3.FirstOrDefault();

                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId3 =
                                        lskqCap3.FirstOrDefault().ThangDiemId;

                                        var cv3 = _chucVuRepos.Get(thongTin.ChucVuCanBoDanhGiaId);
                                        tongHopPhieuDanhGia.TieuDeCot3 = cv3.TenHienThiTrenBieuMau;
                                        tongHopPhieuDanhGia.HienThiChiTietCot3 = (cv3.LoaiChucVu == loaiHinhChucVu) ? true : false;
                                        lvLoaiHinhNhomChucVuTemp = (cv3.LoaiChucVu == loaiHinhChucVu) ? lv : 0;

                                        var nx = lskqCap3.Where(x => x.TieuChiId == -1).FirstOrDefault();
                                        tongHopPhieuDanhGia.NhanXetCapDanhGia3 = (nx == null) ? "" : nx.NhanXet;
                                        tongHopPhieuDanhGia.LichSuDanhGiaId3 = lskqCap3.FirstOrDefault().LichSuDanhGiaId;
                                    }

                                }
                                else if (i == 4)
                                {
                                    lskqCap4 = selectKetQuaDanhGia.Where(x => x.KetQuaTongHop == true && x.CapDoDanhGia == lv)
                                        .Select(x => new LichSuKetQuaDanhGiaDto
                                        {
                                            TieuChiId = x.TieuChiId,
                                            Diem = x.Diem,
                                            NhanXet = x.NhanXet,
                                            ThangDiemId = x.ThangDiemId,
                                            ChucVuCanBoDanhGiaId = x.ChucVuCanBoDanhGiaId,
                                            LichSuDanhGiaId = x.LichSuId,
                                            NguoiCham = x.NguoiCham
                                        }).ToList();

                                    if (lskqCap4.Count > 0 && lskqCap4.FirstOrDefault().NguoiCham == true)
                                    {
                                        var thongTin = lskqCap4.FirstOrDefault();

                                        tongHopPhieuDanhGia.ThangDiemCapDanhGiaId4 =
                                        lskqCap4.FirstOrDefault().ThangDiemId;
                                        var cv4 = _chucVuRepos.Get(thongTin.ChucVuCanBoDanhGiaId);
                                        tongHopPhieuDanhGia.TieuDeCot4 = cv4.TenHienThiTrenBieuMau;
                                        tongHopPhieuDanhGia.HienThiChiTietCot4 = (cv4.LoaiChucVu == loaiHinhChucVu) ? true : false;
                                        lvLoaiHinhNhomChucVuTemp = (cv4.LoaiChucVu == loaiHinhChucVu) ? lv : 0;

                                        var nx = lskqCap4.Where(x => x.TieuChiId == -1).FirstOrDefault();
                                        tongHopPhieuDanhGia.NhanXetCapDanhGia4 = (nx == null) ? "" : nx.NhanXet;
                                        tongHopPhieuDanhGia.LichSuDanhGiaId4 = lskqCap4.FirstOrDefault().LichSuDanhGiaId;
                                    }
                                }
                                i++;
                            }

                            //ket thuc select nhung cap da danh gia roi (danh gia roi hoac chua danh gia)
                            tongHopPhieuDanhGia.TongDiemCapDanhGia1 = lskqCap1.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemCapDanhGia2 = lskqCap2.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemCapDanhGia3 = lskqCap3.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);
                            tongHopPhieuDanhGia.TongDiemCapDanhGia4 = lskqCap4.Where(x => x.Diem.HasValue).Sum(x => x.Diem.Value);


                            phieuDanhGiaChiTiet = (from pdgct in phieuDanhGiaChiTiet
                                                   from c1 in lskqCap1.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from c2 in lskqCap2.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from c3 in lskqCap3.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   from c4 in lskqCap4.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                                   select new PhieuDanhGiaChiTietDto
                                                   {
                                                       Id = pdgct.Id,
                                                       Ten = pdgct.Ten,
                                                       TieuChiChaId = pdgct.TieuChiChaId,
                                                       DiemToiDa = pdgct.DiemToiDa,
                                                       DiemToiThieu = pdgct.DiemToiThieu,
                                                       //DiemTuCham = tdg.Diem,
                                                       //DiemLanhDaoCham = (lddg != null) ? lddg.Diem : null,
                                                       //TuNhanXet = tdg.NhanXet,
                                                       //LanhDaoNhanXet = (lddg != null) ? lddg.NhanXet : "",
                                                       KyHieu = pdgct.KyHieu,
                                                       LichSuNhanXetTuDanhgia = (c1 != null) ? c1.NhanXet : "",
                                                       LichSuDiemCapDanhGia1 = (c1 != null) ? c1.Diem : 0,
                                                       LichSuDiemCapDanhGia2 = (c2 != null) ? c2.Diem : 0,
                                                       LichSuDiemCapDanhGia3 = (c3 != null) ? c3.Diem : 0,
                                                       LichSuDiemCapDanhGia4 = (c4 != null) ? c4.Diem : 0,
                                                   }).ToList();

                            tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                        }
                        else
                        {
                            tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                        }
                        #endregion
                    }
                    else
                    {
                        tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return tongHopPhieuDanhGia;
        }

        public ChucVuDto GetNhomChucVu(long canBoId, long chucVuCanBoId, int thuTu)
        {
            var chucVu = new ChucVuDto();
            var isChucVuChinh = _canBoChucVuRepos.GetAll().Any(x => x.CanBoId == canBoId
            && x.ChucVuChinh == (int)AppConsts.LoaiChucVu.ChucVuChinh);
            if (isChucVuChinh)
            {
                var getChucVuNhomDanhGiaId = (from cbcvdg in _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == canBoId)
                                              from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcvdg.ChucVuDanhGiaId)
                                              where cbcvdg.ThuTu == thuTu
                                              select new
                                              {
                                                  ChucVuId = cv.Id,
                                                  TenHienThiTrenBieuMau = cv.TenHienThiTrenBieuMau
                                              }).FirstOrDefault();

                chucVu.Ten = (getChucVuNhomDanhGiaId == null) ? "" : getChucVuNhomDanhGiaId.TenHienThiTrenBieuMau;
                chucVu.Id = (getChucVuNhomDanhGiaId == null) ? 0 : getChucVuNhomDanhGiaId.ChucVuId;
            }
            else
            {
                var getChucVuNhomDanhGiaId = (from cbcvdg in _chucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDuocDanhGiaId == chucVuCanBoId)
                                              from cv in _chucVuRepos.GetAll().Where(x => x.Id == cbcvdg.ChucVuThucHienDanhGiaId)
                                              where cbcvdg.SoThuTu == thuTu
                                              select new
                                              {
                                                  ChucVuId = cv.Id,
                                                  TenHienThiTrenBieuMau = cv.TenHienThiTrenBieuMau
                                              }).FirstOrDefault();

                chucVu.Ten = (getChucVuNhomDanhGiaId == null) ? "" : getChucVuNhomDanhGiaId.TenHienThiTrenBieuMau;
                chucVu.Id = (getChucVuNhomDanhGiaId == null) ? 0 : getChucVuNhomDanhGiaId.ChucVuId;
            }

            return chucVu;
        }

        public bool KiemTraChucVuCuoiCung(long canBoId, long chucVuCanBoId, long chucVuCanKiemTraId)
        {
            bool result = false;
            var chucVu = new ChucVuDto();
            var isChucVuChinh = _canBoChucVuRepos.GetAll().Any(x => x.CanBoId == canBoId
            && x.ChucVuChinh == (int)AppConsts.LoaiChucVu.ChucVuChinh);

            if (isChucVuChinh)
            {
                var lstCanBoDanhGia = (from cbcvdg in _canBoChucVuDanhGiaRepos.GetAll().Where(x => x.CanBoId == canBoId)
                                       select new
                                       {
                                           ThuTu = cbcvdg.ThuTu,
                                           CanBoId = cbcvdg.CanBoId,
                                           ChucVuDanhGiaId = cbcvdg.ChucVuDanhGiaId,
                                       });

                if (lstCanBoDanhGia.Count() > 0)
                {
                    var ttMax = lstCanBoDanhGia.Select(x => x.ThuTu).Max();
                    var getTTCanBoCanKiemTra = lstCanBoDanhGia.Where(x => x.ChucVuDanhGiaId == chucVuCanKiemTraId)
                        .Select(x => x.ThuTu).FirstOrDefault();

                    if (getTTCanBoCanKiemTra > 0 && getTTCanBoCanKiemTra == ttMax)
                    {
                        result = true;
                    }
                }
            }
            else
            {
                var getChucVuDanhGia = (from cbcvdg in _chucVuDanhGiaRepos.GetAll().Where(x => x.ChucVuDuocDanhGiaId == chucVuCanBoId)
                                        select new
                                        {
                                            ThuTu = cbcvdg.SoThuTu,
                                            ChucVuDuocDanhGiaId = cbcvdg.ChucVuDuocDanhGiaId,
                                            ChucVuThucHienDanhGiaId = cbcvdg.ChucVuThucHienDanhGiaId,
                                        });
                if (getChucVuDanhGia.Count() > 0)
                {
                    var ttMax = getChucVuDanhGia.Select(x => x.ThuTu).Max();
                    var getTTChucVuCanKiemTra = getChucVuDanhGia.Where(x => x.ChucVuThucHienDanhGiaId == chucVuCanKiemTraId)
                        .Select(x => x.ThuTu).FirstOrDefault();

                    if (getTTChucVuCanKiemTra > 0 && getTTChucVuCanKiemTra == ttMax)
                    {
                        result = true;
                    }
                }
            }

            return result;
        }
        private ThongTinChung GetThongTinChungPhieuDanhGiaCanBo(long phieuDanhGiaCanBoId)
        {
            var thongTinChung = (from pdgcb in _phieuDanhGiaCanBoRepos.GetAll()
                                 from pdg in _phieuDanhGiaRepos.GetAll()
                                 from cb in _canBoRepos.GetAll()
                                 from u in _userManager.Users
                                 from cv in _chucVuRepos.GetAll()
                                 from dv in _donViRepos.GetAll()
                                 from h in _huyenRepos.GetAll()
                                 where pdgcb.PhieuDanhGiaId == pdg.Id
                                 && pdgcb.CanBoId == cb.Id
                                 && u.Id == cb.UserId
                                 && cv.Id == pdgcb.ChucVuId
                                 && dv.Id == cb.DonViId
                                 && dv.HuyenId == h.Id
                                 && pdgcb.Id == phieuDanhGiaCanBoId
                                 select new ThongTinChung
                                 {
                                     PhieuDanhGiaCanBoId = phieuDanhGiaCanBoId,
                                     Nam = pdgcb.Nam,
                                     Thang = pdgcb.Thang,
                                     Quy = pdgcb.Quy,
                                     Ky = pdgcb.Ky,
                                     DaHoanThanh = pdgcb.DaHoanThanh,
                                     TenPhieuDanhGia = pdg.Ten,
                                     PhieuDanhGiaId = pdg.Id,
                                     TieuDe = pdg.TieuDe,
                                     GhiChu = string.Empty,
                                     TenQuanHuyen = h.Ten,
                                     DuongDanPhieuDanhGia = pdg.DuongDanMauBaoCao,
                                     // Thong tin can bo
                                     CanBoId = cb.Id,
                                     TenCanBo = ((!string.IsNullOrEmpty(u.Surname) ? u.Surname : string.Empty) + " " + (!string.IsNullOrEmpty(u.Name) ? u.Name : string.Empty)).Trim(),

                                     // Thong tin chuc vu duoc danh gia
                                     TenChucVu = cv.Ten,
                                     ChucVuId = cv.Id,

                                     // Thong tin don vi cua can bo
                                     TenDonVi = dv.Ten,
                                     DonViId = dv.Id
                                 }).FirstOrDefault();

            return thongTinChung;
        }
        public PhieuDanhGiaTongHop getKetQuaDanhGiaCanBoTheoPhieuDanhGiaId(long phieuDanhGiaCanBoId)
        {
            var tongHopPhieuDanhGia = new PhieuDanhGiaTongHop();
            var phieuDanhGiaChiTiet = new List<PhieuDanhGiaChiTietDto>();

            var pdgcb = _phieuDanhGiaCanBoRepos.Get(phieuDanhGiaCanBoId);

            phieuDanhGiaChiTiet =
                _tieuChiDanhGiaRepos.GetAll().Where(x => x.PhieuDanhGiaId == pdgcb.PhieuDanhGiaId)
                .Select(x => new PhieuDanhGiaChiTietDto
                {
                    Id = x.Id,
                    Ten = x.Ten,
                    DiemToiDa = x.DiemToiDa,
                    DiemToiThieu = x.DiemToiThieu,
                    TieuChiChaId = x.TieuChiChaId,
                    KyHieu = x.KyHieu,
                    ThuTu = x.ThuTu
                }).ToList();

            #region Kiem tra su ton tai cua phieuDanhGiaCanBo trong Db
            var selectKetQuaDanhGia = (from ls in _lichSuKetQuaDanhGiaChiTietRepos.GetAll()
                                       from kqct in _ketQuaDanhGiaChiTietRepos.GetAll()
                                       .Where(x => x.LichSuKetQuaDanhGiaChiTietId == ls.Id)
                                       where ls.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                       && kqct.PhieuDanhGiaId != 0
                                       && (ls.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia
                                       || ls.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                                       && ls.KetQuaTongHop == true
                                       select new
                                       {
                                           CapDanhGia = ls.CapDanhGia,
                                           TieuChiId = kqct.TieuChiId,
                                           Diem = kqct.Diem,
                                           NhanXet = kqct.NhanXet,
                                           ThangDiemId = ls.ThangDiemChiTietId,
                                           CanBoDanhGiaId = ls.CanBoDanhGiaId,
                                           ChucVuCanBoDanhGiaId = ls.ChucVuCanBoDanhGiaId,
                                           LichSuId = ls.Id,
                                           LoaiChucVuDanhGia = ls.LoaiChucVuDanhGia,
                                           KetQuaTongHop = ls.KetQuaTongHop
                                       }).OrderBy(x => x.LichSuId).ToList();

            if (selectKetQuaDanhGia.Count() > 0)
            {
                var kqTuDanhGia = selectKetQuaDanhGia.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.TuDanhGia).ToList();

                var kqTongHopDanhGia = selectKetQuaDanhGia.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
                                     && x.CanBoDanhGiaId == 0 && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.NhomChucVu
                                       && x.KetQuaTongHop == true).ToList();

                var kqKetQuaLanhDao = selectKetQuaDanhGia.Where(x => x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
                                   && x.CanBoDanhGiaId != 0 && x.LoaiChucVuDanhGia == (int)AppConsts.LoaiHinhChucVu.ChucVu).ToList();


                if (kqTuDanhGia.Count > 0)
                {
                    var thangDiem = kqTuDanhGia.FirstOrDefault();
                    tongHopPhieuDanhGia.ThangDiemCaNhanTuDanhGiaId = (thangDiem != null) ? thangDiem.ThangDiemId : 0;
                }

                if (kqTongHopDanhGia.Count > 0)
                {
                    var thangDiem = kqTongHopDanhGia.FirstOrDefault();

                    tongHopPhieuDanhGia.ThangDiemLanhDaoDanhGiaId = (thangDiem != null) ? thangDiem.ThangDiemId : 0;

                    var nx = kqTongHopDanhGia.Where(x => x.TieuChiId == -1).FirstOrDefault();
                    tongHopPhieuDanhGia.NhanXetLanhDaoDanhGia = (nx == null) ? "" : nx.NhanXet;/// kqTongHopDanhGia.Where(x => x.TieuChiId == -1).FirstOrDefault().NhanXet;
                }

                if (kqKetQuaLanhDao.Count > 0)
                {
                    var thangDiem = kqKetQuaLanhDao.FirstOrDefault();

                    //tongHopPhieuDanhGia.ThangDiemLanhDaoDanhGiaId2 = (thangDiem != null) ? thangDiem.ThangDiemId : 0;
                    //tongHopPhieuDanhGia.NhanXetLanhDaoDanhGia2 = kqKetQuaLanhDao.Where(x => x.TieuChiId == -1).FirstOrDefault().NhanXet;

                    if (thangDiem != null)
                    {
                        var getCanbo = (from cb in _canBoRepos.GetAll().Where(x => x.Id == thangDiem.CanBoDanhGiaId)
                                        from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                        select new
                                        {
                                            HoTen = u.Surname + " " + u.Name,
                                        }).FirstOrDefault();

                        tongHopPhieuDanhGia.HoTenLanhDao = getCanbo.HoTen;
                    }

                }

                phieuDanhGiaChiTiet = (from pdgct in phieuDanhGiaChiTiet
                                       from tdg in kqTuDanhGia.Where(x => x.TieuChiId == pdgct.Id)
                                       from thdg in kqTongHopDanhGia.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                       from kqcc in kqKetQuaLanhDao.Where(x => x.TieuChiId == pdgct.Id).DefaultIfEmpty()
                                       select new PhieuDanhGiaChiTietDto
                                       {
                                           Id = pdgct.Id,
                                           Ten = pdgct.Ten,
                                           TieuChiChaId = pdgct.TieuChiChaId,
                                           DiemToiDa = pdgct.DiemToiDa,
                                           DiemToiThieu = pdgct.DiemToiThieu,
                                           DiemTuCham = tdg.Diem,
                                           DiemLanhDaoCham = (thdg != null) ? thdg.Diem : 0,
                                           TuNhanXet = tdg.NhanXet,
                                           KyHieu = pdgct.KyHieu,
                                           LichSuDiemCapDanhGia1 = (kqcc != null) ? kqcc.Diem : 0,
                                           ThuTu = pdgct.ThuTu
                                       }).ToList();

                tongHopPhieuDanhGia.PhieuDanhGiaChiTietDto = TinhTongDiemCapCha(phieuDanhGiaChiTiet);
            }
            #endregion

            return tongHopPhieuDanhGia;
        }
        public FileDto XuatWord(long phieuDanhGiaCanBoId)
        {
            // 1. get thong tin chung cua phieu danh gia
            var thongTinChung = GetThongTinChungPhieuDanhGiaCanBo(phieuDanhGiaCanBoId);

            var getThongTinTongHop = GetChiTietPhieuDanhGiaTheoPhieuDanhGiaCanBoId(phieuDanhGiaCanBoId, null, null, true);

            string templateRelativePath = _appConfiguration.GetValue<string>("Template:PhieuDanhGiaCanBo02");
            if (!string.IsNullOrWhiteSpace(thongTinChung.DuongDanPhieuDanhGia))
            {
                templateRelativePath = thongTinChung.DuongDanPhieuDanhGia;
            }

            string currentDirectory = Directory.GetCurrentDirectory();
            string templateAbsolutePath = Path.Combine(currentDirectory, templateRelativePath);
            FileDto outputFile = new FileDto("PhieuDanhGiaCanBo.docx", MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentWordprocessingmlDocument);


            IFormatProvider<RadFlowDocument> fileFormatProvider = new DocxFormatProvider();
            RadFlowDocument document = null;

            using (FileStream input = new FileStream(templateAbsolutePath, FileMode.Open))
            {
                document = fileFormatProvider.Import(input);
            }

            ThemableFontFamily DefaultFontFamily = new ThemableFontFamily(new FontFamily("Times New Roman"));

            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(document);
            editor.CharacterFormatting.FontFamily.LocalValue = DefaultFontFamily;


            editor.ReplaceText("[TEN_PHIEU]", thongTinChung.TenPhieuDanhGia);
            editor.ReplaceText("[TEN_DON_VI]", thongTinChung.TenDonVi);
            editor.ReplaceText("[TEN_QUAN_HUYEN]", thongTinChung.TenQuanHuyen ?? "");
            editor.ReplaceText("[NAM]", thongTinChung.Nam.ToString());
            editor.ReplaceText("[TIEU_DE_PHIEU_DANH_GIA]", thongTinChung.TieuDe);
            editor.ReplaceText("[MO_TA_PHIEU_DANH_GIA]", thongTinChung.GhiChu ?? "");
            editor.ReplaceText("[THANG_DANH_GIA]", $"{thongTinChung.Thang}/{thongTinChung.Nam}");
            editor.ReplaceText("[HO_TEN_CAN_BO]", thongTinChung.TenCanBo);
            editor.ReplaceText("[CHUC_VU_CAN_BO]", thongTinChung.TenChucVu);


            var tables = editor.Document.EnumerateChildrenOfType<Table>();
            var ketQuaDanhGiaTable = tables.ElementAt(1);

            int maxCell = ketQuaDanhGiaTable.GridColumnsCount;

            #region Tạo bản ghi tổng điểm
            var newObj = new PhieuDanhGiaChiTietDto();
            newObj.Id = 1000;
            newObj.KyHieu = "";
            newObj.Ten = "Tổng";
            newObj.LichSuNhanXetTuDanhgia = "";
            newObj.DiemToiDa = 100;
            newObj.LichSuDiemCapDanhGia1 = getThongTinTongHop.TongDiemCapDanhGia1;
            newObj.LichSuDiemCapDanhGia2 = getThongTinTongHop.TongDiemCapDanhGia2;
            newObj.LichSuDiemCapDanhGia3 = getThongTinTongHop.TongDiemCapDanhGia3;
            newObj.LichSuDiemCapDanhGia4 = getThongTinTongHop.TongDiemCapDanhGia4;
            getThongTinTongHop.PhieuDanhGiaChiTietDto.Add(newObj);
            #endregion

            var ktMau1C = thongTinChung.TenPhieuDanhGia.Contains("01c");
            var ktMau1B = thongTinChung.TenPhieuDanhGia.Contains("01b");

            foreach (var item in getThongTinTongHop.PhieuDanhGiaChiTietDto)
            {
                var tempRow = ketQuaDanhGiaTable.Rows[2].Clone();
                var row = ketQuaDanhGiaTable.Rows.AddTableRow();
                foreach (var tempCell in tempRow.Cells)
                {
                    var elCell = tempCell.EnumerateChildrenOfType<Run>();
                    switch (elCell.ElementAt(0).Text)
                    {
                        case "[R11]":
                            elCell.ElementAt(0).Text = item.KyHieu.ToString();
                            break;
                        case "[R12]":
                            elCell.ElementAt(0).Text = item.Ten.ToString();
                            break;
                        case "[R13]":
                            string sDiem = "";
                            if (item.DiemToiThieu == 0)
                            {
                                sDiem = item.DiemToiDa.ToString();
                            }
                            else
                            {
                                sDiem = "Từ " + item.DiemToiDa.ToString() + " đến " + item.DiemToiThieu;
                            }
                            elCell.ElementAt(0).Text = sDiem;
                            break;
                        case "[R14]":
                            elCell.ElementAt(0).Text = item.LichSuNhanXetTuDanhgia.ToString();
                            break;
                        case "[R15]":
                            if (!item.IsCapCha)
                            {

                                elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia1, 1).ToString();
                            }
                            else
                            {
                                elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia1, 1).ToString();
                            }
                            break;
                        case "[R16]":
                            if (ktMau1B)
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia3, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia3, 1).ToString();
                                }
                            }
                            else
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia2, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia2, 1).ToString();
                                }
                            }
                            break;
                        case "[R17]":
                            if (ktMau1C)
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia4, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia4, 1).ToString();
                                }
                            }
                            else
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia3, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia3, 1).ToString();
                                }
                            }
                            break;
                    }
                    row.Cells.Add(tempCell.Clone());
                }
            }

            ketQuaDanhGiaTable.Rows.RemoveAt(2);

            var tuXepLoai = new string[4] { "", "", "", "" };
            #region Thong tin ve phan xep loai            
            if (getThongTinTongHop.ThangDiemCapDanhGiaId1 > 0)
            {
                var loaiThangDiemChiTiet = getThongTinTongHop.ThangDiemCapDanhGiaId1;
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(loaiThangDiemChiTiet).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    tuXepLoai[checkLoaiThangDiem - 1] = "x";
                }

                editor.ReplaceText("[XL1]", tuXepLoai[0]);
                editor.ReplaceText("[XL2]", tuXepLoai[1]);
                editor.ReplaceText("[XL3]", tuXepLoai[2]);
                editor.ReplaceText("[XL4]", tuXepLoai[3]);
            }
            else
            {
                editor.ReplaceText("[XL1]", "");
                editor.ReplaceText("[XL2]", "");
                editor.ReplaceText("[XL3]", "");
                editor.ReplaceText("[XL4]", "");
            }

            var toChuyenMon = new string[4] { "", "", "", "" };
            if (getThongTinTongHop.ThangDiemCapDanhGiaId2 > 0)
            {
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(getThongTinTongHop.ThangDiemCapDanhGiaId2).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    toChuyenMon[checkLoaiThangDiem - 1] = "x";
                }
                editor.ReplaceText("[CM1]", toChuyenMon[0]);
                editor.ReplaceText("[CM2]", toChuyenMon[1]);
                editor.ReplaceText("[CM3]", toChuyenMon[2]);
                editor.ReplaceText("[CM4]", toChuyenMon[3]);
            }
            else
            {
                editor.ReplaceText("[CM1]", "");
                editor.ReplaceText("[CM2]", "");
                editor.ReplaceText("[CM3]", "");
                editor.ReplaceText("[CM4]", "");
            }

            var lanhDao = new string[4] { "", "", "", "" };

            long thangDiemCapDanhGiaCuoi = 0;
            if (ktMau1B == true || ktMau1C == true)
            {
                thangDiemCapDanhGiaCuoi = getThongTinTongHop.ThangDiemCapDanhGiaId4;
            }
            else
            {
                thangDiemCapDanhGiaCuoi = getThongTinTongHop.ThangDiemCapDanhGiaId3;
            }

            if (thangDiemCapDanhGiaCuoi > 0)
            {
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(thangDiemCapDanhGiaCuoi).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    lanhDao[checkLoaiThangDiem - 1] = "x";
                }

                editor.ReplaceText("[LD1]", lanhDao[0]);
                editor.ReplaceText("[LD2]", lanhDao[1]);
                editor.ReplaceText("[LD3]", lanhDao[2]);
                editor.ReplaceText("[LD4]", lanhDao[3]);
            }
            else
            {
                editor.ReplaceText("[LD1]", "");
                editor.ReplaceText("[LD2]", "");
                editor.ReplaceText("[LD3]", "");
                editor.ReplaceText("[LD4]", "");
            }
            #endregion

            #region tách nhận xét của phó chủ tich
            var nxc2 = "";
            if (!string.IsNullOrWhiteSpace(getThongTinTongHop.NhanXetCapDanhGia2))
            {
                var lstNhanXet = getThongTinTongHop.NhanXetCapDanhGia2.Split('\n').ToList();
                if (lstNhanXet.Count >= 1)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", lstNhanXet[0]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", "");
                }
                if (lstNhanXet.Count >= 2)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", lstNhanXet[1]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", "");
                }
                if (lstNhanXet.Count >= 3)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", lstNhanXet[2]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", "");
                }
            }
            else
            {
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", "");
            }
            #endregion

            #region Nhận xét đánh giá câp 3

            if (!string.IsNullOrWhiteSpace(getThongTinTongHop.NhanXetCapDanhGia3))
            {
                var lstNhanXet = getThongTinTongHop.NhanXetCapDanhGia3.Split('\n').ToList();
                if (lstNhanXet.Count >= 1)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", lstNhanXet[0]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", "");
                }
                if (lstNhanXet.Count >= 2)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_2]", lstNhanXet[1]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_S_2O_2]", "");
                }
                if (lstNhanXet.Count >= 3)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", lstNhanXet[2]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", "");
                }
            }
            else
            {
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_2]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", "");
            }
            #endregion

            #region get ten lanh dao cac cap
            string tenLanhDaoCap1 = "";
            string tenLanhDaoCap2 = "";
            string tenLanhDaoCap3 = "";

            var lsCanBo = _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
            && x.KetQuaTongHop == true).OrderBy(x => x.CapDoDanhGia);

            int index = 1;
            foreach (var item in lsCanBo)
            {
                if (item.CanBoDanhGiaId > 0)
                {
                    var getCanbo = (from cb in _canBoRepos.GetAll().Where(x => x.Id == item.CanBoDanhGiaId)
                                    from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                    select new
                                    {
                                        HoTen = u.Surname + " " + u.Name,
                                    }).FirstOrDefault();
                    if (index == 1)
                    {
                        tenLanhDaoCap1 = getCanbo.HoTen;
                    }
                    else if (index == 2)
                    {
                        tenLanhDaoCap2 = getCanbo.HoTen;
                    }
                    else if (index == 3)
                    {
                        tenLanhDaoCap3 = getCanbo.HoTen;
                    }
                }
                index++;
            }
            #endregion
            //editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON]", nxc2 ?? "");
            editor.ReplaceText("[NHAN_XET_LANH_DAO]", getThongTinTongHop.NhanXetCapDanhGia3 ?? "");
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_1]", tenLanhDaoCap1);
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_2]", tenLanhDaoCap2);
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_3]", tenLanhDaoCap3);

            byte[] renderedBytes = null;

            using (MemoryStream ms = new MemoryStream())
            {
                fileFormatProvider.Export(document, ms);
                renderedBytes = ms.ToArray();
            }

            _tempFileCacheManager.SetFile(outputFile.FileToken, renderedBytes);

            return outputFile;
        }

        public FileDto XuatPDF(long phieuDanhGiaCanBoId)
        {

            FontsRepository.RegisterFont(new FontFamily("Times New Roman"), FontStyles.Normal, FontWeights.Normal,
                File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "./Resources/Fonts/TIMES.TTF")));
            FontsRepository.RegisterFont(new FontFamily("Times New Roman"), FontStyles.Normal, FontWeights.Bold,
                File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "./Resources/Fonts/TIMESBD.TTF")));
            FontsRepository.RegisterFont(new FontFamily("Times New Roman"), FontStyles.Italic, FontWeights.Bold,
                File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "./Resources/Fonts/TIMESBI.TTF")));
            FontsRepository.RegisterFont(new FontFamily("Times New Roman"), FontStyles.Italic, FontWeights.Normal,
                File.ReadAllBytes(Path.Combine(Directory.GetCurrentDirectory(), "./Resources/Fonts/TIMESI.TTF")));

            // 1. get thong tin chung cua phieu danh gia
            var thongTinChung = GetThongTinChungPhieuDanhGiaCanBo(phieuDanhGiaCanBoId);

            var getThongTinTongHop = GetChiTietPhieuDanhGiaTheoPhieuDanhGiaCanBoId(phieuDanhGiaCanBoId, null, null, true);

            string templateRelativePath = _appConfiguration.GetValue<string>("Template:PhieuDanhGiaCanBo02");
            if (!string.IsNullOrWhiteSpace(thongTinChung.DuongDanPhieuDanhGia))
            {
                templateRelativePath = thongTinChung.DuongDanPhieuDanhGia;
            }

            string currentDirectory = Directory.GetCurrentDirectory();
            string templateAbsolutePath = Path.Combine(currentDirectory, templateRelativePath);
            FileDto outputFile = new FileDto("PhieuDanhGiaCanBo.pdf", MimeTypeNames.ApplicationPdf);

            IFormatProvider<RadFlowDocument> fileFormatProvider = new DocxFormatProvider();
            PdfFormatProvider convertFormatProvider = new PdfFormatProvider();

            RadFlowDocument document = null;

            using (FileStream input = new FileStream(templateAbsolutePath, FileMode.Open))
            {
                document = fileFormatProvider.Import(input);
            }

            RadFlowDocumentEditor editor = new RadFlowDocumentEditor(document);

            editor.ReplaceText("[TEN_PHIEU]", thongTinChung.TenPhieuDanhGia);
            editor.ReplaceText("[TEN_DON_VI]", thongTinChung.TenDonVi);
            editor.ReplaceText("[TEN_QUAN_HUYEN]", thongTinChung.TenQuanHuyen ?? "");
            editor.ReplaceText("[NAM]", thongTinChung.Nam.ToString());
            editor.ReplaceText("[TIEU_DE_PHIEU_DANH_GIA]", thongTinChung.TieuDe);
            editor.ReplaceText("[MO_TA_PHIEU_DANH_GIA]", thongTinChung.GhiChu ?? "");
            editor.ReplaceText("[THANG_DANH_GIA]", $"{thongTinChung.Thang}/{thongTinChung.Nam}");
            editor.ReplaceText("[HO_TEN_CAN_BO]", thongTinChung.TenCanBo);
            editor.ReplaceText("[CHUC_VU_CAN_BO]", thongTinChung.TenChucVu);

            var tables = editor.Document.EnumerateChildrenOfType<Table>();
            var ketQuaDanhGiaTable = tables.ElementAt(1);
            int maxCell = ketQuaDanhGiaTable.GridColumnsCount;

            #region Tạo bản ghi tổng điểm
            var newObj = new PhieuDanhGiaChiTietDto();
            newObj.Id = 1000;
            newObj.KyHieu = "";
            newObj.Ten = "Tổng";
            newObj.LichSuNhanXetTuDanhgia = "";
            newObj.DiemToiDa = 100;
            newObj.LichSuDiemCapDanhGia1 = getThongTinTongHop.TongDiemCapDanhGia1;
            newObj.LichSuDiemCapDanhGia2 = getThongTinTongHop.TongDiemCapDanhGia2;
            newObj.LichSuDiemCapDanhGia3 = getThongTinTongHop.TongDiemCapDanhGia3;
            newObj.LichSuDiemCapDanhGia4 = getThongTinTongHop.TongDiemCapDanhGia4;
            getThongTinTongHop.PhieuDanhGiaChiTietDto.Add(newObj);
            #endregion
            var ktMau1C = thongTinChung.TenPhieuDanhGia.Contains("01c");
            var ktMau1B = thongTinChung.TenPhieuDanhGia.Contains("01b");

            foreach (var item in getThongTinTongHop.PhieuDanhGiaChiTietDto)
            {
                var row = ketQuaDanhGiaTable.Rows[2].Clone();
                foreach (var cell in row.Cells)
                {
                    var elCell = cell.EnumerateChildrenOfType<Run>();
                    var elCellText = elCell.ElementAt(0).Text;
                    switch (elCellText)
                    {
                        case "[R11]":
                            elCell.ElementAt(0).Text = item.KyHieu.ToString();
                            break;
                        case "[R12]":
                            elCell.ElementAt(0).Text = item.Ten.ToString();
                            break;
                        case "[R13]":
                            string sDiem = "";
                            if (item.DiemToiThieu == 0)
                            {
                                sDiem = item.DiemToiDa.ToString();
                            }
                            else
                            {
                                sDiem = "Từ " + item.DiemToiDa.ToString() + " đến " + item.DiemToiThieu;
                            }
                            elCell.ElementAt(0).Text = sDiem;
                            break;
                        case "[R14]":
                            elCell.ElementAt(0).Text = item.LichSuNhanXetTuDanhgia.ToString();
                            break;
                        case "[R15]":
                            if (!item.IsCapCha)
                            {

                                elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia1, 1).ToString();
                            }
                            else
                            {
                                elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia1, 1).ToString();
                            }
                            break;
                        case "[R16]":
                            if (ktMau1B)
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia3, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia3, 1).ToString();
                                }
                            }
                            else
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia2, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia2, 1).ToString();
                                }
                            }
                            break;
                        case "[R17]":
                            if (ktMau1C)
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia4, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia4, 1).ToString();
                                }
                            }
                            else
                            {
                                if (!item.IsCapCha)
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuDiemCapDanhGia3, 1).ToString();
                                }
                                else
                                {
                                    elCell.ElementAt(0).Text = Math.Round((decimal)item.LichSuTongDiemCapDanhGia3, 1).ToString();
                                }
                            }
                            break;
                    }

                }
                ketQuaDanhGiaTable.Rows.Add(row);
            }
            ketQuaDanhGiaTable.Rows.RemoveAt(2);

            var tuXepLoai = new string[4] { "", "", "", "" };
            #region Thong tin ve phan xep loai            
            if (getThongTinTongHop.ThangDiemCapDanhGiaId1 > 0)
            {
                var loaiThangDiemChiTiet = getThongTinTongHop.ThangDiemCapDanhGiaId1;
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(loaiThangDiemChiTiet).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    tuXepLoai[checkLoaiThangDiem - 1] = "x";
                }

                editor.ReplaceText("[XL1]", tuXepLoai[0]);
                editor.ReplaceText("[XL2]", tuXepLoai[1]);
                editor.ReplaceText("[XL3]", tuXepLoai[2]);
                editor.ReplaceText("[XL4]", tuXepLoai[3]);
            }
            else
            {
                editor.ReplaceText("[XL1]", "");
                editor.ReplaceText("[XL2]", "");
                editor.ReplaceText("[XL3]", "");
                editor.ReplaceText("[XL4]", "");
            }

            var toChuyenMon = new string[4] { "", "", "", "" };
            if (getThongTinTongHop.ThangDiemCapDanhGiaId2 > 0)
            {
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(getThongTinTongHop.ThangDiemCapDanhGiaId2).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    toChuyenMon[checkLoaiThangDiem - 1] = "x";
                }
                editor.ReplaceText("[CM1]", toChuyenMon[0]);
                editor.ReplaceText("[CM2]", toChuyenMon[1]);
                editor.ReplaceText("[CM3]", toChuyenMon[2]);
                editor.ReplaceText("[CM4]", toChuyenMon[3]);
            }
            else
            {
                editor.ReplaceText("[CM1]", "");
                editor.ReplaceText("[CM2]", "");
                editor.ReplaceText("[CM3]", "");
                editor.ReplaceText("[CM4]", "");
            }

            var lanhDao = new string[4] { "", "", "", "" };
            if (getThongTinTongHop.ThangDiemCapDanhGiaId3 > 0)
            {
                var checkLoaiThangDiem = _thangDiemChiTietRepos.Get(getThongTinTongHop.ThangDiemCapDanhGiaId3).LoaiThangDiemChiTiet;
                if (checkLoaiThangDiem > 0)
                {
                    lanhDao[checkLoaiThangDiem - 1] = "x";
                }

                editor.ReplaceText("[LD1]", lanhDao[0]);
                editor.ReplaceText("[LD2]", lanhDao[1]);
                editor.ReplaceText("[LD3]", lanhDao[2]);
                editor.ReplaceText("[LD4]", lanhDao[3]);
            }
            else
            {
                editor.ReplaceText("[LD1]", "");
                editor.ReplaceText("[LD2]", "");
                editor.ReplaceText("[LD3]", "");
                editor.ReplaceText("[LD4]", "");
            }
            #endregion

            #region tách nhận xét của phó chủ tich
            var nxc2 = "";
            if (!string.IsNullOrWhiteSpace(getThongTinTongHop.NhanXetCapDanhGia2))
            {
                var lstNhanXet = getThongTinTongHop.NhanXetCapDanhGia2.Split('\n').ToList();
                if (lstNhanXet.Count >= 1)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", lstNhanXet[0]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", "");
                }
                if (lstNhanXet.Count >= 2)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", lstNhanXet[1]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", "");
                }
                if (lstNhanXet.Count >= 3)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", lstNhanXet[2]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", "");
                }
            }
            else
            {
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_1]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_2]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_1_3]", "");
            }
            #endregion

            #region Nhận xét đánh giá câp 3

            if (!string.IsNullOrWhiteSpace(getThongTinTongHop.NhanXetCapDanhGia3))
            {
                var lstNhanXet = getThongTinTongHop.NhanXetCapDanhGia3.Split('\n').ToList();
                if (lstNhanXet.Count >= 1)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", lstNhanXet[0]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", "");
                }
                if (lstNhanXet.Count >= 2)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_2]", lstNhanXet[1]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_S_2O_2]", "");
                }
                if (lstNhanXet.Count >= 3)
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", lstNhanXet[2]);
                }
                else
                {
                    editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", "");
                }
            }
            else
            {
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_1]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_2]", "");
                editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON_SO_2_3]", "");
            }
            #endregion

            #region get ten lanh dao cac cap
            string tenLanhDaoCap1 = "";
            string tenLanhDaoCap2 = "";
            string tenLanhDaoCap3 = "";

            var lsCanBo = _lichSuKetQuaDanhGiaChiTietRepos.GetAll().Where(x => x.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
            && x.CapDanhGia == (int)AppConsts.CapDanhGia.CapTren
            && x.KetQuaTongHop == true).OrderBy(x => x.CapDoDanhGia);

            int index = 1;
            foreach (var item in lsCanBo)
            {
                if (item.CanBoDanhGiaId > 0)
                {
                    var getCanbo = (from cb in _canBoRepos.GetAll().Where(x => x.Id == item.CanBoDanhGiaId)
                                    from u in UserManager.Users.Where(x => x.Id == cb.UserId)
                                    select new
                                    {
                                        HoTen = u.Surname + " " + u.Name,
                                    }).FirstOrDefault();
                    if (index == 1)
                    {
                        tenLanhDaoCap1 = getCanbo.HoTen;
                    }
                    else if (index == 2)
                    {
                        tenLanhDaoCap2 = getCanbo.HoTen;
                    }
                    else if (index == 3)
                    {
                        tenLanhDaoCap3 = getCanbo.HoTen;
                    }
                }
                index++;
            }
            #endregion
            //editor.ReplaceText("[NHAN_XET_TO_CHUYEN_MON]", nxc2 ?? "");
            editor.ReplaceText("[NHAN_XET_LANH_DAO]", getThongTinTongHop.NhanXetCapDanhGia3 ?? "");
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_1]", tenLanhDaoCap1);
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_2]", tenLanhDaoCap2);
            editor.ReplaceText("[HO_TEN_CAN_BO_CAP_3]", tenLanhDaoCap3);

            byte[] renderedBytes = null;

            using (MemoryStream ms = new MemoryStream())
            {
                convertFormatProvider.Export(document, ms);
                renderedBytes = ms.ToArray();
            }

            _tempFileCacheManager.SetFile(outputFile.FileToken, renderedBytes);

            return outputFile;
        }


    }
}
