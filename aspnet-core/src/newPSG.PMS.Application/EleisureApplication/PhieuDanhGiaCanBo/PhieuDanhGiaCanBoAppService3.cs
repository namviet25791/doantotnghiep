﻿using Abp;
using Abp.AspNetZeroCore.Net;
using Abp.Authorization;
using Abp.Configuration;
using Abp.Domain.Uow;
using Abp.Net.Mail;
using Abp.Notifications;
using Abp.Runtime.Security;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Configuration;
using newPSG.PMS.Configuration.Host.Dto;
using newPSG.PMS.Dto;
using newPSG.PMS.EleisureApplicationShared;
using newPSG.PMS.EleisureApplicationShared.PhieuDanhGia.Dto.PhieuDanhGiaCanBo.Export;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telerik.Documents.Common.Model;
using Telerik.Documents.Core.Fonts;
using Telerik.Windows.Documents.Common.FormatProviders;
using Telerik.Windows.Documents.Fixed.Model.Fonts;
using Telerik.Windows.Documents.Flow.FormatProviders.Docx;
using Telerik.Windows.Documents.Flow.FormatProviders.Pdf;
using Telerik.Windows.Documents.Flow.Model;
using Telerik.Windows.Documents.Flow.Model.Editing;

namespace newPSG.PMS.EleisureApplication
{
    public partial class PhieuDanhGiaCanBoAppService
    {
        public async Task TaoThongBaoChoCanbo(long phieuDanhGiaCanBoId, bool isEdit, int capDanhGia)
        {
            string msg = string.Empty;
            var ngDangDanhGiaId = AbpSession.UserId;

            var getPhieuDanhGiaCanBo = (from pdgcb in _phieuDanhGiaCanBoRepos.GetAll()
                                        from cb in _canBoRepos.GetAll().Where(x => x.Id == pdgcb.CanBoId)
                                        where pdgcb.Id == phieuDanhGiaCanBoId
                                        select new
                                        {
                                            UserId = cb.UserId,
                                            CanBoId = cb.Id,
                                            Thang = pdgcb.Thang,
                                            Nam = pdgcb.Nam,
                                        }).FirstOrDefault();

            var canBoGuiPhieu = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == getPhieuDanhGiaCanBo.UserId);
            var tenCanBoGuiPhieu = canBoGuiPhieu.Surname + " " + canBoGuiPhieu.Name;

            var getThongTinCanBoDanhGia = (from tt in _thongTinDanhGiaKeTiepTempRepos.GetAll()
                                           from cb in _canBoRepos.GetAll().Where(x => x.Id == tt.CanBoDanhGiaKeTiepId)
                                           where tt.PhieuDanhGiaCanBoId == phieuDanhGiaCanBoId
                                           select new
                                           {
                                               Id = tt.Id,
                                               TrangThai = tt.TrangThai,
                                               UserId = cb.UserId,
                                               ChucVuLanhDaoId = tt.ChucVuId
                                           }).ToList();


            if (isEdit)
            {
                string tenCanboDangDanhGia = "";

                if (capDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                {
                    // gửi thông báo cho cán bộ gửi phiếu
                    var ngDangDanhGia = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == ngDangDanhGiaId);
                    tenCanboDangDanhGia = ngDangDanhGia.Surname + " " + ngDangDanhGia.Name;
                    msg = "<b>" + tenCanboDangDanhGia + "</b> đã thay đổi kết quả phiếu tự đánh giá của bạn.";
                    LuuThongBao(canBoGuiPhieu, msg, phieuDanhGiaCanBoId, 0, (int)AppConsts.EmailSendTo.GuiToiCaNhan);

                    #region get thong tin can bo da danh gia
                    var lstThongTinCanBoDangDanhGia = getThongTinCanBoDanhGia.Where(x => x.TrangThai == 1).ToList();
                    foreach (var cb in lstThongTinCanBoDangDanhGia)
                    {
                        var ngNhan = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == cb.UserId);
                        msg = "<b>" + tenCanboDangDanhGia + "</b> đã thay đổi kết quả đánh giá phiếu tự đánh giá của cán bộ: <b>" + tenCanBoGuiPhieu + "</b>";
                        LuuThongBao(ngNhan, msg, phieuDanhGiaCanBoId, cb.ChucVuLanhDaoId, (int)AppConsts.EmailSendTo.GuiToiLanhDao);
                    }
                    #endregion
                }
                else
                {
                    #region gui thong bao cho can bo danh gia ke tiep
                    var lstCanBoDanhGiaKeTiep = getThongTinCanBoDanhGia.Where(x => x.TrangThai == 0).ToList();
                    foreach (var cb in lstCanBoDanhGiaKeTiep)
                    {
                        var ngNhan = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == cb.UserId);
                        msg = "<b> " + tenCanBoGuiPhieu + " </b> đã gửi phiếu tự đánh giá tháng: " + getPhieuDanhGiaCanBo.Thang + "/" + getPhieuDanhGiaCanBo.Nam;
                        LuuThongBao(ngNhan, msg, phieuDanhGiaCanBoId, cb.ChucVuLanhDaoId, (int)AppConsts.EmailSendTo.GuiToiLanhDao);
                    }
                    #endregion
                }
            }
            else
            {
                string tenCanboDangDanhGia = "";
                // đây là thêm mới
                if (capDanhGia == (int)AppConsts.CapDanhGia.CapTren)
                {
                    // gửi thông báo cho cán bộ gửi phiếu
                    msg = "<b>" + tenCanboDangDanhGia + "</b> đã đánh giá phiếu tự đánh giá của bạn.";
                    LuuThongBao(canBoGuiPhieu, msg, phieuDanhGiaCanBoId, 0, (int)AppConsts.EmailSendTo.GuiToiCaNhan);


                    #region get thong tin can bo da danh gia
                    var lstThongTinCanBoDangDanhGia = getThongTinCanBoDanhGia.Where(x => x.TrangThai == 1).ToList();
                    foreach (var cb in lstThongTinCanBoDangDanhGia)
                    {
                        var ngNhan = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == cb.UserId);
                        msg = "<b>" + tenCanboDangDanhGia + "</b> đã đánh giá kết quả phiếu tự đánh giá của cán bộ: <b>" + tenCanBoGuiPhieu + "</b>";
                        LuuThongBao(ngNhan, msg, phieuDanhGiaCanBoId, cb.ChucVuLanhDaoId, (int)AppConsts.EmailSendTo.GuiToiLanhDao);
                    }
                    #endregion
                }

                #region gui thong bao cho can bo danh gia ke tiep
                var lstCanBoDanhGiaKeTiep = getThongTinCanBoDanhGia.Where(x => x.TrangThai == 0).ToList();
                foreach (var cb in lstCanBoDanhGiaKeTiep)
                {
                    var ngNhan = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == cb.UserId);
                    msg = "<b> " + tenCanBoGuiPhieu + " </b> đã gửi phiếu tự đánh giá tháng: " + getPhieuDanhGiaCanBo.Thang + "/" + getPhieuDanhGiaCanBo.Nam;
                    LuuThongBao(ngNhan, msg, phieuDanhGiaCanBoId, cb.ChucVuLanhDaoId, (int)AppConsts.EmailSendTo.GuiToiLanhDao);
                }
                #endregion

            }
        }

        public void LuuThongBao(User ngNhan, string msg, long phieuDanhGiaCanBoId, long chucVuLanhDaoId, int emailSendTo)
        {
            var isSendMailNotification = SettingManager.GetSettingValue<bool>(AppSettings.TenantManagement.SendMailNotificationEnabled);
            var domain = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Domain, AbpSession.GetTenantId());

            string url = string.Empty;

            if (emailSendTo == (int)AppConsts.EmailSendTo.GuiToiCaNhan)
            {
                url = "/app/admin/phieu-danh-gia-ca-nhan?id=" + phieuDanhGiaCanBoId;

                _appNotifier.ThongBaoDanhGiaCaNhan(ngNhan, msg, phieuDanhGiaCanBoId);
            }
            else if (emailSendTo == (int)AppConsts.EmailSendTo.GuiToiLanhDao)
            {
                url = "/app/admin/danh-gia-nhan-vien?id=" + phieuDanhGiaCanBoId + "&cv=" + chucVuLanhDaoId;
                _appNotifier.ThongBaoPhieuDanhGiaNhanVien(ngNhan, msg, phieuDanhGiaCanBoId, chucVuLanhDaoId);
            }

            var urlString = domain + url;
            // send email
            if (isSendMailNotification == true && !string.IsNullOrWhiteSpace(url))
            {
                var input = new SendEmailInput();
                input.EmailAddress = ngNhan.EmailAddress;
                input.BodyTemplate = new List<KeyValuePair<string, string>>();
                var key1 = new KeyValuePair<string, string>("<%FullName%>", ngNhan.Surname + " " + ngNhan.Name);
                var key2 = new KeyValuePair<string, string>("<%NoiDung%>", msg);
                var key3 = new KeyValuePair<string, string>("<%LinkChiTiet%>", urlString);
                input.BodyTemplate.Add(key1);
                input.BodyTemplate.Add(key2);
                input.BodyTemplate.Add(key3);

                string template = "./TemplateBaoCao/email-template/Notification.html";
                SendEmail(input, template, 0);
            }
            //AsyncHelper.RunSync(() => _userEmailer.TryToSendNotificationMessageMail(user, msg));
        }


        #region Send Email

        [AbpAllowAnonymous]
        public void SendEmail(SendEmailInput input, string template, int nTenantId)
        {
            string body = UtilityExtensions.GetContentWidthTemplate(template, input.BodyTemplate);

            try
            {
                int tenantId = (nTenantId == 0) ? AbpSession.GetTenantId() : nTenantId;


                var host = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Host, tenantId);
                var port = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Port, tenantId);
                var sendfrom = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.UserName, tenantId);

                var smtpPassword = SettingManager.GetSettingValueForTenant(EmailSettingNames.Smtp.Password,
                   tenantId);
                var emailpass = SimpleStringCipher.Instance.Decrypt(smtpPassword);

                var ssl = SettingManager.GetSettingValueForTenant<bool>(EmailSettingNames.Smtp.EnableSsl, tenantId);

                var client = new SmtpClient(host, int.Parse(port));
                client.EnableSsl = ssl;
                client.Credentials = new System.Net.NetworkCredential(sendfrom, emailpass);

                var message = new MailMessage();
                message.From = new MailAddress(sendfrom, "e-KPI");
                message.Subject = "Thông báo từ e-KPI";

                message.To.Add(new MailAddress(input.EmailAddress));
                message.Body = body;
                message.IsBodyHtml = true;
                message.BodyEncoding = Encoding.UTF8;

                var mailThread = new Thread(new ThreadStart(() =>
                {
                    client.SendMailAsync(message);
                }));

                mailThread.Start();
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
        #endregion

        [AbpAllowAnonymous]
        public void NhacNhoGuiPhieuDanhGiaCaNhan()
        {
            var input = new SendEmailInput();
            string msg = "Bạn có 01 phiếu đánh giá tháng 05/2020 chưa thực hiện. Vui lòng truy cập hệ thống E-KPI tại địa chỉ " +
                "https://{tenancy_name}.e-kpi.vn và thực hiện đánh giá trước ngày [HẠN ĐÁNH GIÁ].";
            input.EmailAddress = "trangqt2908@gmail.com";
            input.BodyTemplate = new List<KeyValuePair<string, string>>();
            var key1 = new KeyValuePair<string, string>("{TEN_CAN_BO}", "Heo xinh");
            var key2 = new KeyValuePair<string, string>("{Body}", msg);
            var key3 = new KeyValuePair<string, string>("{TENANCY_NAME}", "danphuong");

            input.BodyTemplate.Add(key1);
            input.BodyTemplate.Add(key2);
            input.BodyTemplate.Add(key3);

            string template = "./TemplateBaoCao/email-template/nhac_nho_danh_gia.html";
            SendEmail(input, template, 20);

            //var getAllTenantGuiNhacNho = _settingRepos.GetAll().Where(x => x.Name == AppSettings.TenantManagement.NhacNhoGuiPhieuDanhGiaEnabled
            //&& x.Value == "true");

            //foreach (var item in getAllTenantGuiNhacNho)
            //{
            //    var sNgayKetThucDanhGia = _settingRepos.GetAll().Where(x => x.TenantId == item.TenantId &&
            //    x.Name == AppSettings.TenantManagement.ThoiGianKetThucDanhGia).FirstOrDefault().Value;

            //    var sGuiNhacNhoTruocNgayKetThuc = _settingRepos.GetAll().Where(x => x.TenantId == item.TenantId &&
            //    x.Name == AppSettings.TenantManagement.GuiNhacNhoTruocNgayKetThuc).FirstOrDefault().Value;

            //    int nNgayKetThucDanhGia = 0; int.TryParse(sNgayKetThucDanhGia, out nNgayKetThucDanhGia);
            //    int nGuiNhacNhoTruocNgayKetThuc = 0; int.TryParse(sGuiNhacNhoTruocNgayKetThuc, out nGuiNhacNhoTruocNgayKetThuc);


            //    var ngayHienTai = DateTime.Now.Date;

            //    var ngayBatDauDg = new DateTime(ngayHienTai.Year, ngayHienTai.Month, 1).Date;
            //    var ngayKetThucDg = new DateTime(ngayHienTai.Year, ngayHienTai.Month, nNgayKetThucDanhGia).Date;

            //    double khoangCachNgayKetThuc = (ngayKetThucDg - ngayHienTai).TotalDays;
            //    if (khoangCachNgayKetThuc == nGuiNhacNhoTruocNgayKetThuc)
            //    {
            //        GuiEmailNhacNho(ngayKetThucDg);
            //    }
            //}
        }

        public void GuiEmailNhacNho(DateTime ngayKetThucDg)
        {
            //var dateNow = DateTime.Now;
            //var thangDanhGia = new DateTime(dateNow.Year, dateNow.Month - 1, 1);

            //var dsPhieuDaGuiThangNay = _phieuDanhGiaCanBoRepos.GetAll().Where(x => x.Thang == thangDanhGia.Month && x.Nam == thangDanhGia.Year)
            //    .Select(x => new
            //    {
            //        CanBoId = x.CanBoId,
            //        ChucVuId = x.ChucVuId
            //    });

            //var lstEmail = (from cb in _canBoRepos.GetAll()
            //                from u in UserManager.Users.Where(x => x.Id == cb.UserId)
            //                from cbcv in _canBoChucVuRepos.GetAll().Where(x => x.CanBoId == cb.Id)
            //                where cb.DonViId > 0 && !dsPhieuDaGuiThangNay.Any(d => d.ChucVuId == cbcv.ChucVuId && d.CanBoId == cb.Id)
            //                select new
            //                {
            //                    HoVaTen = u.Surname + " " + u.Name,
            //                    EmailAddress = u.EmailAddress
            //                }).ToList();

            //var tenancyName = TenantManager.Tenants.FirstOrDefault(x => x.Id == AbpSession.TenantId.Value).TenancyName;

            //string msg = "Bạn có 01 phiếu đánh giá tháng " + thangDanhGia.ToString("MM/yyyy") + "chưa thực hiện." +
            //    " Vui lòng truy cập hệ thống E-KPI tại địa chỉ https://" + tenancyName + ".e-kpi.vn và thực hiện đánh giá trước ngày " + ngayKetThucDg + ".";

            //foreach (var item in lstEmail)
            //{
            //    var input = new SendEmailInput();
            //    input.BodyTemplate = new List<KeyValuePair<string, string>>();
            //    var key1 = new KeyValuePair<string, string>("{TEN_CAN_BO}", item.HoVaTen);
            //    var key2 = new KeyValuePair<string, string>("{Body}", msg);
            //    var key3 = new KeyValuePair<string, string>("{TENANCY_NAME}", tenancyName);

            //    input.BodyTemplate.Add(key1);
            //    input.BodyTemplate.Add(key2);
            //    input.BodyTemplate.Add(key3);

            //    string template = "./TemplateBaoCao/email-template/nhac_nho_danh_gia.html";
            //    SendEmail(input, template);
            //}



        }

    }
}
