﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using newPSG.PMS.Authorization.Users;
using newPSG.PMS.Notifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace newPSG.PMS.EleisureApplication
{
    interface IDemoThongBaoAppService: IApplicationService
    {
        Task GuiThongBao();
    }
    public class DemoThongBaoAppService: PMSAppServiceBase
    {
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<User, long> _userRepos;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public DemoThongBaoAppService(IAppNotifier appNotifier,
            IRepository<User, long> userRepos,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _appNotifier = appNotifier;
                _userRepos = userRepos;
            _unitOfWorkManager = unitOfWorkManager;
        }
        public async Task GuiThongBao()
        {

            using (_unitOfWorkManager.Current.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user1 = _userRepos.Get(127);// userName : 0949716699
                var user2 = _userRepos.Get(128);// user Name: 0983691420
                Random rd = new Random();
                await _appNotifier.DemoThongBao(user1, "Nguyễn Văn Việt", rd.Next(1, 100), rd.Next(1, 100));
                await _appNotifier.DemoThongBao(user2, "Nguyễn Văn Việt", rd.Next(1, 100), rd.Next(1, 100));
            }

           
        }
    }
}
