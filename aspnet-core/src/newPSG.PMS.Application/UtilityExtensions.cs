﻿using Stimulsoft.System.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace newPSG.PMS
{
    public class UtilityExtensions
    {
        public static string GetContentWidthTemplate(string tempPath, List<KeyValuePair<string, string>> keyValueReplace)
        {
            try
            {
                //string filepath = HttpContext.Current.Server.MapPath(tempPath);
                //string str2 = string.Empty;
                //if (HttpContext.Current.Cache[filepath] != null)
                //{
                //    str2 = HttpContext.Current.Cache[filepath].ToString();
                //}
                //else
                //{
                //    str2 = ReadFileContent(filepath);
                //    HttpContext.Current.Cache[filepath] = str2;
                //}

                string filepath = tempPath;
                string str2 = string.Empty;
                str2 = ReadFileContent(filepath);

                foreach (KeyValuePair<string, string> pair in keyValueReplace)
                {
                    try
                    {
                        str2 = str2.Replace(pair.Key, pair.Value);
                    }
                    catch (Exception)
                    {
                    }
                }
                return str2;
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public static string ReadFileContent(string filepath)
        {
            string str;
            try
            {
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(filepath);
                    str = reader.ReadToEnd();
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                str = "";
            }
            return str;
        }
    }
}
