import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import { DanhMucChungServiceProxy } from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
@Component({
    selector: "shared-ui-tinh-combo",
    template: `
     <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        [disabled]="isDisable"
                        ></dx-select-box>
    `
})

export class TinhComboComponent implements OnInit {
    @Input() item: any;
    @Input() isDisable = false;
    @Output() itemChange = new EventEmitter();
    @Output() onChange = new EventEmitter();

    filterItem: any = {};

    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];

    constructor(private _danhMucChungService: DanhMucChungServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }
    getDataOptions() {
        this._danhMucChungService.getTinhByFilter("", "", 1000, 0)
            .subscribe(result => {
                this.lstData = result.items;
            });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
            this.onChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
            this.onChange.emit(0);
        }
    }
}
