import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { KhoiDonViComboComponent } from "./khoi-don-vi-combo.component";
import { Select2Module } from "ng2-select2";
import { FormsModule } from "@angular/forms";
import { TinhComboComponent } from "./tinh-combo.component";
import { HuyenComboComponent } from "./huyen-combo.component";
import { XaComboComponent } from "./xa-combo.component";
import { LoaiDonViComboComponent } from "./loai-don-vi-combo.component";
import { CapDonViComboComponent } from "./cap-don-vi-combo.component";
import { PhieuDanhGiaComboComponent } from "./phieu-danh-gia-combo.component";
import { ChucVuComboComponent } from "./chuc-vu-combo.component";
import { DonViComboComponent } from "./don-vi-combo.component";
import { MaBieuMauBaoCaoComboComponent } from "./ma-bieu-mau-bao-cao-combo.component";
import { NamComboComponent } from "./nam-combo.component";
import { CanBoChucVuComboComponent } from "./chuc-vu-can-bo-combo.component";
import { BaoCaoChucVuCauHinhComboComponent } from "./bao-cao-chuc-vu-cau-hinh-combo.component";
import { LoaiThoiGianComboComponent } from "./loai-thoi-gian.component";
import { ThangDiemComboComponent } from "./thang-diem-combo.component";
import { DxDateBoxModule, DxSelectBoxModule } from 'devextreme-angular';
import { LoaiHopDongComboComponent } from "./loai-hop-dong-combo.component";
import { ChucVuCaNhanComboComponent } from "./chuc-vu-ca-nhan-combo.component";
@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        Select2Module,
        DxDateBoxModule,
        DxSelectBoxModule
    ],
    declarations: [
        KhoiDonViComboComponent,
        TinhComboComponent,
        HuyenComboComponent,
        XaComboComponent,
        LoaiDonViComboComponent,
        CapDonViComboComponent,
        PhieuDanhGiaComboComponent,
        ChucVuComboComponent,
        DonViComboComponent,
        MaBieuMauBaoCaoComboComponent,
        NamComboComponent,
        CanBoChucVuComboComponent,
        BaoCaoChucVuCauHinhComboComponent,
        LoaiThoiGianComboComponent,
        ThangDiemComboComponent,
        LoaiHopDongComboComponent,
        ChucVuCaNhanComboComponent
    ],
    providers: [],
    exports: [
        KhoiDonViComboComponent,
        TinhComboComponent,
        HuyenComboComponent,
        XaComboComponent,
        LoaiDonViComboComponent,
        CapDonViComboComponent,
        PhieuDanhGiaComboComponent,
        ChucVuComboComponent,
        DonViComboComponent,
        MaBieuMauBaoCaoComboComponent,
        NamComboComponent,
        CanBoChucVuComboComponent,
        BaoCaoChucVuCauHinhComboComponent,
        LoaiThoiGianComboComponent,
        ThangDiemComboComponent,
        LoaiHopDongComboComponent,
        ChucVuCaNhanComboComponent
    ]
})
export class SharedUiComboBoxModule { }
