import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import { LoaiDonViServiceProxy } from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
@Component({
    selector: "shared-ui-loai-don-vi-combo",
    template: `
        <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        ></dx-select-box>
        </div>
    `
})
export class LoaiDonViComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];
    constructor(private _loaiDonViService: LoaiDonViServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {

    }
    getDataOptions() {

        this._loaiDonViService.getByFilter("", "", 1000, 0)
            .subscribe(result => {
                this.lstData = result.items;
            });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
        }
    }
}
