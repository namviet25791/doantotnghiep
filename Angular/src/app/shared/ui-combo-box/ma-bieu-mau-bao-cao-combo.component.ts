import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";

@Component({
    selector: "shared-ui-ma-bieu-mau-bao-cao-combo",
    template: `
        <select2
            [data]="dataSelect2"
            [options]="options"
            [value]="item"
            (valueChanged)="changed($event)"
        ></select2>
    `
})
export class MaBieuMauBaoCaoComboComponent implements OnInit {
    public dataSelect2: Array<any>;
    public options: Select2Options;
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();
    constructor() {
        this.getDataOptions();
    }
    ngOnInit(): void {
        this.options = {
            width: "100%",
            matcher: (term: string, text: string) => {
                return text.toUpperCase().indexOf(term.toUpperCase()) == 0;
            }
        };
    }
    getDataOptions() {
        this.dataSelect2 = [
            { id: "1", ten: "Báo cáo tổng hợp" },
            { id: "2", ten: "Báo cáo đánh giá kết quả theo đơn vị" },
            { id: "3", ten: "Thông báo kết quả đánh giá" }];

    }
    public changed(e: any): void {
        this.item = e.value;
        this.itemChange.emit(e.value);
        this.valueChange.emit(e.value);
    }
}
