import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { DonViServiceProxy } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "shared-ui-don-vi-combo",
    template: `
    <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        [disabled]="isDisable"
                        ></dx-select-box>
        </div>
    `
})
export class DonViComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Input() isDisable = false;

    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];


    constructor(private _donViService: DonViServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }

    getDataOptions() {
        this._donViService.getByFilter("", undefined, undefined, undefined,
            undefined, undefined, undefined, 0, 0, "", 1000, 0)
            .subscribe(result => {
                this.lstData = result.items;
            });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
        }
    }
}
