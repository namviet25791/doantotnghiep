import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    Injector
} from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import {
    CapDonViServiceProxy,
    ChucVuServiceProxy
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";

@Component({
    selector: "shared-ui-loai-hop-dong-combo",
    template: `
    <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="name"
                        valueExpr="id"                     
                        [value]="item"                       
                        (onValueChanged)="changed($event)"
                        [disabled]="isDisable"
                        ></dx-select-box>
        </div>
    `
})
export class LoaiHopDongComboComponent extends AppComponentBase
    implements OnInit {
    @Input() item: any;
    @Input() isDisable = false;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();

    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "tenChucVu";
    lstData: any = [];
    constructor(
        injector: Injector,
        private _chucVuService: ChucVuServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.lstData = [
            { id: 1, name: "Công chức" },
            { id: 2, name: "Viên chức" },
            { id: 3, name: "Hợp đồng" },
            {
                id: 4, name: "LĐHĐ NĐ 68"
            }
        ];
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
            this.valueChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
            this.valueChange.emit(0);
        }
    }
}
