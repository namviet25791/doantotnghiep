import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { ThangDiemServiceProxy } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "shared-ui-thang-diem-combo",
    template: `
        <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"
                        displayExpr="ten"
                        valueExpr="id" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        [value]="item"
                        (onValueChanged)="changed($event)"></dx-select-box>
        </div>
    `
})
export class ThangDiemComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();

    filterItem: any = {};

    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];
    constructor(private _thangDiemService: ThangDiemServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }
    getDataOptions() {
        this._thangDiemService.getAllThangDiemCombo().subscribe(result => {
            this.lstData = result;
        });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
        }
    }
}
