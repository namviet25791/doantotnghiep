import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { number } from "@amcharts/amcharts4/core";

@Component({
    selector: "shared-ui-nam-combo",
    template: `
    <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        (onValueChanged)="changed($event)"
                        ></dx-select-box>
        </div>
    `
})
export class NamComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();
    lstData: any = [];
    constructor() {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }

    getDataOptions() {
        var d = new Date();
        var n = d.getFullYear();
        for (let index = 2018; index <= n; index++) {
            var item = {
                id: index,
                ten: index.toString()
            };
            this.lstData.push(item);
        }
        this.lstData = _.orderBy(
            this.lstData,
            ["id"],
            ['desc']
        );
    }

    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
            this.valueChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
            this.valueChange.emit(0);
        }
    }
}
