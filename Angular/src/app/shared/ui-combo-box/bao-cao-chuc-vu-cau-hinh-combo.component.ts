import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { BaoCaoServiceProxy } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "shared-ui-bao-cao-chuc-vu-cau-hinh-combo",
    template: `
        <select2
            [data]="dataSelect2"
            [options]="options"
            [value]="item"
            (valueChanged)="changed($event)"
        ></select2>
    `
})
export class BaoCaoChucVuCauHinhComboComponent implements OnInit {
    public dataSelect2: Array<any>;
    public options: Select2Options;
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    constructor(private _baoCaoViService: BaoCaoServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
        this.options = {
            width: "100%",
            matcher: (term: string, text: string) => {
                return text.toUpperCase().indexOf(term.toUpperCase()) == 0;
            }
        };
    }
    getDataOptions() {
        // this._baoCaoViService.getBaoCaoChucVuCauHinh(2).subscribe(result => {
        //     this.dataSelect2 = _.map(result, function(x) {
        //         return _.assign(x, {
        //             id: x.id,
        //             text: x.ten
        //         });
        //     });
        //     this.item = this.dataSelect2[0].id;
        // });
    }
    public changed(e: any): void {
        this.item = e.value;
        this.itemChange.emit(e.value);
    }
}
