import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import * as _ from "lodash";

@Component({
    selector: "shared-ui-loai-thoi-gian-combo",
    template: `
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="text"
                        valueExpr="id"                     
                        [value]="item" 
                        (onValueChanged)="changed($event)"
                        ></dx-select-box>
    `
    // template: `
    //     <select2
    //         [data]="dataSelect2"
    //         [options]="options"
    //         [value]="item"
    //         (valueChanged)="changed($event)"
    //     ></select2>
    // `
})
export class LoaiThoiGianComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();
    lstData: any = [];
    constructor() {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }

    getDataOptions() {
        this.lstData = [
            { id: 1, text: "Theo tháng" },
            { id: 2, text: "Theo quý" },
            { id: 3, text: "Theo năm" }
        ];
    }

    public changed(e: any): void {
        this.item = e.value;
        this.itemChange.emit(e.value);
        this.valueChange.emit(e.value);
    }
}
