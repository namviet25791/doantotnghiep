import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { CapDonViServiceProxy, CommonLookupServiceProxy, CommonEnumServiceProxy } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "shared-ui-cap-don-vi-combo",
    template: `
       <div class="dx-field-value">
       <dx-select-box [dataSource]="lstData"           
                        displayExpr="name"
                        valueExpr="id"                     
                        [value]="item" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        ></dx-select-box>
        </div>
    `
})


// <select2
//             [data]="dataSelect2"
// [options] = "options"
// [value] = "item"
//     (valueChanged) = "changed($event)"
//     > </select2>

export class CapDonViComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];

    constructor(private _commonEnumService: CommonEnumServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }
    getDataOptions() {

        // this._capDonViService.getByFilter("", "", 1000, 0)
        //     .subscribe(result => {
        //         this.lstData = result.items;
        //     });

        this._commonEnumService.getCapDonViEnum()
            .subscribe(result => {
                this.lstData = result;
            });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
        }
    }
}
