import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import { ChucVuServiceProxy } from "@shared/service-proxies/service-proxies";

@Component({
    selector: "shared-ui-chuc-vu-ca-nhan-combo",
    template: `
      <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        ></dx-select-box>
        </div>
    `
})

// (onValueChanged) ="changed($event)"
export class ChucVuCaNhanComboComponent implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();

    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];
    constructor(private _chucVuService: ChucVuServiceProxy) {
        this.getDataOptions();
    }
    ngOnInit(): void {
    }

    getDataOptions() {
        this._chucVuService.getByFilter("", undefined, undefined, 1, "", 1000, 0)
            .subscribe(result => {
                this.lstData = result.items;
                // this.valueChange.emit(this.item);
            });
    }

    changed(e) {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
            this.valueChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
            this.valueChange.emit(0);
        }
    }
}
