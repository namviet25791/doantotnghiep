import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectorRef } from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import { DanhMucChungServiceProxy } from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";

@Component({
    selector: "shared-ui-xa-combo",
    template: `
       <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="ten"
                        valueExpr="id"                     
                        [value]="item" 
                        [searchMode]="'contains'"
                        [searchExpr]="searchExprOption"
                        [minSearchLength]="minSearchLengthOption"
                        [searchEnabled]="true" 
                        (onValueChanged)="changed($event)"
                        valueChangeEvent="keyup"
                        [disabled]="isDisable"
                        ></dx-select-box>
        </div>
    `
})
export class XaComboComponent implements OnInit {
    @Input() item: any;
    @Input() isDisable = false;
    @Output() itemChange = new EventEmitter();
    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];
    constructor(private _danhMucChungService: DanhMucChungServiceProxy, private cdr: ChangeDetectorRef) {
        this.getDataOptions();
    }
    ngOnInit(): void {
        if (this.item == null) {
            this.item = 0;
        }
    }
    ngAfterViewInit() {
        this.cdr.detectChanges();
    }
    getDataOptions(huyenId?: string) {
        // console.log("tinhId", huyenId);
        if (huyenId) {
            this._danhMucChungService.getXaByFilter("", huyenId, "", 1000, 0)
                .subscribe(result => {
                    this.lstData = result.items;
                });
        }
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
        }
    }
}
