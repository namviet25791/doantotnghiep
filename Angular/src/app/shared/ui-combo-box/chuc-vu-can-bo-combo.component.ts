import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit,
    Injector
} from "@angular/core";
import { Select2OptionData } from "ng2-select2";
import * as _ from "lodash";
import {
    CapDonViServiceProxy,
    ChucVuServiceProxy
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";

@Component({
    selector: "shared-ui-can-bo-chuc-vu-combo",
    template: `
    <div class="dx-field-value">
           <dx-select-box [dataSource]="lstData"           
                        displayExpr="tenChucVu"
                        valueExpr="chucVuId"                     
                        [value]="item"                       
                        (onValueChanged)="changed($event)"
                        ></dx-select-box>
        </div>
    `
})
export class CanBoChucVuComboComponent extends AppComponentBase
    implements OnInit {
    @Input() item: any;
    @Output() itemChange = new EventEmitter();
    @Output() valueChange = new EventEmitter();

    filterItem: any = {};
    minSearchLengthOption: number = 0;
    searchExprOption: any = "tenChucVu";
    lstData: any = [];
    constructor(
        injector: Injector,
        private _chucVuService: ChucVuServiceProxy
    ) {
        super(injector);
    }
    ngOnInit(): void {
        this.getDataOptions();
    }
    getDataOptions(canboId?: any) {
        let canboInput = 0;

        if (this.appSession.user.userName == "admin") {
            canboInput = canboId;
        } else {
            canboInput = this.appSession.canBoId;
        }
        this._chucVuService
            .getChucVuTheoCanBo(canboInput)
            .subscribe(result => {
                this.lstData = result;

                if (result.length > 0) {
                    const checkChucVuChinh = result.filter(epic => epic.chucVuChinh == 1)[0];
                    if (checkChucVuChinh) {
                        this.item = checkChucVuChinh.chucVuId;
                    }
                    else {
                        this.item = result[0].chucVuId;
                    }
                    this.itemChange.emit(this.item);
                    this.valueChange.emit(this.item);
                }
            });
    }
    public changed(e: any): void {
        if (e.event) {
            e.event.preventDefault();
        }

        if (e.value > 0) {
            this.item = e.value;
            this.itemChange.emit(e.value);
            this.valueChange.emit(e.value);
        }
        else {
            this.item = 0;
            this.itemChange.emit(0);
            this.valueChange.emit(0);
        }
    }
}
