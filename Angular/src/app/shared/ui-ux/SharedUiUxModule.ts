import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { Select2Module } from "ng2-select2";
import { FormsModule } from "@angular/forms";
import { MonthSelectionComponent } from "./month-selection.component";
import { DxDateBoxModule, DxCalendarModule } from 'devextreme-angular';
import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService
} from "ngx-bootstrap/datepicker";
import { NgxBootstrapDatePickerConfigService } from "assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service";
import { DateMommentComponent } from "./date-time-moment.component";
@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        BsDatepickerModule.forRoot(),
        Select2Module,
        DxDateBoxModule,
        DxCalendarModule
    ],
    declarations: [
        MonthSelectionComponent,
        DateMommentComponent],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        },
    ],
    exports: [
        MonthSelectionComponent,
        DateMommentComponent]
})
export class SharedUiUxModule { }
