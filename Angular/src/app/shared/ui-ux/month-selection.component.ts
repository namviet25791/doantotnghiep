import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";

@Component({
    selector: "shared-month-selection",
    template: `
          <div class="dx-field-value">
            <dx-date-box
                [(value)]="modelDate"
                type="date"
                displayFormat="MM/yyyy"
                (onValueChanged)="onDateChange($event)">
                 <dxo-calendar-options                    
                    [zoomLevel]="'year'"
                    [maxZoomLevel]="'year'">
                </dxo-calendar-options>
            </dx-date-box>
        </div>
    `
})
export class MonthSelectionComponent implements OnInit {
    @Input() modelDate: Date = new Date;
    @Output() modelDateChange = new EventEmitter();
    constructor() { }
    ngOnInit(): void { }
    onDateChange(e) {
        this.modelDateChange.emit(e.value);
    }
}