import { Component, Input, Output, EventEmitter, OnInit, SimpleChanges } from "@angular/core";
import * as moment from "moment";
import * as _ from 'lodash';
@Component({
    selector: "shared-ui-date-moment",
    template: `
     <div class="dx-field-value">
            <dx-date-box
                [(value)]="item"
                type="date"
                displayFormat="dd/MM/yyyy"
                (onValueChanged)="onDateChange($event)">
            </dx-date-box>
        </div>
    `
})
export class DateMommentComponent implements OnInit {
    @Input() item: moment.Moment;
    @Output() itemChange = new EventEmitter<moment.Moment>();
    itemStr: string = "";
    constructor() { }
    ngOnInit(): void {
    }
    onDateChange(e) {
        this.itemChange.emit(e.value);
    }
}
