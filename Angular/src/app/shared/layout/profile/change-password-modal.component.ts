import { Component, ElementRef, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ChangePasswordInput, PasswordComplexitySetting, ProfileServiceProxy, CanBoServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'changePasswordModal',
    templateUrl: './change-password-modal.component.html',
    styleUrls: ['./change-password-modal.component.less']
})
export class ChangePasswordModalComponent extends AppComponentBase {

    @ViewChild('currentPasswordInput', { static: true }) currentPasswordInput: ElementRef;
    @ViewChild('changePasswordModal', { static: true }) modal: ModalDirective;

    passwordComplexitySetting: PasswordComplexitySetting = new PasswordComplexitySetting();
    currentPassword: string;
    password: string;
    confirmPassword: string;

    saving = false;
    active = false;
    userId = 0;

    constructor(
        injector: Injector,
        private _profileService: ProfileServiceProxy,
        private _canBoService: CanBoServiceProxy
    ) {
        super(injector);
    }

    show(canBoId?: number): void {
        this.active = true;
        this.currentPassword = '';
        this.password = '';
        this.confirmPassword = '';

        this.userId = (canBoId > 0) ? canBoId : 0;

        this._profileService.getPasswordComplexitySetting().subscribe(result => {
            this.passwordComplexitySetting = result.setting;
            this.modal.show();
        });
    }

    onShown(): void {
        document.getElementById('CurrentPassword').focus();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    save(): void {

        let input = new ChangePasswordInput();
        input.currentPassword = this.currentPassword;
        input.newPassword = this.password;
        input.userId = this.userId;

        if (this.userId > 0) {
            this.saving = true;
            this._profileService.changePasswordCanBo(input)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(result => {
                    if (result) {
                        this.notify.success(this.l('YourPasswordHasChangedSuccessfully'));
                        this.close();
                    } else {
                        this.notify.error("Có lỗi xảy ra vui lòng thao tác lại.");
                    }

                });
        } else {
            this.saving = true;
            this._profileService.changePassword(input)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.success(this.l('YourPasswordHasChangedSuccessfully'));
                    this.close();
                });
        }
    }
}
