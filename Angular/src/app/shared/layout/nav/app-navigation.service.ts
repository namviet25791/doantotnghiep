import { PermissionCheckerService } from '@abp/auth/permission-checker.service';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {
    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService
    ) {
    }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            new AppMenuItem('Trang chủ', '', 'flaticon-line-graph', '/app/main/dashboard'),
            new AppMenuItem(
                'Phiếu đánh giá',
                'Pages.Systems',
                'flaticon2-paper',
                '',
                [
                    new AppMenuItem(
                        'Đánh giá cá nhân',
                        'Pages.User.DanhGiaCaNhan',
                        'flaticon2-user-1',
                        '/app/main/phieu-danh-gia-ca-nhan'
                    ),
                    new AppMenuItem(
                        'Đánh giá nhân viên',
                        'Pages.User.DanhGiaNhanVien',
                        'flaticon2-group',
                        '/app/main/danh-gia-nhan-vien'
                    ),
                    new AppMenuItem(
                        'Phiếu xin ý kiến',
                        'Pages.User.DanhGiaCaNhan.ChuTich',
                        'flaticon2-group',
                        '/app/main/ct-danh-gia-nhan-vien'
                    ),
                    new AppMenuItem(
                        'Phiếu xin ý kiến',
                        'Pages.User.DanhGiaCaNhan.PhoChuTich',
                        'flaticon2-group',
                        '/app/main/pct-danh-gia-nhan-vien'
                    )
                ]
            ),
            new AppMenuItem(
                'Thống kê',
                'Pages.ThongKe',
                'flaticon2-paper',
                '',
                [
                    new AppMenuItem(
                        'Thống kê phiếu đánh giá',
                        'Pages.ThongKe.TinhTrangPhieuDanhGia',
                        'flaticon2-user-1',
                        '/app/main/thong-ke-phieu-danh-gia'
                    )
                ]
            ),
            new AppMenuItem(
                'Cấu hình',
                'Pages.Administration',
                'flaticon-interface-8',
                '',
                [
                    new AppMenuItem(
                        'Chức vụ',
                        'Pages.Admin.ChucVu',
                        'flaticon-map',
                        '/app/quan-tri/chuc-vu'
                    ),
                    new AppMenuItem(
                        'Quản lý nhân sự',
                        'Pages.Admin.CanBo',
                        'flaticon-users',
                        '/app/quan-tri/nhan-su'
                    ),
                    new AppMenuItem(
                        'Quản lý phòng ban',
                        '',
                        'flaticon-interface-8',
                        '',
                        [
                            // new AppMenuItem(
                            //     'Khối hành chính',
                            //     'Pages.Admin.Khoi',
                            //     'flaticon-map',
                            //     '/app/quan-tri/khoi-hanh-chinh'
                            // ),
                            // new AppMenuItem(
                            //     'Cấp đơn vị',
                            //     'Pages.Admin.CapDonVi',
                            //     'flaticon-map',
                            //     '/app/quan-tri/cap-don-vi'
                            // ),
                            // new AppMenuItem(
                            //     'Loại đơn vị',
                            //     'Pages.Admin.LoaiDonVi',
                            //     'flaticon-suitcase',
                            //     '/app/quan-tri/loai-don-vi'
                            // ),
                            new AppMenuItem(
                                'Đơn vị công tác',
                                'Pages.Admin.DonVi',
                                'flaticon-suitcase',
                                '/app/quan-tri/don-vi'
                            )
                        ]
                    ),

                    new AppMenuItem(
                        'Quản lý thang điểm',
                        'Pages.Admin.ThangDiem',
                        'flaticon-suitcase',
                        '/app/quan-tri/thang-diem'
                    ),
                    new AppMenuItem(
                        'Phiếu đánh giá',
                        'Pages.Admin.PhieuDanhGia',
                        'flaticon-map',
                        '/app/quan-tri/phieu-danh-gia'
                    ),
                ]
            ),
            new AppMenuItem(
                'Administration',
                'Pages.Administration',
                'flaticon-interface-8',
                '',
                [
                    new AppMenuItem(
                        'Tenants',
                        'Pages.Tenants',
                        'flaticon-map',
                        '/app/admin/tenants'
                    ),
                    // new AppMenuItem(
                    //     "Roles",
                    //     "Pages.Administration.Roles",
                    //     "flaticon-suitcase",
                    //     "/app/admin/roles"
                    // ),
                    new AppMenuItem(
                        'Phân quyền người dùng',
                        'Pages.Administration.Roles',
                        'flaticon-suitcase',
                        '/app/admin/roles'
                    ),
                    new AppMenuItem(
                        'Users',
                        'Pages.Administration.Users',
                        'flaticon-users',
                        '/app/admin/users'
                    ),
                    new AppMenuItem(
                        'Languages',
                        'Pages.Administration.Languages',
                        'flaticon-tabs',
                        '/app/admin/languages'
                    ),
                    new AppMenuItem(
                        'AuditLogs',
                        'Pages.Administration.AuditLogs',
                        'flaticon-folder-1',
                        '/app/admin/auditLogs'
                    ),
                    new AppMenuItem(
                        'Maintenance',
                        'Pages.Administration.Host.Maintenance',
                        'flaticon-lock',
                        '/app/admin/maintenance'
                    ),
                    new AppMenuItem(
                        'Subscription',
                        'Pages.Administration.Tenant.SubscriptionManagement',
                        'flaticon-refresh',
                        '/app/admin/subscription-management'
                    ),
                    new AppMenuItem(
                        'VisualSettings',
                        'Pages.Administration.UiCustomization',
                        'flaticon-medical',
                        '/app/admin/ui-customization'
                    ),
                    new AppMenuItem(
                        'Settings',
                        'Pages.Administration.Host.Settings',
                        'flaticon-settings',
                        '/app/admin/hostSettings'
                    ),
                    new AppMenuItem(
                        'Settings',
                        'Pages.Administration.Tenant.Settings',
                        'flaticon-settings',
                        '/app/admin/tenantSettings'
                    )
                ]
            )
        ]);
    }

    checkChildMenuItemPermission(menuItem): boolean {
        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (
                subMenuItem.permissionName === '' ||
                subMenuItem.permissionName === null ||
                (subMenuItem.permissionName &&
                    this._permissionCheckerService.isGranted(
                        subMenuItem.permissionName
                    ))
            ) {
                return true;
            } else if (subMenuItem.items && subMenuItem.items.length) {
                return this.checkChildMenuItemPermission(subMenuItem);
            }
        }

        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (
            menuItem.permissionName ===
            'Pages.Administration.Tenant.SubscriptionManagement' &&
            this._appSessionService.tenant &&
            !this._appSessionService.tenant.edition
        ) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (
            menuItem.permissionName &&
            !this._permissionCheckerService.isGranted(menuItem.permissionName)
        ) {
            hideMenuItem = true;
        }

        if (
            this._appSessionService.tenant ||
            !abp.multiTenancy.ignoreFeatureCheckForHostUsers
        ) {
            if (
                menuItem.hasFeatureDependency() &&
                !menuItem.featureDependencySatisfied()
            ) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }
}
