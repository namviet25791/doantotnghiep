import {
    PhieuDanhGiaCanBoServiceProxy,
    KetQuaDanhGiaChiTietServiceProxy,
    PhieuDanhGiaDto
} from '@shared/service-proxies/service-proxies';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component
} from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { PhieuDanhGiaCaNhanComponent } from './phieu-danh-gia-ca-nhan/phieu-danh-gia-ca-nhan.component';
import { ChiTietPhieuDanhGiaCaNhanComponent } from './chi-tiet-phieu/chi-tiet-phieu-danh-gia-ca-nhan.component';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { RouterOutlet, ActivatedRoute } from '@angular/router';
import { LuongDanhGiaModalComponent } from './luong-danh-gia/luong-danh-gia-modal.component';


@Component({
    selector: 'app-phieu-danh-gia-can-bo',
    templateUrl: './phieu-danh-gia-can-bo.component.html',
    animations: [appModuleAnimation()]
})
export class PhieuDanhGiaCanBoComponent extends AppComponentBase
    implements OnInit {
    filterText = '';
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('paginator', { static: true }) paginator: Paginator;
    @ViewChild('chiTietPhieuComponentView', { static: true })
    chiTietPhieuComponentView: ChiTietPhieuDanhGiaCaNhanComponent;

    @ViewChild('luongDanhGiaModal', { static: true })
    luongDanhGiaModal: LuongDanhGiaModalComponent;

    isViewDsLichSu = true;
    filterYear = (new Date()).getFullYear();
    isDaGuiPhieu = true;
    selectChucVuId: number = this.appSession.chucVuChinhId;
    paramPhieuDanhGiaCanBoId: any;
    paramChucVuCanBoDanhGiaId: any;
    dataItemSlected: any = {};

    isViewDanhGia = false;
    isViewChiTiet = false;

    constructor(
        injector: Injector,
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        private _ketQuaDanhGiaChiTietService: KetQuaDanhGiaChiTietServiceProxy,
        private _fileService: FileDownloadService,
        private route: ActivatedRoute
    ) {
        super(injector);
        // this.getPhieuDanhGiaCanBo();
    }

    ngOnInit() {

        let phieuDanhGiaCanBoId: any;
        let chucVuDanhGiaId: any;
        this.route.paramMap.subscribe(params => {
            phieuDanhGiaCanBoId = (params.get('id') == null) ? 0 : params.get('id');

            if (phieuDanhGiaCanBoId != null && phieuDanhGiaCanBoId > 0) {
                this._phieuDanhGiaCanBoService.kiemTraThongTinPhieuTuUrl(parseInt(params.get('id')), 0)
                    .subscribe(rs => {
                        //console.log("rsrsrsrs", rs);
                        if (rs.result == false) {
                            if (rs.msg != null && rs.msg != '') {
                                abp.message.error(
                                    rs.msg,
                                    'Thông báo');

                                setTimeout(() => {
                                    window.location.href = '/app/main/phieu-danh-gia-ca-nhan';
                                }, 3000);
                            } else {
                                let input = new PhieuDanhGiaDto();
                                input.id = phieuDanhGiaCanBoId;
                                this.isViewDsLichSu = false;
                                this.chiTietPhieuComponentView.getTieuChiDanhGia(input, null, true, false);
                            }
                        }
                    });
            }

        });

        if (phieuDanhGiaCanBoId == 0) {
            this.getPhieuDanhGiaCanBo();
        }
    }


    public projectData: Object[];

    kiemTraPhieuDaGui() {
        this._ketQuaDanhGiaChiTietService
            .getDaGuiPhieu(this.appSession.canBoId)
            .subscribe(result => {
                this.isDaGuiPhieu = result;
            });
    }


    getPhieuDanhGiaCanBo(event?: LazyLoadEvent) {
        this.kiemTraPhieuDaGui();
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );


        this._phieuDanhGiaCanBoService
            .getAllPhieuDanhGiaCanBo(
                this.filterText,
                this.filterYear,
                undefined,
                this.appSession.canBoId,
                undefined,
                undefined,
                this.selectChucVuId,
                undefined,
                undefined,
                '',
                12,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();

            });
    }

    close(): void {
        this.closeEvent.emit(null);
    }

    xuatWord(data: any) {
        //console.log(data, "XuatWord");
        this._phieuDanhGiaCanBoService.xuatWord(data.id).subscribe(response => {
            this._fileService.downloadTempFile(response);
        });
    }

    xuatPDF(data: any) {
        //console.log(data, "Xuat PDF");
        this._phieuDanhGiaCanBoService.xuatPDF(data.id).subscribe(response => {
            this._fileService.downloadTempFile(response);
        });
    }

    showChiTietPhieuDanhGia(data?: any) {
        this.isViewDsLichSu = false;
        this.dataItemSlected = data;
    }

    showPhieuDanhGia(data?: any) {
        this.isViewDsLichSu = false;
        this.dataItemSlected = data;
    }

    showLuongDanhGiaModal(data?: any): void {
        this.luongDanhGiaModal.show(this.selectChucVuId, data);
    }

    onModalPhieuDanhGiaClose($event: boolean) {
        this.isViewDsLichSu = true;
        this.isViewDanhGia = false;
        this.isViewChiTiet = false;
        if ($event) {
            this.getPhieuDanhGiaCanBo();
        }
    }
}
