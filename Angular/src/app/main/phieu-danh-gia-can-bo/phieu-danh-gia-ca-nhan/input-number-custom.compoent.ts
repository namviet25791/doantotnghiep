import { Component, forwardRef, Injector, Input, OnInit, Provider } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

const VALUE_ACCESSOR: Provider = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputNumberCustomComponent),
    multi: true
};
@Component({
    selector: 'input-number-custom',
    template: `
             <input type="number"
            class="className"
            (keypress)="onKeypress($event)"
            [(ngModel)]= "_value"
            (ngModelChange)="onChangeValue($event)" />
    `,
    providers: [VALUE_ACCESSOR]

})

export class InputNumberCustomComponent implements OnInit, ControlValueAccessor {
    _value = 0;
    _isDisabled = false;
    @Input() className: string = "form-control text-right";
    @Input() maxValue: number;
    private onChange: Function = (v: any) => {
    };
    private onTouched: Function = () => {
    };

    @Input()
    get value() {
        return this.value;
    }

    set value(v: any) {
        this._value = v;
    }

    @Input()
    get disabled() {
        return this._isDisabled;
    }

    set disabled(v: boolean) {
        this._isDisabled = v;
    }

    ngOnInit(): void {
    }


    onKeypress($event: any) {
        debugger;
        if ($event.target.value > this.maxValue) {
            return false;
        } else return true;
    }
    onChangeValue(event: any): void {
        if (event > this.maxValue) {

        } else {
            this.onChange(event);
        }
    }

    onFocus(event: any): void {
        this.onTouched();
    }


    writeValue(obj: any): void {
        this._value = obj;
    }

    registerOnChange(fn: Function): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: Function): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        this._isDisabled = isDisabled;
    }



}