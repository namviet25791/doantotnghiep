import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { PhieuDanhGiaCanBoServiceProxy, PhieuDanhGiaTongHop } from '@shared/service-proxies/service-proxies';
import { DxTreeListComponent } from 'devextreme-angular';
import * as _ from "lodash";

enum TypeRow {
    Title = 1,
    Field = 2
}
@Component({
    selector: 'chi-tiet-nhom-danh-gia-modal',
    templateUrl: './chi-tiet-nhom-danh-gia-modal.component.html',
    styleUrls: ['./chi-tiet-phieu-danh-gia-ca-nhan.component.css']
})
export class ChiTietNhomDanhGiaModalComponent extends AppComponentBase {
    @ViewChild("donViTreeList2", { static: false }) donViTree: DxTreeListComponent;
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    submitted = false;
    active = false;
    saving = false;
    public projectData: any[];
    phieuDanhGiaTongHop = new PhieuDanhGiaTongHop();
    typeRow = TypeRow;
    maxTotalMark = 0;
    constructor(
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
    }

    show(phieuDanhGiaCanBoid?: any, phieuDanhGiaId?: any, lichSuId?: any): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;
        const that = this;
        this.phieuDanhGiaTongHop = new PhieuDanhGiaTongHop();

        this._phieuDanhGiaCanBoService.getKetQuaNhomDanhGia(phieuDanhGiaCanBoid, phieuDanhGiaId, lichSuId).subscribe(result => {
            this.projectData = this.chuanHoaData(result.phieuDanhGiaChiTietDto);
            this.phieuDanhGiaTongHop = result;


            const root = _.sortBy(result.phieuDanhGiaChiTietDto.filter(f => f.tieuChiChaId === null), ['thuTu'], ['asc']);
            root.forEach((item) => {
                let index = result.phieuDanhGiaChiTietDto.findIndex(x => x.id === item.id);
                this.onExpandClick(index);
            });
        });

        self.modal.show();
    }

    onExpandClick(index: number) {
        const dataItem = this.projectData[index];
        dataItem.expanded = !dataItem.expanded;
        let expand = (currentId: any) => {
            this.projectData.filter(f => f.tieuChiChaId === currentId).forEach(item => {
                item.isShow = dataItem.expanded;
                item.expanded = dataItem.expanded;
                expand(item.id);
            });
        };
        expand(dataItem.id);
        // this.projectData[index].expanded = !dataItem.expanded;
    }

    checkTypeRow(isCapCha: boolean): TypeRow {
        //1: Row title || 2 : row field
        if (isCapCha) {
            return this.typeRow.Title;
        } else {
            return this.typeRow.Field;
        }
    }

    chuanHoaData(data: any[]): any[] {
        this.maxTotalMark = 0;
        let res: any[] = [];
        let chuanHoa = (data: any[], parentId: any, level: number = 1, classNameParent = '') => {
            const root = _.sortBy(data.filter(f => f.tieuChiChaId === parentId), ['thuTu'], ['asc']);
            root.forEach((item, index) => {
                const isLeaf = data.filter(f => f.tieuChiChaId === item.id).length <= 0;
                const className = (classNameParent ? classNameParent + ' ' : '') + 'item-gr' + item.id;

                res.push({
                    ...item,
                    level: level,
                    isLeaf: isLeaf,
                    className: className,
                    isShow: true,
                    expanded: true,
                });
                chuanHoa(data, item.id, level + 1, className);
                if (item.isCapCha === true && item.tieuChiChaId === null) {
                    this.maxTotalMark += item.diemToiDa;
                }
            });
        };
        chuanHoa(data, null, 1);
        return res;
    }
    onShown(): void {
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
