import { Component, OnInit, ElementRef, EventEmitter, Injector, Output, ViewChild, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { TieuChiDanhGiaDto, KetQuaDanhGiaChiTietServiceProxy, DanhSachPhieuDanhGiaCanBoDto } from '@shared/service-proxies/service-proxies';


@Component({
    selector: 'luongDanhGiaModal',
    templateUrl: './luong-danh-gia-modal.component.html'
})
export class LuongDanhGiaModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal2', { static: true }) modal: ModalDirective;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    public projectData: any[];
    saving = false;
    active = false;
    phieuDanhGiaCanBo = new DanhSachPhieuDanhGiaCanBoDto();
    constructor(
        injector: Injector,
        private _ketQuaDanhGiaChiTietService: KetQuaDanhGiaChiTietServiceProxy,
    ) {
        super(injector);

    }
    // @Input() phieuDanhGiaCanBoInput: any;
    // @Input() chucVuId: any;

    show(chucVuId?: number, phieuDanhGiaCanBoInput?: any): void {

        if (phieuDanhGiaCanBoInput) {
            this.phieuDanhGiaCanBo = phieuDanhGiaCanBoInput;
        }

        const self = this;
        self.active = true;
        self.modal.show();
        this._ketQuaDanhGiaChiTietService.getChiTietLuongDanhGia(phieuDanhGiaCanBoInput.id, phieuDanhGiaCanBoInput.canBoId, chucVuId).subscribe(result => {
            this.projectData = result;
        });
    }

    onShown() {

    }

    close(): void {
        this.modal.hide();
    }
}
