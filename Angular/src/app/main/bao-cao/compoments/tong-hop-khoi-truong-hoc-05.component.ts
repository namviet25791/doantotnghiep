import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component,
    ViewEncapsulation,
    Input
} from "@angular/core";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import {
    BieuMauBaoCaoServiceProxy,
    BaoCaoServiceProxy,
    FilterBaoCaoInput,
    BaoCaoDto,
    DonViServiceProxy,
    DonViChuyenTrachServiceProxy,
    NhomChucVuDto,
    TongHopKhoiCacPhuong04Dto,
    KetQuaDonVi,
    TongHopKhoiTruongHoc05Dto
} from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
import { List } from "@amcharts/amcharts4/core";

@Component({
    selector: "app-tong-hop-khoi-truong-hoc-05",
    templateUrl: "./tong-hop-khoi-truong-hoc-05.component.html",
    encapsulation: ViewEncapsulation.None
})
export class BaoCaoTongHopKhoiTruongHoc05Component extends AppComponentBase implements OnInit {
    @Input() filterData: FilterBaoCaoInput;

    constructor(
        injector: Injector,
        private _baoCaoService: BaoCaoServiceProxy
    ) {
        super(injector);
    }

    tongHopKetQua = new TongHopKhoiTruongHoc05Dto();
    lstNhomChucVuQuanLy: any[];
    lstChucVuNhanVien: any[];
    lstChucVuQuanLy: any[];
    lstKetQua = new List<KetQuaDonVi>();
    lstThangDiemChiTiet = [];
    lstTongTHCS:any=[];
    lstTongTieuHoc:any=[];
    lstTongMamNon:any=[];

    ngOnInit() {
    }


    getBaoCao(input?: any) {
        this._baoCaoService
            .tongHopKhoiTruongHoc05(input)
            .subscribe(result => {
                this.tongHopKetQua = new TongHopKhoiTruongHoc05Dto();
                this.tongHopKetQua = result;

                this.lstNhomChucVuQuanLy = [];
                this.lstChucVuNhanVien = [];
                this.lstChucVuQuanLy = [];

                this.tongHopKetQua.lstNhomChucVu.forEach(epic => {
                        this.lstChucVuNhanVien.push(epic);
                });
                this.lstTongTHCS=new Array(this.tongHopKetQua.soLuongThangDiemDanhGia+4);

                console.log(this.lstTongTHCS);
                this.lstTongTieuHoc=new Array(this.tongHopKetQua.soLuongThangDiemDanhGia+4);
                this.lstTongMamNon=new Array(this.tongHopKetQua.soLuongThangDiemDanhGia+4);
                this.tongHopKetQua.lstKetQuaDonViTHCS.forEach((item,index)=>{
                    if(this.lstTongTHCS[0]== undefined) this.lstTongTHCS[0]=0;
                    if(this.lstTongTHCS[1]== undefined) this.lstTongTHCS[1]=0;
                    if(this.lstTongTHCS[2]== undefined) this.lstTongTHCS[2]=0;
                    this.lstTongTHCS[0] += item.tongSo;
                    this.lstTongTHCS[1] += item.soDuocDanhGia;
                    this.lstTongTHCS[2] += item.soKhongDanhGia;
                    let i=4;
                    item.lstNhomChucVu.forEach((item2,index2)=>{
                        item2.lstThangDiem.forEach((item3,index3)=>{
                            if(this.lstTongTHCS[i+index3]==undefined) this.lstTongTHCS[i+index3]=0;
                            this.lstTongTHCS[i+index3] += item3.soLuong;
                        })
                        i+=item2.lstThangDiem.length;
                    })
                })
                this.tongHopKetQua.lstKetQuaDonViTieuHoc.forEach((item,index)=>{
                    if(this.lstTongTieuHoc[0]==undefined) this.lstTongTieuHoc[0]=0;
                    if(this.lstTongTieuHoc[1]==undefined) this.lstTongTieuHoc[1]=0;
                    if(this.lstTongTieuHoc[2]==undefined) this.lstTongTieuHoc[2]=0;
                    this.lstTongTieuHoc[0] += item.tongSo;
                    this.lstTongTieuHoc[1] += item.soDuocDanhGia;
                    this.lstTongTieuHoc[2] += item.soKhongDanhGia;
                    let i=4;
                    item.lstNhomChucVu.forEach((item2,index2)=>{
                        item2.lstThangDiem.forEach((item3,index3)=>{
                            if(this.lstTongTieuHoc[i+index3]==undefined) this.lstTongTieuHoc[i+index3]=0;
                            this.lstTongTieuHoc[i+index3] += item3.soLuong;
                        })
                        i += item2.lstThangDiem.length;
                    })
                })
                this.tongHopKetQua.lstKetQuaDonViMamNon.forEach((item,index)=>{
                    if(this.lstTongMamNon[0]==undefined) this.lstTongMamNon[0]=0;
                    if(this.lstTongMamNon[1]==undefined) this.lstTongMamNon[1]=0;
                    if(this.lstTongMamNon[2]==undefined) this.lstTongMamNon[2]=0;
                    this.lstTongMamNon[0] += item.tongSo;
                    this.lstTongMamNon[1] += item.soDuocDanhGia;
                    this.lstTongMamNon[2] += item.soKhongDanhGia;
                    let i=4;
                    item.lstNhomChucVu.forEach((item2,index2)=>{
                        item2.lstThangDiem.forEach((item3,index3)=>{
                            if(this.lstTongMamNon[i+index3]==undefined) this.lstTongMamNon[i+index3]=0;
                            this.lstTongMamNon[i+index3] += item3.soLuong;
                        })
                        i+=item2.lstThangDiem.length;
                    })
                })
                console.log(" this.tongHopKetQuaTH04", this.lstNhomChucVuQuanLy, this.lstChucVuQuanLy, this.lstChucVuNhanVien);
                console.log(" this.listTongHop", this.lstTongTHCS, this.lstTongTieuHoc, this.lstTongMamNon);
            });
    }




}
