import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component,
    ViewEncapsulation,
    Input
} from "@angular/core";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import {
    BieuMauBaoCaoServiceProxy,
    BaoCaoServiceProxy,
    FilterBaoCaoInput,
    BaoCaoDto,
    DonViServiceProxy,
    DonViChuyenTrachServiceProxy,
    NhomChucVuDto,
    TongHopKhoiCacPhuong04Dto,
    KetQuaDonVi,
    TongHopKhoiDang06Dto
} from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
import { List } from "@amcharts/amcharts4/core";

@Component({
    selector: "app-tong-hop-khoi-cac-dang-06",
    templateUrl: "./tong-hop-khoi-cac-dang-06.component.html",
    encapsulation: ViewEncapsulation.None
})
export class BaoCaoTongHopKhoiCacDang06Component extends AppComponentBase implements OnInit {
    @Input() filterData: FilterBaoCaoInput;

    constructor(
        injector: Injector,
        private _baoCaoService: BaoCaoServiceProxy
    ) {
        super(injector);
    }

    tongHopKetQuaTH06 = new TongHopKhoiDang06Dto();
    lstNhomChucVuQuanLy: any[];
    lstChucVuNhanVien: any[];
    lstChucVuQuanLy: any[];
    lstKetQua = new List<KetQuaDonVi>();
    lstThangDiemChiTiet = [];

    lstHeader = [
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 }
    ];


    ngOnInit() {
    }


    getBaoCao(input?: any) {
        this._baoCaoService
            .tongHopKhoiCacDang06(input)
            .subscribe(result => {
                this.tongHopKetQuaTH06 = new TongHopKhoiDang06Dto();
                this.tongHopKetQuaTH06 = result;

                // this.tongHopKetQuaTH06.lstNhomChucVu.forEach(epic => {
                //     if (epic.isNhomChucVu == true) {
                //         this.lstChucVuNhanVien.push(epic);
                //     } else {
                //         this.lstNhomChucVuQuanLy.push(epic);
                //         this.lstChucVuQuanLy = this.lstChucVuQuanLy.concat(epic.lstChucVu);
                //     }
                // });

                console.log("this.tongHopKetQuaTH06 ", this.tongHopKetQuaTH06);
            });
    }




}
