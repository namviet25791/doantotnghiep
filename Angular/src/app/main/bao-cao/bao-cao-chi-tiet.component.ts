import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component
} from "@angular/core";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import {
    BieuMauBaoCaoServiceProxy,
    BaoCaoServiceProxy,
    FilterBaoCaoInput,
    BaoCaoDto,
    DonViServiceProxy,
    DonViChuyenTrachServiceProxy,
    NhomChucVuDto,
    TongHopKhoiCacPhuong04Dto,
    KetQuaDonVi
} from "@shared/service-proxies/service-proxies";
import * as _ from "lodash";
import * as moment from "moment";
import { List } from "@amcharts/amcharts4/core";
import { BaoCaoTongHopKhoiCacPhuong04Component } from "./compoments/tong-hop-khoi-cac-phuong-04/tong-hop-khoi-cac-phuong-04.component";
import { BaoCaoTongHopKhoiCacDang06Component } from "./compoments/tong-hop-khoi-cac-dang-06/tong-hop-khoi-cac-dang-06.component";
import { BaoCaoTongHopKhoiTruongHoc05Component } from '@app/main/bao-cao/compoments/tong-hop-khoi-truong-hoc-05.component';

declare var Stimulsoft: any;
@Component({
    selector: "app-bao-cao-chi-tiet",
    templateUrl: "./bao-cao-chi-tiet.component.html",
    styleUrls: ['./bao-cao-style.component.css'],
    animations: [appModuleAnimation()]
})
export class BaoCaoChiTietComponent extends AppComponentBase implements OnInit {
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild('tonghopkhoicacphuong', { static: true })
    tonghopkhoicacphuong: BaoCaoTongHopKhoiCacPhuong04Component;
    @ViewChild('tonghopkhoitruonghoc', { static: true })
    tonghopkhoitruonghoc: BaoCaoTongHopKhoiTruongHoc05Component;

    @ViewChild('tonghopkhoicacdang', { static: true })
    tonghopkhoicacdang: BaoCaoTongHopKhoiCacDang06Component;


    viewer: any = new Stimulsoft.Viewer.StiViewer(null, "StiViewer", false);
    report: any = new Stimulsoft.Report.StiReport();

    isStimulsoft: boolean = true;
    isShowBaoCaoChieTiet: any = false;
    baoCaoInfo: BaoCaoDto = new BaoCaoDto();
    selectChucVuId: number = 0;
    //isShowFilter
    loaiThoiGianShow: boolean = false;
    thoiGianBatDauShow: boolean = false;
    doiTuongHienThiShow: boolean = false;
    donViShow: boolean = false;
    //
    isShowTongHopKhoiCacPhuong04 = false;
    isShowTongHopKhoiCacDang06 = false;
    //
    loaiThoiGian: number = 1;
    thoiGianBatDau: Date = new Date();
    lstDoiTuongHienThi: number = 0;
    lstDonVi: number = 0;
    source: string = "";
    //isViewDsBaoCao = true;
    dropdownListDoiTuongHienThi = [];
    selectedItemsDoiTuongHienThi = [];
    dropdownListDonVi = [];
    selectedItemsDonVi = [];

    filterModel = new FilterBaoCaoInput();
//TruongHoc
    isShowTongHopKhoiTruongHoc05 = false;

    constructor(
        injector: Injector,
        private _baoCaoService: BaoCaoServiceProxy,
        private _donViChuyenTrachService: DonViChuyenTrachServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);
        this.thoiGianBatDau = d;

        Stimulsoft.Base.StiLicense.key =
            "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHkNSjqqTQXqZKEyG+hEaLwKCxh12NH7yDNnL3F6QPuAbs8tPF" +
            "x9/x5K2FFRzPJu13hNHmW9FB7Pi0rF2v+0lQKRwHZQ6WpbzWCgZ2WaYC+2FrQE1JmWwNZy8gEO9wj2bTCTwWztSbi6" +
            "LgfWMdq/sxrXR8uq/xBaszqL1tXI+3iAqXBW6BD5xJiMCOhxTTMpFWjGVDcixuDd+lMxRX3wtmmRZfw//qxN3YioMc" +
            "52CrBpNmS5DqNj9tS8Cziz1ZVN7gWd7U4R8xKex9Fl3EWng72Qu0zrJIXK9SJMV/ggAocSDyaH6UI+pjSfvMYNtrrP" +
            "DkyrO9jsRdAK2GycgmuiJpI3c2VUwYbCUY/VaZB2WcLIi+5gFr+5Lq8En0moyHI6atCPcZ8/YjXdPd6vlBZ09H4jqA" +
            "YTasROfhKn+jVTZh3qHK/qQiKNQ/EP0RZmIu04V5wn2POa/8zHsAVhmqdfjDhrJmDnaSAWJihaXrnav/8hw0vtOSz/" +
            "hEbKenduTnHPyhRRlHaIeTQlSmgxH5I1JmMqN127e+xjEpGs1TKSOGv0TA==";
        //
        this.dropdownListDoiTuongHienThi = [];
        this.selectedItemsDoiTuongHienThi = [];
        //
        this.dropdownListDonVi = [];
        this.selectedItemsDonVi = [];
        //
    }

    loadDonViChuyenTrach(chucVuId: any) {
        this._donViChuyenTrachService
            .loadDonViChuyenTrach(this.appSession.canBoId, chucVuId)
            .subscribe(result => {
                this.dropdownListDonVi = result;
            });
    }

    onItemSelect(item: any) {
        // console.log(item);
    }
    onSelectAll(items: any) {
        // console.log(items);
    }

    frmReset() {
        //
        this.loaiThoiGianShow = false;
        this.thoiGianBatDauShow = false;
        this.doiTuongHienThiShow = false;
        this.donViShow = false;

        //
        this.dropdownListDoiTuongHienThi = [];
        this.selectedItemsDoiTuongHienThi = [];
        //
        //this.dropdownListDonVi = [];
        this.selectedItemsDonVi = [];
        //
        this.report.load("");
        this.viewer.report = this.report;
        this.viewer.renderHtml("viewer");
        //
        this.isShowTongHopKhoiCacPhuong04 = false;
        this.isShowTongHopKhoiCacDang06 = false;
    }

    loadBaoCaoChucVuCauHinh(chucVuId) {
        this._baoCaoService
            .getBaoCaoChucVuCauHinhTheoChucVu(chucVuId, undefined, undefined)
            .subscribe(result => {
                //this.dropdownListDoiTuongHienThi = result;
                this.dropdownListDoiTuongHienThi = _.map(result, function (x) {
                    return _.assign(x, {
                        id: x.baoCaoNhomDoiTuongId,
                        ten: x.tenNhomDoiTuong
                    });
                });

            });
    }
    refreshBangBaoCao(){
        this.isShowTongHopKhoiCacPhuong04 = false;
        this.isShowTongHopKhoiCacDang06 = false;
        this.isShowTongHopKhoiTruongHoc05 = false;
    }
    viewBaoCaoChiTiet(chucVuId?: any, bieuMauBaoCao?: any, isShow?: any) {
        this.isShowBaoCaoChieTiet = isShow;
        this.baoCaoInfo = new BaoCaoDto();
        this.baoCaoInfo.id = 0;
        this.selectChucVuId = chucVuId;

        //
        this.frmReset();
        //

        if (isShow == true) {
            this.baoCaoInfo = bieuMauBaoCao;

            if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong") {
                this.loaiThoiGianShow = true;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.loadBaoCaoChucVuCauHinh(chucVuId);

            } else if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = false;
                this.donViShow = true;
            } else if (bieuMauBaoCao.source == "BaoCaoChiTietKeQuaDanhGiaTheoDonVi") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = false;
                this.donViShow = true;
            } else if (bieuMauBaoCao.source == "BaoCaoThongBaoKetQuaDanhGia") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            }
            else if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau01" ||
                bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau02" ||
                bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau03") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            } else if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiHangThangMau10") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = false;
                this.donViShow = true;
                this.isStimulsoft = true;
            } else if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoThangMau07") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
                this.isStimulsoft = false;
            } else if (bieuMauBaoCao.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11") {
                this.loaiThoiGianShow = true;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            } else if (bieuMauBaoCao.source == "TongHopKhoiCacPhuong04") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.isStimulsoft = true;
                this.isShowTongHopKhoiCacPhuong04 = true;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            }
            else if (bieuMauBaoCao.source == "TongHopKhoiCacDang06") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.isStimulsoft = true;
                this.isShowTongHopKhoiCacDang06 = true;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            }
            else if (bieuMauBaoCao.source == "TongHopKhoiCacTruongHoc05") {
                this.loaiThoiGianShow = false;
                this.thoiGianBatDauShow = true;
                this.doiTuongHienThiShow = true;
                this.donViShow = false;
                this.isStimulsoft = true;
                this.refreshBangBaoCao();
                this.isShowTongHopKhoiTruongHoc05 = true;
                this.loadBaoCaoChucVuCauHinh(chucVuId);
            }
        }
    }

    getBaoCaoChiTiet() {

        let input = new FilterBaoCaoInput();
        input.loaiThoiGian = this.loaiThoiGian;
        input.thoiGianBatDau = this.thoiGianBatDau.toString();
        input.lstDonVi = this.selectedItemsDonVi;
        input.lstNhomChucVu = this.selectedItemsDoiTuongHienThi;
        input.source = this.source;
        input.baoCaoId = this.baoCaoInfo.id;


        if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong") {
            this._baoCaoService
                .baoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuong(input)
                .subscribe(result => {
                    this.report.load(result);
                    this.viewer.report = this.report;
                    this.viewer.renderHtml("viewer");
                });
        } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi") {
            this._baoCaoService
                .baoCaoTongHopKetQuaDanhGiaXepLoaiTheoDonVi(input)
                .subscribe(result => {
                    this.report.load(result);
                    this.viewer.report = this.report;
                    this.viewer.renderHtml("viewer");
                });
        } else if (this.baoCaoInfo.source == "BaoCaoChiTietKeQuaDanhGiaTheoDonVi") {
            this._baoCaoService
                .baoCaoChiTietKeQuaDanhGiaTheoDonVi(input)
                .subscribe(result => {
                    this.report.load(result);
                    this.viewer.report = this.report;
                    this.viewer.renderHtml("viewer");
                });
        } else if (this.baoCaoInfo.source == "BaoCaoThongBaoKetQuaDanhGia") {
            this._baoCaoService
                .baoCaoThongBaoKetQuaDanhGia(input)
                .subscribe(result => {
                    this.report.load(result);
                    this.viewer.report = this.report;
                    this.viewer.renderHtml("viewer");
                });
        }
        else if (this.baoCaoInfo.source == "TongHopKhoiCacPhuong04") {
            //this.filterModel = input;
            this.tonghopkhoicacphuong.getBaoCao(input);
        }
        else if (this.baoCaoInfo.source == "TongHopKhoiCacDang06") {
            //this.filterModel = input;
            this.tonghopkhoicacdang.getBaoCao(input);
        }
        else if (this.baoCaoInfo.source == "TongHopKhoiCacTruongHoc05") {
            //this.filterModel = input;
            this.tonghopkhoitruonghoc.getBaoCao(input);
        }
        else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau01"
            || this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau02"
            || this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau03"
            || this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoThangMau07"
            || this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiHangThangMau10"
            || this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11") {

            if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau01") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiMau01(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau02") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiMau02(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiMau03") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiMau03(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoThangMau07") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiTheoThangMau07(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiHangThangMau10") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiHangThangMau10(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            } else if (this.baoCaoInfo.source == "BaoCaoTongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11") {
                this._baoCaoService
                    .tongHopKetQuaDanhGiaXepLoaiTheoLoaiDoiTuongMau11(input)
                    .subscribe(result => {
                        this.report.load(result);
                        this.viewer.report = this.report;
                        this.viewer.renderHtml("viewer");
                    });
            }
        }
    }




    close(): void {
        this.isShowBaoCaoChieTiet = false;
        this.modalSave.emit();
        //location.href = "/app/main/dashboard";
    }

    // Method in component class
    trackByFn(index, item) {
        if (index > 5)
            return false;
        else return true;
        // return item.id;
    }
}
