import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component,
    ViewEncapsulation
} from "@angular/core";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { BaoCaoServiceProxy } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { BaoCaoChiTietComponent } from "./bao-cao-chi-tiet.component";

@Component({
    selector: "app-danh-sach-bao-cao",
    templateUrl: "./danh-sach-bao-cao.component.html",
    styleUrls: ['./bao-cao-style.component.css'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DanhSachBaoCaoComponent extends AppComponentBase
    implements OnInit {
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("baoCaoChiTietComponentView", { static: true })
    baoCaoChiTietComponentView: BaoCaoChiTietComponent;

    selectChucVuId: number = this.appSession.chucVuChinhId;
    isViewDsBaoCao = true;
    itemActive: number = 0;

    constructor(
        injector: Injector,
        private _baoCaoService: BaoCaoServiceProxy
    ) {
        super(injector);
    }

    public projectData: Object[];

    ngOnInit() {
        this.getBieuMauBaoCao();
        this.baoCaoChiTietComponentView.loadDonViChuyenTrach(this.selectChucVuId);
    }

    onChangeChucVuChinh() {
        this.getBieuMauBaoCao();
        this.baoCaoChiTietComponentView.loadDonViChuyenTrach(this.selectChucVuId);
    }

    getBieuMauBaoCao() {
        this._baoCaoService
            .getBaoCaoTheoChucVu(this.selectChucVuId, undefined, undefined, 2, "", 1000, 0)
            .subscribe(result => {
                this.projectData = result;
            });

        this.baoCaoChiTietComponentView.viewBaoCaoChiTiet(
            this.selectChucVuId,
            null,
            false
        );
    }
    openViewBaoCao(data?: any): void {
        this.itemActive = data.id;
        this.baoCaoChiTietComponentView.viewBaoCaoChiTiet(
            this.selectChucVuId,
            data,
            true
        );
    }
}
