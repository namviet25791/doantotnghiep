import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OnInit, EventEmitter, Output, ViewChild, Injector, Component } from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { PhieuDanhGiaCanBoServiceProxy, PhieuDanhGiaDto, ThangDiemChiTietServiceProxy, PhieuDanhGiaCanBoUpdateDto, DanhSachPhieuDanhGiaCanBoDto } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-pct-danh-gia-nhan-vien',
    templateUrl: './pct-danh-gia-nhan-vien.component.html',
    styleUrls: ["./style-lanh-dao-danh-gia.component.css"],
    animations: [appModuleAnimation()]
})


export class PhoChuTichDanhGiaNhanVienComponent extends AppComponentBase implements OnInit {
    filterText = '';
    filterMonth = new Date();
    trangThaiPhieu: number = -1;
    selectChucVuId: number = this.appSession.chucVuChinhId;
    thangDiemChiTietDto: any = [];
    saving = false;
    isViewDsPhieuDanhGia = true;
    isViewChiTiet = false;
    dataItemSlected: any = {};

    constructor(
        injector: Injector,
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        private _fileService: FileDownloadService, private route: ActivatedRoute,
        private _thangDiemChiTietService: ThangDiemChiTietServiceProxy,
    ) {
        super(injector);
    }


    lstTrangThaiPhieu: any = [
        { id: -1, text: "Tất cả phiếu đã / chờ đánh giá" },
        { id: 0, text: "Chờ đánh giá" },
        { id: 1, text: "Đã đánh giá" },
        { id: 2, text: "Chưa gửi đánh giá" }
    ];
    public projectData: Object[];

    ngOnInit() {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);
        this.filterMonth = d;
        this.getThangDiemChiTiet();

    }

    getThangDiemChiTiet() {
        this._thangDiemChiTietService
            .getByFilter("", undefined, undefined, undefined,
                "", 4, 0
            )
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.thangDiemChiTietDto = result.items;
                this.getDanhSachPhieuDanhGia();
            });
    }

    getDanhSachPhieuDanhGia() {
        if (this.trangThaiPhieu == 2) {
            this._phieuDanhGiaCanBoService
                .getAllNhanVienChuaDanhGia(
                    this.filterText,
                    undefined,
                    this.trangThaiPhieu,
                    this.appSession.canBoId,
                    undefined,
                    undefined,
                    this.selectChucVuId,
                    undefined,
                    moment(this.filterMonth),
                    '', 1000, 0
                )
                .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
                .subscribe(result => {
                    this.primengTableHelper.records = result.items;
                });
        } else {
            this._phieuDanhGiaCanBoService
                .getDanhSachPhieuCanDuyetPCT(
                    this.filterText,
                    undefined,
                    this.trangThaiPhieu,
                    this.appSession.canBoId,
                    undefined,
                    undefined,
                    this.selectChucVuId,
                    undefined,
                    moment(this.filterMonth),
                    "", 1, 0
                )
                .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
                .subscribe(result => {
                    this.primengTableHelper.records = result;
                    // for (let index = 0; index < 150; index++) {
                    //     let testItem = result[0];
                    //     this.primengTableHelper.records.push(testItem);
                    // }
                });
        }
    }

    onChangeMonthSelection(value) {
        this.filterMonth = value;
        this.getDanhSachPhieuDanhGia();
    }

    async save() {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn với thao tác này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_.saveProsess();
                }
            }
        );
    }

    saveProsess() {
        this.saving = true;
        let lstData = [];

        this.primengTableHelper.records.filter(epic => epic.thangDiemChiTietId > 0).forEach(item => {
            let input: PhieuDanhGiaCanBoUpdateDto = new PhieuDanhGiaCanBoUpdateDto();
            input.phieuDanhGiaCanBoId = item.phieuDanhGiaCanBoId;
            input.thangDiemChiTietId = item.thangDiemChiTietId;
            input.canBoId = item.canBoId;
            input.canBoDanhGiaId = this.appSession.canBoId;
            input.capDanhGia = 2;
            input.nhanXet = item.nhanXet;
            input.ketQuaChiTietId = item.ketQuaDanhGiaChiTietId;
            input.chucVuId = this.selectChucVuId;
            input.loaiChucVuDanhGia = item.loaiChucVuDanhGia;

            lstData.push(input);
        });

        if (lstData.length > 0) {
            this._phieuDanhGiaCanBoService
                .taoMoiVaCapNhatKetQuaNguoiDuyet(lstData)
                .pipe(finalize(() => (this.saving = false)))
                .subscribe(result => {
                    if (result) {
                        this.notify.success(this.l("SavedSuccessfully"));
                    } else {
                        this.notify.error(
                            "Có lỗi xảy ra vui lòng thao tác lại!"
                        );
                    }
                });
        } else {
            this.notify.error(
                "Chưa có ai được đánh giá!"
            );
        }
    }

    showChiTietPhieuDanhGia(data?: any) {
        this.isViewDsPhieuDanhGia = false;

        let input = new DanhSachPhieuDanhGiaCanBoDto();
        input.id = data.phieuDanhGiaCanBoId;
        this.dataItemSlected = input;
    }

    onModalPhieuDanhGiaClose($event: boolean) {
        this.isViewDsPhieuDanhGia = true;
        this.isViewChiTiet = false;

        if ($event) {
            this.getDanhSachPhieuDanhGia();
        }
    }
}
