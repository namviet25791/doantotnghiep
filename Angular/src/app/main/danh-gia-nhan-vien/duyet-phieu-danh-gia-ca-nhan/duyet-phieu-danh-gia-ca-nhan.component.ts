import {
    Component,
    OnInit,
    Injector,
    ViewChild,
    EventEmitter,
    Output,
    Input
} from "@angular/core";
import * as moment from "moment";
import * as _ from "lodash";
import {
    TieuChiDanhGiaDto,
    ThangDiemChiTietDto,
    KetQuaDanhGiaChiTietServiceProxy,
    DanhSachPhieuDanhGiaCanBoDto,
    ThongTinDuyetPhieuDanhGiaCanBoDto,
    PhieuDanhGiaCanBoServiceProxy,
    PhieuDanhGiaDto,
    CanBoGuiDanhGiaDto,
    PhieuDanhGiaTongHop,
    PhieuDanhGiaCanBoUpdateDto,
} from "@shared/service-proxies/service-proxies";
import { AppComponentBase } from "@shared/common/app-component-base";
import { DxTreeListComponent } from "devextreme-angular";
import { ActivatedRoute } from "@angular/router";
import { finalize } from "rxjs/operators";

enum TypeRow {
    Title = 1,
    Field = 2
}
@Component({
    selector: "app-duyet-phieu-danh-gia-ca-nhan",
    templateUrl: "./duyet-phieu-danh-gia-ca-nhan.component.html",
    styleUrls: ['./duyet-phieu-danh-gia-ca-nhan.component.css'],
})
export class DuyetPhieuDanhGiaNhanVienComponent extends AppComponentBase
    implements OnInit {
    phieuDanhGia: TieuChiDanhGiaDto = new TieuChiDanhGiaDto();
    constructor(
        injector: Injector,
        private _ketQuaDanhGiaChiTietService: KetQuaDanhGiaChiTietServiceProxy,
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        private route: ActivatedRoute
    ) {
        super(injector);

    }
    @Input() phieuDanhGiaCanBoInput: any;
    @Input() chucVuDanhGiaId: any;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    public projectData: any[];
    public dateValue: moment.Moment;
    public totalMark: number = 0;
    public maxTotalMark: number = 0;
    public totalMarkDisplayText: string = "0";

    saving = false;
    typeRow = TypeRow;
    thangDiemId = 0;
    perMark: number = 0;
    thangDiemChiTietDto: ThangDiemChiTietDto[] = [];
    isViewPhieuDanhGia = false;
    phieuDanhGiaCanBoDto = new DanhSachPhieuDanhGiaCanBoDto();
    isLoadingForm = false;
    ketQuaTongHop: any = [];
    thongTinDuyetPhieuDanhGiaCanBoDto: ThongTinDuyetPhieuDanhGiaCanBoDto = new ThongTinDuyetPhieuDanhGiaCanBoDto();
    phieuDanhGiaDto = new PhieuDanhGiaDto();
    canBoGuiDanhGiaDto: CanBoGuiDanhGiaDto = new CanBoGuiDanhGiaDto();
    @ViewChild("donViTreeList", { static: false }) donViTree: DxTreeListComponent;
    isViewChiTietTuEmail = true;
    isDanhGiaPhieu = true;
    linkPhieuDanhGiaCanBo = "";
    tenCanBo = "";
    phieuDanhGiaTongHop: PhieuDanhGiaTongHop = new PhieuDanhGiaTongHop();
    nhanXetChung = "";

    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            if (params.get('id') != null && params.get('cv') != null) {
                this.isViewChiTietTuEmail = true;
                this.linkPhieuDanhGiaCanBo = "/app/main/danh-gia-nhan-vien;id=" + params.get('id') + ";cv=" + params.get('cv') + ";t=2";
            }
        });

        this.getTieuChiDanhGia(this.phieuDanhGiaCanBoInput, this.chucVuDanhGiaId);
    }

    chuyenTrang() {
        window.location.href = this.linkPhieuDanhGiaCanBo;
    }

    onExpandClick(index: number) {
        const dataItem = this.projectData[index];
        dataItem.expanded = !dataItem.expanded;
        let expand = (currentId: any) => {
            this.projectData.filter(f => f.tieuChiChaId === currentId).forEach(item => {
                item.isShow = dataItem.expanded;
                item.expanded = dataItem.expanded;
                expand(item.id);
            });
        };
        expand(dataItem.id);
        // this.projectData[index].expanded = !dataItem.expanded;
    }

    getTieuChiDanhGia(phieuDanhGiaCanBoInput?: any, chucVuDanhGiaId?: any, isEdit?: any, isDanhGia?: any) {
        if (isDanhGia == true) {
            this.isViewChiTietTuEmail = true;
            this.isDanhGiaPhieu = true;
        } else {
            this.isViewChiTietTuEmail = false;
            this.isDanhGiaPhieu = false;
        }

        const that = this;
        this.projectData = [];
        this.totalMark = 0;
        this.maxTotalMark = 0;
        this.totalMarkDisplayText = "0";
        this.thangDiemId = 0;
        this.perMark = 0;
        this.thangDiemChiTietDto = [];
        this.ketQuaTongHop = [];
        this.phieuDanhGiaCanBoDto = new DanhSachPhieuDanhGiaCanBoDto();
        this.phieuDanhGiaTongHop = new PhieuDanhGiaTongHop();
        this.phieuDanhGiaDto = new PhieuDanhGiaDto();

        this.phieuDanhGiaCanBoDto = phieuDanhGiaCanBoInput;
        let canBoId = this.appSession.canBoId;
        this.isLoadingForm = true;

        this._phieuDanhGiaCanBoService.getChiTietPhieuDanhGiaTheoPhieuDanhGiaCanBoId(this.phieuDanhGiaCanBoDto.id, canBoId, chucVuDanhGiaId, false).subscribe(result => {
            this.thangDiemChiTietDto = _.sortBy(result.thangDiemChiTietDto, ['diemSan'], ['desc']);
            this.projectData = this.chuanHoaData(result.phieuDanhGiaChiTietDto);
            this.phieuDanhGiaDto = result.phieuDanhGiaDto;
            this.canBoGuiDanhGiaDto = result.thongTinCanBoDto;
            this.phieuDanhGiaTongHop = result;

            this.ketQuaTongHop = [];
            this._ketQuaDanhGiaChiTietService
                .getKetQuaChiTiet(this.phieuDanhGiaCanBoDto.id)
                .subscribe(resultKetQuaDanhGia => {
                    this.ketQuaTongHop = [];
                    let index = resultKetQuaDanhGia.findIndex(x => x.canBoDanhGiaId == this.appSession.canBoId
                        && x.chucVuCanBoDanhGiaID == chucVuDanhGiaId);

                    if (index > 0) {
                        resultKetQuaDanhGia.splice(index, 1);
                    }
                    this.ketQuaTongHop = resultKetQuaDanhGia;

                    this.loadThongTinDuyetPhieuTheoCanBoDanhGia();
                });

            this.isLoadingForm = false;
        });


    }

    loadThongTinDuyetPhieuTheoCanBoDanhGia(): void {
        this._phieuDanhGiaCanBoService
            .getThongTinDuyetPhieuTheoCanBoDanhGia(
                this.phieuDanhGiaCanBoDto.id,
                this.phieuDanhGiaCanBoDto.canBoId,
                this.appSession.canBoId,
                this.chucVuDanhGiaId
            )
            .subscribe(result => {
                this.thongTinDuyetPhieuDanhGiaCanBoDto = result;
                this.nhanXetChung = result.nhanXet;

                // if (this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId == undefined
                //     || this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId == null) {
                //     this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId = this.thangDiemLanhDaoChamId;
                // }
            });
    }

    checkTypeRow(isCapCha: boolean): TypeRow {
        //1: Row title || 2 : row field
        if (isCapCha) {
            return this.typeRow.Title;
        } else {
            return this.typeRow.Field;
        }
    }

    chuanHoaData(data: any[]): any[] {
        let res: any[] = [];
        this.totalMark = 0;
        this.maxTotalMark = 0;
        let chuanHoa = (data: any[], parentId: any, level: number = 1, classNameParent = '') => {
            const root = _.sortBy(data.filter(f => f.tieuChiChaId === parentId), ['thuTu'], ['asc']);
            root.forEach((item, index) => {
                const isLeaf = data.filter(f => f.tieuChiChaId === item.id).length <= 0;
                const className = (classNameParent ? classNameParent + ' ' : '') + 'item-gr' + item.id;

                res.push({
                    ...item,
                    level: level,
                    isLeaf: isLeaf,
                    className: className,
                    isShow: true,
                    expanded: true,
                });
                this.totalMark += isLeaf ? item.diemLanhDaoCham : 0;
                chuanHoa(data, item.id, level + 1, className);
                if (item.isCapCha === true && item.tieuChiChaId === null) {
                    this.maxTotalMark += item.diemToiDa;
                }
            });
        };
        chuanHoa(data, null, 1);
        return res;
    }

    async save() {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn với thao tác này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_.saveProsess();
                }
            }
        );
    }
    saveProsess() {
        this.saving = true;
        if (
            this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId ==
            undefined ||
            this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId == 0
        ) {
            this.saving = false;
            this.notify.error("Vui lòng chọn xếp loại đánh giá.");
        } else {
            let input: PhieuDanhGiaCanBoUpdateDto = new PhieuDanhGiaCanBoUpdateDto();
            input.phieuDanhGiaCanBoId = this.phieuDanhGiaCanBoDto.id;
            input.thangDiemChiTietId = this.thongTinDuyetPhieuDanhGiaCanBoDto.thangDiemChiTietId;
            input.canBoId = this.phieuDanhGiaCanBoDto.canBoId;
            input.canBoDanhGiaId = this.appSession.canBoId;
            input.capDanhGia = 2; // lanh dao danh gia
            input.nhanXet = this.thongTinDuyetPhieuDanhGiaCanBoDto.nhanXet;
            input.ketQuaChiTietId = this.thongTinDuyetPhieuDanhGiaCanBoDto.ketQuaChiTietId;
            input.chucVuId = this.chucVuDanhGiaId;
            input.loaiChucVuDanhGia = this.phieuDanhGiaCanBoDto.loaiChucVuDanhGia;
            input.lichSuKetQuaDanhGiaChiTietId = this.thongTinDuyetPhieuDanhGiaCanBoDto.lichSuKetQuaDanhGiaChiTietId;

            this._phieuDanhGiaCanBoService
                .taoMoiVaCapNhatKetQuaNguoiDuyetProcess(input)
                .pipe(finalize(() => (this.saving = false)))
                .subscribe(result => {
                    if (result) {
                        this.loadThongTinDuyetPhieuTheoCanBoDanhGia();
                        this.notify.success(this.l("SavedSuccessfully"));
                        this.close();
                    } else {
                        this.notify.error(
                            "Có lỗi xảy ra vui lòng thao tác lại!"
                        );
                    }
                });
        }
    }

    close(): void {
        this.isViewPhieuDanhGia = false;
        this.modalSave.emit(true);
    }

}
