import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OnInit, EventEmitter, Output, ViewChild, Injector, Component } from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import { CanBoServiceProxy, PhieuDanhGiaCanBoServiceProxy, PhieuDanhGiaDto } from '@shared/service-proxies/service-proxies';
import { ChamDiemPhieuDanhGiaNhanVienComponent } from './cham-diem-phieu-danh-gia-ca-nhan/cham-diem-phieu-danh-gia-ca-nhan.component';
//import { ChiTietPhieuDanhGiaNhanVienComponent } from "./chi-tiet-phieu/chi-tiet-phieu-danh-gia-nhan-vien.component";
import * as moment from 'moment';
import { any } from '@amcharts/amcharts4/.internal/core/utils/Array';
import { DuyetPhieuDanhGiaNhanVienComponent } from './duyet-phieu-danh-gia-ca-nhan/duyet-phieu-danh-gia-ca-nhan.component';
import { ChiTietPhieuDanhGiaCaNhanComponent } from '../phieu-danh-gia-can-bo/chi-tiet-phieu/chi-tiet-phieu-danh-gia-ca-nhan.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-danh-gia-nhan-vien',
    templateUrl: './danh-gia-nhan-vien.component.html',
    animations: [appModuleAnimation()]
})
export class DanhGiaNhanVienComponent extends AppComponentBase implements OnInit {
    filterText = '';
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild('paginator', { static: true }) paginator: Paginator;

    @ViewChild('chamDiemPhieuDanhGiaNhanVienView', { static: true })
    chamDiemPhieuDanhGiaNhanVienView: ChamDiemPhieuDanhGiaNhanVienComponent;

    @ViewChild('duyetPhieuDanhGiaNhanVienView', { static: true })
    duyetPhieuDanhGiaNhanVienView: DuyetPhieuDanhGiaNhanVienComponent;

    @ViewChild('chiTietPhieuComponentView', { static: true })
    chiTietPhieuComponentView: ChiTietPhieuDanhGiaCaNhanComponent;
    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    isViewDsLichSu = true;
    filterMonth: Date = new Date();
    selectChucVuId: number = this.appSession.chucVuChinhId;
    trangThaiPhieu: number = -1;
    dataItemSlected: any = {};

    isViewDanhGia = false;
    isViewChiTiet = false;
    isViewDuyetPhieu = false;
    //dropdow list
    public options: Select2Options;
    lstTrangThaiPhieu: any = [
        { id: -1, text: "Tất cả phiếu đã / chờ đánh giá" },
        { id: 0, text: "Chờ đánh giá" },
        { id: 1, text: "Đã đánh giá" },
        { id: 2, text: "Chưa gửi đánh giá" }
    ];
    //end dropdow list

    constructor(
        injector: Injector,
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        private _fileService: FileDownloadService, private route: ActivatedRoute
    ) {
        super(injector);
    }

    public projectData: Object[];

    ngOnInit() {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);
        this.filterMonth = d;

        this.route.paramMap.subscribe(params => {

            //t = 1 la view
            //t = 2 la cham

            var cv = parseInt(params.get('cv'));
            var id = parseInt(params.get('id'));
            if (id > 0 && id != NaN && cv != NaN && cv > 0) {
                this._phieuDanhGiaCanBoService.kiemTraThongTinPhieuTuUrl(id, cv)
                    .subscribe(rs => {
                        if (rs.result == false) {
                            if (rs.msg != null) {
                                // abp.message.error(
                                //     rs.msg,
                                //     "Thông báo");

                                // setTimeout(() => {
                                //     window.location.href = "/app/main/danh-gia-nhan-vien";
                                // }, 3000);
                            }
                            else {

                                this.isViewDsLichSu = false
                                let chucVu = cv;
                                let input = new PhieuDanhGiaDto();
                                input.id = id;
                                this.chiTietPhieuComponentView.getTieuChiDanhGia(input, chucVu, true, false);
                            }
                        } else {
                            this.isViewDsLichSu = false
                            let chucVu = cv;
                            let input = new PhieuDanhGiaDto();
                            input.id = id;

                            let viewType = (parseInt(params.get('t')) != null) ? parseInt(params.get('t')) : 1;

                            if (viewType == 1) {
                                this.chiTietPhieuComponentView.getTieuChiDanhGia(input, chucVu, true, true);
                            }

                            if (viewType == 2) {
                                this.chiTietPhieuComponentView.getTieuChiDanhGia(input, chucVu, true, false);
                                if (rs.isNguoiCham == true) {
                                    this.chamDiemPhieuDanhGiaNhanVienView.getTieuChiDanhGia(input, chucVu, true, true);
                                } else {
                                    this.duyetPhieuDanhGiaNhanVienView.getTieuChiDanhGia(input, chucVu, true, true);
                                }
                            }
                        }
                    });
            } else {
                this.getCanBo();
            }
        });
    }

    getCanBo(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);
        let maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);

        if (this.trangThaiPhieu == 2) {
            this._phieuDanhGiaCanBoService
                .getAllNhanVienChuaDanhGia(
                    this.filterText,
                    undefined,
                    this.trangThaiPhieu,
                    this.appSession.canBoId,
                    undefined,
                    undefined,
                    this.selectChucVuId,
                    undefined,
                    moment(this.filterMonth),
                    '',
                    maxResultCount,
                    skipCount
                )
                .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
                .subscribe(result => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    // this.primengTableHelper.records = result.items;
                    this.primengTableHelper.records = [];
                    let soThuTu = skipCount + 1;
                    result.items.forEach(value => {
                        let model: any = Object.assign({}, value);
                        model.stt = soThuTu++;
                        this.primengTableHelper.records.push(model);
                    });

                    this.primengTableHelper.hideLoadingIndicator();
                });
        } else {
            this._phieuDanhGiaCanBoService
                .getAllPhieuDanhGiaNhanVienTheoChucVuLanhDao(
                    this.filterText,
                    undefined,
                    this.trangThaiPhieu,
                    this.appSession.canBoId,
                    undefined,
                    undefined,
                    this.selectChucVuId,
                    undefined,
                    moment(this.filterMonth),
                    '',
                    maxResultCount,
                    skipCount
                )
                .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
                .subscribe(result => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    // this.primengTableHelper.records = result.items;
                    this.primengTableHelper.records = [];
                    let soThuTu = skipCount + 1;
                    result.items.forEach(value => {
                        let model: any = Object.assign({}, value);
                        model.stt = soThuTu++;
                        this.primengTableHelper.records.push(model);
                    });

                    this.primengTableHelper.hideLoadingIndicator();
                });
        }
    }

    onChangeMonthSelection(value) {
        this.filterMonth = value;
        this.getCanBo();
    }

    xuatWord(data: any) {
        this._phieuDanhGiaCanBoService.xuatWord(data.id).subscribe(response => {
            this._fileService.downloadTempFile(response);
        });
    }

    xuatPDF(data: any) {
        this._phieuDanhGiaCanBoService.xuatPDF(data.id).subscribe(response => {
            this._fileService.downloadTempFile(response);
        });
    }

    showPhieuDanhGia(data?: any) {
        this.isViewDsLichSu = false;
        this.dataItemSlected = data;
    }

    showDuyetPhieuDanhGia(data?: any) {
        this.isViewDsLichSu = false;
        this.dataItemSlected = data;
    }

    showChiTietPhieuDanhGia(data?: any) {
        this.isViewDsLichSu = false;
        this.dataItemSlected = data;
    }

    onModalPhieuDanhGiaClose($event: boolean) {
        this.isViewDsLichSu = true;
        this.isViewDanhGia = false;
        this.isViewChiTiet = false;
        this.isViewDuyetPhieu = false;
        if ($event) {
            this.getCanBo();
        }
    }
}
