import { Component, EventEmitter, Injector, OnInit, Output, Input, OnDestroy } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as _ from 'lodash';
import {
    CanBoGuiDanhGiaDto,
    DanhSachPhieuDanhGiaCanBoDto,
    KetQuaDanhGiaChiTietServiceProxy,
    PhieuDanhGiaCanBoServiceProxy, PhieuDanhGiaCanBoUpdateDto,
    PhieuDanhGiaChucVuServiceProxy,
    PhieuDanhGiaDto,
    ThangDiemChiTietDto,
    TieuChiDanhGiaServiceProxy, TieuChiItem, PhieuDanhGiaTongHop,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { finalize, tap, debounceTime } from '@node_modules/rxjs/operators';
import { Observable } from 'rxjs';

enum TypeRow {
    Title = 1,
    Field = 2
}

enum TrangThaiGui {
    LuuNhap = 2,
    DaGui = 3,
}

@Component({
    selector: "app-cham-diem-phieu-danh-gia-ca-nhan",
    templateUrl: "./cham-diem-phieu-danh-gia-ca-nhan.component.html",
    styleUrls: ["./cham-diem-phieu-danh-gia-ca-nhan.component.css"],
    animations: [appModuleAnimation()]
})
export class ChamDiemPhieuDanhGiaNhanVienComponent extends AppComponentBase
    implements OnInit, OnDestroy {
    @Input() phieuDanhGiaCanBoInput: any;
    @Input() chucVuDanhGiaId: any;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    isLoadingForm = false;
    prefixField = 'item_';
    rfDataModal: FormGroup;
    typeRow = TypeRow;
    trangThaiGui = TrangThaiGui;
    public projectData: any[];
    phieuDanhGiaCanBoDto: any = new DanhSachPhieuDanhGiaCanBoDto();
    phieuDanhGiaDto: PhieuDanhGiaDto = new PhieuDanhGiaDto();
    thoiGianTaoPhieuText = '';
    canBoGuiDanhGiaDto: CanBoGuiDanhGiaDto = new CanBoGuiDanhGiaDto();
    thangDiemChiTietDto: ThangDiemChiTietDto[] = [];
    phieuDanhGiaTongHop: PhieuDanhGiaTongHop = new PhieuDanhGiaTongHop();
    thangDiemId = 0;
    perMark = 0;
    thangDiemDisplayText = '';
    totalMark = 0;
    maxTotalMark = 0;
    saving = false;
    ketQuaTongHop = [];
    nhanXetChung = "";
    oldData: any = [];

    constructor(
        injector: Injector,
        private _tieuChiDanhGiaService: TieuChiDanhGiaServiceProxy,
        private _phieuDanhGiaChucVuService: PhieuDanhGiaChucVuServiceProxy,
        private _ketQuaDanhGiaChiTietService: KetQuaDanhGiaChiTietServiceProxy,
        private _phieuDanhGiaCanBoService: PhieuDanhGiaCanBoServiceProxy,
        private fb: FormBuilder,
    ) {
        super(injector);
    }


    ngOnInit() {
        this.rfDataModal = this.fb.group({});
        $('#form').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault();
            });
        });
        $('#form').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll');
        });
        this.getTieuChiDanhGia(this.phieuDanhGiaCanBoInput, this.chucVuDanhGiaId);
        // this.rfDataModal.valueChanges.pipe(
        //     debounceTime(2000),
        //     tap(val => console.log(val))
        // ).subscribe(data =>
        //     console.log("data", data)
        // );
    }
    ngOnDestroy(): void {
        // throw new Error("Method not implemented.");
    }
    getTieuChiDanhGia(
        phieuDanhGiaCanBoInput?: any,
        chucVuDanhGiaId?: any,
        isEdit?: any,
        isShow?: any
    ) {
        // this.isViewPhieuDanhGia = true;
        if (!phieuDanhGiaCanBoInput) {
            this.phieuDanhGiaCanBoDto.trangThai = 2;
            let d = new Date();
            let date = new Date(d.setMonth(d.getMonth() - 1));
            this.phieuDanhGiaCanBoDto.thoiGianTaoPhieu = date;
            this.thoiGianTaoPhieuText = (date.getMonth() + 1).toString() + '/' + date.getFullYear().toString();
            this.phieuDanhGiaCanBoDto.hetHanDanhGia = false;
            this.phieuDanhGiaCanBoDto.isSuaKetQua = true;
        } else {
            this.phieuDanhGiaCanBoDto = phieuDanhGiaCanBoInput;
        }
        //let canBoId = this._sessionAppService.canBoId;
        this.isLoadingForm = true;
        this._phieuDanhGiaCanBoService
            .getChiTietPhieuDanhGia(
                this.appSession.canBoId,
                chucVuDanhGiaId,
                phieuDanhGiaCanBoInput ? phieuDanhGiaCanBoInput.id : undefined
            )
            .subscribe(result => {
                this.thangDiemChiTietDto = _.sortBy(result.thangDiemChiTietDto, ['diemSan'], ['desc']);
                this.projectData = this.chuanHoaData(result.phieuDanhGiaChiTietDto);

                let rfDataModalTemp = this.fb.group({});
                this.projectData.forEach(item => {
                    let formGr = this.fb.group({
                        type: this.typeRow.Title,
                        tieuChiId: item.id,
                        diemToiDa: item.diemToiDa,
                        diemToiThieu: item.diemToiThieu,
                        tieuChiChaId: item.tieuChiChaId,
                    });
                    if (this.checkTypeRow(item.isCapCha) === this.typeRow.Field) {
                        formGr = this.fb.group({
                            tieuChiId: item.id,
                            tieuChiChaId: item.tieuChiChaId,
                            type: this.typeRow.Field,
                            diemToiDa: item.diemToiDa,
                            diemToiThieu: item.diemToiThieu,
                            diemTuCham: item.diemTuCham,
                            diem: [item.diemLanhDaoCham, [
                                Validators.required,
                                Validators.min(0),
                                (c: AbstractControl) => {
                                    const v = c.value;
                                    // console.log("AbstractControl", v);

                                    if (v > 0 && (v < item.diemToiThieu || v > item.diemToiDa))
                                        return {
                                            errorMaxValue: true
                                        }
                                }
                            ]],
                        }, { updateOn: 'change' });
                    }
                    rfDataModalTemp.addControl(this.prefixField + item.id, formGr);
                });

                this.oldData = rfDataModalTemp;
                this.rfDataModal = rfDataModalTemp;
                this.phieuDanhGiaDto = result.phieuDanhGiaDto;
                this.canBoGuiDanhGiaDto = result.thongTinCanBoDto;
                this.nhanXetChung = result.nhanXetChung;
                this.phieuDanhGiaTongHop = result;

                this._ketQuaDanhGiaChiTietService
                    .getKetQuaChiTiet(this.phieuDanhGiaCanBoDto.id)
                    .subscribe(resultKetQuaDanhGia => {

                        this.ketQuaTongHop = [];

                        let index = resultKetQuaDanhGia.findIndex(x => x.canBoDanhGiaId == this.appSession.canBoId
                            && x.chucVuCanBoDanhGiaID == chucVuDanhGiaId);

                        if (index > 0) {
                            resultKetQuaDanhGia.splice(index, 1);
                        }
                        this.ketQuaTongHop = resultKetQuaDanhGia;
                    });

                this.isLoadingForm = false;
            });
    }

    checkTypeRow(isCapCha: boolean): TypeRow {
        //1: Row title || 2 : row field
        if (isCapCha) {
            return this.typeRow.Title;
        } else {
            return this.typeRow.Field;
        }
    }

    //#region validate Diem
    diemIsInValid(id: any, errorName: string = 'required'): boolean {
        const field = this.rfDataModal.get(this.prefixField + id).get('diem');
        const isTouch = field.touched;
        const inVAlid = field.invalid;
        return inVAlid && isTouch;
    }

    diemHasError(id: any, errorName: string = 'required'): boolean {
        const field = this.rfDataModal.get(this.prefixField + id).get('diem');
        const hasError = field.hasError(errorName);
        const isTouch = field.touched;
        return hasError && isTouch;
    }


    //#endregion


    close(): void {
        // this.isViewPhieuDanhGia = false;
        this.modalSave.emit(true);
        //location.href = "/app/main/dashboard";
    }

    save() {
        const that = this;
        abp.message.confirm(
            'Bạn chắc chắn với thao tác này?',
            'Thông báo',
            function (result) {
                if (result) {
                    if (that.rfDataModal.invalid) {
                        that.notify.error('Vui lòng kiểm tra lại thông tin!');
                        // tslint:disable-next-line:forin
                        for (const i in that.rfDataModal.controls) {
                            that.rfDataModal.controls[i].markAsDirty();
                            that.rfDataModal.controls[i].markAllAsTouched();
                            that.rfDataModal.controls[i].updateValueAndValidity();
                        }
                    } else {
                        that.saving = true;
                        that.saveProsess();
                    }
                }
            }
        );
    }

    saveProsess() {
        if (this.totalMark > this.maxTotalMark) {
            this.notify.error(
                'Tổng điểm đánh giá không thể cao hơn tổng điểm cả phiếu!'
            );
            this.saving = false;
            return;
        }
        const formData = this.rfDataModal.value;
        let listTieuChi: TieuChiItem[] = [];
        // tslint:disable-next-line:forin
        for (const property in formData) {
            const valueField = formData[property];
            if (valueField.type === this.typeRow.Field) {
                const temp = new TieuChiItem();
                temp.tieuChiId = formData[property].tieuChiId;
                temp.diem = formData[property].diem;
                listTieuChi.push(temp);
            }
        }

        let item = new TieuChiItem();
        item.tieuChiId = -1;
        item.diem = 0;
        item.nhanXet = this.nhanXetChung;
        listTieuChi.push(item);

        let input: PhieuDanhGiaCanBoUpdateDto = new PhieuDanhGiaCanBoUpdateDto();
        input.phieuDanhGiaCanBoId = this.phieuDanhGiaCanBoDto.id;
        input.phieuDanhGiaId = this.phieuDanhGiaDto.id;
        input.canBoId = this.phieuDanhGiaCanBoDto.canBoId;
        input.canBoDanhGiaId = this.appSession.canBoId;
        input.capDanhGia = 2;
        input.trangThai = 3; // Moi = 2, DaGui = 3
        input.lstTieuChi = listTieuChi;
        input.thangDiemChiTietId = this.thangDiemId;
        input.chucVuId = this.chucVuDanhGiaId;
        input.lichSuKetQuaDanhGiaChiTietId = this.phieuDanhGiaTongHop.lichSuCanBoDangDanhGiaId;
        input.loaiChucVuDanhGia = this.phieuDanhGiaCanBoDto.loaiChucVuDanhGia;

        this._phieuDanhGiaCanBoService
            .taoMoiHoacCapNhatPhieuDanhGiaCanBo(input)
            .pipe(finalize(() => {
                this.saving = false;
            }))
            .subscribe(result => {
                if (result) {
                    this.modalSave.emit(true);
                    this.notify.success(this.l('SavedSuccessfully'));
                } else {
                    this.notify.error(
                        'Có lỗi xảy ra vui lòng thao tác lại!'
                    );
                }
            });
    }

    //#region Table Event


    chuanHoaData(data: any[]): any[] {
        let res: any[] = [];
        this.totalMark = 0;
        this.maxTotalMark = 0;
        let chuanHoa = (data: any[], parentId: any, level: number = 1, classNameParent = '') => {
            const root = _.sortBy(data.filter(f => f.tieuChiChaId === parentId), ['thuTu'], ['asc']);
            root.forEach((item, index) => {
                const isLeaf = data.filter(f => f.tieuChiChaId === item.id).length <= 0;
                const className = (classNameParent ? classNameParent + ' ' : '') + 'item-gr' + item.id;
                res.push({
                    ...item,
                    level: level,
                    isLeaf: isLeaf,
                    className: className,
                    isShow: true,
                    expanded: true,
                });
                this.totalMark += isLeaf ? item.diemLanhDaoCham : 0;
                chuanHoa(data, item.id, level + 1, className);
                if (item.isCapCha === true && item.tieuChiChaId === null) {
                    this.maxTotalMark += item.diemToiDa;
                }
            });
        };
        chuanHoa(data, null, 1);
        // this.onDiemChange();
        this.perMark = (this.totalMark / this.maxTotalMark) * 100;
        this.getThangDiemDisplayText(this.totalMark);
        return res;
    }

    onExpandClick(index: number) {
        const dataItem = this.projectData[index];
        dataItem.expanded = !dataItem.expanded;
        let expand = (currentId: any) => {
            this.projectData.filter(f => f.tieuChiChaId === currentId).forEach(item => {
                item.isShow = dataItem.expanded;
                item.expanded = dataItem.expanded;
                expand(item.id);
            });
        };
        expand(dataItem.id);
        // this.projectData[index].expanded = !dataItem.expanded;
    }



    onDiemChange() {

        const formData = this.rfDataModal.value;
        this.totalMark = 0;
        // tslint:disable-next-line:forin
        for (const property in formData) {
            const valueField = formData[property];
            if (valueField.type === this.typeRow.Field) {
                let diem = parseFloat(valueField.diem);
                if (Number.isNaN(diem)) {
                    diem = 0;
                }

                this.totalMark += diem;
            }
        }

        this.perMark = this.totalMark / this.maxTotalMark * 100;
        this.getThangDiemDisplayText(this.totalMark);
    }


    only_number(event) {
        var k;
        k = event.charCode;

        if (k === 46 || (k >= 48 && k <= 57)) {
            var character = String.fromCharCode(k)
            var newValue = (event.target.value + character).toString();
            if (isNaN(+newValue) || this.hasDecimalPlace(newValue, 2)) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    hasDecimalPlace(value, x) {
        var pointIndex = value.indexOf('.');
        return pointIndex >= 0 && pointIndex < value.length - x;
    }

    getTongDiemGroup(id: any, lv: any): any {
        let lstId = [];
        const typeRow = this.typeRow;
        let sum = 0;
        let listChildId = (parentId: any) => {
            try {
                const formData = this.rfDataModal.value;
                // tslint:disable-next-line:forin
                for (const property in formData) {
                    const valueField = formData[property];
                    if (valueField.tieuChiChaId === parentId) {
                        listChildId(valueField.tieuChiId);
                        lstId.push(valueField.tieuChiId);
                        if (valueField.type = typeRow.Field) {
                            // const diem = isNaN(valueField.diem) ? 0 : valueField.diem;
                            // const diemToiDa = isNaN(valueField.diemToiDa) ? 0 : valueField.diemToiDa;
                            // sum += diem > diemToiDa ? diemToiDa : diem;

                            let diem = parseFloat(valueField.diem);
                            if (Number.isNaN(diem)) {
                                diem = 0;
                            }
                            sum += diem;
                        }
                    }
                }
            } catch (e) {
            }
        };
        listChildId(id);
        return sum;

    }

    getThangDiemDisplayText(mark: number) {
        const that = this;
        _.forEach(this.thangDiemChiTietDto, function (it) {
            if (mark >= it.diemSan && mark <= it.diemTran) {
                that.thangDiemDisplayText = it.ten;
                that.thangDiemId = it.id;
                return;
            }
            if (mark >= it.diemTran && mark < it.diemSan) {
                that.thangDiemDisplayText = it.ten;
                that.thangDiemId = it.id;
                return;
            }
        });
    }
    //#endregion

    dongYTieuChiConLai(): void {
        const formData = this.rfDataModal.value;
        for (const property in formData) {
            const fieldValue = this.rfDataModal.get(property).value;
            if (fieldValue.type === this.typeRow.Field) {
                let diemtt = 0;
                if (fieldValue.diem || fieldValue.diem == 0 ) {
                    diemtt = fieldValue.diem;
                } else {
                    diemtt = fieldValue.diemTuCham;
                    this.rfDataModal.get(property).patchValue({ 'diem': fieldValue.diemTuCham });
                }
                this.totalMark += diemtt;
            }
        }
        this.onDiemChange();
    }

    dongYTatCaDiem(): void {
        // event.preventBubble = true;
        const formData = this.rfDataModal.value;
        for (const property in formData) {
            const fieldValue = this.rfDataModal.get(property).value;
            if (fieldValue.type === this.typeRow.Field) {
                this.rfDataModal.get(property).patchValue({ 'diem': fieldValue.diemTuCham });
            }
        }
        this.onDiemChange();
    }



}
