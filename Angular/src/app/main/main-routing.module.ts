import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DemoUiSharedComponent } from './demo-ui/demo-ui.component';
import { PhieuDanhGiaCanBoComponent } from './phieu-danh-gia-can-bo/phieu-danh-gia-can-bo.component';
import { DanhGiaNhanVienComponent } from './danh-gia-nhan-vien/danh-gia-nhan-vien.component';
import { DanhSachBaoCaoComponent } from './bao-cao/danh-sach-bao-cao.component';
import { ThongKePhieuDanhGiaComponent } from './thong-ke/thong-ke-phieu-danh-gia.component';
import { ChuTichDanhGiaNhanVienComponent } from './lanh-dao-danh-gia/ct-danh-gia-nhan-vien.component';
import { PhoChuTichDanhGiaNhanVienComponent } from './lanh-dao-danh-gia/pct-danh-gia-nhan-vien.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                children: [
                    {
                        path: 'dashboard',
                        component: DashboardComponent,
                        //data: { permission: 'Pages.Systems.Dashboard' }
                    },
                    {
                        path: 'demo-ui',
                        component: DemoUiSharedComponent
                    }, {
                        path: 'phieu-danh-gia-ca-nhan',
                        component: PhieuDanhGiaCanBoComponent,
                        data: { permission: 'Pages.User.DanhGiaCaNhan' }
                    },
                    {
                        path: 'phieu-danh-gia-ca-nhan/:id',
                        component: PhieuDanhGiaCanBoComponent,
                        data: { permission: 'Pages.User.DanhGiaCaNhan' }
                    },
                    {
                        path: 'danh-gia-nhan-vien',
                        component: DanhGiaNhanVienComponent,
                        data: { permission: 'Pages.User.DanhGiaNhanVien' }
                    },
                    {
                        path: 'danh-gia-nhan-vien/:id/:cv/:t',
                        component: DanhGiaNhanVienComponent,
                        data: { permission: 'Pages.User.DanhGiaNhanVien' }
                    },
                    {
                        path: 'bao-cao',
                        component: DanhSachBaoCaoComponent,
                        data: { permission: 'Pages.Systems.Report' }
                    },
                    {
                        path: 'thong-ke-phieu-danh-gia',
                        component: ThongKePhieuDanhGiaComponent,
                        data: { permission: 'Pages.ThongKe.TinhTrangPhieuDanhGia' }
                    },
                    {
                        path: 'ct-danh-gia-nhan-vien',
                        component: ChuTichDanhGiaNhanVienComponent,
                        data: { permission: 'Pages.User.DanhGiaCaNhan.ChuTich' }
                    },
                    {
                        path: 'pct-danh-gia-nhan-vien',
                        component: PhoChuTichDanhGiaNhanVienComponent,
                        data: { permission: 'Pages.User.DanhGiaCaNhan.PhoChuTich' }
                    }

                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class MainRoutingModule { }
