import {
    AfterViewInit,
    Component,
    Injector,
    ViewEncapsulation,
    NgZone,
    OnDestroy,
} from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';

import * as am4core from '@amcharts/amcharts4/core';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
am4core.useTheme(am4themes_kelly);

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()],
})
export class DashboardComponent extends AppComponentBase
    implements AfterViewInit, OnDestroy {
    filterYear = new Date().getFullYear();
    selectChucVuId: number = this.appSession.chucVuChinhId;
    constructor(
        injector: Injector,
    ) {
        super(injector);
        // this.permission = this.permission;
        console.log("this.permission", this.permission);
    }

    ngAfterViewInit(): void {
    }
    ngOnDestroy(): void {
    }



}
