import {Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DashboardInput, DashboardServiceProxy, LoaiThangDiemChiTiet} from '@shared/service-proxies/service-proxies';
import * as am4core from '@node_modules/@amcharts/amcharts4/core';
import * as am4charts from '@node_modules/@amcharts/amcharts4/charts';
import * as moment from 'moment';
import {debounce, debounceTime} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {AppComponentBase} from '@shared/common/app-component-base';

const prefixThang = 'Thg ';
const listThang = [prefixThang + '1', prefixThang + '2', prefixThang + '3', prefixThang + '4', prefixThang + '5', prefixThang + '6', prefixThang + '7', prefixThang + '8', prefixThang + '9',
    prefixThang + '10', prefixThang + '11', prefixThang + '12'];

@Component({
    selector: 'bieu-do-xlca-nhan-theo-thang',
    templateUrl: './bieu-do-xlca-nhan-theo-thang.component.html',
    styleUrls: ['./bieu-do-xlca-nhan-theo-thang.component.css']
})
export class BieuDoXLCaNhanTheoThangComponent extends AppComponentBase implements OnInit, OnChanges, OnDestroy {
    @Input() chucVuId: any;
    @Input() filterYear: any;
    chart: am4charts.XYChart;
    dataChart = [];
    subject = new Subject<SimpleChanges>();
    loading = false;

    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.loadData();
        this.subject.pipe(debounceTime(200)).subscribe({
            next: (value) => {
                this.loadData();
            }
        });
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        this.subject.next(changes);
    }

    ngOnDestroy(): void {
        this.subject.unsubscribe();
    }


    loadData() {
        this.loading = true;
        let input = new DashboardInput();
        input.chucVuId = this.chucVuId;
        input.canBoId = this.appSession.canBoId;
        input.date = this.filterYear.toString();
        this._dashboardService
            .dashboard_DiemXepLoaiTheoThang(input)
            .subscribe((result) => {
                setTimeout(() => {
                    this.dataChart = result;
                    this.initChart();
                    this.loading = false;
                }, 500);
            });
    }

    initChart() {
        let chart = am4core.create(
            'bieuDoDiemXepLoaiTheoThang',
            am4charts.XYChart
        );
        chart.colors.list = [
            am4core.color('red'),
            am4core.color('gold'),
            am4core.color('green'),
            am4core.color('purple'),
        ];
        listThang.forEach(thang => {
            const itemFind = this.dataChart.find(x => thang === prefixThang + x.thang);
            let temp = {
                thang: thang,
            };
            if (itemFind) {
                temp['danhGia'] = itemFind.danhGia;
                temp[itemFind.loaiThangDiemChiTietEnum] = itemFind.diem;
            }
            chart.data.push(temp);
        });
        // Create axes
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'thang';
        // categoryAxis.title.text = 'Tháng';
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 20;
        categoryAxis.renderer.cellStartLocation = 0.1;
        categoryAxis.renderer.cellEndLocation = 0.9;
        categoryAxis.renderer.minWidth = 20;
        categoryAxis.title.fontWeight = '500';
        categoryAxis.title.fontFamily = 'Roboto';

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.min = 0;
        valueAxis.max = 100;
        valueAxis.title.text = 'Điểm';


        // Create series
        function createSeries(field, name, showSum) {
            let series = chart.series.push(new am4charts.ColumnSeries());
            series.dataFields.valueY = field;
            series.dataFields.categoryX = 'thang';
            series.name = name;
            series.columns.template.tooltipText = `[bold] Tháng {categoryX} [/]
                Điểm: {valueY}
                Nhận xét: {danhGia}`;
            series.stacked = true;
            series.columns.template.width = am4core.percent(95);

            let bullet = series.bullets.push(new am4charts.LabelBullet());
            bullet.interactionsEnabled = false;
            bullet.dy = 30;
            bullet.label.text = `[bold] {valueY} điểm [/]`;
            bullet.label.fill = am4core.color('#ffffff');

        }

        createSeries(LoaiThangDiemChiTiet.KHTNV, 'Không hoàn thành nhiệm vụ', true);
        createSeries(LoaiThangDiemChiTiet.HTNV, 'Hoàn thành nhiệm vụ', true);
        createSeries(LoaiThangDiemChiTiet.HTTNV, 'Hoàn thành tốt nhiệm vụ', true);
        createSeries(LoaiThangDiemChiTiet.HTXSNV, 'Hoàn thành xuất sắc nhiệm vụ', true);
        // Add legend
        chart.legend = new am4charts.Legend();
        this.chart = chart;
    }

}
