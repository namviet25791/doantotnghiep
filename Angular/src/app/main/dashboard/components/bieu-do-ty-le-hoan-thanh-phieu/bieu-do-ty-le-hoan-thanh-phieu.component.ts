import { Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import * as am4charts from '@node_modules/@amcharts/amcharts4/charts';
import { DashboardInput, DashboardServiceProxy } from '@shared/service-proxies/service-proxies';
import * as am4core from '@node_modules/@amcharts/amcharts4/core';
import { debounceTime } from '@node_modules/rxjs/operators';
import { Subject } from '@node_modules/rxjs';
import * as moment from '@node_modules/moment';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'bieu-do-ty-le-hoan-thanh-phieu',
    templateUrl: './bieu-do-ty-le-hoan-thanh-phieu.component.html',
    styleUrls: ['./bieu-do-ty-le-hoan-thanh-phieu.component.css']
})
export class BieuDoTyLeHoanThanhPhieuComponent extends AppComponentBase implements OnInit, OnChanges, OnDestroy {
    @Input() chucVuId: any;
    dataChart = [];
    subject = new Subject<SimpleChanges>();
    modelDateTyLePhanLoaiDanhGia: Date = new Date();
    loading = false;

    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);
        this.modelDateTyLePhanLoaiDanhGia = d;


        this.loadData();
        this.loadData();
        this.subject.pipe(debounceTime(200)).subscribe({
            next: (value) => {
                this.loadData();
            }
        });
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        this.subject.next(changes);
    }

    ngOnDestroy(): void {
        this.subject.unsubscribe();
    }

    loadData() {
        this.loading = true;
        let input = new DashboardInput();
        input.chucVuId = this.chucVuId;
        input.canBoId = this.appSession.canBoId;
        input.date = this.modelDateTyLePhanLoaiDanhGia.toString();
        this._dashboardService
            .dashboard_TyLeHoanThanhPhieu(input)
            .subscribe((result) => {
                setTimeout(() => {
                    this.dataChart = result;
                    this.initChart()
                    this.loading = false;
                }, 500);
            });
    }

    onThangChange($event: any) {
        this.loadData();
    }

    initChart() {
        let chart = am4core.create('tyLeHoanThanhPhieu', am4charts.PieChart);
        chart.data = this.dataChart;
        chart.innerRadius = am4core.percent(60);

        let series = chart.series.push(new am4charts.PieSeries());
        series.dataFields.value = 'tyLe';
        series.dataFields.category = 'ten';
        series.slices.template.stroke = am4core.color('#fff');
        series.slices.template.strokeWidth = 1;
        series.slices.template.strokeOpacity = 1;

        series.labels.template.disabled = true;
        series.ticks.template.disabled = true;

        series.hiddenState.properties.opacity = 1;
        series.hiddenState.properties.endAngle = -90;
        series.hiddenState.properties.startAngle = 90;

        let label = series.createChild(am4core.Label);
        // label.text = '26';
        label.horizontalCenter = 'middle';
        label.verticalCenter = 'middle';
        label.fontSize = 28;
        label.fontWeight = '500';

        let colorSet = new am4core.ColorSet();
        colorSet.list = ['#374AFB', '#34BFA3', '#FD3995', '#F44336', '#8E24AA'].map(function (color) {
            return am4core.color(color);
        });
        series.colors = colorSet;

        chart.legend = new am4charts.Legend();
        chart.legend.position = 'right';
        chart.legend.labels.template.text = '{name}';
        chart.legend.valueLabels.template.disabled = true;

        let markerTemplate = chart.legend.markers.template;
        markerTemplate.width = 15;
        markerTemplate.height = 15;
        markerTemplate.strokeWidth = 0;
        chart.legend.useDefaultMarker = true;
        chart.colors.step = 4;
    }


}
