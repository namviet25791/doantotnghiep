import { Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import * as am4core from '@node_modules/@amcharts/amcharts4/core';
import * as am4charts from '@node_modules/@amcharts/amcharts4/charts';
import { DashboardInput, DashboardServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from '@node_modules/moment';
import { debounceTime } from '@node_modules/rxjs/operators';
import { Subject } from '@node_modules/rxjs';
import { AppComponentBase } from '@shared/common/app-component-base';

@Component({
    selector: 'bieu-do-kq-dg-cac-thang',
    templateUrl: './bieu-do-kq-dg-cac-thang.component.html',
    styleUrls: ['./bieu-do-kq-dg-cac-thang.component.css']
})
export class BieuDoKqDgCacThangComponent extends AppComponentBase implements OnInit, OnChanges, OnDestroy {


    @Input() chucVuId: any;
    @Input() filterYear: number;
    chart: am4charts.XYChart;
    chartData = [];
    subject = new Subject<SimpleChanges>();
    loading = false;

    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        //if ("Pages.Systems.DashboardDonVi")
        this.loadData();
        this.subject.pipe(debounceTime(200)).subscribe({
            next: (value) => {
                this.loadData();
            }
        });
    }

    ngOnChanges(changes: import('@angular/core').SimpleChanges): void {
        this.subject.next(changes);
    }

    ngOnDestroy(): void {
        this.subject.unsubscribe();
    }

    loadData() {
        this.loading = true;
        let input = new DashboardInput();
        input.chucVuId = this.chucVuId;
        input.canBoId = this.appSession.canBoId;
        input.date = this.filterYear.toString();
        this._dashboardService
            .dashboard_KetQuaDanhGiaCacThang(input)
            .subscribe(result => {
                setTimeout(() => {
                    this.chartData = result;
                    this.initChar();
                    this.loading = false;
                }, 500);
            });
    }

    initChar() {
        let chart = am4core.create('bieuDoKQDGCacThang', am4charts.XYChart);
        chart.data = this.chartData;

        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = 'thang';
        categoryAxis.renderer.grid.template.location = 0;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        // valueAxis.renderer.inside = true;
        // valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;
        valueAxis.title.text = 'Số lượng';

        let series1 = chart.series.push(new am4charts.ColumnSeries());
        series1.stacked = true;
        series1.sequencedInterpolation = true;
        series1.name = 'Không hoàn thành';
        series1.dataFields.categoryX = 'thang';
        series1.dataFields.valueY = 'khongHoanThanh';
        series1.fill = am4core.color('#FD6666');
        series1.strokeWidth = 0;
        series1.columns.template.tooltipText = `[bold] Không hoàn thành nhiệm vụ [/]
            Số lượng: {khongHoanThanh}`;

        let series2 = chart.series.push(new am4charts.ColumnSeries());
        series2.stacked = true;
        series2.sequencedInterpolation = true;
        series2.name = 'Hoàn thành';
        series2.dataFields.categoryX = 'thang';
        series2.dataFields.valueY = 'hoanThanh';
        series2.fill = am4core.color('#FCCB59');
        series2.strokeWidth = 0;
        series2.columns.template.tooltipText = `[bold] Hoàn thành nhiệm vụ [/]
            Số lượng: {hoanThanh}`;

        let series3 = chart.series.push(new am4charts.ColumnSeries());
        series3.stacked = true;
        series3.sequencedInterpolation = true;
        series3.name = 'Hoàn thành tốt';
        series3.dataFields.categoryX = 'thang';
        series3.dataFields.valueY = 'hoanThanhTot';
        series3.fill = am4core.color('#57A9FB');
        series3.strokeWidth = 0;

        series3.columns.template.tooltipText = `[bold] Hoàn thành tốt nhiệm vụ [/]
            Số lượng: {hoanThanhTot}`;

        let series4 = chart.series.push(new am4charts.ColumnSeries());
        series4.stacked = true;
        series4.sequencedInterpolation = true;
        series4.name = 'Hoàn thành xuất sắc';
        series4.dataFields.categoryX = 'thang';
        series4.dataFields.valueY = 'hoanThanhXuatSac';
        series4.fill = am4core.color('#BBE491');
        series4.strokeWidth = 0;

        series4.columns.template.tooltipText = `[bold] Hoàn thành xuất sắc nhiệm vụ [/]
            Số lượng: {hoanThanhXuatSac}`;
        this.chart = chart;
    }

}
