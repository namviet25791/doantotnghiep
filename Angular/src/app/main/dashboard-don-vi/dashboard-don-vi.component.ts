import { AppComponentBase } from '@shared/common/app-component-base';
import { AfterViewInit, OnDestroy, ViewEncapsulation, Component, NgZone, Injector } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';
import * as _ from 'lodash';
import { TenantDashboardServiceProxy, DashboardServiceProxy, DashboardInput } from '@shared/service-proxies/service-proxies';
import { inject } from '@angular/core/testing';

am4core.useTheme(am4themes_kelly);

abstract class DashboardChartBase {
    _zone: NgZone;

    constructor(zone: NgZone) {
        this._zone = zone;
    }

    loading = true;

    showLoading() {
        setTimeout(() => {
            this.loading = true;
        });
    }

    hideLoading() {
        setTimeout(() => {
            this.loading = false;
        });
    }

    abstract init(): void;
    abstract destroy(): void;
}

/**
 * Biểu đồ kết quả đánh giá theo tháng tháng
 */
class BieuKetQuaDanhGiaTheoThang extends DashboardChartBase {
    chart: am4charts.XYChart;

    ketQuaDanhGiaChiTiet: Array<{ Thang: number; Diem: number; DanhGia: string }>;
    lstDataOrg: any = [];
    init(lstData?: any): void {
        this.lstDataOrg = lstData;
        this.reload();
    }

    destroy() {
        this._zone.runOutsideAngular(() => {
            if (this.chart) {
                this.chart.dispose();
            }
        });
    }

    reload() {
        this._zone.runOutsideAngular(() => {
            let chart = am4core.create('bieuDoDiemXepLoaiTheoThang', am4charts.XYChart);
            chart.data = this.lstDataOrg;

            let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
            categoryAxis.dataFields.category = 'thang';
            categoryAxis.renderer.grid.template.location = 0;

            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
            valueAxis.renderer.inside = true;
            valueAxis.renderer.labels.template.disabled = true;
            valueAxis.min = 0;

            let series1 = chart.series.push(new am4charts.ColumnSeries());
            series1.stacked = true;
            series1.sequencedInterpolation = true;
            series1.name = 'Không hoàn thành';
            series1.dataFields.categoryX = 'thang';
            series1.dataFields.valueY = 'khongHoanThanh';
            series1.fill = am4core.color('#34BFA3');
            series1.strokeWidth = 0;
            series1.columns.template.tooltipText = `[bold] Không hoàn thành nhiệm vụ [/]
            Số lượng: {KhongHoanThanh}`;

            let series2 = chart.series.push(new am4charts.ColumnSeries());
            series2.stacked = true;
            series2.sequencedInterpolation = true;
            series2.name = 'Hoàn thành';
            series2.dataFields.categoryX = 'thang';
            series2.dataFields.valueY = 'hoanThanh';
            series2.fill = am4core.color('#F7C469');
            series2.strokeWidth = 0;
            series2.columns.template.tooltipText = `[bold] Hoàn thành nhiệm vụ [/]
            Số lượng: {HoanThanh}`;

            let series3 = chart.series.push(new am4charts.ColumnSeries());
            series3.stacked = true;
            series3.sequencedInterpolation = true;
            series3.name = 'Hoàn thành tốt';
            series3.dataFields.categoryX = 'thang';
            series3.dataFields.valueY = 'hoanThanhTot';
            series3.fill = am4core.color('#62BCE3');
            series3.strokeWidth = 0;

            series3.columns.template.tooltipText = `[bold] Hoàn thành tốt nhiệm vụ [/]
            Số lượng: {hoanThanhTot}`;

            let series4 = chart.series.push(new am4charts.ColumnSeries());
            series4.stacked = true;
            series4.sequencedInterpolation = true;
            series4.name = 'Hoàn thành xuất sắc';
            series4.dataFields.categoryX = 'thang';
            series4.dataFields.valueY = 'hoanThanhXuatSac';
            series4.fill = am4core.color('#B6D860');
            series4.strokeWidth = 0;

            series4.columns.template.tooltipText = `[bold] Hoàn thành xuất sắc nhiệm vụ [/]
            Số lượng: {hoanThanhXuatSac}`;
            this.chart = chart;
        });
    }
}

class BieuDoPhanBoDiemTheoNhomTieuChi extends DashboardChartBase {
    _chart: am4charts.PieChart;

    init(lstData?: any) {
        this._zone.runOutsideAngular(() => {
            let chart = am4core.create('tyLeThangDiem', am4charts.PieChart);
            chart.data = lstData;
            // chart.data = [
            //     {
            //         ThangDiem: 'Hoàn thành xuất sắc nhiệm vụ',
            //         SoLuong: 23
            //     },
            //     {
            //         ThangDiem: 'Hoàn thành tốt nhiệm vụ',
            //         SoLuong: 16
            //     },
            //     {
            //         ThangDiem: 'Hoàn thành nhiệm vụ',
            //         SoLuong: 24
            //     },
            //     {
            //         ThangDiem: 'Không hoàn thành nhiệm vụ',
            //         SoLuong: 40
            //     }
            // ];

            chart.innerRadius = am4core.percent(60);

            let series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = 'soLuong';
            series.dataFields.category = 'thangDiem';
            series.slices.template.stroke = am4core.color('#fff');
            series.slices.template.strokeWidth = 1;
            series.slices.template.strokeOpacity = 1;

            series.labels.template.disabled = true;
            series.ticks.template.disabled = true;

            series.hiddenState.properties.opacity = 1;
            series.hiddenState.properties.endAngle = -90;
            series.hiddenState.properties.startAngle = 90;

            let label = series.createChild(am4core.Label);
            // label.text = '26';
            label.horizontalCenter = 'middle';
            label.verticalCenter = 'middle';
            label.fontSize = 28;
            label.fontWeight = '500';

            chart.legend = new am4charts.Legend();
            chart.legend.position = 'right';
            chart.legend.labels.template.text = '{name}';
            chart.legend.valueLabels.template.disabled = true;

            let markerTemplate = chart.legend.markers.template;
            markerTemplate.width = 15;
            markerTemplate.height = 15;
            markerTemplate.strokeWidth = 0;
            chart.legend.useDefaultMarker = true;
            chart.colors.step = 4;

            this._chart = chart;
        });
    }

    destroy(): void {
        this._zone.runOutsideAngular(() => {
            if (this._chart) {
                this._chart.dispose();
            }
        });
    }
}

class BieuDoTyLeHoanThanhPhieu extends DashboardChartBase {
    _chart: am4charts.PieChart;

    init(lstData?: any) {
        this._zone.runOutsideAngular(() => {
            let chart = am4core.create('tyLeHoanThanhPhieu', am4charts.PieChart);
            chart.data = lstData;
            // chart.data = [
            //     {
            //         ten: 'Hoàn thành',
            //         tyLe: 23
            //     },
            //     {
            //         ten: 'Chưa được đánh giá',
            //         tyLe: 23
            //     },
            //     {
            //         ten: 'Chưa gửi đánh giá',
            //         tyLe: 24
            //     }
            // ];

            chart.innerRadius = am4core.percent(60);

            let series = chart.series.push(new am4charts.PieSeries());
            series.dataFields.value = 'tyLe';
            series.dataFields.category = 'ten';
            series.slices.template.stroke = am4core.color('#fff');
            series.slices.template.strokeWidth = 1;
            series.slices.template.strokeOpacity = 1;

            series.labels.template.disabled = true;
            series.ticks.template.disabled = true;

            series.hiddenState.properties.opacity = 1;
            series.hiddenState.properties.endAngle = -90;
            series.hiddenState.properties.startAngle = 90;

            let label = series.createChild(am4core.Label);
            // label.text = '26';
            label.horizontalCenter = 'middle';
            label.verticalCenter = 'middle';
            label.fontSize = 28;
            label.fontWeight = '500';

            var colorSet = new am4core.ColorSet();
            colorSet.list = ["#374AFB", "#34BFA3", "#FD3995", "#F44336", "#8E24AA"].map(function (color) {
                return am4core.color(color);
            });
            series.colors = colorSet;

            // var colors = ["#FF0000", "#FF00FF", "#FFFF00", "#00FFFF", "#0000FF", "#00FF00"];
            // var colorset = new am4core.ColorSet();
            // colorset.list = [];
            // for (var i = 0; i < colors.length; i++)
            //     colorset.list.push(am4core.color(colors[i]));
            // series.colors = colorset;


            chart.legend = new am4charts.Legend();
            chart.legend.position = 'right';
            chart.legend.labels.template.text = '{name}';
            chart.legend.valueLabels.template.disabled = true;

            let markerTemplate = chart.legend.markers.template;
            markerTemplate.width = 15;
            markerTemplate.height = 15;
            markerTemplate.strokeWidth = 0;
            chart.legend.useDefaultMarker = true;
            chart.colors.step = 4;

            this._chart = chart;
        });
    }

    destroy(): void {
        this._zone.runOutsideAngular(() => {
            if (this._chart) {
                this._chart.dispose();
            }
        });
    }
}

@Component({
    selector: 'dashboard-don-vi',
    templateUrl: './dashboard-don-vi.component.html',
    styleUrls: ['./dashboard-don-vi.component.less'],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class DashboardDonViComponent extends AppComponentBase implements AfterViewInit, OnDestroy {
    private _bieuDoPhanBoDiemTheoNhomTieuChi: BieuDoPhanBoDiemTheoNhomTieuChi;
    private _bieuDoKetQuaDanhGiaTheoThang: BieuKetQuaDanhGiaTheoThang;
    private _bieuDoTyLeHoanThanhPhieu: BieuDoTyLeHoanThanhPhieu;
    selectChucVuId: number = this.appSession.chucVuChinhId;
    filterYear: number = 2019;
    modelDateTyLePhanLoaiDanhGia: Date = new Date();
    modelDateTyLeHoanThanhPhieu: Date = new Date();
    lstNhanVienXuatSac = [];
    lstNhanVienKem = [];

    constructor(injector: Injector, private _dashboardService: DashboardServiceProxy, private zone: NgZone) {
        super(injector);
        this._bieuDoPhanBoDiemTheoNhomTieuChi = new BieuDoPhanBoDiemTheoNhomTieuChi(zone);
        this._bieuDoKetQuaDanhGiaTheoThang = new BieuKetQuaDanhGiaTheoThang(zone);
        this._bieuDoTyLeHoanThanhPhieu = new BieuDoTyLeHoanThanhPhieu(zone);
    }

    ngAfterViewInit(): void {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);

        var d2 = new Date();
        this.filterYear = d2.getFullYear();

        this.modelDateTyLePhanLoaiDanhGia = d;
        this.modelDateTyLeHoanThanhPhieu = d;

        this.loadBieuDoDiemXepLoaiTheoThang(this.filterYear);

        this.loadTyLePhanLoaiDanhGia(this.modelDateTyLePhanLoaiDanhGia);
        this.loadTyLeHoanThanhPhieu(this.modelDateTyLeHoanThanhPhieu);
        this.loadTopNhanVienXuatSac();
        this.loadTopNhanVienKem();


        console.log("loadTyLeHoanThanhPhieu", this.appSession.chucVuChinhId, this.selectChucVuId);
    }
    ngOnDestroy(): void {
        this._bieuDoPhanBoDiemTheoNhomTieuChi.destroy();
        this._bieuDoKetQuaDanhGiaTheoThang.destroy();
    }

    onChangeChucVuChinh() {
        this.ngAfterViewInit();
    }

    loadTyLeHoanThanhPhieu(value?: any): void {
        this.modelDateTyLeHoanThanhPhieu = value;

        var input = new DashboardInput();
        input.canBoId = this.appSession.canBoId;
        input.date = value.toString();
        input.chucVuId = this.selectChucVuId;
        this._dashboardService
            .dashboard_TyLeHoanThanhPhieu(input)
            .subscribe(result => {
                // console.log("resultresult", result);
                this._bieuDoTyLeHoanThanhPhieu.init(result);
            });
    }

    loadTyLePhanLoaiDanhGia(value?: any): void {
        this.modelDateTyLePhanLoaiDanhGia = value;
        var input = new DashboardInput();
        input.canBoId = this.appSession.canBoId;
        input.date = value.toString();
        input.chucVuId = this.selectChucVuId;
        this._dashboardService
            .dashboard_TyLeThangDiem(input)
            .subscribe(result => {
                // console.log("resultresult", result);
                this._bieuDoPhanBoDiemTheoNhomTieuChi.init(result);
            });
    }
    loadBieuDoDiemXepLoaiTheoThang(value?: any): void {
        this.filterYear = value;

        var input = new DashboardInput();
        input.canBoId = this.appSession.canBoId;
        input.date = (this.filterYear).toString();
        input.chucVuId = this.selectChucVuId;
        this._dashboardService
            .dashboard_KetQuaDanhGiaCacThang(input)
            .subscribe(result => {
                // console.log("resultresult", result);
                this._bieuDoKetQuaDanhGiaTheoThang.init(result);
            });
    }

    loadTopNhanVienXuatSac(): void {
        var input = new DashboardInput();
        input.canBoId = this.appSession.canBoId;
        input.chucVuId = this.selectChucVuId;
        this._dashboardService
            .dashboard_DsCanBoDiemTrungBinhCaoNhatTrongNam(input)
            .subscribe(result => {
                //console.log("resultresult", result);
                this.lstNhanVienXuatSac = result;
            });
    }
    loadTopNhanVienKem(): void {
        var input = new DashboardInput();
        input.canBoId = this.appSession.canBoId;
        input.chucVuId = this.selectChucVuId;
        this._dashboardService
            .dashboard_DsCanBoDiemTrungBinhThapNhatTrongNam(input)
            .subscribe(result => {
                // console.log("resultresult", result);
                this.lstNhanVienKem = result;
            });
    }
}
