import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import {
    OnInit,
    EventEmitter,
    Output,
    ViewChild,
    Injector,
    Component
} from "@angular/core";
import { Paginator } from "primeng/paginator";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { DashboardServiceProxy, DashboardInput, ThongKeCanBoInput } from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { FileDownloadService } from "@shared/utils/file-download.service";

@Component({
    selector: "app-thong-ke-phieu-danh-gia",
    templateUrl: "./thong-ke-phieu-danh-gia.component.html",
    animations: [appModuleAnimation()]
})
export class ThongKePhieuDanhGiaComponent extends AppComponentBase
    implements OnInit {
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    trangThaiPhieu = -1;
    selectChucVuId: number = this.appSession.chucVuChinhId;
    isViewDsBaoCao = true;
    itemActive: number = 0;
    filterMonth = new Date();
    constructor(
        injector: Injector,
        private _dashboardService: DashboardServiceProxy,
        private _fileDownloadService: FileDownloadService,
    ) {
        super(injector);
    }

    public projectData: Object[];

    lstTrangThaiPhieu: any = [
        { id: -1, text: "Tất cả" },
        { id: 0, text: "Đang thực hiện" },
        { id: 1, text: "Đã hoàn thành" },
        { id: 2, text: "Chưa gửi đánh giá" }
    ];

    ngOnInit() {
        var d = new Date();
        d.setMonth(d.getMonth(), 0);
        this.filterMonth = d;
    }

    onChangeChucVuChinh() {
        this.thongKeTinhTrangPhieuDanhGiaCanBo();
    }

    onChangeMonthSelection(value) {
        this.filterMonth = value;
        this.thongKeTinhTrangPhieuDanhGiaCanBo();
    }

    thongKeTinhTrangPhieuDanhGiaCanBo(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(this.paginator, event);
        let maxResultCount = this.primengTableHelper.getMaxResultCount(this.paginator, event);

        let input = new ThongKeCanBoInput();
        input.chucVuId = this.selectChucVuId;
        input.canBoId = this.appSession.canBoId;
        input.date = this.filterMonth.toString();
        input.trangThai = this.trangThaiPhieu;
        input.sorting = "";
        input.skipCount = skipCount;
        input.maxResultCount = maxResultCount;

        this._dashboardService
            .thongKeTinhTrangPhieuDanhGiaCanBo(input)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });

                this.primengTableHelper.hideLoadingIndicator();
            });
    }


    exportToExcel(): void {

        let input = new ThongKeCanBoInput();
        input.chucVuId = this.selectChucVuId;
        input.canBoId = this.appSession.canBoId;
        input.date = this.filterMonth.toString();
        input.trangThai = this.trangThaiPhieu;
        input.sorting = "";

        this._dashboardService
            .exportCanBoToExcel(input)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator()))
            .subscribe(result => {
                this._fileDownloadService.downloadTempFile(result);
            });
    }
}
