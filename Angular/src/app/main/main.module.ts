import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import {
    ModalModule,
    TabsModule,
    TooltipModule,
    BsDropdownModule,
    PopoverModule
} from 'ngx-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import { DxDateBoxModule, DxSelectBoxModule, DxNumberBoxModule } from 'devextreme-angular';
import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService
} from 'ngx-bootstrap/datepicker';
import {
    DxSwitchModule,
    DxButtonModule,
    DxDataGridModule,
    DxTreeListModule,
    DxTagBoxModule
} from 'devextreme-angular';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { DemoUiSharedComponent } from './demo-ui/demo-ui.component';
import { SharedUiComboBoxModule } from '@app/shared/ui-combo-box/ui-combo-box.module';
import { SharedUiUxModule } from '@app/shared/ui-ux/SharedUiUxModule';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

import { PhieuDanhGiaCanBoComponent } from './phieu-danh-gia-can-bo/phieu-danh-gia-can-bo.component';
import { PhieuDanhGiaCaNhanComponent } from './phieu-danh-gia-can-bo/phieu-danh-gia-ca-nhan/phieu-danh-gia-ca-nhan.component';
import { DashboardDonViComponent } from './dashboard-don-vi/dashboard-don-vi.component';
import { DanhGiaNhanVienComponent } from './danh-gia-nhan-vien/danh-gia-nhan-vien.component';
import { ChiTietPhieuDanhGiaCaNhanComponent } from './phieu-danh-gia-can-bo/chi-tiet-phieu/chi-tiet-phieu-danh-gia-ca-nhan.component';
import { ChamDiemPhieuDanhGiaNhanVienComponent } from './danh-gia-nhan-vien/cham-diem-phieu-danh-gia-ca-nhan/cham-diem-phieu-danh-gia-ca-nhan.component';
import { DuyetPhieuDanhGiaNhanVienComponent } from './danh-gia-nhan-vien/duyet-phieu-danh-gia-ca-nhan/duyet-phieu-danh-gia-ca-nhan.component';
import { DanhSachBaoCaoComponent } from './bao-cao/danh-sach-bao-cao.component';
import { BaoCaoChiTietComponent } from './bao-cao/bao-cao-chi-tiet.component';
import '../localization';
import { ChiTietNhomDanhGiaModalComponent } from './phieu-danh-gia-can-bo/chi-tiet-phieu/chi-tiet-nhom-danh-gia-modal.component';
import { BieuDoXLCaNhanTheoThangComponent } from './dashboard/components/bieu-do-xlca-nhan-theo-thang/bieu-do-xlca-nhan-theo-thang.component';
import { BieuDoKqDgCacThangComponent } from './dashboard/components/bieu-do-kq-dg-cac-thang/bieu-do-kq-dg-cac-thang.component';
import { BieuDoTyLeHoanThanhPhieuComponent } from './dashboard/components/bieu-do-ty-le-hoan-thanh-phieu/bieu-do-ty-le-hoan-thanh-phieu.component';
import { InputNumberCustomComponent } from './phieu-danh-gia-can-bo/phieu-danh-gia-ca-nhan/input-number-custom.compoent';
import { ThongKePhieuDanhGiaComponent } from './thong-ke/thong-ke-phieu-danh-gia.component';
import { ChuTichDanhGiaNhanVienComponent } from './lanh-dao-danh-gia/ct-danh-gia-nhan-vien.component';
import { PhoChuTichDanhGiaNhanVienComponent } from './lanh-dao-danh-gia/pct-danh-gia-nhan-vien.component';
import { BaseChiTietPhieuDanhGiaCaNhanComponent } from './phieu-danh-gia-can-bo/chi-tiet-phieu/base-chi-tiet-pdgcb.component';
import { LuongDanhGiaModalComponent } from './phieu-danh-gia-can-bo/luong-danh-gia/luong-danh-gia-modal.component';
import { BaoCaoTongHopKhoiCacPhuong04Component } from './bao-cao/compoments/tong-hop-khoi-cac-phuong-04/tong-hop-khoi-cac-phuong-04.component';
import { BaoCaoTongHopKhoiCacDang06Component } from './bao-cao/compoments/tong-hop-khoi-cac-dang-06/tong-hop-khoi-cac-dang-06.component';
import { BaoCaoTongHopKhoiTruongHoc05Component } from './bao-cao/compoments/tong-hop-khoi-truong-hoc-05.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SharedUiComboBoxModule,
        SharedUiUxModule,
        TableModule,
        PaginatorModule,
        HttpClientModule,
        DxButtonModule,
        DxDataGridModule,
        DxSwitchModule,
        DxTreeListModule,
        DxTagBoxModule,
        DxDateBoxModule,
        DxSelectBoxModule,
        DxNumberBoxModule
    ],
    declarations: [
        DashboardComponent,
        DashboardDonViComponent,
        DemoUiSharedComponent,
        PhieuDanhGiaCanBoComponent,
        PhieuDanhGiaCaNhanComponent,
        DanhGiaNhanVienComponent,
        ChiTietPhieuDanhGiaCaNhanComponent,
        ChamDiemPhieuDanhGiaNhanVienComponent,
        DuyetPhieuDanhGiaNhanVienComponent,
        DanhSachBaoCaoComponent,
        BaoCaoChiTietComponent, InputNumberCustomComponent,
        ChiTietNhomDanhGiaModalComponent,
        BieuDoXLCaNhanTheoThangComponent,
        BieuDoKqDgCacThangComponent,
        BieuDoTyLeHoanThanhPhieuComponent,
        ThongKePhieuDanhGiaComponent,
        ChuTichDanhGiaNhanVienComponent,
        PhoChuTichDanhGiaNhanVienComponent,
        BaseChiTietPhieuDanhGiaCaNhanComponent,
        LuongDanhGiaModalComponent,
        BaoCaoTongHopKhoiCacPhuong04Component,
        BaoCaoTongHopKhoiCacDang06Component,
        BaoCaoTongHopKhoiTruongHoc05Component

    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        }
    ]
})
export class MainModule {
}
