import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { CapDonViDto, KhoiServiceProxy, CapDonViServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'createOrEditCapDonViModal',
    templateUrl: './create-or-edit-cap-don-vi-modal.component.html'
})
export class CreateOrEditCapDonViModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateCapDonViForm: FormGroup;
    submitted = false;
    capDonViDto: CapDonViDto = new CapDonViDto();
    active = false;
    saving = false;


    constructor(
        private formBuilder: FormBuilder,
        private _capDonViService: CapDonViServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateCapDonViForm = this.formBuilder.group({
            ten: ['', Validators.required]
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateCapDonViForm.controls; }

    show(modelUpdate?: CapDonViDto): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (modelUpdate) {
            //edit
            this.capDonViDto = Object.assign({}, modelUpdate);;
        } else {
            // addnew
            this.capDonViDto = new CapDonViDto();
            this.capDonViDto.id = 0;
        }
        self.modal.show();
    }

    onShown(): void {

    }

    save(): void {
        this.submitted = true;
        // stop here if form is invalid
        if (this.createOrUpdateCapDonViForm.invalid) {
            return;
        }
        this.saving = true;
        this._capDonViService.upsert(this.capDonViDto)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
