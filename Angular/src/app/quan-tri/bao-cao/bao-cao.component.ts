import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Injector
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import {
  BaoCaoServiceProxy
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator } from "primeng/paginator";
import { CreateOrEditBaoCaoModalComponent } from "./create-or-edit-bao-cao-modal.component";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { NhomDoiTuongChiTietModalComponent } from "./cau-hinh-nhom-doi-tuong/nhom-doi-tuong-chi-tiet-modal.component";

@Component({
  selector: 'app-bao-cao',
  templateUrl: './bao-cao.component.html',
  animations: [appModuleAnimation()]
})
export class BaoCaoComponent extends AppComponentBase implements OnInit {
  [x: string]: any;
  filterText = "";
  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild("createOrEditBaoCaoModal", { static: true })
  createOrEditBaoCaoModal: CreateOrEditBaoCaoModalComponent;
  @ViewChild("nhomDoiTuongChiTietModal", { static: true })
  nhomDoiTuongChiTietModal: NhomDoiTuongChiTietModalComponent;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  viewType = 0;
  selectCurrentItem: any = {};
  lstChucVuSuDungBaoCao: any = [];

  constructor(
    injector: Injector,
    private _baoCaoService: BaoCaoServiceProxy
  ) {
    super(injector);
  }

  public projectData: Object[];
  ngOnInit() {
    this.getBaoCao();
  }

  getBaoCao(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    let skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );
    let maxResultCount = this.primengTableHelper.getMaxResultCount(
      this.paginator,
      event
    );
    this._baoCaoService
      .getAllBaoCao(undefined, this.filterText, undefined, 2, "", maxResultCount, skipCount)
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = [];
        let soThuTu = skipCount + 1;
        result.items.forEach(value => {
          let model: any = Object.assign({}, value);
          model.stt = soThuTu++;
          this.primengTableHelper.records.push(model);
        });
        //this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  createBaoCao(): void {
    this.createOrEditBaoCaoModal.show();
  }

  async xoaBaoCao(item?: any) {
    const that_ = this;
    abp.message.confirm(
      "Bạn chắc chắn muốn xóa mục này?",
      "Thông báo",
      function (result) {
        if (result) {
          that_._baoCaoService.deleteBaoCao(item.id).subscribe(() => {
            that_.getBaoCao();
            abp.notify.success("Thao tác thành công!");
          });
        }
      }
    );
  }

  openChucVuSuDungBaoCao(item?: any) {
    this.viewType = 1;
    this.selectCurrentItem = item;
    this.getDsChucVuSuDungBaoCao();

  }
  getDsChucVuSuDungBaoCao() {
    this._baoCaoService
      .getChucVuTheoBaoCao(this.selectCurrentItem.id)
      .subscribe(result => {
        this.lstChucVuSuDungBaoCao = result;
      });
  }

  dongChucVuSuDungBaoCao() {
    this.viewType = 0;
  }

  removeChucVuSuDungBaoCao(id) {
    this._baoCaoService.deleteBaoCaoChucVu(id)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.getDsChucVuSuDungBaoCao();
      });
  }



  close(): void {
    this.closeEvent.emit(null);
  }
}
