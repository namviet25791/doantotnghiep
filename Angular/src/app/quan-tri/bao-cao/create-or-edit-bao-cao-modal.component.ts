import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BaoCaoDto, BaoCaoServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'createOrEditBaoCaoModal',
    templateUrl: './create-or-edit-bao-cao-modal.component.html'
})
export class CreateOrEditBaoCaoModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateBaoCaoForm: FormGroup;
    submitted = false;
    baoCaoDto: BaoCaoDto = new BaoCaoDto();
    active = false;
    saving = false;

    constructor(
        private formBuilder: FormBuilder,
        private _baoCaoService: BaoCaoServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateBaoCaoForm = this.formBuilder.group({
            mabaocao: ['', Validators.required],
            tenbaocao: ['', Validators.required],
            source: ['', Validators.required],
            duongDanBieuMau: ['', Validators.required],
            mota: [""],
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateBaoCaoForm.controls; }

    show(modelUpdate?: BaoCaoDto, isEdit?: any): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (modelUpdate) {
            //edit
            this.baoCaoDto = Object.assign({}, modelUpdate);;
        } else {
            // addnew
            this.baoCaoDto = new BaoCaoDto();
            this.baoCaoDto.id = 0;
        }
        self.modal.show();
    }

    onShown(): void {

    }

    save(): void {
        this.submitted = true;
        // stop here if form is invalid
        if (this.createOrUpdateBaoCaoForm.invalid) {
            return;
        }
        this.saving = true;
        this._baoCaoService.insertOrUpdateBaoCao(this.baoCaoDto)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
