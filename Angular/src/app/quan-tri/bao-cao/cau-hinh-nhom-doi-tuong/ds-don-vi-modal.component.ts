import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BaoCaoServiceProxy, BaoCaoNhomDoiTuongDto, ChucVuServiceProxy, BaoCaoNhomDoiTuongChiTietInsert, BaoCaoChucVuCauHinhInsert, BaoCaoChucVuInsert, DonViServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import * as _ from "lodash";
@Component({
    selector: 'dsDonViModal',
    templateUrl: './ds-don-vi-modal.component.html'
})
export class DanhSachDonViModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    primengTableHelper: any;
    filterText: string = "";
    lstDonViDuocChon: any = [];
    lstDonViDuocChonOrg: any = [];
    nhomDoiTuong: BaoCaoNhomDoiTuongDto = new BaoCaoNhomDoiTuongDto();
    saving = false;
    source = 0; // 1: nhom doi tuong chi tiet, 2: nhom doi tuong su dung
    constructor(
        private formBuilder: FormBuilder,
        private _baoCaoService: BaoCaoServiceProxy,
        private _donViService: DonViServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {

    }
    // // convenience getter for easy access to form fields
    // get f() { return this.createOrUpdateNhomDoiTuongForm.controls; }

    show(lstChucVuDaChon?: any, nhomDoiTuongModel?: any): void {
        this.lstDonViDuocChon = [];
        if (nhomDoiTuongModel != undefined) {
            this.nhomDoiTuong = nhomDoiTuongModel;

            this.lstDonViDuocChon = _.map(lstChucVuDaChon, function (x) {
                return _.assign(x, {
                    id: x.chucVuId,
                    ten: x.tenChucVu
                });
            });

            this.saving = false;
            const self = this;
            self.modal.show();
            this.getDonVi();
        }
    }

    getDonVi(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );
        this._donViService
            .getByFilter(
                this.filterText,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                undefined,
                0, 0,
                "",
                1000,
                0
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);

                    const isCheckDonVi = this.lstDonViDuocChon.filter(epic => epic.chucVuId == value.id)[0];
                    if (isCheckDonVi) {
                        model.isChecked = true;
                    }
                    else {
                        model.isChecked = false;
                    }
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                this.primengTableHelper.hideLoadingIndicator();

                // console.log(" this.primengTableHelper", this.primengTableHelper);
            });
    }


    chonChucVu(event?: any, data?: any): void {
        if (event) {
            this.lstDonViDuocChon.push(data);
        } else {
            let indexOf = this.lstDonViDuocChon.indexOf(data.id);
            this.lstDonViDuocChon.splice(indexOf, 1);
        }
    }


    onShown(): void {

    }

    save(): void {
        let lstDonViInsert = this.lstDonViDuocChon.map(function (item) {
            return item['id'];
        });

        // let input = new BaoCaoChucVuInsert();
        // input.baoCaoId = this.nhomDoiTuong.id;
        // input.lstChucVuId = lstChucVuInsert;
        // this.saving = true;
        // this._baoCaoService.insertBaoCaoChucVu(input)
        //     .pipe(finalize(() => this.saving = false))
        //     .subscribe(result => {
        //         if (result) {
        //             this.notify.success(this.l('SavedSuccessfully'));
        //             this.modalSave.emit(lstChucVuInsert);
        //             this.modal.hide();
        //         } else {
        //             this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
        //         }
        //     });

    }

    close(): void {
        this.modal.hide();
    }
}
