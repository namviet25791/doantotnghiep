import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BaoCaoServiceProxy, BaoCaoNhomDoiTuongDto, ChucVuServiceProxy, BaoCaoNhomDoiTuongChiTietInsert, BaoCaoChucVuCauHinhInsert, BaoCaoChucVuInsert, DonViServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import * as _ from "lodash";
@Component({
    selector: 'nhomDoiTuongChiTietModal',
    templateUrl: './nhom-doi-tuong-chi-tiet-modal.component.html'
})
export class NhomDoiTuongChiTietModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    primengTableHelper: any;
    filterText: string = "";
    capDonVi: any;
    khoi: any;
    lstChucVuDuocChon: any = [];
    lstChucVuDuocChonOrg: any = [];
    nhomDoiTuong: BaoCaoNhomDoiTuongDto = new BaoCaoNhomDoiTuongDto();
    saving = false;
    source = 0; // 1: nhom doi tuong chi tiet, 2: nhom doi tuong su dung
    constructor(
        private formBuilder: FormBuilder,
        private _baoCaoService: BaoCaoServiceProxy,
        private _chucVuService: ChucVuServiceProxy,
        private _donViService: DonViServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {

    }
    // // convenience getter for easy access to form fields
    // get f() { return this.createOrUpdateNhomDoiTuongForm.controls; }

    show(lstChucVuDaChon?: any, nhomDoiTuongModel?: any, source?: any): void {
        this.source = source;
        this.lstChucVuDuocChon = [];
        if (nhomDoiTuongModel != undefined) {
            this.nhomDoiTuong = nhomDoiTuongModel;

            this.lstChucVuDuocChon = _.map(lstChucVuDaChon, function (x) {
                return _.assign(x, {
                    id: x.chucVuId,
                    ten: x.tenChucVu
                });
            });

            this.saving = false;
            const self = this;
            self.modal.show();
            this.getChucVu();
        }
    }

    getChucVu(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );
        if (this.nhomDoiTuong.loaiNhomDoiTuong == 2) {
            this._donViService
                .getDonViBaoCaoDoiTuongChiTiet(
                    this.capDonVi,
                    this.khoi,
                    this.filterText,
                    this.nhomDoiTuong.id,
                    "",
                    maxResultCount,
                    skipCount
                )
                .pipe(
                    finalize(() => this.primengTableHelper.hideLoadingIndicator())
                )
                .subscribe(result => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = [];
                    let soThuTu = skipCount + 1;
                    result.items.forEach(value => {
                        let model: any = Object.assign({}, value);

                        const isCheckChucVu = this.lstChucVuDuocChon.filter(epic => epic.chucVuId == value.id)[0];
                        if (isCheckChucVu) {
                            model.isChecked = true;
                        }
                        else {
                            model.isChecked = false;
                        }
                        model.stt = soThuTu++;
                        this.primengTableHelper.records.push(model);
                    });
                    this.primengTableHelper.hideLoadingIndicator();

                    // console.log(" this.primengTableHelper", this.primengTableHelper);
                });
        } else {
            this._chucVuService
                .getChucVuBaoCaoDoiTuongChiTiet(
                    this.filterText,
                    this.nhomDoiTuong.id,
                    "",
                    maxResultCount,
                    skipCount
                )
                .pipe(
                    finalize(() => this.primengTableHelper.hideLoadingIndicator())
                )
                .subscribe(result => {
                    this.primengTableHelper.totalRecordsCount = result.totalCount;
                    this.primengTableHelper.records = [];
                    let soThuTu = skipCount + 1;
                    result.items.forEach(value => {
                        let model: any = Object.assign({}, value);

                        const isCheckChucVu = this.lstChucVuDuocChon.filter(epic => epic.chucVuId == value.id)[0];
                        if (isCheckChucVu) {
                            model.isChecked = true;
                        }
                        else {
                            model.isChecked = false;
                        }
                        model.stt = soThuTu++;
                        this.primengTableHelper.records.push(model);
                    });
                    this.primengTableHelper.hideLoadingIndicator();

                    // console.log(" this.primengTableHelper", this.primengTableHelper);
                });
        }

    }


    chonChucVu(event?: any, data?: any): void {
        if (event) {
            this.lstChucVuDuocChon.push(data);
        } else {
            let indexOf = this.lstChucVuDuocChon.indexOf(data.id);
            this.lstChucVuDuocChon.splice(indexOf, 1);
        }
    }


    onShown(): void {

    }

    save(): void {
        let lstChucVuInsert = this.lstChucVuDuocChon.map(function (item) {
            return item['id'];
        });
        if (this.source == 1) {
            let input = new BaoCaoNhomDoiTuongChiTietInsert();
            input.lstChucVu = lstChucVuInsert;
            input.baoCaoNhomDoiTuongId = this.nhomDoiTuong.id;
            this.saving = true;
            this._baoCaoService.insertBaoCaoNhomDoiTuongChiTiet(input)
                .pipe(finalize(() => this.saving = false))
                .subscribe(result => {
                    if (result) {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.modalSave.emit(lstChucVuInsert);
                        this.modal.hide();
                    } else {
                        this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                    }
                });
        }
        else if (this.source == 2) {
            let input = new BaoCaoChucVuCauHinhInsert();
            input.baoCaoNhomDoiTuongId = this.nhomDoiTuong.id;
            input.lstChucVuId = lstChucVuInsert;
            this.saving = true;
            this._baoCaoService.insertOrUpdateBaoCaoChucVuCauHinh(input)
                .pipe(finalize(() => this.saving = false))
                .subscribe(result => {
                    if (result) {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.modalSave.emit(lstChucVuInsert);
                        this.modal.hide();
                    } else {
                        this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                    }
                });
        }
        else if (this.source == 3) {
            let input = new BaoCaoChucVuInsert();
            input.baoCaoId = this.nhomDoiTuong.id;
            input.lstChucVuId = lstChucVuInsert;
            this.saving = true;
            this._baoCaoService.insertBaoCaoChucVu(input)
                .pipe(finalize(() => this.saving = false))
                .subscribe(result => {
                    if (result) {
                        this.notify.success(this.l('SavedSuccessfully'));
                        this.modalSave.emit(lstChucVuInsert);
                        this.modal.hide();
                    } else {
                        this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                    }
                });
        }
    }

    close(): void {
        this.modal.hide();
    }
}
