import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { BaoCaoServiceProxy, BaoCaoNhomDoiTuongDto, ChucVuServiceProxy, BaoCaoNhomDoiTuongChiTietInsert } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';

@Component({
    selector: 'nhomChucVuSuDungDoiTuongModal',
    templateUrl: './nhom-chuc-vu-su-dung-doi-tuong-modal.component.html'
})
export class NhomChucVuSuDungDoiTuongModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    primengTableHelper: any;
    filterText: string = "";
    lstChucVuDuocChon: any = [];
    lstChucVuDuocChonOrg: any = [];
    nhomDoiTuong: BaoCaoNhomDoiTuongDto = new BaoCaoNhomDoiTuongDto();
    saving = false;
    constructor(
        private formBuilder: FormBuilder,
        private _baoCaoService: BaoCaoServiceProxy,
        private _chucVuService: ChucVuServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {

    }
    // // convenience getter for easy access to form fields
    // get f() { return this.createOrUpdateNhomDoiTuongForm.controls; }

    show(nhomDoiTuongModel?: any): void {
        if (nhomDoiTuongModel != undefined) {
            this.nhomDoiTuong = nhomDoiTuongModel;
            // this.lstChucVuDuocChon = lstChucVuDaChon.map(x => Object.assign({}, x));
            this.saving = false;
            const self = this;
            self.modal.show();
            this.getChucVu();
        }
    }

    getChucVu(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );
        this._chucVuService
            .getByFilter(
                this.filterText,
                undefined,
                undefined,
                undefined,
                "",
                maxResultCount,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);

                    const isCheckChucVu = this.lstChucVuDuocChon.filter(epic => epic.chucVuId == value.id)[0];
                    if (isCheckChucVu) {
                        model.isChecked = true;
                    }
                    else {
                        model.isChecked = false;
                    }
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                this.primengTableHelper.hideLoadingIndicator();
            });
    }


    chonChucVu(event?: any, data?: any): void {
        if (event) {
            this.lstChucVuDuocChon.push(data);
        } else {
            let indexOf = this.lstChucVuDuocChon.indexOf(data.id);
            this.lstChucVuDuocChon.splice(indexOf, 1);
        }
    }


    onShown(): void {

    }

    save(): void {
        let input = new BaoCaoNhomDoiTuongChiTietInsert();

        input.lstChucVu = this.lstChucVuDuocChon.map(function (item) {
            return item['id'];
        });
        input.baoCaoNhomDoiTuongId = this.nhomDoiTuong.id;

        this.saving = true;
        this._baoCaoService.insertBaoCaoNhomDoiTuongChiTiet(input)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.modalSave.emit(this.lstChucVuDuocChon);
                    this.modal.hide();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });

        // this.modalSave.emit(this.lstChucVuDuocChon);
        // this.modal.hide();
    }

    close(): void {
        this.modal.hide();
    }
}
