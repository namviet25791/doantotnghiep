import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Injector
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
  BaoCaoServiceProxy, BaoCaoNhomDoiTuongDto, CommonEnumServiceProxy
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from '@shared/common/app-component-base';
import { Paginator } from "primeng/paginator";
import notify from "devextreme/ui/notify";
import { NhomDoiTuongChiTietModalComponent } from "./nhom-doi-tuong-chi-tiet-modal.component";
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { NhomChucVuSuDungDoiTuongModalComponent } from "./nhom-chuc-vu-su-dung-doi-tuong-modal.component";

@Component({
  selector: 'app-cau-hinh-nhom-doi-tuong',
  templateUrl: './cau-hinh-nhom-doi-tuong.component.html',
  animations: [appModuleAnimation()]
})
export class CauHinhNhomDoiTuongComponent extends AppComponentBase {

  createOrUpdateNhomDoiTuongForm: FormGroup;
  filterText = "";
  @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild("nhomDoiTuongChiTietModal", { static: true })
  nhomDoiTuongChiTietModal: NhomDoiTuongChiTietModalComponent;

  @ViewChild("nhomChucVuSuDungDoiTuongModal", { static: true })
  nhomChucVuSuDungDoiTuongModal: NhomChucVuSuDungDoiTuongModalComponent;
  @ViewChild("paginator", { static: true }) paginator: Paginator;
  primengTableHelper: any;
  submitted = false;
  isEdit = null;
  saving = false;
  viewType = 0;
  lstDoiTuongChiTiet: any = [];
  selectCurrentItem: any = {};
  lstChucVuSuDungNhomDoiTuong: any = [];
  lstLoaiNhomDoiTuong = [];

  loaiNhomDoiTuong = 1;

  nhomDoiTuongDto: BaoCaoNhomDoiTuongDto = new BaoCaoNhomDoiTuongDto();
  constructor(injector: Injector, private formBuilder: FormBuilder, private _baoCaoService: BaoCaoServiceProxy, private _commonEnumService: CommonEnumServiceProxy) {
    super(injector);
  }

  public projectData: Object[];
  ngOnInit() {
    this.getNhomDoiTuong();
    this.submitted = false;
    this.createOrUpdateNhomDoiTuongForm = this.formBuilder.group({
      tenNhomDoiTuong: ['', Validators.required],
      tenHienThiTrenBaoCao: ['', Validators.required],
      loaiNhomDoiTuong: [0]
    });
    this.getLoaiNhomDoiTuongEnum();
  }
  get f() { return this.createOrUpdateNhomDoiTuongForm.controls; }

  getNhomDoiTuong(event?: LazyLoadEvent) {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    let skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );
    let maxResultCount = this.primengTableHelper.getMaxResultCount(
      this.paginator,
      event
    );

    this._baoCaoService
      .getBaoCaoNhomDoiTuong(0, this.filterText, 0, this.loaiNhomDoiTuong, "", maxResultCount, skipCount)
      .pipe(
        finalize(() => this.primengTableHelper.hideLoadingIndicator())
      )
      .subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        this.primengTableHelper.records = [];
        let soThuTu = skipCount + 1;
        result.items.forEach(value => {
          let model: any = Object.assign({}, value);
          model.stt = soThuTu++;
          this.primengTableHelper.records.push(model);
        });
        //this.primengTableHelper.records = result.items;
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  getLoaiNhomDoiTuongEnum() {
    this._commonEnumService.getLoaiNhomDoiTuongEnum()
      .subscribe(result => {
        this.lstLoaiNhomDoiTuong = result;
      });
  }

  viewChiTietNhomDoiTuong(item?: any, isEdit?: any) {
    this.viewType = 1;
    this.isEdit = isEdit;
    this.submitted = false;

    if (isEdit = true) {
      //edit
      this.nhomDoiTuongDto = Object.assign({}, item);
      this.getDsChucVuTheoNhomDoiTuong();

    } else {
      // addnew
      this.nhomDoiTuongDto = new BaoCaoNhomDoiTuongDto();
      this.nhomDoiTuongDto.id = 0;
      this.lstDoiTuongChiTiet = [];
    }
  }

  onChangeLoaiDoiTuong() {
    if (this.nhomDoiTuongDto.id == 0) {
      this.lstDoiTuongChiTiet = [];
    }
  }


  dongChiTietNhomDoiTuong() {
    this.viewType = 0;
    this.isEdit = null;
  }

  getDsChucVuTheoNhomDoiTuong() {
    this._baoCaoService
      .getBaoCaoNhomDoiTuongChiTiet(0, "", this.nhomDoiTuongDto.id, this.nhomDoiTuongDto.loaiNhomDoiTuong, "", 1, 0)
      .subscribe(result => {
        this.lstDoiTuongChiTiet = result;
      });
  }

  saveNhomDoiTuong() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.createOrUpdateNhomDoiTuongForm.invalid) {
      return;
    }
    this.saving = true;
    this._baoCaoService.insertOrUpdateBaoCaoNhomDoiTuong(this.nhomDoiTuongDto)
      .pipe(finalize(() => this.saving = false))
      .subscribe(result => {
        if (result > 0) {
          this.getNhomDoiTuong();
          this.nhomDoiTuongDto.id = result;
          this.isEdit = true;
          this.notify.success(this.l('SavedSuccessfully'));
        } else {
          this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
        }
      });
  }

  removeChucVu(id) {
    this._baoCaoService.deleteBaoCaoNhomDoiTuongChiTiet(id)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.getDsChucVuTheoNhomDoiTuong();
      });
  }

  viewDoiTuongSuDung(item?: any) {
    this.selectCurrentItem = item;
    this.viewType = 2;
    this.isEdit = null;
    this.submitted = false;

    this.getDsChucVuSuDungDoiTuong();

  }

  getDsChucVuSuDungDoiTuong() {
    this._baoCaoService
      .getBaoCaoChucVuCauHinh(undefined, undefined, this.selectCurrentItem.id)
      .subscribe(result => {
        this.lstChucVuSuDungNhomDoiTuong = result;
      });
  }

  refreshData() {
    if (this.viewType == 1) {
      this.getDsChucVuTheoNhomDoiTuong();
    } else if (this.viewType == 2) {
      this.getDsChucVuSuDungDoiTuong();
    }

  }


  removeBaoCaoChucVuCauHinh(id) {
    this._baoCaoService.deleteBaoCaoChucVuCauHinh(id)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.getDsChucVuSuDungDoiTuong();
      });
  }

  async xoaNhomDoiTuong(item?: any) {
    const that_ = this;
    abp.message.confirm(
      "Bạn chắc chắn muốn xóa mục này?",
      "Thông báo",
      function (result) {
        if (result) {
          that_._baoCaoService.deleteBaoCaoNhomDoiTuong(item.id).subscribe(() => {
            that_.getNhomDoiTuong();
            abp.notify.success("Thao tác thành công!");
          });
        }
      }
    );
  }

  close(): void {
    this.closeEvent.emit(null);
  }



}
