import {
    Component,
    EventEmitter,
    Injector,
    Output,
    ViewChild
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap";
import { finalize } from "rxjs/operators";
import {
    TieuChiDanhGiaDto,
    TieuChiDanhGiaServiceProxy
} from "@shared/service-proxies/service-proxies";
import {
    FormBuilder,
    Validators,
    FormGroup,
    FormControl
} from "@angular/forms";
import * as _ from "lodash";
@Component({
    selector: "createOrEditTieuChiDanhGiaModal",
    templateUrl: "./create-or-edit-tieu-chi-danh-gia-modal.component.html"
})
export class CreateOrEditTieuChiDanhGiaModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateTieuChiDanhGiaForm: FormGroup;
    submitted = false;
    tieuChiDanhGiaDto: TieuChiDanhGiaDto = new TieuChiDanhGiaDto();
    active = false;
    saving = false;

    minSearchLengthOption: number = 0;
    searchExprOption: any = "ten";
    lstData: any = [];


    public onReady(editor) {
    }

    constructor(
        private formBuilder: FormBuilder,
        private _tieuChiDanhGiaService: TieuChiDanhGiaServiceProxy,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateTieuChiDanhGiaForm = this.formBuilder.group({
            diemToiDa: [0, [Validators.required, Validators.min(0), Validators.max(100)]],
            diemToiThieu: [0, [Validators.required, Validators.min(0)]],
            thuTu: ["", Validators.required],
            kyHieu: ["", Validators.required],
            ten: ["", Validators.required],
            tieuChiChaId: [0]
        });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.createOrUpdateTieuChiDanhGiaForm.controls;
    }

    show(
        modelUpdate?: TieuChiDanhGiaDto,
        phieuDanhGiaId?: any,
        isEdit?: false,
        lstTieuChi?: any
    ): void {
        this.lstData = [];
        let data = [];
        _.forEach(lstTieuChi, function (it) {
            let model: any = {};
            model.id = it.id;
            model.ten = it.kyHieu + ": " + it.ten
            data.push(model);
        });
        this.lstData = data;

        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (isEdit) {
            //edit
            this.tieuChiDanhGiaDto = Object.assign({}, modelUpdate);;
        } else {
            let stt = 1;
            if (lstTieuChi.length > 0) {
                const sapXep = _.orderBy(
                    lstTieuChi,
                    ["thuTu"],
                    ["desc"]
                );
                stt = sapXep[0].thuTu + 1;
            }
            // addnew
            this.tieuChiDanhGiaDto = new TieuChiDanhGiaDto();
            this.tieuChiDanhGiaDto.thuTu = stt;
            this.tieuChiDanhGiaDto.diemToiDa = 0;
            this.tieuChiDanhGiaDto.diemToiThieu = 0;
            this.tieuChiDanhGiaDto.ten = "";
            if (modelUpdate) {
                this.tieuChiDanhGiaDto.tieuChiChaId = modelUpdate.id;
                this.tieuChiDanhGiaDto.phieuDanhGiaId =
                    modelUpdate.phieuDanhGiaId;
            } else {
                this.tieuChiDanhGiaDto.phieuDanhGiaId = phieuDanhGiaId;
            }
        }
        self.modal.show();
    }

    onShown(): void { }

    save(): void {
        this.submitted = true;
        // stop here if form is invalid
        if (this.createOrUpdateTieuChiDanhGiaForm.invalid) {
            return;
        }

        this.saving = true;
        this._tieuChiDanhGiaService
            .upsert(this.tieuChiDanhGiaDto)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
