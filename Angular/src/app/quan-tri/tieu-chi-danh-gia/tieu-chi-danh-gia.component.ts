import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import * as moment from "moment";
import { NgModule, ViewChild } from '@angular/core';
import { TieuChiDanhGiaDto, TieuChiDanhGiaServiceProxy } from "@shared/service-proxies/service-proxies";
import { CreateOrEditTieuChiDanhGiaModalComponent } from "./create-or-edit-tieu-chi-danh-gia-modal.component";
import { DxTreeListComponent } from "devextreme-angular";


@Component({
    selector: "app-tieu-chi-danh-gia",
    templateUrl: "./tieu-chi-danh-gia.component.html",
    styleUrls: ['./tieu-chi-danh-gia.component.css']
})
export class TieuChiDanhGiaComponent implements OnInit {
    [x: string]: any;

    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    phieuDanhGia: TieuChiDanhGiaDto = new TieuChiDanhGiaDto;
    @ViewChild('createOrEditTieuChiDanhGiaModal', { static: true })
    createOrEditTieuChiDanhGiaModal: CreateOrEditTieuChiDanhGiaModalComponent;
    @ViewChild("donViTreeList", { static: false }) donViTree: DxTreeListComponent;
    tieuDe = "";

    constructor(
        private _tieuChiDanhGiaService: TieuChiDanhGiaServiceProxy) {
    }

    public projectData: Object[];
    public dateValue: moment.Moment;
    ngOnInit() {
        this.getTieuChiDanhGia();
    }
    submitDate() {
        // console.log(this.dateValue.valueOf());
        // console.log(this.dateValue);
    }

    getTieuChiDanhGia() {
        this.projectData = [];

        this._tieuChiDanhGiaService.getByFilter("", undefined, this.phieuDanhGia.id, undefined, "", 1000, 0)
            .subscribe(result => {
                this.projectData = result.items;
                for (let index = 1; index <= 1000; index++) {
                    this.donViTree.instance.expandRow(index);
                }
            });
    }

    createTieuChiDanhGia(): void {
        this.createOrEditTieuChiDanhGiaModal.show();
    }

    viewPhieuDanhGia(item?: any) {
        this.phieuDanhGia = item;
        this.tieuDe = item.tieuDe;
        this.getTieuChiDanhGia();
    }

    async xoaTieuChi(item?: any) {
        const that_ = this;
        abp.message.confirm("Bạn chắc chắn muốn xóa mục này?", "Thông báo", function (result) {
            if (result) {
                that_._tieuChiDanhGiaService.delete(item.id)
                    .subscribe(() => {
                        that_.getTieuChiDanhGia();
                        abp.notify.success('Thao tác thành công!');
                    });
            }
        })
    }

    close(): void {
        this.closeEvent.emit(null);
    }

}
