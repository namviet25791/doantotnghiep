import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { DonViServiceProxy } from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import * as _ from "lodash";
import * as $ from "jquery";

@Component({
    selector: 'timKiemDonViModal',
    templateUrl: './tim-kiem-don-vi-modal.component.html'
})
export class TimKiemDonViModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    submitted = false;
    active = false;
    saving = false;
    capDonViId?: 0;
    khoiId?: 0;
    filterText: '';
    lstDonViDuocChon: any[];
    lstDonViDuocChonOrg: any[];
    chucVuId: any;
    canBoId: any;
    constructor(
        private _donViService: DonViServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
    }

    show(chucVuId?: any, canboId?: any): void {
        this.chucVuId = chucVuId;
        this.canBoId = canboId;
        this.lstDonViDuocChon = [];
        const self = this;
        self.active = true;
        this.getDanhSachDonVi();
        self.modal.show();
    }

    getDanhSachDonVi(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );

        let ncapDonViId = this.capDonViId != 0 ? this.capDonViId : undefined;
        let nkhoiId = this.khoiId != 0 ? this.khoiId : undefined;

        this._donViService.getDonViChoCanBoChucVu(this.filterText, undefined, ncapDonViId, nkhoiId,
            undefined, undefined, undefined, this.canBoId, this.chucVuId, "", maxResultCount, skipCount)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;

                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);

                    const isCheckDonVi = this.lstDonViDuocChon.filter(epic => epic.id == value.id)[0];

                    if (isCheckDonVi) {
                        model.isChecked = true;
                    }
                    else {
                        model.isChecked = false;
                    }
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                this.primengTableHelper.hideLoadingIndicator();
            });
    }


    onShown(): void {

    }
    chonDonViCHuyenTrach(event?: any, data?: any): void {
        if (event) {
            this.lstDonViDuocChon.push(data);
        } else {
            let indexOf = this.lstDonViDuocChon.indexOf(data.id);
            this.lstDonViDuocChon.splice(indexOf, 1);
        }
    }

    save(): void {
        this.modalSave.emit(this.lstDonViDuocChon);
        this.modal.hide();
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
