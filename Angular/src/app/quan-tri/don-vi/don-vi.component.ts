import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Injector
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    DonViDto,
    DonViServiceProxy
} from "@shared/service-proxies/service-proxies";
//import { CreateOrEditDonViModalComponent } from "./create-or-edit-cap-don-vi-modal.component";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator } from "primeng/paginator";
import { CreateOrEditDonViModalComponent } from "./create-or-edit-don-vi-modal.component";
import { DxTreeListComponent } from "devextreme-angular";
@Component({
    selector: "app-don-vi",
    templateUrl: "./don-vi.component.html",
    animations: [appModuleAnimation()]
})
export class DonViComponent extends AppComponentBase implements OnInit {
    [x: string]: any;
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    donViDto: DonViDto = new DonViDto();
    @ViewChild("createOrEditDonViModal", { static: true })
    createOrEditDonViModal: CreateOrEditDonViModalComponent;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    @ViewChild("donViTreeList", { static: false }) donViTree: DxTreeListComponent;

    capDonViId?: 0;
    khoiId?: 0;

    public employees: any[] = [];

    constructor(injector: Injector, private _donViService: DonViServiceProxy) {
        super(injector);
    }
    public projectData: any[] = [];




    ngOnInit() {
        this.getDonVi();
    }

    getDonVi() {
        let ncapDonViId = this.capDonViId != 0 ? this.capDonViId : undefined;
        let nkhoiId = this.khoiId != 0 ? this.khoiId : undefined;

        //this.employees = [];
        this.projectData = [];


        this._donViService
            .getByFilter(
                this.filterText,
                undefined,
                ncapDonViId,
                nkhoiId,
                undefined,
                undefined,
                undefined,
                0, 0,
                "",
                1000,
                0
            )
            .subscribe(result => {
                this.projectData = result.items;

            });
    }

    createDonVi(): void {
        this.createOrEditDonViModal.show(null, false);
    }

    async xoaDonVi(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_._donViService.delete(item.id).subscribe(() => {
                        that_.getDonVi();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }

    close(): void {
        this.closeEvent.emit(null);
    }


}
