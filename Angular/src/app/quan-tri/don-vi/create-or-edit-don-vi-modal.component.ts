import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { DonViDto, DonViServiceProxy, LoaiDonViServiceProxy, LoaiDonViDto, CommonEnumServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { HuyenComboComponent } from '@app/shared/ui-combo-box/huyen-combo.component';
import * as _ from "lodash";
@Component({
    selector: 'createOrEditDonViModal',
    templateUrl: './create-or-edit-don-vi-modal.component.html'
})
export class CreateOrEditDonViModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild("huyencombo", { static: true }) huyencombo: HuyenComboComponent;

    createOrUpdateDonViForm: FormGroup;
    submitted = false;
    donViDto: DonViDto = new DonViDto();
    active = false;
    saving = false;
    isEdit = null;
    lstKhoiTruongHoc = [];
    public dataSelect2: Array<any>;
    public options: Select2Options;

    public dataLoaiDonVi: Array<LoaiDonViDto>;

    constructor(
        private formBuilder: FormBuilder,
        private _donViService: DonViServiceProxy,
        private _commonEnumService: CommonEnumServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._commonEnumService.getKhoiTruongHocEnum().subscribe(result => {
            this.lstKhoiTruongHoc = result;
        });

        this.submitted = false;
        this.createOrUpdateDonViForm = this.formBuilder.group({
            ten: ['', Validators.required],
            capDonViId: [0, [Validators.required, Validators.min(1)]],
            khoiId: [0, [Validators.required, Validators.min(1)]],
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateDonViForm.controls; }

    show(modelUpdate?: DonViDto, isEdit?: false): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        this.isEdit = isEdit;
        self.active = true;

        if (isEdit) {
            //edit
            this.donViDto = Object.assign({}, modelUpdate);
        } else {
            // addnew
            this.donViDto = new DonViDto();
            this.donViDto.id = 0;
            this.donViDto.capDonViId = undefined;
            this.donViDto.khoiId = undefined;
            this.donViDto.donViQuanLyId = undefined;
            this.donViDto.tinhId = this.appSession.macDinhTinhId;
            this.donViDto.huyenId = this.appSession.macDinhHuyenId;
            this.donViDto.khoiTruongHocId = 0;

            if (modelUpdate) {
                setTimeout(function () {
                    this.donViDto.donViQuanLyId = modelUpdate.id;
                }.bind(this), 1);
            }
        }
        this.huyencombo.getDataOptions(this.donViDto.tinhId);
        self.modal.show();
    }

    onShown(): void {

    }

    save(): void {
        this.submitted = true;
        if (this.createOrUpdateDonViForm.invalid) {
            return;
        }
        this.saving = true;

        if (this.donViDto.khoiId == 5 && this.donViDto.khoiTruongHocId == 0) {
            this.donViDto.khoiTruongHocId = this.lstKhoiTruongHoc[0].id;
        }


        this._donViService.upsert(this.donViDto)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result > 0) {

                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else if (result == 0) {
                    this.notify.error("Có lỗi xảy ra vui lòng thao tác lại!");
                } else if (result == -1) {
                    this.notify.error("Chức vụ đã tồn tại.");
                }
            });
    }

    close(): void {
        this.isEdit = null;
        this.active = false;
        this.modal.hide();
    }
}
