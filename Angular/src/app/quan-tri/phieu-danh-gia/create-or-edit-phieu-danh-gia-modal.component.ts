import {
    Component,
    ElementRef,
    EventEmitter,
    Injector,
    Output,
    ViewChild
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap";
import { finalize } from "rxjs/operators";
import {
    PhieuDanhGiaDto,
    PhieuDanhGiaServiceProxy
} from "@shared/service-proxies/service-proxies";
import {
    FormBuilder,
    Validators,
    FormGroup,
    FormControl
} from "@angular/forms";

@Component({
    selector: "createOrEditPhieuDanhGiaModal",
    templateUrl: "./create-or-edit-phieu-danh-gia-modal.component.html"
})
export class CreateOrEditPhieuDanhGiaModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdatePhieuDanhGiaForm: FormGroup;
    submitted = false;
    phieuDanhGia: PhieuDanhGiaDto = new PhieuDanhGiaDto();
    active = false;
    saving = false;

    constructor(
        private formBuilder: FormBuilder,
        private _phieuDanhGiaService: PhieuDanhGiaServiceProxy,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdatePhieuDanhGiaForm = this.formBuilder.group({
            ten: ["", [Validators.required, this.noWhitespaceValidator]],
            tieuDe: ["", Validators.required],
            ghiChu: [""],
            thangDiemId: [0, [Validators.required, Validators.min(1)]],
        });
    }
    public noWhitespaceValidator(control: FormControl) {
        const isWhitespace = (control.value || "").trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : { whitespace: true };
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.createOrUpdatePhieuDanhGiaForm.controls;
    }

    show(modelUpdate?: PhieuDanhGiaDto): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (modelUpdate) {
            this.phieuDanhGia = Object.assign({}, modelUpdate);
        } else {
            this.phieuDanhGia = new PhieuDanhGiaDto();
            this.phieuDanhGia.thangDiemId = 0;
        }

        self.modal.show();
    }

    onShown(): void { }

    save(): void {
        this.submitted = true;

        // stop here if form is invalid
        if (this.createOrUpdatePhieuDanhGiaForm.invalid) {
            this.notify.error("Vui lòng điền đẩy đủ thông tin!");
            return;
        }

        this.saving = true;
        this._phieuDanhGiaService
            .upsert(this.phieuDanhGia)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
