import { Component, OnInit, Injector, ViewChild } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { CreateOrEditPhieuDanhGiaModalComponent } from "./create-or-edit-phieu-danh-gia-modal.component";
import { LazyLoadEvent } from "primeng/api";
import { Table } from "primeng/table";
import { Paginator } from "primeng/paginator";
import {
    PhieuDanhGiaServiceProxy,
    CopyPhieuDanhGiaInput
} from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";
import * as moment from "moment";
import { format } from "url";
import { TieuChiDanhGiaComponent } from "../tieu-chi-danh-gia/tieu-chi-danh-gia.component";

@Component({
    selector: "app-phieu-danh-gia",
    animations: [appModuleAnimation()],
    templateUrl: "./phieu-danh-gia.component.html"
})
export class PhieuDanhGiaComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditPhieuDanhGiaModal", { static: true })
    createOrEditPhieuDanhGiaModal: CreateOrEditPhieuDanhGiaModalComponent;
    @ViewChild("dataTable", { static: true }) dataTable: Table;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("tieuChiDanhGiaView", { static: true })
    tieuChiDanhGiaView: TieuChiDanhGiaComponent;

    isViewDanhSach: any = false;
    filterText = "";
    hieuLucTuNgay: null;
    hieuLucToiNgay: null;

    constructor(
        injector: Injector,
        private _phieuDanhGiaService: PhieuDanhGiaServiceProxy
    ) {
        super(injector);
    }

    ngOnInit(): void {
        this.getPhieuDanhGia();
    }

    getPhieuDanhGia(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );


        this._phieuDanhGiaService
            .getByFilter(
                this.filterText,
                this.hieuLucTuNgay,
                this.hieuLucToiNgay,
                "",
                maxResultCount,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                //this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    createPhieuDanhGia(): void {
        this.createOrEditPhieuDanhGiaModal.show();
    }

    loadChiTietPhieu(data?: any): void {
        this.isViewDanhSach = true;
        //this.tieuChiDanhGiaView.show(data);
    }
    veTrangDanhSach(): void {
        this.isViewDanhSach = false;
    }
    async xoaPhieuDanhGia(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_._phieuDanhGiaService.delete(item.id).subscribe(() => {
                        that_.getPhieuDanhGia();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }

    async copyPhieuDanhGia(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn muốn sao chép phiếu đánh giá '" + item.ten + "'?",
            "Thông báo",
            function (result) {
                if (result) {
                    let input: CopyPhieuDanhGiaInput = new CopyPhieuDanhGiaInput();
                    input.phieuDanhGiaTruocDoId = item.id;
                    that_._phieuDanhGiaService
                        .copyPhieuDanhGia(input)
                        .subscribe(() => {
                            that_.getPhieuDanhGia();
                            abp.notify.success("Thao tác thành công!");
                        });
                }
            }
        );
    }
}
