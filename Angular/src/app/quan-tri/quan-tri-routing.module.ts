import { NgModule } from "@angular/core";
import { RouterModule, Router, NavigationEnd } from "@angular/router";
import { ThangDiemComponent } from "./thang-diem/thang-diem.component";
import { PhieuDanhGiaComponent } from "./phieu-danh-gia/phieu-danh-gia.component";
import { KhoiComponent } from "./khoi/khoi.component";
import { CapDonViComponent } from "./cap-don-vi/cap-don-vi.component";
import { LoaiDonViComponent } from "./loai-don-vi/loai-don-vi.component";
import { DonViComponent } from "./don-vi/don-vi.component";
import { ChucVuComponent } from "./chuc-vu/chuc-vu.component";
import { CanBoComponent } from "./can-bo/can-bo.component";
import { CauHinhNhomDoiTuongComponent } from "./bao-cao/cau-hinh-nhom-doi-tuong/cau-hinh-nhom-doi-tuong.component";
import { BaoCaoComponent } from "./bao-cao/bao-cao.component";
@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: "",
                children: [
                    {
                        path: "thang-diem",
                        component: ThangDiemComponent,
                        data: { permission: 'Pages.Admin.ThangDiem' }
                    },
                    {
                        path: "phieu-danh-gia",
                        component: PhieuDanhGiaComponent,
                        data: { permission: 'Pages.Admin.PhieuDanhGia' }
                    },
                    {
                        path: "khoi-hanh-chinh",
                        component: KhoiComponent,
                        data: { permission: 'Pages.Admin.Khoi' }
                    },
                    {
                        path: "cap-don-vi",
                        component: CapDonViComponent,
                        data: { permission: 'Pages.Admin.CapDonVi' }
                    },
                    {
                        path: "loai-don-vi",
                        component: LoaiDonViComponent,
                        data: { permission: 'Pages.Admin.LoaiDonVi' }
                    },
                    {
                        path: "don-vi",
                        component: DonViComponent,
                        data: { permission: 'Pages.Admin.DonVi' }
                    },
                    {
                        path: "chuc-vu",
                        component: ChucVuComponent,
                        data: { permission: 'Pages.Admin.ChucVu' }
                    },
                    {
                        path: "nhan-su",
                        component: CanBoComponent,
                        data: { permission: 'Pages.Admin.CanBo' }
                    },
                    {
                        path: "cau-hinh-bao-cao",
                        component: BaoCaoComponent,
                        data: { permission: 'Pages.Admin.ChucVu' }
                    },
                    {
                        path: "nhom-doi-tuong",
                        component: CauHinhNhomDoiTuongComponent,
                        data: { permission: 'Pages.Admin.ChucVu' }
                    },
                ]
            }
        ])
    ],
    exports: [RouterModule]
})
export class QuanTriRoutingModule {
}
