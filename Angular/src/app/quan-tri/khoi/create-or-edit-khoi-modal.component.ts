import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { KhoiDto, KhoiServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'createOrEditKhoiModal',
    templateUrl: './create-or-edit-khoi-modal.component.html'
})
export class CreateOrEditKhoiModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateKhoiForm: FormGroup;
    submitted = false;
    khoiDto: KhoiDto = new KhoiDto();
    active = false;
    saving = false;

    constructor(
        private formBuilder: FormBuilder,
        private _khoiService: KhoiServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateKhoiForm = this.formBuilder.group({
            ten: ["", Validators.required]
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateKhoiForm.controls; }

    show(modelUpdate?: KhoiDto): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (modelUpdate) {
            //edit
            this.khoiDto = Object.assign({}, modelUpdate);;
        } else {
            // addnew
            this.khoiDto = new KhoiDto();
            this.khoiDto.id = 0;
        }
        self.modal.show();
    }

    onShown(): void {

    }

    save(): void {
        this.submitted = true;
        // stop here if form is invalid
        if (this.createOrUpdateKhoiForm.invalid) {
            return;
        }
        this.saving = true;
        this._khoiService.upsert(this.khoiDto)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
