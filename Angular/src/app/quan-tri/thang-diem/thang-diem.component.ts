import { Component, Injector, ViewChild, OnInit } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import { AppComponentBase } from "@shared/common/app-component-base";
import * as _ from "lodash";
import { finalize } from "rxjs/operators";
import {
    ThangDiemChiTietServiceProxy,
    ThangDiemServiceProxy,
    ThangDiemDto,
    CopyThangDiemChiTietInput,
    CopyThangDiemInput
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent } from "primeng/api";
import * as moment from "moment";
import { Paginator } from "primeng/paginator";

@Component({
    templateUrl: "./thang-diem.component.html",
    animations: [appModuleAnimation()]
})
export class ThangDiemComponent extends AppComponentBase implements OnInit {
    public filterText: string;
    public chiTietThangdiem: any[];
    hieuLucTuNgay: moment.Moment;
    hieuLucToiNgay: moment.Moment;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    constructor(
        injector: Injector,
        private _thangDiemService: ThangDiemServiceProxy,
        private _thangDiemChiTietService: ThangDiemChiTietServiceProxy
    ) {
        super(injector);
    }
    // abp.auth.hasPermission('Pages.QuanLiNhaCungCap');


    ngOnInit(): void {
        this.searchItems();
    }
    insertItem(): void { }

    searchItems(event?: LazyLoadEvent): void {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );
        // if (this.hieuLucTuNgay && !this.hieuLucTuNgay.isValid()) {
        //     this.hieuLucTuNgay = undefined;
        // }
        // if (this.hieuLucToiNgay && !this.hieuLucToiNgay.isValid()) {
        //     this.hieuLucToiNgay = undefined;
        // }

        this._thangDiemService
            .getByFilter(
                this.filterText,
                this.hieuLucTuNgay,
                this.hieuLucToiNgay,
                undefined,
                "",
                maxResultCount,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                //this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }
    detailItem(data) {
        this.chiTietThangdiem = [];

        this.chiTietThangdiem = data.thangDiemChiTiet;
    }

    async copyThangDiem(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn muốn tạo bản sao thang điểm của phiếu đánh giá: " +
            item.ten +
            "?",
            "Thông báo",
            function (result) {
                if (result) {
                    let input = new CopyThangDiemInput();
                    input.thangDiemTruocDoId = item.id;

                    that_._thangDiemService
                        .copyThangDiem(input)
                        .subscribe(() => {
                            that_.searchItems();
                            abp.notify.success("Thao tác thành công!");
                        });
                }
            }
        );
    }
    async xoaThangDiem(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_._thangDiemService.delete(item.id).subscribe(() => {
                        that_.searchItems();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }
}
