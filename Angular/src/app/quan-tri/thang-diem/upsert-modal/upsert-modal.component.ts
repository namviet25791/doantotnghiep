import {
    Component,
    EventEmitter,
    Output,
    Injector,
    ViewChild
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap";
import * as $ from "jquery";
import * as _ from "lodash";
import {
    ThangDiemChiTietDto,
    ThangDiemChiTietServiceProxy,
    ThangDiemDto,
    ThangDiemServiceProxy
} from "@shared/service-proxies/service-proxies";
import { finalize, publish } from "rxjs/operators";
import { stringify } from "@angular/compiler/src/util";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { fail } from "assert";

@Component({
    selector: "app-thang-diem-upsert-modal",
    templateUrl: "./upsert-modal.component.html"
})
export class ThangDiemUpsertModalComponent extends AppComponentBase {
    isEditItem: boolean = false;
    chiTietThangDiem: any[] = [];
    thangDiemChiTietDto: ThangDiemChiTietDto = new ThangDiemChiTietDto();
    thangDiemDto: ThangDiemDto = new ThangDiemDto();
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild("modal", { static: true }) modal: ModalDirective;
    saving: boolean = false;
    submitted = false;
    createOrUpdateThangDiemForm: FormGroup;

    constructor(
        injector: Injector,
        private formBuilder: FormBuilder,
        private _thangDiemService: ThangDiemServiceProxy,
        private _thangDiemChiTietService: ThangDiemChiTietServiceProxy
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateThangDiemForm = this.formBuilder.group({
            ten: ["", Validators.required]
            // diemTran: [0, [Validators.required, Validators.min(0), Validators.max(100)]],
            // diemSan: [0, [Validators.required, Validators.min(0), Validators.max(100)]]
        });
    }
    // convenience getter for easy access to form fields
    get f() {
        return this.createOrUpdateThangDiemForm.controls;
    }

    show(modelUpdate?: ThangDiemDto, isEdit?: false): void {
        this.submitted = false;
        this.isEditItem = isEdit;
        if (isEdit) {
            this.thangDiemDto = Object.assign({}, modelUpdate);
            this.loadThangDiemChiTiet();
        } else {
            this.thangDiemDto = new ThangDiemDto();
            this.thangDiemDto.id = undefined;
            this.thangDiemDto.phieuDanhGiaId = 0;
            this.thangDiemDto.hieuLucTuNgay = undefined;
            this.thangDiemDto.hieuLucToiNgay = undefined;
        }

        this.modal.show();
    }

    loadThangDiemChiTiet(): void {
        this._thangDiemChiTietService
            .getByFilter(
                "",
                this.thangDiemDto.id,
                undefined,
                undefined,
                "",
                1000,
                0
            )
            .subscribe(result => {
                this.chiTietThangDiem = result.items;
            });
    }

    save(): void {
        this.submitted = true;
        //this.isEditItem = true;

        if (this.createOrUpdateThangDiemForm.invalid) {
            return;
        }

        this.saving = true;
        if (this.isEditItem) {
            this.themHoacSuaThangDiem();
            this.themHoacSuaThangDiemChiTiet();
        } else {
            this.themHoacSuaThangDiem();
        }
    }

    themHoacSuaThangDiem() {
        let num = this.thangDiemDto.phieuDanhGiaId;
        this._thangDiemService
            .upsert(this.thangDiemDto)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(result => {
                if (result > 0) {
                    this.thangDiemDto.id = result;
                    this.isEditItem = true;
                }
            });
    }
    themHoacSuaThangDiemChiTiet() {
        //console.log("this.chiTietThangDiem", this.chiTietThangDiem);
        this._thangDiemChiTietService
            .upsert(this.chiTietThangDiem)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    setDefault(): void {
        this.chiTietThangDiem = [
            {
                id: 1,
                ten: "Hoàn thành xuất sắc nhiệm vụ",
                diemTran: 10,
                diemSan: 90
            },
            {
                id: 2,
                ten: "Hoàn thành tốt nhiệm vụ",
                diemTran: 89,
                diemSan: 70
            },
            {
                id: 3,
                ten: "Hoàn thành nhiệm vụ",
                diemTran: 69,
                diemSan: 50
            },
            {
                id: 4,
                ten: "Không hoàn thành nhiệm vụ",
                diemTran: 50,
                diemSan: 0
            }
        ];
    }
    addItem(): void {

        const check = this.chiTietThangDiem.filter(epic => epic.ten == "")[0];

        if (!check) {
            this.chiTietThangDiem.push({
                id: undefined,
                ten: "",
                diemTran: 0,
                diemSan: 0,
                thangDiemId: this.thangDiemDto.id,
                loaiThangDiemChiTiet: 0
            });

            this.orderByItem();
        }
    }
    removeItem(idx: number, id?: number): void {
        if (id) {
            this.saving = true;
            this._thangDiemChiTietService
                .delete(id)
                .pipe(finalize(() => (this.saving = false)))
                .subscribe(() => {
                    this.loadThangDiemChiTiet();
                });
        } else {
            this.chiTietThangDiem.splice(idx, 1);
        }
        this.orderByItem();
    }
    orderByItem(): void {
        this.chiTietThangDiem = _.orderBy(
            this.chiTietThangDiem,
            ["diemTran"],
            ["desc"]
        );
    }
    resetForm(): void {
        this.chiTietThangDiem = [];
    }
    close(): void {
        this.resetForm();
        this.modal.hide();
    }
}
