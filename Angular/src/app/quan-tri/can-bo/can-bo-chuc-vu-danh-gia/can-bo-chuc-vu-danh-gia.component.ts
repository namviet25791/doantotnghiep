import { Component, EventEmitter, Injector, Output, ViewChild, ViewEncapsulation, Input } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize, filter } from 'rxjs/operators';
import { CanBoChucVuDanhGiaDto, CanBoChucVuDanhGiaServiceProxy, ChucVuDanhGiaServiceProxy, CanBoServiceProxy, ChucVuServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from "lodash";
import { ModalDirective } from 'ngx-bootstrap';
import { AppUtilityService } from '@app/shared/common/custom/utility.service';

@Component({
  selector: 'app-can-bo-chuc-vu-danh-gia',
  templateUrl: './can-bo-chuc-vu-danh-gia.component.html',
  encapsulation: ViewEncapsulation.None
})
export class CanBoChucVuDanhGiaComponent extends AppComponentBase {
  @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  donViId = 0;
  canBoId = 0;
  canBoDto: any;
  active = false;
  saving = false;
  minSearchLengthOption: number = 0;
  searchExprOption: any = "ten";
  lstChucVuCanBo: any = [];
  submitted = false;
  chucVuCanBoId = 0;
  lstChucVuDanhGia: any = [];

  lstChamDuyet: any = [
    { id: true, text: "Người chấm" },
    { id: false, text: "Ngưới duyệt" }
  ];

  constructor(
    private _canBoChucVuDanhGiaService: CanBoChucVuDanhGiaServiceProxy,
    private _chucVuDanhGiaService: ChucVuDanhGiaServiceProxy,
    private _chucVuService: ChucVuServiceProxy,
    injector: Injector,
  ) {
    super(injector);
  }

  orderByItem(): void {
    this.lstChucVuDanhGia = _.orderBy(
      this.lstChucVuDanhGia,
      ["thuTu"]
    );
  }

  show(canBo?: any, chucVuCanBoId?: any): void {
    this.submitted = false;
    this.saving = false;
    const self = this;
    self.active = true;
    self.modal.show();

    this.chucVuCanBoId = chucVuCanBoId;
    this.canBoId = canBo.id;
    this.donViId = canBo.donViId;
    this.loadCanBoChucVu();
  }


  loadCanBoChucVu() {
    this._canBoChucVuDanhGiaService.getCanBoChucVuDanhGiaByCanBoId(this.canBoId, this.chucVuCanBoId)
      .subscribe(result => {
        this.lstChucVuDanhGia = result;
        this.orderByItem();

      });
  }

  loadChucVuDanhGiaMacDinh(): void {
    this.lstChucVuDanhGia = [];
    let canBoId = this.canBoId;
    let chucVuCanBoId = this.chucVuCanBoId;

    this._chucVuDanhGiaService.getChucVuDanhGiaTheoChucVuCanBoId(canBoId, chucVuCanBoId)
      .subscribe(result => {
        this.lstChucVuDanhGia = result;
        this.orderByItem();
      });
  }
  onChangeChucvu(item) {

    this._canBoChucVuDanhGiaService.getCanBoTheoChucVu(item.chucVuDanhGiaId, this.donViId)
      .subscribe(result => {
        item.listCanBoChucVu = result;
        if (result.length > 0) {
          if (item.canBoDanhGiaId == null) {
            item.canBoDanhGiaId = result[0].canBoId;
          }

        } else {
          item.canBoDanhGiaId = 0;
        }
      });
  }


  createModelCanBoChucVuDanhGia() {
    var item = new CanBoChucVuDanhGiaDto();
    item.id = 0;
    item.canBoId = this.canBoId;
    item.chucVuCanBoId = this.chucVuCanBoId;
    item.nguoiCham = true;
    item.chucVuDanhGiaId = 0;
    item.thuTu = 0;
    item.canBoDanhGiaId = 0;
    return item;
  }


  addItem(): void {
    let stt = 1;

    if (this.lstChucVuDanhGia.length > 0) {
      const sapXep = _.orderBy(
        this.lstChucVuDanhGia,
        ["thuTu"],
        ["desc"]
      );
      stt = sapXep[0].thuTu + 1;

      var item = this.createModelCanBoChucVuDanhGia();

      if (sapXep[0].chucVuDanhGiaId != 0) {
        item.thuTu = stt;
      }
      else {
        item.thuTu = stt;
        this.lstChucVuDanhGia.push(item);
      }
      this.lstChucVuDanhGia.push(item);
    }

    this.orderByItem();
  }
  removeItem(idx: number, id?: number): void {
    if (id != undefined && id > 0) {
      this.xoaChucVuDanhGia(id);
    } else {
      this.lstChucVuDanhGia.splice(idx, 1);
    }
    this.orderByItem();
  }
  async xoaChucVuDanhGia(id?: any) {
    const that_ = this;
    abp.message.confirm(
      "Bạn chắc chắn muốn xóa mục này?",
      "Thông báo",
      function (result) {
        if (result) {
          that_.saving = true;
          that_._canBoChucVuDanhGiaService.delete(id)
            .pipe(finalize(() => that_.saving = false))
            .subscribe(() => {
              that_.loadCanBoChucVu();
            });
        }
      }
    );
  }


  save(): void {
    if (this.lstChucVuDanhGia.length == 0) {
      this.notify.error("Xin chọn chức vụ chấm/duyệt.");
      return;
    }

    let kiemTraCanBoDanhGia = this.lstChucVuDanhGia.filter(epic => epic.loaiChucVu == 1 && epic.canBoDanhGiaId == 0);
    if (kiemTraCanBoDanhGia.length > 0) {
      this.notify.error("Xin chọn cán bộ chấm/duyệt.");
      return;
    }

    //check chucVu duplicate
    var valueArr = this.lstChucVuDanhGia.map(function (item) { return item.chucVuDanhGiaId });
    var isDuplicate = valueArr.some(function (item, idx) {
      return valueArr.indexOf(item) != idx
    });
    if (isDuplicate == true) {
      this.submitted = false;
      this.saving = false;
      this.notify.error("Chức vụ đánh giá không được trùng.");
      return;
    }
    //end check chucVu duplicate


    this.submitted = true;
    this.saving = true;

    this._canBoChucVuDanhGiaService.upsert(this.lstChucVuDanhGia)
      .pipe(finalize(() => this.saving = false))
      .subscribe(rs => {
        if (rs.result) {
          this.notify.success(this.l('SavedSuccessfully'));
          this.close();
          this.modalSave.emit();
        } else {
          this.notify.error(rs.msg);
        }
      });
  }

  onShown(): void { }
  close(): void {
    this.active = false;
    this.modal.hide();
  }


  upPositionParameterCategory(idx) {
    if (idx > 0) {
      this.lstChucVuDanhGia = AppUtilityService.array_move(
        this.lstChucVuDanhGia,
        idx,
        idx - 1
      );
      this.changeStt();
    }
  }

  downPositionParameterCategory(idx) {
    if (idx < this.lstChucVuDanhGia.length - 1) {
      this.lstChucVuDanhGia = AppUtilityService.array_move(
        this.lstChucVuDanhGia,
        idx,
        idx + 1
      );
      this.changeStt();
    }
  }

  changeStt() {
    this.lstChucVuDanhGia.forEach((item, index) => {
      item.thuTu = index + 1;
    });
  }

}  
