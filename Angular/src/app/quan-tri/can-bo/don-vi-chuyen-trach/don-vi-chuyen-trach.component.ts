import { Component, OnInit, ViewChild, Injector } from '@angular/core';
import { TimKiemDonViModalComponent } from '@app/quan-tri/don-vi/tim-kiem-don-vi-modal.component';
import { AppComponentBase } from '@shared/common/app-component-base';
import { DonViChuyenTrachServiceProxy, DonViChuyenTrachUpdate } from '@shared/service-proxies/service-proxies';
import { Paginator } from 'primeng/paginator';
import { LazyLoadEvent } from 'primeng/api';
import { finalize } from 'rxjs/operators';
import * as _ from "lodash";
import { CanBoChucVuComboComponent } from '@app/shared/ui-combo-box/chuc-vu-can-bo-combo.component';
@Component({
  selector: 'app-don-vi-chuyen-trach',
  templateUrl: './don-vi-chuyen-trach.component.html'
})
export class DonViChuyenTrachComponent extends AppComponentBase implements OnInit {
  @ViewChild('timKiemDonViModal', { static: true })
  timKiemDonViModal: TimKiemDonViModalComponent;
  @ViewChild("canbochucvu", { static: true }) canbochucvu: CanBoChucVuComboComponent;

  @ViewChild("paginator", { static: true }) paginator: Paginator;
  canboInfo: any;
  filterText: any = "";
  saving = false;
  selectChucVuId: any;
  constructor(injector: Injector,
    private _donViChuyenTrachService: DonViChuyenTrachServiceProxy) {
    super(injector);
  }

  ngOnInit() {

  }

  loadDonViChuyenTrach(event?: LazyLoadEvent): void {
    if (this.primengTableHelper.shouldResetPaging(event)) {
      this.paginator.changePage(0);
      return;
    }
    let skipCount = this.primengTableHelper.getSkipCount(
      this.paginator,
      event
    );
    let maxResultCount = this.primengTableHelper.getMaxResultCount(
      this.paginator,
      event
    );

    this._donViChuyenTrachService.getByFilter(this.filterText, this.canboInfo.id, undefined, this.selectChucVuId, "", maxResultCount, skipCount)
      .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
        this.primengTableHelper.totalRecordsCount = result.totalCount;
        // this.primengTableHelper.records = result.items;
        this.primengTableHelper.records = [];
        let soThuTu = skipCount + 1;
        result.items.forEach(value => {
          let model: any = Object.assign({}, value);
          model.stt = soThuTu++;
          this.primengTableHelper.records.push(model);
        });
        this.primengTableHelper.hideLoadingIndicator();
      });
  }

  openTimKiemDonViChuyenTrach(): void {
    this.timKiemDonViModal.show(this.selectChucVuId, this.canboInfo.id);
  }

  showDanhSachDonViChuyenTrach(canBoInfo?: any): void {
    this.canboInfo = canBoInfo;
    this.canbochucvu.getDataOptions(this.canboInfo.id);
    this.loadDonViChuyenTrach();
  }

  xoaDonViChuyenTrach(data?: any): void {
    this.saving = true;
    this._donViChuyenTrachService.delete(data.id)
      .pipe(finalize(() => this.saving = false))
      .subscribe(() => {
        this.loadDonViChuyenTrach();
      });
  }

  updateDonViChuyenTrach(lstData?: any): void {
    //console.log("lstData", dataInput);

    let canBoId = this.canboInfo.id;
    var lstNumber = lstData.map(function (item) {
      return item['id'];
    });

    let lstInput: DonViChuyenTrachUpdate[] = [];

    let input = new DonViChuyenTrachUpdate;
    input.canBoId = canBoId;
    input.lstDonVi = lstNumber;
    input.chucVuId = this.selectChucVuId;
    lstInput.push(input);

    this.saving = true;
    this._donViChuyenTrachService.insertDonViChuyenTrach(input)
      .pipe(finalize(() => this.saving = false))
      .subscribe(result => {
        if (result) {
          this.notify.success(this.l('SavedSuccessfully'));
          this.loadDonViChuyenTrach();
        } else {
          this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
        }
      });
  }
}
