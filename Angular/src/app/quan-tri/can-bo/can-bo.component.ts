import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Injector,
    ViewEncapsulation
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    CanBoDto,
    CanBoServiceProxy
} from "@shared/service-proxies/service-proxies";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator } from "primeng/paginator";
import { CreateOrEditCanBoComponent } from "./thong-tin-can-bo.component";
import { ChangePasswordModalComponent } from "@app/shared/layout/profile/change-password-modal.component";

@Component({
    selector: "app-can-bo",
    templateUrl: "./can-bo.component.html",
    styleUrls: ["./can-bo.component.css"],
    encapsulation: ViewEncapsulation.None,
    animations: [appModuleAnimation()]
})
export class CanBoComponent extends AppComponentBase implements OnInit {
    [x: string]: any;
    filterText = "";
    donViId?: 0;
    chucVuId = 0;
    isViewDanhSach: boolean = true;
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    canBoDto: CanBoDto = new CanBoDto();
    // @ViewChild('createOrEditCanBoModal', { static: true })
    // createOrEditCanBoModal: CreateOrEditCanBoModalComponent;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    @ViewChild("createOrEditCanBoView", { static: true })
    createOrEditCanBoView: CreateOrEditCanBoComponent;

    // @ViewChild("changePasswordModal", { static: true })
    // changePasswordModal: ChangePasswordModalComponent;
    isViewTimKiemNangCao = false;
    isViewChucVuCanBo = false;

    activeTab: any = 1;
    viewMode = "danh_sach";

    dsChucNangNguoiDung: any[] = [
        { id: 1, name: "Thông tin nhân viên" },
        { id: 2, name: "Chức vụ" },
        { id: 3, name: "Đổi mật khẩu" }
    ];

    constructor(injector: Injector, private _canBoService: CanBoServiceProxy) {
        super(injector);
    }

    public projectData: Object[];
    ngOnInit() {
        if (this.appSession.user.userName == "admin") {
            this.isViewTimKiemNangCao = true;
            this.isViewChucVuCanBo = false;
        } else {
            this.chucVuId = this.appSession.chucVuChinhId;
            this.isViewTimKiemNangCao = false;
            this.isViewChucVuCanBo = true;
        }
        this.getCanBo();
    }

    onChangeChucVuChinh(event) {
        this.getCanBo();
    }

    getCanBo(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );

        let ndonViId = this.donViId != 0 ? this.donViId : undefined;
        let nchucVuId = this.chucVuId != 0 ? this.chucVuId : undefined;

        this._canBoService
            .getByFilter(
                this.filterText,
                ndonViId,
                nchucVuId,
                "",
                maxResultCount,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                //this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }



    async xoaCanBo(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa nhân viên này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_._canBoService.delete(item.id, item.userId).subscribe(() => {
                        that_.getCanBo();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }
    changeTab(idTab?: any): void {
        this.activeTab = idTab;
    }

    close(): void {
        this.closeEvent.emit(null);
    }
}
