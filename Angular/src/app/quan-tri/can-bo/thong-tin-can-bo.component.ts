import {
    Component,
    EventEmitter,
    Injector,
    Output,
    ViewChild,
    ViewEncapsulation
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap";
import {
    CanBoDto,
    CanBoServiceProxy,
    ProfileServiceProxy,
    UserRoleDto
} from "@shared/service-proxies/service-proxies";
import {
    FormBuilder,
    Validators,
    FormGroup,
    FormControl
} from "@angular/forms";
import { HttpClient, HttpEventType } from "@angular/common/http";
import { finalize } from "rxjs/operators";
import { CanBoChucVuComponent } from "./can-bo-chuc-vu/can-bo-chuc-vu.component";
import { DonViChuyenTrachComponent } from "./don-vi-chuyen-trach/don-vi-chuyen-trach.component";
import { CanBoChucVuDanhGiaComponent } from "./can-bo-chuc-vu-danh-gia/can-bo-chuc-vu-danh-gia.component";
import { ChangeProfilePictureModalComponent } from "@app/shared/layout/profile/change-profile-picture-modal.component";
import * as _ from "lodash";
import { HuyenComboComponent } from "@app/shared/ui-combo-box/huyen-combo.component";
import { XaComboComponent } from "@app/shared/ui-combo-box/xa-combo.component";
import { ResetPasswordComponent } from "@account/password/reset-password.component";
@Component({
    selector: "app-thong-tin-can-bo",
    templateUrl: "./thong-tin-can-bo.component.html",
})
export class CreateOrEditCanBoComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @ViewChild("canBoChucVuView", { static: true })
    canBoChucVuView: CanBoChucVuComponent;
    @ViewChild("canBoChuyenTrachView", { static: true })
    canBoChuyenTrachView: DonViChuyenTrachComponent;
    @ViewChild("canBoChucVuDanhGiaView", { static: true })
    canBoChucVuDanhGiaView: CanBoChucVuDanhGiaComponent;
    @ViewChild("changeUserProfilePictureModal", { static: true })
    changeProfilePictureModal: ChangeProfilePictureModalComponent;

    profilePicture: string;
    @ViewChild("huyencombo", { static: true }) huyencombo: HuyenComboComponent;
    @ViewChild("xacombo", { static: true }) xacombo: XaComboComponent;

    minSearchLengthOption: number = 0;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    // createOrUpdateCanBoForm: FormGroup;
    submitted = false;
    canBoDto: CanBoDto = new CanBoDto();
    canBoInfo: CanBoDto = new CanBoDto();
    active = false;
    saving = false;
    activeTab: any = 1;
    viewDetail: boolean = true;
    isEdit: boolean = false;
    roles: UserRoleDto[];
    roleName = "";
    matKhauLapLai = "";
    tenDonVi = "";
    isAllowEdit: boolean;

    constructor(
        private http: HttpClient,
        private formBuilder: FormBuilder,
        private _canBoService: CanBoServiceProxy,
        private _profileService: ProfileServiceProxy,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        // this.createOrUpdateCanBoForm = this.formBuilder.group({
        //     ho: ["", Validators.required],
        //     ten: ["", Validators.required],
        //     emailAddress: ["", Validators.required],
        //     phoneNumber: ["", Validators.required],
        //     address: [""],
        //     donViId: [0, [Validators.required, Validators.min(1)]],
        //     roleName: [''],
        //     // setRandomPassword: [''],
        //     // password: ['', [Validators.minLength(6), Validators.maxLength(10)]],
        //     // matKhauLapLai: ['']
        // });
    }
    //convenience getter for easy access to form fields
    // get f() {
    //     return this.createOrUpdateCanBoForm.controls;
    // }

    show(modelUpdate?: any, isViewDetail?: any) {
        this.canBoInfo = new CanBoDto();

        this.submitted = false;
        this.activeTab = 1;
        // this.viewDetail = viewDetail;
        if (isViewDetail == true) {
            this.viewDetail = false;
        } else {
            this.viewDetail = true;
        }

        if (isViewDetail) {
            this.saving = false;
            let canBoId = (modelUpdate == null) ? 0 : modelUpdate.id;
            this._canBoService
                .getChiTietCanBo(canBoId)
                .subscribe(result => {
                    this.roles = result.roles;
                    if (result.id == 0) {
                        this.canBoInfo = result;
                        this.canBoInfo.donViId = undefined;
                        this.canBoInfo.tinhId = this.appSession.macDinhTinhId;
                        this.canBoInfo.huyenId = this.appSession.macDinhHuyenId;
                        this.canBoInfo.xaId = "0";
                        this.canBoInfo.loaiHopDong = 2;
                        this.isEdit = false;
                        this.canBoInfo.password = "123qwe";
                    } else {
                        this.canBoInfo = Object.assign({}, modelUpdate);
                        this.canBoInfo.password = "";
                        // this.canBoInfo.loaiHopDong = "";
                        this.isEdit = true;
                        this.tenDonVi = modelUpdate.tenDonVi;
                    }

                    const role = this.roles.filter(epic => epic.isAssigned == true)[0];
                    if (role) {
                        this.roleName = role.roleName;
                    }

                    this.canBoInfo.setRandomPassword = false;

                    this.getProfilePicture(result.profilePictureId);
                    this.huyencombo.getDataOptions(this.canBoInfo.tinhId);
                    this.xacombo.getDataOptions(this.canBoInfo.huyenId);
                });
        }
    }

    onShown(): void { }


    isViewValidatePassword: true;
    isViewValidateRepeatPassword: true;
    validatePassword(): void {
        // if (this.canBoInfo.password != null || this.canBoInfo.password != "") {
        //     isViewValidatePassword == true;
        //     this.isViewValidateRepeatPassword =
        // }
    }


    save(form): void {
        // if (form.invalid) {
        //     alert("Chua vali")
        //     return;
        // } else {
        //     alert("Save")
        //     return;
        // }

        // this.submitted = true;
        // return;
        // stop here if form is invalid
        // if (this.createOrUpdateCanBoForm.invalid) {
        //     return;
        // }

        this.saving = true;

        this.canBoInfo.assignedRoleNames = [];
        this.canBoInfo.assignedRoleNames.push(this.roleName);

        this._canBoService
            .upsert(this.canBoInfo)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(result => {
                if (result.erroMsg) {
                    this.notify.error(result.erroMsg);
                } else {
                    if (result.id <= 0) {
                        this.notify.error("Thao tác lỗi, vui lòng kiểm tra lại!");
                    } else {
                        if (!this.isEdit) {
                            this.canBoInfo.id = result.id;
                            this.canBoInfo.userId = result.userId;
                            this.isEdit = true;
                        }
                        this.notify.success(this.l("SavedSuccessfully"));
                    }
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    changeTab(idTab?: any): void {
        this.activeTab = idTab;
        //this.canBoChucVuComponentView.showCanBoChucVu(this.canBoInfo);
    }

    // upload img
    fileData: File = null;
    previewUrl: any = null;
    fileUploadProgress: string = null;
    uploadedFilePath: string = null;


    openChangeProfilePictureModal() {
        this.changeProfilePictureModal.show(this.canBoInfo.userId);
    }

    getProfilePicture(profilePictureId: string): void {
        // console.log('profilePictureId', profilePictureId);
        if (!profilePictureId) {
            this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
        } else {
            this._profileService.getProfilePictureById(profilePictureId).subscribe(result => {

                if (result && result.profilePicture) {
                    this.profilePicture = 'data:image/jpeg;base64,' + result.profilePicture;
                } else {
                    this.profilePicture = this.appRootUrl() + 'assets/common/images/default-profile-picture.png';
                }
            });
        }
    }


    getQuyenChinhSuaChucVuChoCaNhan() {
        let result = false;

        if (this.isEdit == true && (this.isGranted('Pages.Admin.CanBo.Edit.CauHinhLuongDanhGia') || this.canBoInfo.id == this.appSession.canBoId)) {
            result = true;
        }

        return result;
    }



    // onSubmit() {
    //     const formData = new FormData();
    //     formData.append('files', this.fileData);
    //     this.fileUploadProgress = '0%';
    //     this.http.post('https://us-central1-tutorial-e6ea7.cloudfunctions.net/fileUpload', formData, {
    //         reportProgress: true,
    //         observe: 'events'
    //     }).subscribe(events => {
    //         if (events.type === HttpEventType.UploadProgress) {
    //             this.fileUploadProgress = Math.round(events.loaded / events.total * 100) + '%';
    //             console.log(this.fileUploadProgress);
    //         } else if (events.type === HttpEventType.Response) {
    //             this.fileUploadProgress = '';
    //             console.log(events.body);
    //             alert('SUCCESS !!');
    //         }

    //     })
    // };
    // ket thuc upload img
}
