import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { finalize, filter } from 'rxjs/operators';
import { CanBoChucVuDto, CanBoChucVuServiceProxy, CanBoDto, ChucVuServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as _ from "lodash";
import { CanBoChucVuDanhGiaComponent } from '../can-bo-chuc-vu-danh-gia/can-bo-chuc-vu-danh-gia.component';
import { number } from '@amcharts/amcharts4/core';

enum LoaiChucVu {
    ChucVuChinh = 1,
    ChucVuKiemNhiem = 2,
}
@Component({
    selector: 'app-child-can-bo-chuc-vu',
    templateUrl: './can-bo-chuc-vu.component.html',
})
export class CanBoChucVuComponent extends AppComponentBase {
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("canBoChucVuDanhGiaView", { static: true })
    canBoChucVuDanhGiaView: CanBoChucVuDanhGiaComponent;

    submitted = false;
    canBoDto: CanBoDto = new CanBoDto();
    active = false;
    saving = false;
    public lstChucVu: any[];

    lstLoaiChucVu: any = [
        { id: LoaiChucVu.ChucVuChinh, text: "Chức vụ chính" },
        { id: LoaiChucVu.ChucVuKiemNhiem, text: "Chức vụ kiêm nhiệm" }
    ];

    constructor(
        private _canBoChucVuService: CanBoChucVuServiceProxy,
        private _chucVuService: ChucVuServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
    }

    showCanBoChucVu(modelUpdate?: any): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;
        this.canBoDto = Object.assign({}, modelUpdate);;
        this.loadCanBoChucVu();
    }


    loadCanBoChucVu() {
        this.lstChucVu = [];
        this._chucVuService.getChucVuTheoCanBo(this.canBoDto.id)
            .subscribe(result => {
                this.lstChucVu = result;
            });
    }

    addItem(): void {
        let checkItem = this.lstChucVu.filter(epic => epic.chucVuId == 0)[0];
        if (!checkItem) {
            var item = new CanBoChucVuDto();
            item.id = undefined;
            item.canBoId = this.canBoDto.id;
            item.chucVuId = 0;
            item.chucVuChinh = 2; // chuc vu kiem nhiem
            this.lstChucVu.push(item);
        }
    }
    removeItem(idx: number, id?: number, chucVuId?: any): void {
        if (id) {
            this.xoaChucVuCanBo(id, chucVuId);
        } else {
            this.lstChucVu.splice(idx, 1);
        }
    }

    async xoaChucVuCanBo(id?: any, chucVuId?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_.saving = true;
                    that_._canBoChucVuService.delete(id, that_.canBoDto.id, chucVuId)
                        .pipe(finalize(() => that_.saving = false))
                        .subscribe(() => {
                            that_.loadCanBoChucVu();
                        });

                }
            }
        );
    }

    save(): void {
        this.submitted = true;
        this.saving = true;

        let lstInput = [];
        lstInput = this.lstChucVu.map(x => Object.assign({}, x));
        //check chucVu duplicate
        var valueArr = lstInput.map(function (item) { return item.chucVuId });
        var isDuplicate = valueArr.some(function (item, idx) {
            return valueArr.indexOf(item) != idx
        });

        if (isDuplicate == true) {
            this.submitted = false;
            this.saving = false;
            this.notify.error("Chức vụ cán bộ không được trùng.");
            return;
        }

        let kiemTraChucVuChinh = this.lstChucVu.filter(epic => epic.chucVuChinh == 1);
        if (kiemTraChucVuChinh.length == 0) {
            this.submitted = false;
            this.saving = false;
            this.notify.error("Vui lòng chọn chức vụ chính.");
            return;
        } else if (kiemTraChucVuChinh.length > 1) {
            this.submitted = false;
            this.saving = false;
            this.notify.error("Vui lòng chọn 1 chức vụ chính.");
            return;
        }

        //end check chucVu duplicate
        this._canBoChucVuService.upsert(lstInput)
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.success(this.l('SavedSuccessfully'));
                this.loadCanBoChucVu();
            });
    }

    openConfigChucVuDanhGia(chucVuId) {
        this.canBoChucVuDanhGiaView.show(this.canBoDto, chucVuId);
    }

    close(): void {
        this.active = false;
    }
}
