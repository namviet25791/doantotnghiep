import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppCommonModule } from '@app/shared/common/app-common.module';
import { UtilsModule } from '@shared/utils/utils.module';
import { CountoModule } from 'angular2-counto';
import {
    ModalModule,
    TabsModule,
    TooltipModule,
    BsDropdownModule,
    PopoverModule
} from 'ngx-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import {
    BsDatepickerModule,
    BsDatepickerConfig,
    BsDaterangepickerConfig,
    BsLocaleService
} from 'ngx-bootstrap/datepicker';
import { NgxBootstrapDatePickerConfigService } from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();
import { InputMaskModule } from 'primeng/inputmask';

import { ThangDiemComponent } from './thang-diem/thang-diem.component';
import { QuanTriRoutingModule } from './quan-tri-routing.module';
import { SharedUiComboBoxModule } from '@app/shared/ui-combo-box/ui-combo-box.module';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ThangDiemUpsertModalComponent } from './thang-diem/upsert-modal/upsert-modal.component';
import { PhieuDanhGiaComponent } from './phieu-danh-gia/phieu-danh-gia.component';
import { CreateOrEditPhieuDanhGiaModalComponent } from './phieu-danh-gia/create-or-edit-phieu-danh-gia-modal.component';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { HttpClientModule } from '@angular/common/http';
import {
    DxSwitchModule,
    DxButtonModule,
    DxDataGridModule,
    DxTreeListModule,
    DxDateBoxModule,
    DxSelectBoxModule
} from 'devextreme-angular';
import { TieuChiDanhGiaComponent } from './tieu-chi-danh-gia/tieu-chi-danh-gia.component';
import { CreateOrEditTieuChiDanhGiaModalComponent } from './tieu-chi-danh-gia/create-or-edit-tieu-chi-danh-gia-modal.component';
import { KhoiComponent } from './khoi/khoi.component';
import { CreateOrEditKhoiModalComponent } from './khoi/create-or-edit-khoi-modal.component';
import { CreateOrEditCapDonViModalComponent } from './cap-don-vi/create-or-edit-cap-don-vi-modal.component';
import { CapDonViComponent } from './cap-don-vi/cap-don-vi.component';
import { LoaiDonViComponent } from './loai-don-vi/loai-don-vi.component';
import { CreateOrEditLoaiDonViModalComponent } from './loai-don-vi/create-or-edit-loai-don-vi-modal.component';
import { DonViComponent } from './don-vi/don-vi.component';
import { TimKiemDonViModalComponent } from './don-vi/tim-kiem-don-vi-modal.component';
import { CreateOrEditDonViModalComponent } from './don-vi/create-or-edit-don-vi-modal.component';
import { ChucVuComponent } from './chuc-vu/chuc-vu.component';
import { CreateOrEditChucVuModalComponent } from './chuc-vu/create-or-edit-chuc-vu-modal.component';
import { CreateOrEditChucVuDanhGiaModalComponent } from './chuc-vu/create-or-edit-chuc-vu-danh-gia-modal.component';
import { CanBoComponent } from './can-bo/can-bo.component';
import { CreateOrEditCanBoComponent } from './can-bo/thong-tin-can-bo.component';
import { CanBoChucVuComponent } from './can-bo/can-bo-chuc-vu/can-bo-chuc-vu.component';
import { DonViChuyenTrachComponent } from './can-bo/don-vi-chuyen-trach/don-vi-chuyen-trach.component';
import { CanBoChucVuDanhGiaComponent } from './can-bo/can-bo-chuc-vu-danh-gia/can-bo-chuc-vu-danh-gia.component';
import { SharedUiUxModule } from '@app/shared/ui-ux/SharedUiUxModule';
import { AppModule } from '@app/app.module';
import '../localization';
import { Select2Module } from "ng2-select2";
import { BaoCaoComponent } from './bao-cao/bao-cao.component';
import { CauHinhNhomDoiTuongComponent } from './bao-cao/cau-hinh-nhom-doi-tuong/cau-hinh-nhom-doi-tuong.component';
import { NhomDoiTuongChiTietModalComponent } from './bao-cao/cau-hinh-nhom-doi-tuong/nhom-doi-tuong-chi-tiet-modal.component';
import { NhomChucVuSuDungDoiTuongModalComponent } from './bao-cao/cau-hinh-nhom-doi-tuong/nhom-chuc-vu-su-dung-doi-tuong-modal.component';
import { CreateOrEditBaoCaoModalComponent } from './bao-cao/create-or-edit-bao-cao-modal.component';
import { CreateOrEditThanhVienNhomChucVuModalComponent } from './chuc-vu/create-or-edit-thanh-vien-nhom-cv-modal.component';
import { TimKiemCanBoModalComponent } from './chuc-vu/tim-kiem-can-bo-modal.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        QuanTriRoutingModule,
        CountoModule,
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
        SharedUiComboBoxModule,
        InputMaskModule,
        TableModule,
        PaginatorModule,
        OverlayPanelModule,
        HttpClientModule,
        SharedUiUxModule,
        AppModule,
        DxButtonModule,
        DxDataGridModule,
        DxSwitchModule,
        DxTreeListModule,
        Select2Module,
        DxSelectBoxModule,
        DxDateBoxModule
    ],
    declarations: [
        ThangDiemComponent,
        ThangDiemUpsertModalComponent,
        PhieuDanhGiaComponent,
        CreateOrEditPhieuDanhGiaModalComponent,
        TieuChiDanhGiaComponent,
        CreateOrEditTieuChiDanhGiaModalComponent,
        KhoiComponent,
        CreateOrEditKhoiModalComponent,
        CreateOrEditCapDonViModalComponent,
        CapDonViComponent,
        CreateOrEditLoaiDonViModalComponent,
        LoaiDonViComponent,
        CreateOrEditDonViModalComponent,
        DonViComponent,
        CreateOrEditChucVuModalComponent,
        CreateOrEditChucVuDanhGiaModalComponent,
        ChucVuComponent,
        CreateOrEditCanBoComponent,
        CanBoChucVuComponent,
        CanBoComponent,
        DonViChuyenTrachComponent,
        TimKiemDonViModalComponent,
        CanBoChucVuDanhGiaComponent,
        BaoCaoComponent,
        CauHinhNhomDoiTuongComponent,
        NhomDoiTuongChiTietModalComponent,
        NhomChucVuSuDungDoiTuongModalComponent,
        CreateOrEditBaoCaoModalComponent,
        CreateOrEditThanhVienNhomChucVuModalComponent,
        TimKiemCanBoModalComponent
    ],
    providers: [
        {
            provide: BsDatepickerConfig,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig
        },
        {
            provide: BsDaterangepickerConfig,
            useFactory:
                NgxBootstrapDatePickerConfigService.getDaterangepickerConfig
        },
        {
            provide: BsLocaleService,
            useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale
        },
    ]
})
export class QuanTriModule { }
