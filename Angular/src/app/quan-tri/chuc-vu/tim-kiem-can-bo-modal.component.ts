import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { CanBoServiceProxy, CanBoDto, CanBoInput, ChucVuServiceProxy } from '@shared/service-proxies/service-proxies';
import { LazyLoadEvent } from 'primeng/api';
import { Paginator } from 'primeng/paginator';
import * as _ from "lodash";
import * as $ from "jquery";
import { ChucVuComboComponent } from '@app/shared/ui-combo-box/chuc-vu-combo.component';

@Component({
    selector: 'timKiemCanBoModal',
    templateUrl: './tim-kiem-can-bo-modal.component.html'
})
export class TimKiemCanBoModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    submitted = false;
    active = false;
    saving = false;
    donViId?: 0;
    chucVuId?: 0;
    filterText: '';
    lstCanBoDuocChon: any[];
    lstCanBoDuocChonOrg: any[];
    canBoId: any;
    constructor(
        private _canBoService: CanBoServiceProxy, private _chucVuService: ChucVuServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
    }

    show(chucVuId?: any): void {
        this.chucVuId = chucVuId;
        this.lstCanBoDuocChon = [];
        const self = this;
        self.active = true;
        this.getDanhSachCanBo();
        self.modal.show();
    }

    getDanhSachCanBo(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );

        let nChucVuId = this.chucVuId != 0 ? this.chucVuId : undefined;
        let nDonViId = this.donViId != 0 ? this.donViId : undefined;

        var input = new CanBoInput();
        input.filter = this.filterText;
        input.donViId = nDonViId;
        input.chucVuId = nChucVuId;
        input.sorting = "";
        input.maxResultCount = maxResultCount;
        input.skipCount = skipCount;

        this._canBoService.timKiemThanhVienNhomChucVu(input)
            .pipe(finalize(() => this.primengTableHelper.hideLoadingIndicator())).subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;

                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);

                    const isCheckCanBo = this.lstCanBoDuocChon.filter(epic => epic.id == value.id)[0];

                    if (isCheckCanBo) {
                        model.isChecked = true;
                    }
                    else {
                        model.isChecked = false;
                    }
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                this.primengTableHelper.hideLoadingIndicator();
            });
    }


    onShown(): void {

    }
    chonCanBoThanhvien(event?: any, data?: any): void {
        if (event) {
            this.lstCanBoDuocChon.push(data);
        } else {
            let indexOf = this.lstCanBoDuocChon.indexOf(data.id);
            this.lstCanBoDuocChon.splice(indexOf, 1);
        }
    }

    save(): void {
        if (this.lstCanBoDuocChon.length == 0) {
            this.notify.error("Chưa có cán bộ nào được chọn.");
            return;
        }

        let lstData = [];
        this.lstCanBoDuocChon.forEach(value => {
            let model = {
                canBoId: value.id,
                chucVuCanBoId: value.chucVuId
            };

            lstData.push(model);
        });
        this.submitted = true;
        this.saving = true;
        this._chucVuService.themThanhVienNhomChucVu(lstData, this.chucVuId)
            .pipe(finalize(() => this.saving = false))
            .subscribe(rs => {
                if (rs) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.error("Có lỗi xảy ra vui lòng kiểm tra lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
