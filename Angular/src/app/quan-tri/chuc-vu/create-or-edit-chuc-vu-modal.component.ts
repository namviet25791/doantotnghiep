import {
    Component,
    EventEmitter,
    Injector,
    Output,
    ViewChild
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap";
import { finalize } from "rxjs/operators";
import {
    ChucVuDto,
    PhieuDanhGiaChucVuDto,
    ChucVuServiceProxy,
    PhieuDanhGiaChucVuServiceProxy
} from "@shared/service-proxies/service-proxies";
import {
    FormBuilder,
    Validators,
    FormGroup,
    FormControl
} from "@angular/forms";

@Component({
    selector: "createOrEditChucVuModal",
    templateUrl: "./create-or-edit-chuc-vu-modal.component.html"
})
export class CreateOrEditChucVuModalComponent extends AppComponentBase {
    submitted = false;
    chucVuDto: ChucVuDto = new ChucVuDto();
    phieuDanhGiaChucVuDto: PhieuDanhGiaChucVuDto = new PhieuDanhGiaChucVuDto();
    active = false;
    saving = false;
    isEdit = false;
    loaiHinhChucVu = false;

    constructor(
        private formBuilder: FormBuilder,
        private _chucVuService: ChucVuServiceProxy,
        private _phieuDanhGiaChucVuService: PhieuDanhGiaChucVuServiceProxy,
        injector: Injector
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.mainForm = this.formBuilder.group({
            ten: ["", Validators.required],
            tenHienThiTrenBieuMau: [""],
            phieuDanhGiaId: [0, [Validators.required, Validators.min(1)]],
            capLanhDao: [0]
        });
    }

    show(modelUpdate?: ChucVuDto, isEdit?: any): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;
        this.isEdit = isEdit;

        if (modelUpdate) {
            //edit
            this.chucVuDto = Object.assign({}, modelUpdate);

            if (this.chucVuDto.loaiChucVu == 1) {
                //this.chucVuDto.loaiChucVu  = 1 la chuc vu
                this.loaiHinhChucVu = false;
            } else {
                //this.chucVuDto.loaiChucVu  = 2 la nhom chuc vu
                this.loaiHinhChucVu = true;
            }

            this._phieuDanhGiaChucVuService
                .getByFilter(
                    "",
                    undefined,
                    undefined,
                    undefined,
                    modelUpdate.id,
                    "",
                    1000,
                    0
                )
                .subscribe(result => {
                    if (result.totalCount > 0) {
                        this.phieuDanhGiaChucVuDto = result.items[0];
                    } else {
                        this.phieuDanhGiaChucVuDto.id = undefined;
                        this.phieuDanhGiaChucVuDto.phieuDanhGiaId = 0;
                    }
                });
        } else {
            // addnew
            this.chucVuDto = new ChucVuDto();
            this.chucVuDto.laLanhDao = false;
            this.chucVuDto.khoi = 0;
            this.loaiHinhChucVu = false;

            this.phieuDanhGiaChucVuDto = new PhieuDanhGiaChucVuDto();
            this.phieuDanhGiaChucVuDto.id = undefined;
            this.phieuDanhGiaChucVuDto.phieuDanhGiaId = 0;
        }

        this.changeLoaiHinhChucVu(this.loaiHinhChucVu);
        self.modal.show();
    }

    changeLoaiHinhChucVu(event: any) {
        if (event == true) {
            this.chucVuDto.loaiChucVu = 2; // nhom chuc vu
        } else {
            this.chucVuDto.loaiChucVu = 1; // chuc vu
        }
    }

    save(): void {

        if (this.chucVuDto.loaiChucVu == 1) {
            this.submitted = true;
            // stop here if form is invalid
            if (this.mainForm.invalid) {
                return;
            }
            this.saving = true;
        }
        else {
            if (this.chucVuDto.ten == undefined || this.chucVuDto.ten == "") {
                this.notify.error("Vui lòng điền tên nhóm chức vụ!");
                return;
            }
        }
        this.submitted = true;
        this.saving = true;
        this._chucVuService
            .upsert(this.chucVuDto)
            .pipe(finalize(() => (this.saving = false)))
            .subscribe(rs => {
                if (rs.result) {
                    this.phieuDanhGiaChucVuDto.chucVuId = rs.value;

                    if (this.chucVuDto.loaiChucVu == 1) {
                        this.savePhieuDanhGiaChucVu();
                    }
                    this.notify.success(this.l("SavedSuccessfully"));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.error("Có lỗi xảy ra vui lòng thao tác lại!");
                }
            });
    }

    savePhieuDanhGiaChucVu(): void {
        this._phieuDanhGiaChucVuService
            .upsert(this.phieuDanhGiaChucVuDto)
            .subscribe(idResult => { });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
