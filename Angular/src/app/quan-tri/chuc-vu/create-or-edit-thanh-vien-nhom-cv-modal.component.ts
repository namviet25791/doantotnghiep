import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize, filter } from 'rxjs/operators';
import { ChucVuServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import * as _ from "lodash";
@Component({
    selector: 'createOrEditThanhVienNhomChucVuModal',
    templateUrl: './create-or-edit-thanh-vien-nhom-cv-modal.component.html'
})
export class CreateOrEditThanhVienNhomChucVuModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    submitted = false;
    active = false;
    saving = false;
    chucVuId = 0;
    tenChucVu = "";
    filterText = "";
    lstThanhVien = [];

    constructor(
        private formBuilder: FormBuilder,
        private _chucVuService: ChucVuServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
    }

    show(item?: any): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;
        this.tenChucVu = item.ten;
        this.chucVuId = item.id;
        this.loadThanhVienNhom();
        self.modal.show();
    }

    loadThanhVienNhom() {
        this._chucVuService.getThanhVienNhomCanBoTheoChucVuId(this.filterText, 0, this.chucVuId, "", 1000, 0)
            .subscribe(result => {
                this.primengTableHelper.records = [];
                let soThuTu = 1;

                result.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
            });
    }

    async removeItem(id?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_.saving = true;
                    that_._chucVuService.xoaThanhVienNhomChucVu(id)
                        .pipe(finalize(() => that_.saving = false))
                        .subscribe(() => {
                            that_.loadThanhVienNhom();
                        });

                }
            }
        );
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
