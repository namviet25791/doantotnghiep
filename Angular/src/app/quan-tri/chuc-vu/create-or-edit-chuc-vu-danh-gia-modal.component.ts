import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize, filter } from 'rxjs/operators';
import { ChucVuDanhGiaDto, ChucVuDanhGiaServiceProxy, ChucVuDto } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import * as _ from "lodash";
import { ChucVuComboComponent } from '@app/shared/ui-combo-box/chuc-vu-combo.component';
import { AppUtilityService } from '@app/shared/common/custom/utility.service';
@Component({
    selector: 'createOrEditChucVuDanhGiaModal',
    templateUrl: './create-or-edit-chuc-vu-danh-gia-modal.component.html'
})
export class CreateOrEditChucVuDanhGiaModalComponent extends AppComponentBase {
    @ViewChild("chucvucombo", { static: true })
    chucvucombo: ChucVuComboComponent;

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateChucVuDanhGiaForm: FormGroup;
    submitted = false;
    chucVuDanhGiaNguoiChamDto: ChucVuDanhGiaDto = new ChucVuDanhGiaDto();
    chucVuDto: ChucVuDto = new ChucVuDto();
    active = false;
    saving = false;
    public lstChucVuDanhGia: any[];
    constructor(
        private formBuilder: FormBuilder,
        private _chucVuDanhGiaService: ChucVuDanhGiaServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    lstChamDuyet: any = [
        { id: true, text: "Người chấm" },
        { id: false, text: "Ngưới duyệt" }
    ];

    loadChucVuDanhGia() {
        this.lstChucVuDanhGia = [];
        this._chucVuDanhGiaService.getByFilter("", this.chucVuDto.id, undefined, "", 1000, 0)
            .subscribe(result => {
                this.lstChucVuDanhGia = result.items;
                this.orderByItem();
            });
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateChucVuDanhGiaForm = this.formBuilder.group({
            ten: ['', Validators.required]
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateChucVuDanhGiaForm.controls; }

    show(modelUpdate?: ChucVuDto): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;
        self.modal.show();
        this.chucVuDto = Object.assign({}, modelUpdate);;
        this.loadChucVuDanhGia();

        // this.chucvucombo.getDataOptions();
    }
    onShown(): void {
    }


    addItem(): void {
        let stt = 1;

        if (this.lstChucVuDanhGia.length > 0) {
            const sapXep = _.orderBy(
                this.lstChucVuDanhGia,
                ["soThuTu"],
                ["desc"]
            );
            stt = sapXep[0].soThuTu + 1;

            if (sapXep[0].chucVuThucHienDanhGiaId > 0) {
                var item = new ChucVuDanhGiaDto();
                item.id = undefined;
                item.chucVuThucHienDanhGiaId = 0;
                item.nguoiCham = true;
                item.chucVuDuocDanhGiaId = this.chucVuDto.id;
                item.soThuTu = stt;
                this.lstChucVuDanhGia.push(item);
            }
        }
        else {
            var item = new ChucVuDanhGiaDto();
            item.id = undefined;
            item.chucVuThucHienDanhGiaId = 0;
            item.nguoiCham = true;
            item.chucVuDuocDanhGiaId = this.chucVuDto.id;
            item.soThuTu = stt;
            this.lstChucVuDanhGia.push(item);
        }

        this.orderByItem();
    }

    removeItem(idx: number, id?: number): void {

        const that_ = this;
        if (id) {
            abp.message.confirm(
                "Bạn chắc chắn muốn xóa mục này?",
                "Thông báo",
                function (result) {
                    if (result) {
                        that_.saving = true;
                        that_._chucVuDanhGiaService.delete(id)
                            .pipe(finalize(() => that_.saving = false))
                            .subscribe(() => {
                                that_.loadChucVuDanhGia();
                            });
                    }
                }
            );


        } else {
            this.lstChucVuDanhGia.splice(idx, 1);
        }
        this.orderByItem();
    }
    orderByItem(): void {
        this.lstChucVuDanhGia = _.orderBy(
            this.lstChucVuDanhGia,
            ["soThuTu"]
        );
    }
    resetForm(): void {
        this.lstChucVuDanhGia = [];
    }
    save(): void {
        if (this.lstChucVuDanhGia.length == 0) {
            this.notify.error("Xin chọn đối tượng chấm/duyệt.");
            return;
        }

        //check chucVu duplicate
        var valueArr = this.lstChucVuDanhGia.map(function (item) { return item.chucVuThucHienDanhGiaId });
        var isDuplicate = valueArr.some(function (item, idx) {
            return valueArr.indexOf(item) != idx
        });
        if (isDuplicate == true) {
            this.notify.error("Chức vụ đánh giá không được trùng.");
            return;
        }
        //end check chucVu duplicate


        this.submitted = true;
        this.saving = true;
        this._chucVuDanhGiaService.upsert(this.lstChucVuDanhGia)
            .pipe(finalize(() => this.saving = false))
            .subscribe(rs => {
                if (rs.result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.error(rs.msg);
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    upPosition(idx) {
        if (idx > 0) {
            this.lstChucVuDanhGia = AppUtilityService.array_move(
                this.lstChucVuDanhGia,
                idx,
                idx - 1
            );
            this.changeStt();
        }
    }

    downPosition(idx) {
        if (idx < this.lstChucVuDanhGia.length - 1) {
            this.lstChucVuDanhGia = AppUtilityService.array_move(
                this.lstChucVuDanhGia,
                idx,
                idx + 1
            );
            this.changeStt();
        }
    }

    changeStt() {
        this.lstChucVuDanhGia.forEach((item, index) => {
            item.soThuTu = index + 1;
        });
    }
}
