import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Injector
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    ChucVuDto,
    ChucVuServiceProxy
} from "@shared/service-proxies/service-proxies";
import { CreateOrEditChucVuModalComponent } from "./create-or-edit-chuc-vu-modal.component";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator } from "primeng/paginator";
import { CreateOrEditChucVuDanhGiaModalComponent } from "./create-or-edit-chuc-vu-danh-gia-modal.component";

@Component({
    selector: "app-loai-don-vi",
    templateUrl: "./chuc-vu.component.html",
    animations: [appModuleAnimation()]
})
export class ChucVuComponent extends AppComponentBase implements OnInit {
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    @ViewChild("createOrEditChucVuModal", { static: true })
    createOrEditChucVuModal: CreateOrEditChucVuModalComponent;
    @ViewChild("createOrEditChucVuDanhGiaModal", { static: true })
    createOrEditChucVuDanhGiaModal: CreateOrEditChucVuDanhGiaModalComponent;
    @ViewChild("paginator", { static: true }) paginator: Paginator;
    loaiChucVu = undefined;
    constructor(
        injector: Injector,
        private _chucVuService: ChucVuServiceProxy
    ) {
        super(injector);
    }

    public projectData: Object[];

    lstLoaiChucVu = [
        { id: undefined, name: "Tất cả" },
        { id: 1, name: "Chức vụ" },
        { id: 2, name: "Nhóm chức vụ" }
    ];



    ngOnInit() {
        this.getChucVu();
    }

    getChucVu(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );

        this._chucVuService
            .getByFilter(
                this.filterText,
                undefined,
                undefined,
                this.loaiChucVu,
                "",
                maxResultCount,
                skipCount
            )
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                //this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    createChucVu(): void {
        this.createOrEditChucVuModal.show();
    }

    async xoaChucVu(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function (result) {
                if (result) {
                    that_._chucVuService.delete(item.id).subscribe(() => {
                        that_.getChucVu();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }

    close(): void {
        this.closeEvent.emit(null);
    }
}
