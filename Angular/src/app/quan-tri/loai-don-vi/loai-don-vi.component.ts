import {
    Component,
    OnInit,
    Output,
    EventEmitter,
    Injector
} from "@angular/core";
import { NgModule, ViewChild } from "@angular/core";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
    LoaiDonViDto,
    LoaiDonViServiceProxy
} from "@shared/service-proxies/service-proxies";
import { CreateOrEditLoaiDonViModalComponent } from "./create-or-edit-loai-don-vi-modal.component";
import { LazyLoadEvent } from "primeng/api";
import { finalize } from "rxjs/operators";
import { AppComponentBase } from "@shared/common/app-component-base";
import { Paginator } from "primeng/paginator";

@Component({
    selector: "app-loai-don-vi",
    templateUrl: "./loai-don-vi.component.html",
    animations: [appModuleAnimation()]
})
export class LoaiDonViComponent extends AppComponentBase implements OnInit {
    [x: string]: any;
    filterText = "";
    @Output() closeEvent: EventEmitter<any> = new EventEmitter<any>();
    loaiDonViDto: LoaiDonViDto = new LoaiDonViDto();
    @ViewChild("createOrEditLoaiDonViModal", { static: true })
    createOrEditLoaiDonViModal: CreateOrEditLoaiDonViModalComponent;
    @ViewChild("paginator", { static: true }) paginator: Paginator;

    constructor(
        injector: Injector,
        private _loaiDonViService: LoaiDonViServiceProxy
    ) {
        super(injector);
    }

    public projectData: Object[];
    ngOnInit() {
        this.getLoaiDonVi();
    }

    getLoaiDonVi(event?: LazyLoadEvent) {
        if (this.primengTableHelper.shouldResetPaging(event)) {
            this.paginator.changePage(0);
            return;
        }
        let skipCount = this.primengTableHelper.getSkipCount(
            this.paginator,
            event
        );
        let maxResultCount = this.primengTableHelper.getMaxResultCount(
            this.paginator,
            event
        );
        this._loaiDonViService
            .getByFilter(this.filterText, "", maxResultCount, skipCount)
            .pipe(
                finalize(() => this.primengTableHelper.hideLoadingIndicator())
            )
            .subscribe(result => {
                this.primengTableHelper.totalRecordsCount = result.totalCount;
                this.primengTableHelper.records = [];
                let soThuTu = skipCount + 1;
                result.items.forEach(value => {
                    let model: any = Object.assign({}, value);
                    model.stt = soThuTu++;
                    this.primengTableHelper.records.push(model);
                });
                //this.primengTableHelper.records = result.items;
                this.primengTableHelper.hideLoadingIndicator();
            });
    }

    createLoaiDonVi(): void {
        this.createOrEditLoaiDonViModal.show();
    }

    async xoaLoaiDonVi(item?: any) {
        const that_ = this;
        abp.message.confirm(
            "Bạn chắc chắn muốn xóa mục này?",
            "Thông báo",
            function(result) {
                if (result) {
                    that_._loaiDonViService.delete(item.id).subscribe(() => {
                        that_.getLoaiDonVi();
                        abp.notify.success("Thao tác thành công!");
                    });
                }
            }
        );
    }

    close(): void {
        this.closeEvent.emit(null);
    }
}
