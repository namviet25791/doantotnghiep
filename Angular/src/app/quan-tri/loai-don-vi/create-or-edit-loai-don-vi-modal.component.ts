import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap';
import { finalize } from 'rxjs/operators';
import { LoaiDonViDto, LoaiDonViServiceProxy } from '@shared/service-proxies/service-proxies';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'createOrEditLoaiDonViModal',
    templateUrl: './create-or-edit-loai-don-vi-modal.component.html'
})
export class CreateOrEditLoaiDonViModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    createOrUpdateLoaiDonViForm: FormGroup;
    submitted = false;
    loaiDonViDto: LoaiDonViDto = new LoaiDonViDto();
    active = false;
    saving = false;

    constructor(
        private formBuilder: FormBuilder,
        private _LoaiDonViService: LoaiDonViServiceProxy,
        injector: Injector,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.submitted = false;
        this.createOrUpdateLoaiDonViForm = this.formBuilder.group({
            ten: ['', Validators.required]
        });
    }
    // convenience getter for easy access to form fields
    get f() { return this.createOrUpdateLoaiDonViForm.controls; }

    show(modelUpdate?: LoaiDonViDto): void {
        this.submitted = false;
        this.saving = false;
        const self = this;
        self.active = true;

        if (modelUpdate) {
            //edit
            this.loaiDonViDto = Object.assign({}, modelUpdate);;
        } else {
            // addnew
            this.loaiDonViDto = new LoaiDonViDto();
            this.loaiDonViDto.id = 0;
        }
        self.modal.show();
    }

    onShown(): void {

    }

    save(): void {
        this.submitted = true;
        // stop here if form is invalid
        if (this.createOrUpdateLoaiDonViForm.invalid) {
            return;
        }
        this.saving = true;
        this._LoaiDonViService.upsert(this.loaiDonViDto)
            .pipe(finalize(() => this.saving = false))
            .subscribe(result => {
                if (result) {
                    this.notify.success(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit();
                } else {
                    this.notify.success("Có lỗi xảy ra vui lòng thao tác lại.");
                }
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
