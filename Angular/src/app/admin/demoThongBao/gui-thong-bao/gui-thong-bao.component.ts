import {Component, OnInit} from '@angular/core';
import {DemoThongBaoServiceProxy} from '@shared/service-proxies/service-proxies';

@Component({
    selector: 'app-gui-thong-bao',
    templateUrl: './gui-thong-bao.component.html',
    styleUrls: ['./gui-thong-bao.component.css']
})
export class GuiThongBaoComponent implements OnInit {

    constructor(private _dataService: DemoThongBaoServiceProxy) {
    }

    ngOnInit() {
    }

    guiThongBao() {
        this._dataService.guiThongBao();
    }
}
