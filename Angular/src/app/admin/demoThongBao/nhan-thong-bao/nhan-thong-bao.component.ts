import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-nhan-thong-bao',
    templateUrl: './nhan-thong-bao.component.html',
    styleUrls: ['./nhan-thong-bao.component.css']
})
export class NhanThongBaoComponent implements OnInit {

    giaTri1: any;
    giaTri2: any;

    constructor(
        private _activatedRoute: ActivatedRoute,
    ) {
        this.giaTri1 = this._activatedRoute.snapshot.queryParams['giaTri1'] || '';
        this.giaTri2 = this._activatedRoute.snapshot.queryParams['giaTri2'] || '';
    }

    ngOnInit() {
    }

}
